# Cortex

[![lerna](https://img.shields.io/badge/maintained%20with-lerna-cc00ff.svg)](https://lernajs.io/)

## Install

If you are not intend to develop, just run :

```bash
 yarn run deps:sync
```

Otherwise install `yalc`, clone `synaptix.js` repository, and if that repo run :

```bash
 yalc publish
```

Then back to cortex repository, run :

```bash
 yarn run dev:deps:sync
```

**Important !** During development, in particular when you want to generate graphQL client metafiles  it's important to link synaptix.js via `yalc link`.


## Run dev environment


If the dev environement doesn't exists, run :

```bash
 ./launcher/console install
```

Then launch the dev stack :

```bash
 ./launcher/console start -all
```

Take a ☕.

**This command will download all docker containers related to a dev environement so it might be verrrrry long**.

Then update the DSE schema :

```bash
 ./launcher/console update-schema.
```

Take another ☕☕☕.

## Run application

Simply run :

```bash
 yarn start
```

If your are developping, you should run the relay compiler watcher.

```bash
 yarn run relay:watch
```


## Build

Simply run :

```bash
 yarn build
```

## Test

Not yet...