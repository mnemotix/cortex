/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import React from 'react';
import {Helmet} from 'react-helmet';
import favicon from '../resources/favicon.png';
import {Route, Link, Switch} from 'react-router-dom';
import Package from '../../package.json';
import Schema from './schema/schema.json';
import {Menu, Container, Icon, Sidebar, Segment} from 'semantic-ui-react';
import style from './style.less';
import "semantic-ui-less/semantic.less";
import {translate} from "react-i18next";

import Logo from "./components/Logo/Logo";
import Profile from "./views/Profile/Profile";
import {Login} from '@mnemotix/cortex-login/client';
import {
  Application as BaseApplication,
  generateRoutesDefinition,
  Modules,
  Route as AppRoute
} from '@mnemotix/cortex-core/client';
import {AddressBook, Routes as AddressBookRoutes} from '@mnemotix/cortex-addressbook/client';
import {Finder, Routes as FinderRoutes} from '@mnemotix/cortex-finder/client';
import {Portfolio, Routes as PortfolioRoutes} from '@mnemotix/cortex-portfolio/client';


@translate(['labels'])
export default class Application extends BaseApplication {

  constructor() {
    super();

    this.state = {
      sideBarVisible: true,
      ...this.state
    };
  }

  /**
   * @inheritDoc
   */
  getRoutesDefinition() {
    return generateRoutesDefinition([
      {
        routes: AddressBookRoutes,
        basePath: "/contacts"
      },
      {
        routes: FinderRoutes,
        basePath: "/finder"
      },
      {
        routes: PortfolioRoutes,
        basePath: "/portfolio"
      },
      {
        routes: [
          new AppRoute("Application_resources", "/finder"),
          new AppRoute("Application_contacts", "/contacts"),
          new AppRoute("Application_profile", "/profile"),
          new AppRoute("Application_portfolio", "/portfolio")
        ]
      }
    ]);
  }

  /**
   * @inheritDoc
   */
  getJSONSchema() {
    return Schema;
  }

  /**
   * @inheritDoc
   */
  renderContent() {
    const {t, router} = this.props;

    return (
      <>
        {this.isLogged() ? (
          <Sidebar as={Menu} className={style.sideBar} animation='overlay' icon='labeled' inverted vertical
                   width={'thin'} size={'tiny'} visible={this.state.sideBarVisible}>
            <Menu.Item header>
              <Logo className={style.logo}/>
              <Menu.Menu position='right'>
                <Menu.Item header>
                  {Package.version}
                </Menu.Item>
              </Menu.Menu>
            </Menu.Item>
            <Menu.Item as={Link} to={this.getRoute('Application_contacts')}>
              <Icon name='address book'/>
              {t("Contacts")}
            </Menu.Item>
            <Menu.Item as={Link} to={this.getRoute('Application_resources')}>
              <Icon name='disk'/>
              {t("Resources")}
            </Menu.Item>
            <Menu.Item as={Link} to={this.getRoute('Application_portfolio')}>
              <Icon name='archive'/>
              {t("Projects")}
            </Menu.Item>
          </Sidebar>
        ) : null}

        <div className={style.viewport}>
          <Helmet>
            <meta charSet="utf-8"/>
            <title>{t("Cortex")}</title>
            <link rel="icon" type="image/png" href={favicon}/>
          </Helmet>

          <Container className={style.headerSection}>
            {this.isLogged() ? (
              <Menu size={"huge"} secondary>
                <Menu.Item header className={style.menuItemCenter}>
                  {t("Kitchen Sink")}
                </Menu.Item>

                <Menu.Menu position='right'>
                  {this.isLogged() ? (
                    <Modules.AvatarDropdown dropdownProps={{item: true}} dropdownItems={(
                      <>
                        <Menu.Item as={Link} to={this.getRoute('Application_profile')}>
                          {t("Profile")}
                        </Menu.Item>

                        <Menu.Item link={true}>
                          <a href={"/auth/logout"}>
                            {t("Logout")}
                          </a>
                        </Menu.Item>
                      </>
                    )}
                    />
                  ) : null}
                </Menu.Menu>

                <Menu.Menu position='right'>
                  {this.isLogged() ? (
                    <Modules.LanguageDropdown dropdownProps={{item: true}}/>
                  ) : null}
                </Menu.Menu>
              </Menu>
            ) : null}
          </Container>

          <Container className={style.contentSection}>
            {this.renderContentSection()}
          </Container>

          <Container className={style.footerSection}>
          </Container>
        </div>
      </>
    );
  }

  renderContentSection() {
    return this.isLogged() ? (
      <Switch>
        <Route path={this.getRoute('Application_contacts')} component={AddressBook}/>
        <Route path={this.getRoute('Application_profile')} component={Profile}/>
        <Route path={this.getRoute('Application_resources')} render={() => (
          <Segment>
            <Finder/>
          </Segment>
        )}/>
        <Route path={this.getRoute('Application_portfolio')} component={Portfolio}/>
      </Switch>
    ) : (
      <Login registrationEnabled={true} splashScreen={(
        <div className={style.loginSplashScreen}>
          <Logo className={style.logo}/>
        </div>
      )}/>
    )
  }
}