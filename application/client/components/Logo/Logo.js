/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import React from 'react';

export default class Logo extends React.Component {
  render() {
    return (
      <svg className={this.props.className} viewBox={"0 0 400 400"}>
        <g>
          <path d="M307.5,47.5 C307.5,47.5 72.5,66.5 72.5,66.5 C72.5,66.5 86.5,359.5 86.5,359.5 C86.5,359.5 326.5,359.5 326.5,359.5 C326.5,359.5 307.5,292.5 307.5,292.5 C307.5,292.5 146.5,292.5 146.5,292.5 C146.5,292.5 138.5,116.5 138.5,116.5 C138.5,116.5 320.5,116.5 320.5,116.5 C320.5,116.5 307.5,47.5 307.5,47.5 Z"/>
        </g>
      </svg>
    )
  }
}