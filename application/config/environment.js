/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

/**
 * This file is part of the weever package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 20/01/2017
 */

export default {
  UUID: {
    description: 'This is the application UUID.',
    defaultValue: 'cortex',
    defaultValueInProduction: true
  },
  GRAPH_ORIGIN: {
    description: 'This is the application graph origin name.',
    defaultValue: 'mnx:app:weever',
    defaultValueInProduction: true
  },
  OAUTH_REDIRECT_URL: {
    description: 'This is the OAUTH server base URL (https://keycloak.is.here)',
    defaultValue: 'http://localhost:8181'
  },
  OAUTH_TOKEN_VALIDATION_URL: {
    description: 'This is the OAUTH server token validation URL',
    defaultValue: 'http://localhost:8181'
  },
  OAUTH_REALM: {
    description: 'This is the OAUTH Realm name ',
    defaultValue: 'synaptix'
  },
  OAUTH_REALM_CLIENT_ID: {
    description: 'This is the OAUTH Realm client ',
    defaultValue: 'cortex'
  },
  OAUTH_REALM_CLIENT_SECRET: {
    description: 'This is the OAUTH shared secret between this client app and OAuth2 server.',
    defaultValue: 'e9a7be6b-5850-4d22-8740-a8d535e2880b'
  },
  OAUTH_ADMIN_USERNAME: {
    description: 'This is the OAUTH admin username',
    defaultValue: 'admin'
  },
  OAUTH_ADMIN_PASSWORD: {
    description: 'This is the OAUTH admin password',
    defaultValue: 'mnxpowa!'
  },
  APP_PORT: {
    description: 'This is listening port of the application.',
    defaultValue: 3001
  },
  APP_URL: {
    description: 'This is the base url of the application.',
    defaultValue: 'http://localhost:3001'
  },
  RABBITMQ_HOST : {
    description: 'This is RabbitMQ host.',
    defaultValue: 'localhost'
  },
  RABBITMQ_PORT : {
    description: 'This is RabbitMQ port.',
    defaultValue: 5672
  },
  RABBITMQ_LOGIN : {
    description: 'This is RabbitMQ login.',
    defaultValue: 'guest'
  },
  RABBITMQ_PASSWORD : {
    description: 'This is RabbitMQ password.',
    defaultValue: 'guest'
  },
  RABBITMQ_EXCHANGE_NAME : {
    description: 'This is RabbitMQ exchange name.',
    defaultValue: 'synaptix-local'
  }
}