/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
import path from 'path';
import Package from '../../package.json';
import webpackDevConfig from '../webpack.config';
import {SynaptixAdapter, launchApplication} from '@mnemotix/cortex-core';
import {NetworkLayerAMQP} from '@mnemotix/synaptix.js';

import Session from "./datastores/Session";
import schemaBuilder from "./schema/schemaBuilder";
import {mergeSchemaBuilderWithOnesFromBundles} from "@mnemotix/cortex-core/utilities/schema/helpers";

const bundles = [
  path.resolve(__dirname, '../'),
  "@mnemotix/cortex-core",
  "@mnemotix/cortex-addressbook",
  "@mnemotix/cortex-profile",
  "@mnemotix/cortex-uploader",
  "@mnemotix/cortex-finder",
  "@mnemotix/cortex-portfolio",
  "@mnemotix/cortex-login"
];


(async () => {
  await mergeSchemaBuilderWithOnesFromBundles(schemaBuilder, bundles);

  const amqpURL = `amqp://${process.env.RABBITMQ_LOGIN}:${process.env.RABBITMQ_PASSWORD}@${process.env.RABBITMQ_HOST}:${process.env.RABBITMQ_PORT}`;
  const networkLayer = new NetworkLayerAMQP(amqpURL, process.env.RABBITMQ_EXCHANGE_NAME);

  const datastoreAdapter = new SynaptixAdapter(schemaBuilder.generateModelDefinitionsRegister(), networkLayer, Session);

  let graphQLEndpoints = [
    {
      endpointURI: '/graphql',
      graphQLSchema: schemaBuilder.generateExecutableSchema({
        printGraphQLSchemaPath: path.join(__dirname, 'schema/schema.graphql'), /* Optionnal to print schema.graphql */
        printJSONSchemaPath: path.join(__dirname, 'schema/schema.json'),        /* Optionnal to print schema.json    */
        printJSONSchemaDotOrgPath: path.join(__dirname, '../client/schema/schema.json')        /* Optionnal to print schema.json    */
      }),
      datastoreAdapter
    }
  ];

  await launchApplication({
    Package,
    webpackDevConfig,
    graphQLEndpoints,
    bundles,
    networkLayer
  });
})()
  .then()
  .catch(error => console.log(error));