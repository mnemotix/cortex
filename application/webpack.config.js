/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */


const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  mode: "development",
  devtool: 'eval',
  entry: [
    '@babel/polyfill',
    'webpack-hot-middleware/client?reload=true',
    path.resolve(__dirname, './client/app.js')
  ],
  output: {
    path: '/dist/',
    filename: '[name].js',
    chunkFilename: '[name].js',
    publicPath: '/'
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, './client/index.tpl.html'),
      inject: 'body',
      filename: 'index.html'
    }),
    new webpack.IgnorePlugin(/cortex\\-[a-z]*\/server\/.*/),
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.NamedModulesPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.DefinePlugin({
      'NODE_ENV': JSON.stringify(process.env.NODE_ENV),
      'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV)
    })
  ],
  module: {
    rules: [
      {
        test: /\.m?js$/,
        use: 'babel-loader',
        exclude: /node_modules/
      },
      {
        test: /\.mjs$/,
        include: /node_modules/,
        type: "javascript/auto",
      },
      {
        test: /\.less|\.css/,
        include: [
          path.resolve(__dirname, "../application"),
          path.resolve(__dirname, "../bundles")
        ],
        use: [
          'style-loader',
          {
            loader: 'css-loader',
            options: {
              importLoaders: 2,
              modules: true,
              constLoaders: 1,
              localIdentName: "[name]__[local]___[hash:base64:5]",
              sourceMap: true
            }
           },
            {
              loader: 'postcss-loader',
              options: {
                ident: 'postcss',
                sourceMap: true,
                plugins: (loader) => [
                  require("postcss-import")({addDependencyTo: webpack}),
                  require("postcss-url")({
                    url: 'inline',
                    basePath: path.join(__dirname, './application/client'),
                  }),
                  require("postcss-preset-env")({
                    features: {
                      customProperties: {
                        variables: {},
                      },
                    },
                  }),
                  require("postcss-browser-reporter")(),
                  require("postcss-reporter")(),
                ],
                to: "dist/index.css"
              }
            },
            {
              loader: 'less-loader'
            }
        ]
      },
      {
        test: /\.less$/,
        exclude: [
          path.resolve(__dirname, "../application"),
          path.resolve(__dirname, "../bundles")
        ],
        use: [{
          loader: 'style-loader'
        }, {
          loader: 'css-loader'
        }, {
          loader: 'less-loader'
        }]

      },
      {
        test: /\.css/,
        include: [
          path.resolve(__dirname, "../node_modules"),
        ],
        use: [{
          loader: 'style-loader'
        }, {
          loader: 'css-loader'
        }]

      },
      {
        test: /\.(woff|woff2|eot|ttf)$/,
        use: 'url-loader'
      },
      {
        test: /\.md$/,
        use: {
          loader: 'html-loader',
          options: {
            "markdown": true
          }
        }
      },
      {
        test: /\.(jpe?g|png|gif|svg|ico)$/i,
        use: {
          loader: 'file-loader',
          options: {
            name: '[path][name].[hash].[ext]'
          }
        }
      },
    ]
  },
  resolve: {
    enforceModuleExtension: false,
    extensions: ['.mjs', '.js', '.less', '.css', '.json', '.ttf', '.woff', '.woff2', '.svg', '.eot'],
    alias: {
      '../../theme.config$': path.join(__dirname, 'styling/theme.config'),
      './theme.config$': path.join(__dirname, 'styling/theme.config'),
    }
  }
};