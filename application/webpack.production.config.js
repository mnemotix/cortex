/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');


module.exports = {
  mode: "production",
  devtool: 'source-map',
  entry: [
    '@babel/polyfill',
    path.resolve(__dirname, '/client/app.js')
  ],
  output: {
    path: path.join(__dirname, '/dist/'),
    filename: '[name].js',
    chunkFilename: '[name].js',
    publicPath: '/'
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: 'client/index.tpl.html',
      inject: 'body',
      filename: 'index.html'
    }),
    new ExtractTextPlugin('[name]-[hash].min.css', {
      publicPath: '/'
    }),
    new webpack.IgnorePlugin(/cortex\\-[a-z]*\/server\/.*/),
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.DefinePlugin({
      'NODE_ENV': JSON.stringify('production'),
      'process.env.NODE_ENV': JSON.stringify('production'),
      'THUMBNAIL_GENERATOR_ENDPOINT':  JSON.stringify(process.env.THUMBNAIL_GENERATOR_ENDPOINT)
    })
  ],
  optimization: {
    minimizer: [
      // we specify a custom UglifyJsPlugin here to get source maps in production
      new UglifyJsPlugin({
        cache: true,
        parallel: true,
        uglifyOptions: {
          compress: false,
          ecma: 6,
          mangle: true
        },
        sourceMap: true
      })
    ]
  },
  module: {
    rules: [
      {
        test: /\.m?js$/,
        exclude: /node_modules/,
        use: 'babel-loader'
      },
      {
        test: /\.css$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            {
              loader: 'css-loader',
              options: {
                importLoaders: 1,
                modules: true,
                constLoaders:1,
                localIdentName: "[name]__[local]___[hash:base64:5]"
              }
            },
            {
              loader: 'postcss-loader',
              options: {
                ident: 'postcss',
                plugins: (loader) => [
                  require("postcss-import")({ addDependencyTo: webpack }),
                  require("postcss-url")({
                    url: 'inline',
                    basePath: path.join(__dirname, './client'),
                  }),
                  require("postcss-preset-env")({
                    features: {
                      customProperties: {
                        variables: {}
                      },
                    },
                  }),
                  require("postcss-browser-reporter")(),
                  require("postcss-reporter")(),
                ],
                to: "dist/index.css"
              }
            }
          ]
        }),
        exclude: [
        ]
      },
      {
        use: ExtractTextPlugin.extract({
          use: ['css-loader', 'less-loader']
        }),
        test: /\.less$/
      },
      {
        test: /\.(woff|woff2|eot|ttf)$/,
        use: 'url-loader'
      },
      {
        test: /\.md$/,
        use: {
          loader: 'html-loader',
          options: {
            "markdown": true
          }
        }
      },
      {
        test: /\.(jpe?g|png|gif|svg|ico)$/i,
        use: {
          loader: 'file-loader',
          options: {
            name: '[path][name].[hash].[ext]'
          }
        }
      }
    ]
  },
  resolve: {
    enforceModuleExtension: false,
    extensions: ['.js', '.scss', '.css', '.json', '.ttf', '.woff', '.woff2', '.svg', '.eot'],
    alias: {
      '../../theme.config$': path.join(__dirname, 'styling/theme.config'),
      './theme.config$': path.join(__dirname, 'styling/theme.config'),
    }
  }
};