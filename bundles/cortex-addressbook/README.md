# Cortex Address Book


This is a widget bundle to manipulate data from the FOAF ontology. 

Basically it provides:
 
 - An address book to display a searchable list of persons/organisations.
 - UIs to create/update persons/organisations
 
 
## Related bundles

- @mnemotix/cortex-core
- @mnemotix/cortex-form

