/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
import React from 'react';
import {Route, matchPath, Redirect, Switch} from 'react-router';
import {graphql, createRefetchContainer} from 'react-relay';
import {withRelay, Link, withContext} from '@mnemotix/cortex-core/client';
import {Button, List, Image, Icon, Menu, Input, Segment, Grid} from 'semantic-ui-react';
import PersonEditor from "./components/person/PersonEditor";
import OrganisationEditor from "./components/organisation/OrganisationEditor";
import PersonCreator from "./components/person/PersonCreator";
import OrganisationCreator from "./components/organisation/OrganisationCreator";

@withContext
class AddressBook extends React.Component {
  state = {
    loading: false,
    qs: ''
  };

  render(){
    const {t, match, location} = this.props;

    return match.isExact ? (
        <Redirect to={this.props.context.formatRoute('AddressBook_persons')}/>
      ) : (
        <>
          <Menu attached='top' tabular>
            <Menu.Item name={"PERSON"} as={Link} to={this.props.context.formatRoute('AddressBook_persons')} active={!!matchPath(location.pathname, this.props.context.formatRoute('AddressBook_persons'))} onClick={() => this.onRefreshActorType("Person")}>
              {t('Persons')}
            </Menu.Item>
            <Menu.Item name={"ORGANISATION"} as={Link} to={this.props.context.formatRoute('AddressBook_organisations')} active={!!matchPath(location.pathname, this.props.context.formatRoute('AddressBook_organisations'))} onClick={() => this.onRefreshActorType("Organisation")}>
              {t('Organisations')}
            </Menu.Item>
            <Menu.Menu position='right'>
              <Menu.Item>
                <Button primary size={"huge"}
                        icon={this.isOrganisationView(false) ? 'building' : 'user'}
                        content={this.isOrganisationView(false) ? t("Add an organisation") : t("Add a person")}
                        as={Link}
                        to={this.isOrganisationView(false) ? this.props.context.formatRoute('AddressBook_organisationCreate') : this.props.context.formatRoute('AddressBook_personCreate')}
                />
              </Menu.Item>
            </Menu.Menu>
          </Menu>
          <Segment attached='bottom'>
            <Switch>
              <Route path={this.props.context.getRoute('AddressBook_persons')} render={this.renderList.bind(this)}/>
              <Route path={this.props.context.getRoute('AddressBook_organisations')} render={this.renderList.bind(this)}/>
            </Switch>
          </Segment>
        </>
    )
  }

  isOrganisationView(exact = true){
    const {match, location} = this.props;
    return !!matchPath(location.pathname, {path: this.props.context.formatRoute('AddressBook_organisations'), exact});
  }

  isPersonView(exact = true){
    const {match, location} = this.props;
    return !!matchPath(location.pathname, {path: this.props.context.formatRoute('AddressBook_persons'), exact});
  }

  renderList(){
    const {actorsBook, t, match, location} = this.props;

    let isOneColumnGrid = ( this.isOrganisationView() || this.isPersonView() );

    return (
      <Grid columns={isOneColumnGrid ? 1 : 2}>
        <Grid.Column width={isOneColumnGrid ? null : 4}>
          <Input fluid
                 loading={this.state.loading}
                 placeholder={t('Search...')}
                 value={this.state.qs}
                 icon='search'
                 onChange={(e, { value }) => this.search(value)}
          />

          <List selection verticalAlign='middle'>
            {actorsBook.actors.edges.map(({node: actor}) => {
              let route = this.props.context.formatRoute(`AddressBook_${actor.__typename === "Organisation" ? "organisation" : "person"}`,  {id: actor.id});

              return (
                <List.Item key={actor.id}
                           as={Link}
                           to={route}
                           active={!!matchPath(location.pathname, route)}
                >
                  {actor.avatar ? <Image avatar src={actor.avatar}/> : null}

                  <List.Content>
                    <List.Header>
                      {!actor.avatar ? <List.Icon name={actor.__typename === "Organisation" ? "building" : "user"} circular/> : null}
                      {actor.displayName}
                    </List.Header>
                  </List.Content>
                </List.Item>
              );
            })}
          </List>

          {actorsBook.actors.edges.length === 0 ? (
            <Segment textAlign='center' disabled padded='very'>
              {t("There is no result for this query...")}
            </Segment>
          ) : null}
        </Grid.Column>

        <Switch>
          <Route path={this.props.context.getRoute('AddressBook_personCreate')} component={PersonCreator} />
          <Route path={this.props.context.getRoute('AddressBook_organisationCreate')} component={OrganisationCreator} />

          <Route path={this.props.context.getRoute('AddressBook_person')} render={({match}) => {
            return (
              <Grid.Column width={12}>
                <PersonEditor variables={{id: decodeURIComponent(match.params.id)}}/>
              </Grid.Column>
            );
          }}/>

          <Route path={this.props.context.getRoute('AddressBook_organisation')} render={({match}) => {
            return (
              <Grid.Column width={12}>
                <OrganisationEditor variables={{id: decodeURIComponent(match.params.id)}}/>
              </Grid.Column>
            )
          }}/>
        </Switch>
      </Grid>
    );
  }

  search(qs){
    this.setState({
      qs,
      loading: true
    });

    if (this.searchTimer){
      clearTimeout(this.searchTimer);
    }

    this.searchTimer = setTimeout(() => {
      this.props.relay.refetch(() => ({
        qs,
        first: 10
      }), null, () => {
        this.setState({
          loading: false
        });
      });
    }, 250)
  }

  onRefreshActorType = (filterOn) => {
    this.props.relay.refetch(() => ({
      first: 20,
      filterOn,
      qs: this.state.qs
    }));
  };
}

let query = graphql`
  query AddressBook_Query($first: Int $qs: String $filterOn: ActorEnum) {
    actorsBook{
      ...AddressBook_actorsBook
    }
  }
`;

export default withRelay(
  query,
  (props) => {
    return {
      first: 30,
      filterOn: !!matchPath(props.location.pathname, props.context.getRoute(`AddressBook_organisation`)) ? "Organisation" : "Person"
    }
  },
  createRefetchContainer(AddressBook, graphql`
    fragment AddressBook_actorsBook on ActorsBook {
      id
      actors(qs: $qs first: $first filterOn: $filterOn ) @connection(key: "AddressBook_actors", filters: []) {
        edges{
          node{
            __typename
            id
            displayName
            avatar
            
          }
        }
      }
    }
  `, query)
);