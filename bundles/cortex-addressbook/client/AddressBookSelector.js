/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
import React from 'react';
import PropTypes from 'prop-types';
import {graphql, createRefetchContainer} from 'react-relay';
import {withRelay} from '@mnemotix/cortex-core/client';
import {List, Image, Menu, Input, Segment, Grid, Button} from 'semantic-ui-react';
import _ from 'lodash';
import PersonCreator from "./components/person/PersonCreator";
import OrganisationCreator from "./components/organisation/OrganisationCreator";

class AddressBookSelector extends React.Component {
  static propTypes = {
    onSelectActor: PropTypes.func.isRequired,
    exclusiveFilterOn: PropTypes.string,
    creationEnabled: PropTypes.bool
  };

  static defaultProps = {
    creationEnabled: true
  };

  state = {
    loading: false,
    qs: '',
    filterOn: _.get(this.props, 'variables.filterOn'),
    creationActive: false
  };

  render(){
    const {t, exclusiveFilterOn, creationEnabled} = this.props;

    return (
      <>
        <Menu attached='top' tabular>
          <Menu.Item name={"PERSON"} disabled={exclusiveFilterOn && exclusiveFilterOn !== "Person"} active={this.state.filterOn === "Person"} onClick={() => this.onRefreshActorType("Person")}>
            {t('Persons')}
          </Menu.Item>
          <Menu.Item name={"ORGANISATION"} disabled={exclusiveFilterOn && exclusiveFilterOn !== "Organisation"} active={this.state.filterOn === "Organisation"} onClick={() => this.onRefreshActorType("Organisation")}>
            {t('Organisations')}
          </Menu.Item>
          {creationEnabled ? (
            <Menu.Menu position='right'>
              <Menu.Item>
                <Button primary size={"huge"}
                        icon={this.state.filterOn === "Organisation" ? 'building' : 'user'}
                        content={this.state.filterOn === "Organisation" ? t("Add an organisation") : t("Add a person")}
                        onClick={() => this.setState({creationActive: true})}
                />
              </Menu.Item>
            </Menu.Menu>
          ) : null}
        </Menu>
        <Segment attached='bottom'>
          {this.state.creationActive ? this.renderActorCreator() : this.renderList()}
        </Segment>
      </>
    )
  }

  renderList(){
    const {actorsBook, t} = this.props;

    return (
      <Grid columns={1}>
        <Grid.Column>
          <Input fluid
                 loading={this.state.loading}
                 placeholder={t('Search...')}
                 value={this.state.qs}
                 icon='search'
                 onChange={(e, { value }) => this.search(value)}
          />

          <List selection verticalAlign='middle'>
            {actorsBook.actors.edges.map(({node: actor}) => {
              return (
                <List.Item key={actor.id} onClick={(e) => {
                  e.preventDefault();
                  this.props.onSelectActor(actor);
                }}>
                  {actor.avatar ? <Image avatar src={actor.avatar}/> : null}

                  <List.Content>
                    <List.Header>
                      {!actor.avatar ? <List.Icon name={actor.__typename === "Organisation" ? "building" : "user"} circular/> : null}
                      {actor.displayName}
                    </List.Header>
                  </List.Content>
                </List.Item>
              );
            })}
          </List>

          {actorsBook.actors.edges.length === 0 ? (
            <Segment textAlign='center' disabled padded='very'>
              {t("There is no result for this query...")}
            </Segment>
          ) : null}
        </Grid.Column>
      </Grid>
    );
  }

  renderActorCreator(){
    const {onSelectActor} = this.props;

    let onCompleted = (actor) => {
      this.setState({
        creationActive: false
      }, () => onSelectActor(actor));
    };

    return this.state.filterOn === "Organisation" ? (
      <OrganisationCreator onCompleted={onCompleted}/>
    ): (
      <PersonCreator onCompleted={onCompleted}/>
    );
  }


  search(qs){
    this.setState({
      qs,
      loading: true
    });

    if (this.searchTimer){
      clearTimeout(this.searchTimer);
    }

    this.searchTimer = setTimeout(() => {
      this.props.relay.refetch(() => ({
        qs,
        first: 10
      }), null, () => {
        this.setState({
          loading: false
        });
      });
    }, 250)
  }

  onRefreshActorType = (filterOn) => {
    this.props.relay.refetch(() => ({
      first: 20,
      filterOn,
      qs: this.state.qs
    }), null, () => {
      this.setState({
        filterOn
      });
    });
  };
}

let query = graphql`
  query AddressBookSelector_Query($first: Int $qs: String $filterOn: ActorEnum) {
    actorsBook{
      ...AddressBookSelector_actorsBook
    }
  }
`;

export default withRelay(
  query,
  {
    first: 20
  },
  createRefetchContainer(AddressBookSelector, graphql`
    fragment AddressBookSelector_actorsBook on ActorsBook {
      id
      actors(qs: $qs first: $first filterOn: $filterOn ) @connection(key: "AddressBookSelector_actors", filters: []) {
        edges{
          node{
            __typename
            id
            displayName
            avatar
          }
        }
      }
    }
  `, query)
);