/**
 * @flow
 * @relayHash d1fce65bd9764dadc80d1b74824d008e
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
type AddressBookSelector_actorsBook$ref = any;
export type ActorEnum = "Organisation" | "Person" | "%future added value";
export type AddressBookSelector_QueryVariables = {|
  first?: ?number,
  qs?: ?string,
  filterOn?: ?ActorEnum,
|};
export type AddressBookSelector_QueryResponse = {|
  +actorsBook: ?{|
    +$fragmentRefs: AddressBookSelector_actorsBook$ref
  |}
|};
export type AddressBookSelector_Query = {|
  variables: AddressBookSelector_QueryVariables,
  response: AddressBookSelector_QueryResponse,
|};
*/


/*
query AddressBookSelector_Query(
  $first: Int
  $qs: String
  $filterOn: ActorEnum
) {
  actorsBook {
    ...AddressBookSelector_actorsBook
    id
  }
}

fragment AddressBookSelector_actorsBook on ActorsBook {
  id
  actors(qs: $qs, first: $first, filterOn: $filterOn) {
    edges {
      node {
        __typename
        id
        displayName
        avatar
      }
      cursor
    }
    pageInfo {
      endCursor
      hasNextPage
    }
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "first",
    "type": "Int",
    "defaultValue": null
  },
  {
    "kind": "LocalArgument",
    "name": "qs",
    "type": "String",
    "defaultValue": null
  },
  {
    "kind": "LocalArgument",
    "name": "filterOn",
    "type": "ActorEnum",
    "defaultValue": null
  }
],
v1 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "id",
  "args": null,
  "storageKey": null
},
v2 = [
  {
    "kind": "Variable",
    "name": "filterOn",
    "variableName": "filterOn",
    "type": "ActorEnum"
  },
  {
    "kind": "Variable",
    "name": "first",
    "variableName": "first",
    "type": "Int"
  },
  {
    "kind": "Variable",
    "name": "qs",
    "variableName": "qs",
    "type": "String"
  }
];
return {
  "kind": "Request",
  "operationKind": "query",
  "name": "AddressBookSelector_Query",
  "id": null,
  "text": "query AddressBookSelector_Query(\n  $first: Int\n  $qs: String\n  $filterOn: ActorEnum\n) {\n  actorsBook {\n    ...AddressBookSelector_actorsBook\n    id\n  }\n}\n\nfragment AddressBookSelector_actorsBook on ActorsBook {\n  id\n  actors(qs: $qs, first: $first, filterOn: $filterOn) {\n    edges {\n      node {\n        __typename\n        id\n        displayName\n        avatar\n      }\n      cursor\n    }\n    pageInfo {\n      endCursor\n      hasNextPage\n    }\n  }\n}\n",
  "metadata": {},
  "fragment": {
    "kind": "Fragment",
    "name": "AddressBookSelector_Query",
    "type": "Query",
    "metadata": null,
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "actorsBook",
        "storageKey": null,
        "args": null,
        "concreteType": "ActorsBook",
        "plural": false,
        "selections": [
          {
            "kind": "FragmentSpread",
            "name": "AddressBookSelector_actorsBook",
            "args": null
          }
        ]
      }
    ]
  },
  "operation": {
    "kind": "Operation",
    "name": "AddressBookSelector_Query",
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "actorsBook",
        "storageKey": null,
        "args": null,
        "concreteType": "ActorsBook",
        "plural": false,
        "selections": [
          v1,
          {
            "kind": "LinkedField",
            "alias": null,
            "name": "actors",
            "storageKey": null,
            "args": v2,
            "concreteType": "ActorInterfaceConnection",
            "plural": false,
            "selections": [
              {
                "kind": "LinkedField",
                "alias": null,
                "name": "edges",
                "storageKey": null,
                "args": null,
                "concreteType": "ActorInterfaceEdge",
                "plural": true,
                "selections": [
                  {
                    "kind": "LinkedField",
                    "alias": null,
                    "name": "node",
                    "storageKey": null,
                    "args": null,
                    "concreteType": null,
                    "plural": false,
                    "selections": [
                      {
                        "kind": "ScalarField",
                        "alias": null,
                        "name": "__typename",
                        "args": null,
                        "storageKey": null
                      },
                      v1,
                      {
                        "kind": "ScalarField",
                        "alias": null,
                        "name": "displayName",
                        "args": null,
                        "storageKey": null
                      },
                      {
                        "kind": "ScalarField",
                        "alias": null,
                        "name": "avatar",
                        "args": null,
                        "storageKey": null
                      }
                    ]
                  },
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "cursor",
                    "args": null,
                    "storageKey": null
                  }
                ]
              },
              {
                "kind": "LinkedField",
                "alias": null,
                "name": "pageInfo",
                "storageKey": null,
                "args": null,
                "concreteType": "PageInfo",
                "plural": false,
                "selections": [
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "endCursor",
                    "args": null,
                    "storageKey": null
                  },
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "hasNextPage",
                    "args": null,
                    "storageKey": null
                  }
                ]
              }
            ]
          },
          {
            "kind": "LinkedHandle",
            "alias": null,
            "name": "actors",
            "args": v2,
            "handle": "connection",
            "key": "AddressBookSelector_actors",
            "filters": []
          }
        ]
      }
    ]
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = '651460612af6e2e8dcb9c2ecb6862139';
module.exports = node;
