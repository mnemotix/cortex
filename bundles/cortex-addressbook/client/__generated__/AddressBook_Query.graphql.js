/**
 * @flow
 * @relayHash cdac22a7926e107ced9eef9db007df01
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
type AddressBook_actorsBook$ref = any;
export type ActorEnum = "Organisation" | "Person" | "%future added value";
export type AddressBook_QueryVariables = {|
  first?: ?number,
  qs?: ?string,
  filterOn?: ?ActorEnum,
|};
export type AddressBook_QueryResponse = {|
  +actorsBook: ?{|
    +$fragmentRefs: AddressBook_actorsBook$ref
  |}
|};
export type AddressBook_Query = {|
  variables: AddressBook_QueryVariables,
  response: AddressBook_QueryResponse,
|};
*/


/*
query AddressBook_Query(
  $first: Int
  $qs: String
  $filterOn: ActorEnum
) {
  actorsBook {
    ...AddressBook_actorsBook
    id
  }
}

fragment AddressBook_actorsBook on ActorsBook {
  id
  actors(qs: $qs, first: $first, filterOn: $filterOn) {
    edges {
      node {
        __typename
        id
        displayName
        avatar
      }
      cursor
    }
    pageInfo {
      endCursor
      hasNextPage
    }
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "first",
    "type": "Int",
    "defaultValue": null
  },
  {
    "kind": "LocalArgument",
    "name": "qs",
    "type": "String",
    "defaultValue": null
  },
  {
    "kind": "LocalArgument",
    "name": "filterOn",
    "type": "ActorEnum",
    "defaultValue": null
  }
],
v1 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "id",
  "args": null,
  "storageKey": null
},
v2 = [
  {
    "kind": "Variable",
    "name": "filterOn",
    "variableName": "filterOn",
    "type": "ActorEnum"
  },
  {
    "kind": "Variable",
    "name": "first",
    "variableName": "first",
    "type": "Int"
  },
  {
    "kind": "Variable",
    "name": "qs",
    "variableName": "qs",
    "type": "String"
  }
];
return {
  "kind": "Request",
  "operationKind": "query",
  "name": "AddressBook_Query",
  "id": null,
  "text": "query AddressBook_Query(\n  $first: Int\n  $qs: String\n  $filterOn: ActorEnum\n) {\n  actorsBook {\n    ...AddressBook_actorsBook\n    id\n  }\n}\n\nfragment AddressBook_actorsBook on ActorsBook {\n  id\n  actors(qs: $qs, first: $first, filterOn: $filterOn) {\n    edges {\n      node {\n        __typename\n        id\n        displayName\n        avatar\n      }\n      cursor\n    }\n    pageInfo {\n      endCursor\n      hasNextPage\n    }\n  }\n}\n",
  "metadata": {},
  "fragment": {
    "kind": "Fragment",
    "name": "AddressBook_Query",
    "type": "Query",
    "metadata": null,
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "actorsBook",
        "storageKey": null,
        "args": null,
        "concreteType": "ActorsBook",
        "plural": false,
        "selections": [
          {
            "kind": "FragmentSpread",
            "name": "AddressBook_actorsBook",
            "args": null
          }
        ]
      }
    ]
  },
  "operation": {
    "kind": "Operation",
    "name": "AddressBook_Query",
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "actorsBook",
        "storageKey": null,
        "args": null,
        "concreteType": "ActorsBook",
        "plural": false,
        "selections": [
          v1,
          {
            "kind": "LinkedField",
            "alias": null,
            "name": "actors",
            "storageKey": null,
            "args": v2,
            "concreteType": "ActorInterfaceConnection",
            "plural": false,
            "selections": [
              {
                "kind": "LinkedField",
                "alias": null,
                "name": "edges",
                "storageKey": null,
                "args": null,
                "concreteType": "ActorInterfaceEdge",
                "plural": true,
                "selections": [
                  {
                    "kind": "LinkedField",
                    "alias": null,
                    "name": "node",
                    "storageKey": null,
                    "args": null,
                    "concreteType": null,
                    "plural": false,
                    "selections": [
                      {
                        "kind": "ScalarField",
                        "alias": null,
                        "name": "__typename",
                        "args": null,
                        "storageKey": null
                      },
                      v1,
                      {
                        "kind": "ScalarField",
                        "alias": null,
                        "name": "displayName",
                        "args": null,
                        "storageKey": null
                      },
                      {
                        "kind": "ScalarField",
                        "alias": null,
                        "name": "avatar",
                        "args": null,
                        "storageKey": null
                      }
                    ]
                  },
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "cursor",
                    "args": null,
                    "storageKey": null
                  }
                ]
              },
              {
                "kind": "LinkedField",
                "alias": null,
                "name": "pageInfo",
                "storageKey": null,
                "args": null,
                "concreteType": "PageInfo",
                "plural": false,
                "selections": [
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "endCursor",
                    "args": null,
                    "storageKey": null
                  },
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "hasNextPage",
                    "args": null,
                    "storageKey": null
                  }
                ]
              }
            ]
          },
          {
            "kind": "LinkedHandle",
            "alias": null,
            "name": "actors",
            "args": v2,
            "handle": "connection",
            "key": "AddressBook_actors",
            "filters": []
          }
        ]
      }
    ]
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = '71b36f785f1debf0661c6306590d685e';
module.exports = node;
