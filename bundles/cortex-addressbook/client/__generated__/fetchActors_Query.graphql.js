/**
 * @flow
 * @relayHash 319f9ebf46d1339839360abf56f864c5
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
export type ActorEnum = "Organisation" | "Person" | "%future added value";
export type fetchActors_QueryVariables = {|
  first?: ?number,
  qs?: ?string,
  after?: ?string,
  filterOn?: ?ActorEnum,
|};
export type fetchActors_QueryResponse = {|
  +actorsBook: ?{|
    +actors: ?{|
      +edges: ?$ReadOnlyArray<?{|
        +node: ?{|
          +__typename: string,
          +id: string,
          +displayName: ?string,
          +avatar: ?string,
        |}
      |}>
    |}
  |}
|};
export type fetchActors_Query = {|
  variables: fetchActors_QueryVariables,
  response: fetchActors_QueryResponse,
|};
*/


/*
query fetchActors_Query(
  $first: Int
  $qs: String
  $after: String
  $filterOn: ActorEnum
) {
  actorsBook {
    actors(qs: $qs, first: $first, after: $after, filterOn: $filterOn) {
      edges {
        node {
          __typename
          id
          displayName
          avatar
        }
      }
    }
    id
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "first",
    "type": "Int",
    "defaultValue": null
  },
  {
    "kind": "LocalArgument",
    "name": "qs",
    "type": "String",
    "defaultValue": null
  },
  {
    "kind": "LocalArgument",
    "name": "after",
    "type": "String",
    "defaultValue": null
  },
  {
    "kind": "LocalArgument",
    "name": "filterOn",
    "type": "ActorEnum",
    "defaultValue": null
  }
],
v1 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "id",
  "args": null,
  "storageKey": null
},
v2 = {
  "kind": "LinkedField",
  "alias": null,
  "name": "actors",
  "storageKey": null,
  "args": [
    {
      "kind": "Variable",
      "name": "after",
      "variableName": "after",
      "type": "String"
    },
    {
      "kind": "Variable",
      "name": "filterOn",
      "variableName": "filterOn",
      "type": "ActorEnum"
    },
    {
      "kind": "Variable",
      "name": "first",
      "variableName": "first",
      "type": "Int"
    },
    {
      "kind": "Variable",
      "name": "qs",
      "variableName": "qs",
      "type": "String"
    }
  ],
  "concreteType": "ActorInterfaceConnection",
  "plural": false,
  "selections": [
    {
      "kind": "LinkedField",
      "alias": null,
      "name": "edges",
      "storageKey": null,
      "args": null,
      "concreteType": "ActorInterfaceEdge",
      "plural": true,
      "selections": [
        {
          "kind": "LinkedField",
          "alias": null,
          "name": "node",
          "storageKey": null,
          "args": null,
          "concreteType": null,
          "plural": false,
          "selections": [
            {
              "kind": "ScalarField",
              "alias": null,
              "name": "__typename",
              "args": null,
              "storageKey": null
            },
            v1,
            {
              "kind": "ScalarField",
              "alias": null,
              "name": "displayName",
              "args": null,
              "storageKey": null
            },
            {
              "kind": "ScalarField",
              "alias": null,
              "name": "avatar",
              "args": null,
              "storageKey": null
            }
          ]
        }
      ]
    }
  ]
};
return {
  "kind": "Request",
  "operationKind": "query",
  "name": "fetchActors_Query",
  "id": null,
  "text": "query fetchActors_Query(\n  $first: Int\n  $qs: String\n  $after: String\n  $filterOn: ActorEnum\n) {\n  actorsBook {\n    actors(qs: $qs, first: $first, after: $after, filterOn: $filterOn) {\n      edges {\n        node {\n          __typename\n          id\n          displayName\n          avatar\n        }\n      }\n    }\n    id\n  }\n}\n",
  "metadata": {},
  "fragment": {
    "kind": "Fragment",
    "name": "fetchActors_Query",
    "type": "Query",
    "metadata": null,
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "actorsBook",
        "storageKey": null,
        "args": null,
        "concreteType": "ActorsBook",
        "plural": false,
        "selections": [
          v2
        ]
      }
    ]
  },
  "operation": {
    "kind": "Operation",
    "name": "fetchActors_Query",
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "actorsBook",
        "storageKey": null,
        "args": null,
        "concreteType": "ActorsBook",
        "plural": false,
        "selections": [
          v2,
          v1
        ]
      }
    ]
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = 'a9c586b03295b6d41a910835c95aeba7';
module.exports = node;
