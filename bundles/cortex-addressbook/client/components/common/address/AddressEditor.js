/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
import React from 'react';
import {graphql, createFragmentContainer} from 'react-relay';
import {withRouter} from 'react-router';
import {Modules, withContext, withRelay} from '@mnemotix/cortex-core/client';
import {Container} from 'semantic-ui-react';
import {generateUIFormPropsForJSONSchemaMutation, FormConnection, FormMutationDefinition} from "@mnemotix/cortex-form/client";
import {DefaultMutations as Mutations} from "@mnemotix/synaptix.js/lib/client";

@withContext
@withRouter
class AddressEditor extends React.Component {
  state = {
    loading: false,
  };

  render(){
    const {t, actor, relay:{environment}} = this.props;

    const {schema, uiSchema} = generateUIFormPropsForJSONSchemaMutation({
      JSONSchema: this.props.context.getJSONSchema(),
      mutationName: 'updateLocality',
      groups: [
        [
          {name: 'name', width: 4},
          {name: 'street1', width: 12}
        ],
        [
          {name: 'postCode', width: 4},
          {name: 'city', width: 6},
          {name: 'countryName', width: 6}
        ]
      ]
    });

    return (
      <Container>
        <FormConnection listItemOptions={{icon: "map marker"}}
                        nodeSchema={schema}
                        nodeUiSchema={uiSchema}
                        addButtonLabel={t("Create address")}
                        formData={actor.addresses}
                        submitButtonLabel={t("Update an address")}
                        relayUpdateNodeMutation={this.getUpdateMutation()}
                        relayAddNodeMutation={this.getCreateMutation(actor)}
                        nodeExtraActions={(address) => (
                          <Modules.RemoveObjectButton buttonProps={{negative: true}}
                                                      objectRecord={address}
                                                      parentRecord={actor}
                                                      connectionKey={'AddressEditor_addresses'}
                                                      confirmMessage={t("Do you really want to remove this address ?")}
                                                      successMessage={t("The address has been removed")}
                          />
                        )}
        />
      </Container>
    );
  }

  getUpdateMutation(){
    const {t, relay: {environment}} = this.props;

    if (!this.updateMutation){
      this.updateMutation = new FormMutationDefinition({
        mutation: new Mutations.geonames.UpdateLocalityMutation({
          environment
        }),
        successMessage: t("Address has been updated.")
      });
    }

    return this.updateMutation;
  }

  getCreateMutation(actor){
    const {t, relay: {environment}} = this.props;

    if (!this.createMutation){
      this.createMutation = new FormMutationDefinition({
        mutation: new Mutations.foaf.CreateActorAddressMutation({
          environment,
          parentId: actor.id,
          connectionKey: "AddressEditor_addresses"
        }),
        successMessage: t("Address has been created."),
        extraInputs: {
          actorId: actor.id
        }
      });
    }

    return this.createMutation;
  }
}

export default withRelay(
  graphql`
    query AddressEditor_Query($id: ID! $type: ActorEnum) {
      actor(id: $id type: $type){
        ...AddressEditor_actor
      }
    }
  `,
  {},
  createFragmentContainer(AddressEditor, graphql`
    fragment AddressEditor_actor on ActorInterface {
      id
      addresses(first: 50) @connection(key: "AddressEditor_addresses", filters:[]){
        edges{
          node{
            ...LocalityBasicFragment @relay(mask: false)
          }
        }
      }
    }
  `)
);