/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *  
 */
import React from 'react';
import {graphql, createFragmentContainer} from 'react-relay';
import {withRouter} from 'react-router';
import {Modules, withContext, withRelay} from '@mnemotix/cortex-core/client';
import {Button, Container, Modal, Label, Input} from 'semantic-ui-react';
import {generateUIFormPropsForJSONSchemaMutation, FormConnection, FormMutationDefinition} from "@mnemotix/cortex-form/client";
import {DefaultMutations as Mutations} from "@mnemotix/synaptix.js/lib/client";
import AddressBookSelector from "../../../AddressBookSelector";
import style from './style.less';

@withContext
@withRouter
class AffiliationEditor extends React.Component {
  state = {
    loading: false,
    pickActorModalOpen: false,
    selectedActor: null
  };

  render(){
    const {t, actor, relay:{environment}} = this.props;

    const {schema, uiSchema} = generateUIFormPropsForJSONSchemaMutation({
      JSONSchema: this.props.context.getJSONSchema(),
      mutationName: 'updateAffiliation',
      extraProperties: {
        actor : {
          title: this.isActorPerson() ? t("Organisation") : t("Person"),
          type: 'custom',
          render: this.renderTargetAvatar.bind(this)
        }
      },
      groups: [
        [
          {name: 'actor', width: 4},
          {name: 'startDate', width: 4},
          {name: 'endDate', width: 4},
          {name: 'role', width: 4}
        ]
      ]
    });

    return (
      <Container>
        <FormConnection listItemOptions={{icon: "briefcase"}}
                        nodeSchema={schema}
                        nodeUiSchema={uiSchema}
                        formData={actor.affiliations}
                        submitButtonLabel={t("Update an affiliation")}
                        relayUpdateNodeMutation={this.getUpdateMutation()}
                        relayAddNodeMutation={this.getCreateMutation()}
                        nodeExtraActions={(affiliation) => (
                          <Modules.RemoveObjectButton buttonProps={{negative: true}}
                                                      objectRecord={affiliation}
                                                      parentRecord={actor}
                                                      connectionKey={'AffiliationEditor_affiliations'}
                                                      confirmMessage={t("Do you really want to remove this affiliation ?")}
                                                      successMessage={t("The affiliation has been removed")}
                          />
                        )}
        />
        {this.state.selectedActor ? (
          <div>

          </div>
        ) : (
          <Modal trigger={(
                  <Container textAlign={"center"}>
                    <Button onClick={() => this.setState({pickActorModalOpen: true})}>
                      {("Create affiliation")}
                    </Button>
                  </Container>
                )}
                 open={this.state.pickActorModalOpen}
                 onClose={() => this.setState({pickActorModalOpen: false})}
          >
            <Modal.Header>{
              this.isActorPerson() ?
                t("Select an organisation to affiliate to {{name}}", {name: actor.displayName}) :
                t("Select a person to affiliate to {{name}}", {name: actor.displayName})}
            </Modal.Header>
            <Modal.Content>
              <AddressBookSelector variables={{filterOn: this.isActorPerson() ? "Organisation" : "Person"}}
                                   exclusiveFilterOn={this.isActorPerson() ? "Organisation" : "Person"}
                                   onSelectActor={(selectedActor) => {
                                     this.setState({
                                       pickActorModalOpen: false,
                                       selectedActor
                                     })
                                   }}
              />
            </Modal.Content>
          </Modal>
        )}
      </Container>
    );
  }

  renderTargetAvatar(affiliation){
    const {t} = this.props;
    let actor;

    if (affiliation.id) {
      actor = this.isActorPerson() ? affiliation.organisation : affiliation.person;
    } else {
      actor = this.state.selectedActor;
    }

    return (
      <Label className={style.actor} content={actor.displayName} size='small' image={{
        avatar: true,
        spaced: 'right',
        src: actor.avatar
      }}/>
    )
  }

  getUpdateMutation(){
    const {t, relay: {environment}} = this.props;

    if (!this.updateMutation){
      this.updateMutation = new FormMutationDefinition({
        mutation: new Mutations.foaf.UpdateAffiliationMutation({
          environment
        }),
        successMessage: t("Affiliation has been updated.")
      });
    }

    return this.updateMutation;
  }

  isActorPerson(){
    return this.props.actor.__typename === "Person";
  }

  getCreateMutation(){
    if (this.state.selectedActor){
      const {t, relay: {environment}, actor} = this.props;

      return new FormMutationDefinition({
        mutation: new Mutations.foaf.CreateAffiliationMutation({
          environment,
          parentId: actor.id,
          connectionKey: "AffiliationEditor_affiliations"
        }),
        successMessage: t("Affiliation has been created."),
        extraInputs: {
          personId: this.isActorPerson() ? actor.id : this.state.selectedActor.id,
          organisationId: !this.isActorPerson() ? actor.id : this.state.selectedActor.id
        }
      });
    }
  }
}

export default withRelay(
  graphql`
    query AffiliationEditor_Query($id: ID! $type: ActorEnum) {
      actor(id: $id type: $type){
        ...AffiliationEditor_actor
      }
    }
  `,
  {},
  createFragmentContainer(AffiliationEditor, graphql`
    fragment AffiliationEditor_actor on ActorInterface {
      __typename
      id
      displayName
      affiliations(first: 50) @connection(key: "AffiliationEditor_affiliations", filters:[]){
        edges{
          node{
            ...AffiliationBasicFragment @relay(mask: false)
          }
        }
      }
    }
  `)
);