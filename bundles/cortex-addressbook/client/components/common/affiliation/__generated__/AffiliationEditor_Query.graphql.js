/**
 * @flow
 * @relayHash 969afe8ed15a358a3798cd3d653b2ac3
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
type AffiliationEditor_actor$ref = any;
export type ActorEnum = "Organisation" | "Person" | "%future added value";
export type AffiliationEditor_QueryVariables = {|
  id: string,
  type?: ?ActorEnum,
|};
export type AffiliationEditor_QueryResponse = {|
  +actor: ?{|
    +$fragmentRefs: AffiliationEditor_actor$ref
  |}
|};
export type AffiliationEditor_Query = {|
  variables: AffiliationEditor_QueryVariables,
  response: AffiliationEditor_QueryResponse,
|};
*/


/*
query AffiliationEditor_Query(
  $id: ID!
  $type: ActorEnum
) {
  actor(id: $id, type: $type) {
    __typename
    ...AffiliationEditor_actor
    id
  }
}

fragment AffiliationEditor_actor on ActorInterface {
  __typename
  id
  displayName
  affiliations(first: 50) {
    edges {
      node {
        id
        uri
        role
        endDate
        startDate
        organisation {
          id
          uri
          displayName
          avatar
          name
          shortName
          description
          shortDescription
        }
        person {
          id
          uri
          displayName
          avatar
          firstName
          lastName
          maidenName
          bio
          shortBio
          bday
          gender
        }
        __typename
      }
      cursor
    }
    pageInfo {
      endCursor
      hasNextPage
    }
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "id",
    "type": "ID!",
    "defaultValue": null
  },
  {
    "kind": "LocalArgument",
    "name": "type",
    "type": "ActorEnum",
    "defaultValue": null
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "id",
    "variableName": "id",
    "type": "ID!"
  },
  {
    "kind": "Variable",
    "name": "type",
    "variableName": "type",
    "type": "ActorEnum"
  }
],
v2 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "__typename",
  "args": null,
  "storageKey": null
},
v3 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "id",
  "args": null,
  "storageKey": null
},
v4 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "displayName",
  "args": null,
  "storageKey": null
},
v5 = [
  {
    "kind": "Literal",
    "name": "first",
    "value": 50,
    "type": "Int"
  }
],
v6 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "uri",
  "args": null,
  "storageKey": null
},
v7 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "avatar",
  "args": null,
  "storageKey": null
};
return {
  "kind": "Request",
  "operationKind": "query",
  "name": "AffiliationEditor_Query",
  "id": null,
  "text": "query AffiliationEditor_Query(\n  $id: ID!\n  $type: ActorEnum\n) {\n  actor(id: $id, type: $type) {\n    __typename\n    ...AffiliationEditor_actor\n    id\n  }\n}\n\nfragment AffiliationEditor_actor on ActorInterface {\n  __typename\n  id\n  displayName\n  affiliations(first: 50) {\n    edges {\n      node {\n        id\n        uri\n        role\n        endDate\n        startDate\n        organisation {\n          id\n          uri\n          displayName\n          avatar\n          name\n          shortName\n          description\n          shortDescription\n        }\n        person {\n          id\n          uri\n          displayName\n          avatar\n          firstName\n          lastName\n          maidenName\n          bio\n          shortBio\n          bday\n          gender\n        }\n        __typename\n      }\n      cursor\n    }\n    pageInfo {\n      endCursor\n      hasNextPage\n    }\n  }\n}\n",
  "metadata": {},
  "fragment": {
    "kind": "Fragment",
    "name": "AffiliationEditor_Query",
    "type": "Query",
    "metadata": null,
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "actor",
        "storageKey": null,
        "args": v1,
        "concreteType": null,
        "plural": false,
        "selections": [
          {
            "kind": "FragmentSpread",
            "name": "AffiliationEditor_actor",
            "args": null
          }
        ]
      }
    ]
  },
  "operation": {
    "kind": "Operation",
    "name": "AffiliationEditor_Query",
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "actor",
        "storageKey": null,
        "args": v1,
        "concreteType": null,
        "plural": false,
        "selections": [
          v2,
          v3,
          v4,
          {
            "kind": "LinkedField",
            "alias": null,
            "name": "affiliations",
            "storageKey": "affiliations(first:50)",
            "args": v5,
            "concreteType": "AffiliationConnection",
            "plural": false,
            "selections": [
              {
                "kind": "LinkedField",
                "alias": null,
                "name": "edges",
                "storageKey": null,
                "args": null,
                "concreteType": "AffiliationEdge",
                "plural": true,
                "selections": [
                  {
                    "kind": "LinkedField",
                    "alias": null,
                    "name": "node",
                    "storageKey": null,
                    "args": null,
                    "concreteType": "Affiliation",
                    "plural": false,
                    "selections": [
                      v3,
                      v6,
                      {
                        "kind": "ScalarField",
                        "alias": null,
                        "name": "role",
                        "args": null,
                        "storageKey": null
                      },
                      {
                        "kind": "ScalarField",
                        "alias": null,
                        "name": "endDate",
                        "args": null,
                        "storageKey": null
                      },
                      {
                        "kind": "ScalarField",
                        "alias": null,
                        "name": "startDate",
                        "args": null,
                        "storageKey": null
                      },
                      {
                        "kind": "LinkedField",
                        "alias": null,
                        "name": "organisation",
                        "storageKey": null,
                        "args": null,
                        "concreteType": "Organisation",
                        "plural": false,
                        "selections": [
                          v3,
                          v6,
                          v4,
                          v7,
                          {
                            "kind": "ScalarField",
                            "alias": null,
                            "name": "name",
                            "args": null,
                            "storageKey": null
                          },
                          {
                            "kind": "ScalarField",
                            "alias": null,
                            "name": "shortName",
                            "args": null,
                            "storageKey": null
                          },
                          {
                            "kind": "ScalarField",
                            "alias": null,
                            "name": "description",
                            "args": null,
                            "storageKey": null
                          },
                          {
                            "kind": "ScalarField",
                            "alias": null,
                            "name": "shortDescription",
                            "args": null,
                            "storageKey": null
                          }
                        ]
                      },
                      {
                        "kind": "LinkedField",
                        "alias": null,
                        "name": "person",
                        "storageKey": null,
                        "args": null,
                        "concreteType": "Person",
                        "plural": false,
                        "selections": [
                          {
                            "kind": "ScalarField",
                            "alias": null,
                            "name": "lastName",
                            "args": null,
                            "storageKey": null
                          },
                          v3,
                          v4,
                          v7,
                          {
                            "kind": "ScalarField",
                            "alias": null,
                            "name": "firstName",
                            "args": null,
                            "storageKey": null
                          },
                          v6,
                          {
                            "kind": "ScalarField",
                            "alias": null,
                            "name": "maidenName",
                            "args": null,
                            "storageKey": null
                          },
                          {
                            "kind": "ScalarField",
                            "alias": null,
                            "name": "bio",
                            "args": null,
                            "storageKey": null
                          },
                          {
                            "kind": "ScalarField",
                            "alias": null,
                            "name": "shortBio",
                            "args": null,
                            "storageKey": null
                          },
                          {
                            "kind": "ScalarField",
                            "alias": null,
                            "name": "bday",
                            "args": null,
                            "storageKey": null
                          },
                          {
                            "kind": "ScalarField",
                            "alias": null,
                            "name": "gender",
                            "args": null,
                            "storageKey": null
                          }
                        ]
                      },
                      v2
                    ]
                  },
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "cursor",
                    "args": null,
                    "storageKey": null
                  }
                ]
              },
              {
                "kind": "LinkedField",
                "alias": null,
                "name": "pageInfo",
                "storageKey": null,
                "args": null,
                "concreteType": "PageInfo",
                "plural": false,
                "selections": [
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "endCursor",
                    "args": null,
                    "storageKey": null
                  },
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "hasNextPage",
                    "args": null,
                    "storageKey": null
                  }
                ]
              }
            ]
          },
          {
            "kind": "LinkedHandle",
            "alias": null,
            "name": "affiliations",
            "args": v5,
            "handle": "connection",
            "key": "AffiliationEditor_affiliations",
            "filters": []
          }
        ]
      }
    ]
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = '01fdddd3288027fdeeea6990f7638157';
module.exports = node;
