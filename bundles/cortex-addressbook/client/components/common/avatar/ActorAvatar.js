/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *  
 */
import React from 'react';
import PropTypes from 'prop-types';
import {graphql, createFragmentContainer} from 'react-relay';
import {withContext, withRelay, Modules} from '@mnemotix/cortex-core/client';
import {withRouter} from 'react-router';
import {Link} from 'react-router-dom';
import classnames from 'classnames';
import {Image, Placeholder, Popup} from 'semantic-ui-react';
import ImageLoader from "react-loading-image";
import style from './ActorAvatar.less';

@withContext
@withRouter
class ActorAvatar extends React.Component {
  static propTypes = {
    asIcon: PropTypes.bool,
    hideName: PropTypes.bool,
    hideAvatar: PropTypes.bool,
    popupAvatar: PropTypes.bool,
    imageProps: PropTypes.object,
    className: PropTypes.string,
    popupName: PropTypes.bool,
    popupProps: PropTypes.object,
    extraTextAfterName: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
    extraTextBeforeName:  PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
  };

  static defaultProps = {
    extraTextAfterName:  '',
    extraTextBeforeName: ''
  };

  render() {
    const {t, actor, hideName, imageProps, className, popupProps,
      popupName, extraTextAfterName, extraTextBeforeName, popupAvatar, asIcon} = this.props;

    let avatar = (className, as) => (
      <ImageLoader src={this.props.context.getImageThumbnailURL(actor.avatar, 200)}
                   image={({src}) => (
                     <Image avatar src={src} {...imageProps} className={className} as={as}/>
                   )}
                   loading={() => (
                     <Placeholder>
                       <Placeholder.Image square/>
                     </Placeholder>
                   )}
      />
    );

    let avatarLink = (className) => (
      <Link className={className} to={this.props.context.formatRoute(actor.__typename === "Organisation" ?'AddressBook_organisation' : 'AddressBook_person', {id: actor.id})}>
        {avatar()}
        {popupName || hideName ? null : <>{extraTextBeforeName}{' '}{actor.displayName}{' '}{extraTextAfterName}</>}
      </Link>
    );

    if (asIcon){
      return avatar(classnames("icon", style.asIcon), 'i');
    } else if (popupName){
      return (
        <Popup trigger={avatarLink(className)}
               content={<>{extraTextBeforeName}{' '}{actor.displayName}{' '}{extraTextAfterName}</>}
               position={'top center'}
               hoverable
               {...popupProps}
        />
      );
    } else if (popupAvatar){
      return (
        <Popup trigger={(
          <span className={className}>{extraTextBeforeName}{' '}{actor.displayName}{' '}{extraTextAfterName}</span>
        )}
               content={avatarLink()}
               position={'top center'}
               hoverable
               {...popupProps}
        />
      );
    } else {
      return avatarLink(className);
    }
  }
}

export {ActorAvatar as ActorAvatarRaw};

export default withRelay(
  graphql`
    query ActorAvatar_Query($id: ID!, $type: ActorEnum) {
      actor(id: $id type: $type){
        ...ActorAvatar_actor
      }
    }
  `,
  {},
  createFragmentContainer(ActorAvatar, graphql`
    fragment ActorAvatar_actor on ActorInterface {
      __typename
      id
      avatar
      displayName
    }
  `),
  true
);