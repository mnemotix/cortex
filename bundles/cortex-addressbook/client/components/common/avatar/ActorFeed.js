/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
import React from 'react';
import PropTypes from 'prop-types';
import {graphql, createFragmentContainer} from 'react-relay';
import {withContext, withRelay, Modules} from '@mnemotix/cortex-core/client';
import {withRouter} from 'react-router';
import {Link} from 'react-router-dom';
import {Image, Feed} from 'semantic-ui-react';
import style from './ActorFeed.less';

@withContext
@withRouter
class ActorFeed extends React.Component {
  static propTypes = {
    headerMessage: PropTypes.string,
    inlineMessage: PropTypes.string,
    footerMessage: PropTypes.string,
    size: PropTypes.oneOf(['small', 'large']),
    imageProps: PropTypes.object,
    className: PropTypes.string
  };

  render() {
    const {t, actor, date, headerMessage, inlineMessage, footerMessage, size, imageProps, className} = this.props;

    return (
      <Feed size={size} className={className}>
        <Feed.Event>
          <Feed.Label {...imageProps}>
            <Image avatar src={actor.avatar}/>
          </Feed.Label>
          <Feed.Content>
            <Feed.Date>{headerMessage}</Feed.Date>
            <Feed.Summary>
              <Feed.User as={Link}
                         to={this.props.context.formatRoute(actor.__typename === "Organisation" ? 'AddressBook_organisation' : 'AddressBook_person', {id: actor.id})}>
                {actor.displayName}
              </Feed.User>
              <Feed.Date>{inlineMessage}</Feed.Date>
            </Feed.Summary>
            <Feed.Extra text>
              {footerMessage}
            </Feed.Extra>
          </Feed.Content>
        </Feed.Event>
      </Feed>
    );
  }
}

export {ActorFeed as ActorFeedRaw};

export default withRelay(
  graphql`
    query ActorFeed_Query($id: ID!, $type: ActorEnum) {
      actor(id: $id type: $type){
        ...ActorFeed_actor
      }
    }
  `,
  {},
  createFragmentContainer(ActorFeed, graphql`
    fragment ActorFeed_actor on ActorInterface {
      __typename
      id
      avatar
      displayName
    }
  `)
);