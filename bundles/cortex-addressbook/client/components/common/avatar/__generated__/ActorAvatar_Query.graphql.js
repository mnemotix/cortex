/**
 * @flow
 * @relayHash b505ede6db2f77c5f47dfca1c6a8fa95
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
type ActorAvatar_actor$ref = any;
export type ActorEnum = "Organisation" | "Person" | "%future added value";
export type ActorAvatar_QueryVariables = {|
  id: string,
  type?: ?ActorEnum,
|};
export type ActorAvatar_QueryResponse = {|
  +actor: ?{|
    +$fragmentRefs: ActorAvatar_actor$ref
  |}
|};
export type ActorAvatar_Query = {|
  variables: ActorAvatar_QueryVariables,
  response: ActorAvatar_QueryResponse,
|};
*/


/*
query ActorAvatar_Query(
  $id: ID!
  $type: ActorEnum
) {
  actor(id: $id, type: $type) {
    __typename
    ...ActorAvatar_actor
    id
  }
}

fragment ActorAvatar_actor on ActorInterface {
  __typename
  id
  avatar
  displayName
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "id",
    "type": "ID!",
    "defaultValue": null
  },
  {
    "kind": "LocalArgument",
    "name": "type",
    "type": "ActorEnum",
    "defaultValue": null
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "id",
    "variableName": "id",
    "type": "ID!"
  },
  {
    "kind": "Variable",
    "name": "type",
    "variableName": "type",
    "type": "ActorEnum"
  }
];
return {
  "kind": "Request",
  "operationKind": "query",
  "name": "ActorAvatar_Query",
  "id": null,
  "text": "query ActorAvatar_Query(\n  $id: ID!\n  $type: ActorEnum\n) {\n  actor(id: $id, type: $type) {\n    __typename\n    ...ActorAvatar_actor\n    id\n  }\n}\n\nfragment ActorAvatar_actor on ActorInterface {\n  __typename\n  id\n  avatar\n  displayName\n}\n",
  "metadata": {},
  "fragment": {
    "kind": "Fragment",
    "name": "ActorAvatar_Query",
    "type": "Query",
    "metadata": null,
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "actor",
        "storageKey": null,
        "args": v1,
        "concreteType": null,
        "plural": false,
        "selections": [
          {
            "kind": "FragmentSpread",
            "name": "ActorAvatar_actor",
            "args": null
          }
        ]
      }
    ]
  },
  "operation": {
    "kind": "Operation",
    "name": "ActorAvatar_Query",
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "actor",
        "storageKey": null,
        "args": v1,
        "concreteType": null,
        "plural": false,
        "selections": [
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "__typename",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "id",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "avatar",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "displayName",
            "args": null,
            "storageKey": null
          }
        ]
      }
    ]
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = 'bccf00696f72059e1dc6f49c43bdf29e';
module.exports = node;
