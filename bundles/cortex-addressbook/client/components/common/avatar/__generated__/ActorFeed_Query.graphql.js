/**
 * @flow
 * @relayHash 6a343eba3e8304f2fe6bdd0e25b8e6ca
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
type ActorFeed_actor$ref = any;
export type ActorEnum = "Organisation" | "Person" | "%future added value";
export type ActorFeed_QueryVariables = {|
  id: string,
  type?: ?ActorEnum,
|};
export type ActorFeed_QueryResponse = {|
  +actor: ?{|
    +$fragmentRefs: ActorFeed_actor$ref
  |}
|};
export type ActorFeed_Query = {|
  variables: ActorFeed_QueryVariables,
  response: ActorFeed_QueryResponse,
|};
*/


/*
query ActorFeed_Query(
  $id: ID!
  $type: ActorEnum
) {
  actor(id: $id, type: $type) {
    __typename
    ...ActorFeed_actor
    id
  }
}

fragment ActorFeed_actor on ActorInterface {
  __typename
  id
  avatar
  displayName
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "id",
    "type": "ID!",
    "defaultValue": null
  },
  {
    "kind": "LocalArgument",
    "name": "type",
    "type": "ActorEnum",
    "defaultValue": null
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "id",
    "variableName": "id",
    "type": "ID!"
  },
  {
    "kind": "Variable",
    "name": "type",
    "variableName": "type",
    "type": "ActorEnum"
  }
];
return {
  "kind": "Request",
  "operationKind": "query",
  "name": "ActorFeed_Query",
  "id": null,
  "text": "query ActorFeed_Query(\n  $id: ID!\n  $type: ActorEnum\n) {\n  actor(id: $id, type: $type) {\n    __typename\n    ...ActorFeed_actor\n    id\n  }\n}\n\nfragment ActorFeed_actor on ActorInterface {\n  __typename\n  id\n  avatar\n  displayName\n}\n",
  "metadata": {},
  "fragment": {
    "kind": "Fragment",
    "name": "ActorFeed_Query",
    "type": "Query",
    "metadata": null,
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "actor",
        "storageKey": null,
        "args": v1,
        "concreteType": null,
        "plural": false,
        "selections": [
          {
            "kind": "FragmentSpread",
            "name": "ActorFeed_actor",
            "args": null
          }
        ]
      }
    ]
  },
  "operation": {
    "kind": "Operation",
    "name": "ActorFeed_Query",
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "actor",
        "storageKey": null,
        "args": v1,
        "concreteType": null,
        "plural": false,
        "selections": [
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "__typename",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "id",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "avatar",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "displayName",
            "args": null,
            "storageKey": null
          }
        ]
      }
    ]
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = 'b3f2c2fe9321a0e0d114373cc9918f8b';
module.exports = node;
