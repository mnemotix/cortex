/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *  
 */
import React from 'react';
import {graphql, createFragmentContainer} from 'react-relay';
import {withRouter} from 'react-router';
import {Modules, withContext, withRelay} from '@mnemotix/cortex-core/client';
import {Container} from 'semantic-ui-react';
import {generateUIFormPropsForJSONSchemaMutation, FormConnection, FormMutationDefinition} from "@mnemotix/cortex-form/client";
import {DefaultMutations as Mutations} from "@mnemotix/synaptix.js/lib/client";

@withContext
@withRouter
class EmailAccountEditor extends React.Component {
  state = {
    loading: false,
  };

  render(){
    const {t, actor, relay:{environment}} = this.props;

    const {schema, uiSchema} = generateUIFormPropsForJSONSchemaMutation({
      JSONSchema: this.props.context.getJSONSchema(),
      mutationName: 'updateEmailAccount',
      groups: [
        [
          {name: 'accountName', width: 4},
          {name: 'email', width: 12}
        ]
      ]
    });

    return (
      <Container>
        <FormConnection listItemOptions={{icon: "mail"}}
                        nodeSchema={schema}
                        nodeUiSchema={uiSchema}
                        addButtonLabel={t("Create email account")}
                        formData={actor.emails}
                        submitButtonLabel={t("Update an emails")}
                        relayUpdateNodeMutation={this.getUpdateMutation()}
                        relayAddNodeMutation={this.getCreateMutation(actor)}
                        nodeExtraActions={(emailAccount) => (
                          <Modules.RemoveObjectButton buttonProps={{negative: true}}
                                                      objectRecord={emailAccount}
                                                      parentRecord={actor}
                                                      connectionKey={'EmailAccountEditor_emails'}
                                                      confirmMessage={t("Do you really want to remove this email account ?")}
                                                      successMessage={t("The email account has been removed")}
                          />
                        )}
        />
      </Container>
    );
  }

  getUpdateMutation(){
    const {t, relay: {environment}} = this.props;

    if (!this.updateMutation){
      this.updateMutation = new FormMutationDefinition({
        mutation: new Mutations.foaf.UpdateEmailAccountMutation({
          environment
        }),
        successMessage: t("Email account has been updated.")
      });
    }

    return this.updateMutation;
  }

  getCreateMutation(actor){
    const {t, relay: {environment}} = this.props;

    if (!this.createMutation){
      this.createMutation = new FormMutationDefinition({
        mutation: new Mutations.foaf.CreateEmailAccountMutation({
          environment,
          parentId: actor.id,
          connectionKey: "EmailAccountEditor_emails"
        }),
        successMessage: t("Email account has been created."),
        extraInputs: {
          actorId: actor.id
        }
      });
    }

    return this.createMutation;
  }
}

export default withRelay(
  graphql`
    query EmailAccountEditor_Query($id: ID! $type: ActorEnum) {
      actor(id: $id type: $type){
        ...EmailAccountEditor_actor
      }
    }
  `,
  {},
  createFragmentContainer(EmailAccountEditor, graphql`
    fragment EmailAccountEditor_actor on ActorInterface {
      id
      emails(first: 50) @connection(key: "EmailAccountEditor_emails", filters:[]){
        edges{
          node{
            ...EmailAccountBasicFragment @relay(mask: false)
          }
        }
      }
    }
  `)
);