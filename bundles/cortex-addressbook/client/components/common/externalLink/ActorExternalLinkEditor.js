/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *  
 */
import React from 'react';
import {graphql, createFragmentContainer} from 'react-relay';
import {withRouter} from 'react-router';
import {Modules, withContext, withRelay} from '@mnemotix/cortex-core/client';
import {Container} from 'semantic-ui-react';
import {generateUIFormPropsForJSONSchemaMutation, FormConnection, FormMutationDefinition} from "@mnemotix/cortex-form/client";
import {DefaultMutations as Mutations} from "@mnemotix/synaptix.js/lib/client";

@withContext
@withRouter
class ActorExternalLinkEditor extends React.Component {
  state = {
    loading: false,
  };

  render(){
    const {t, actor, relay:{environment}} = this.props;

    const {schema, uiSchema} = generateUIFormPropsForJSONSchemaMutation({
      JSONSchema: this.props.context.getJSONSchema(),
      mutationName: 'updateExternalLink',
      groups: [
        [
          {name: 'name', width: 5},
          {name: 'link', width: 11}
        ]
      ]
    });

    return (
      <Container>
        <FormConnection listItemOptions={{icon: "linkify"}}
                        nodeSchema={schema}
                        nodeUiSchema={uiSchema}
                        addButtonLabel={t("Create external link")}
                        formData={actor.externalLinks}
                        submitButtonLabel={t("Update a link")}
                        relayUpdateNodeMutation={this.getUpdateMutation()}
                        relayAddNodeMutation={this.getCreateMutation(actor)}
                        nodeExtraActions={(externalLink) => (
                          <Modules.RemoveObjectButton buttonProps={{negative: true}}
                                                      objectRecord={externalLink}
                                                      parentRecord={actor}
                                                      connectionKey={'ActorExternalLinkEditor_externalLinks'}
                                                      confirmMessage={t("Do you really want to remove this external link ?")}
                                                      successMessage={t("The external link has been removed")}
                          />
                        )}
        />
      </Container>
    );
  }

  getUpdateMutation(){
    const {t, relay: {environment}} = this.props;

    if (!this.updateMutation){
      this.updateMutation = new FormMutationDefinition({
        mutation: new Mutations.UpdateExternalLinkMutation({
          environment
        }),
        successMessage: t("Link has been updated.")
      });
    }

    return this.updateMutation;
  }

  getCreateMutation(actor){
    const {t, relay: {environment}} = this.props;

    if (!this.createMutation){
      this.createMutation = new FormMutationDefinition({
        mutation: new Mutations.foaf.CreateActorExternalLinkMutation({
          environment,
          parentId: actor.id,
          connectionKey: "ActorExternalLinkEditor_externalLinks"
        }),
        successMessage: t("Link has been created."),
        extraInputs: {
          objectId: actor.id
        }
      });
    }

    return this.createMutation;
  }
}

export default withRelay(
  graphql`
    query ActorExternalLinkEditor_Query($id: ID! $type: ActorEnum) {
      actor(id: $id type: $type){
        ...ActorExternalLinkEditor_actor
      }
    }
  `,
  {},
  createFragmentContainer(ActorExternalLinkEditor, graphql`
    fragment ActorExternalLinkEditor_actor on ActorInterface {
      id
      externalLinks(first: 50) @connection(key: "ActorExternalLinkEditor_externalLinks", filters:[]){
        edges{
          node{
            ...ExternalLinkBasicFragment @relay(mask: false)
          }
        }
      }
    }
  `)
);