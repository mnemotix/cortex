/**
 * @flow
 * @relayHash fca6b97804a909145af48b1739226567
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
type ActorExternalLinkEditor_actor$ref = any;
export type ActorEnum = "Organisation" | "Person" | "%future added value";
export type ActorExternalLinkEditor_QueryVariables = {|
  id: string,
  type?: ?ActorEnum,
|};
export type ActorExternalLinkEditor_QueryResponse = {|
  +actor: ?{|
    +$fragmentRefs: ActorExternalLinkEditor_actor$ref
  |}
|};
export type ActorExternalLinkEditor_Query = {|
  variables: ActorExternalLinkEditor_QueryVariables,
  response: ActorExternalLinkEditor_QueryResponse,
|};
*/


/*
query ActorExternalLinkEditor_Query(
  $id: ID!
  $type: ActorEnum
) {
  actor(id: $id, type: $type) {
    __typename
    ...ActorExternalLinkEditor_actor
    id
  }
}

fragment ActorExternalLinkEditor_actor on ActorInterface {
  id
  externalLinks(first: 50) {
    edges {
      node {
        id
        link
        name
        __typename
      }
      cursor
    }
    pageInfo {
      endCursor
      hasNextPage
    }
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "id",
    "type": "ID!",
    "defaultValue": null
  },
  {
    "kind": "LocalArgument",
    "name": "type",
    "type": "ActorEnum",
    "defaultValue": null
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "id",
    "variableName": "id",
    "type": "ID!"
  },
  {
    "kind": "Variable",
    "name": "type",
    "variableName": "type",
    "type": "ActorEnum"
  }
],
v2 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "__typename",
  "args": null,
  "storageKey": null
},
v3 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "id",
  "args": null,
  "storageKey": null
},
v4 = [
  {
    "kind": "Literal",
    "name": "first",
    "value": 50,
    "type": "Int"
  }
];
return {
  "kind": "Request",
  "operationKind": "query",
  "name": "ActorExternalLinkEditor_Query",
  "id": null,
  "text": "query ActorExternalLinkEditor_Query(\n  $id: ID!\n  $type: ActorEnum\n) {\n  actor(id: $id, type: $type) {\n    __typename\n    ...ActorExternalLinkEditor_actor\n    id\n  }\n}\n\nfragment ActorExternalLinkEditor_actor on ActorInterface {\n  id\n  externalLinks(first: 50) {\n    edges {\n      node {\n        id\n        link\n        name\n        __typename\n      }\n      cursor\n    }\n    pageInfo {\n      endCursor\n      hasNextPage\n    }\n  }\n}\n",
  "metadata": {},
  "fragment": {
    "kind": "Fragment",
    "name": "ActorExternalLinkEditor_Query",
    "type": "Query",
    "metadata": null,
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "actor",
        "storageKey": null,
        "args": v1,
        "concreteType": null,
        "plural": false,
        "selections": [
          {
            "kind": "FragmentSpread",
            "name": "ActorExternalLinkEditor_actor",
            "args": null
          }
        ]
      }
    ]
  },
  "operation": {
    "kind": "Operation",
    "name": "ActorExternalLinkEditor_Query",
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "actor",
        "storageKey": null,
        "args": v1,
        "concreteType": null,
        "plural": false,
        "selections": [
          v2,
          v3,
          {
            "kind": "LinkedField",
            "alias": null,
            "name": "externalLinks",
            "storageKey": "externalLinks(first:50)",
            "args": v4,
            "concreteType": "ExternalLinkConnection",
            "plural": false,
            "selections": [
              {
                "kind": "LinkedField",
                "alias": null,
                "name": "edges",
                "storageKey": null,
                "args": null,
                "concreteType": "ExternalLinkEdge",
                "plural": true,
                "selections": [
                  {
                    "kind": "LinkedField",
                    "alias": null,
                    "name": "node",
                    "storageKey": null,
                    "args": null,
                    "concreteType": "ExternalLink",
                    "plural": false,
                    "selections": [
                      v3,
                      {
                        "kind": "ScalarField",
                        "alias": null,
                        "name": "link",
                        "args": null,
                        "storageKey": null
                      },
                      {
                        "kind": "ScalarField",
                        "alias": null,
                        "name": "name",
                        "args": null,
                        "storageKey": null
                      },
                      v2
                    ]
                  },
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "cursor",
                    "args": null,
                    "storageKey": null
                  }
                ]
              },
              {
                "kind": "LinkedField",
                "alias": null,
                "name": "pageInfo",
                "storageKey": null,
                "args": null,
                "concreteType": "PageInfo",
                "plural": false,
                "selections": [
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "endCursor",
                    "args": null,
                    "storageKey": null
                  },
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "hasNextPage",
                    "args": null,
                    "storageKey": null
                  }
                ]
              }
            ]
          },
          {
            "kind": "LinkedHandle",
            "alias": null,
            "name": "externalLinks",
            "args": v4,
            "handle": "connection",
            "key": "ActorExternalLinkEditor_externalLinks",
            "filters": []
          }
        ]
      }
    ]
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = 'ea7ff819660256f49c8b216bf66995d5';
module.exports = node;
