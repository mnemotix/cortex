/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
import React from 'react';
import {graphql, createFragmentContainer} from 'react-relay';
import {withRouter} from 'react-router';
import {Modules, withContext, withRelay} from '@mnemotix/cortex-core/client';
import {Container} from 'semantic-ui-react';
import {generateUIFormPropsForJSONSchemaMutation, FormConnection, FormMutationDefinition} from "@mnemotix/cortex-form/client";
import {DefaultMutations as Mutations} from "@mnemotix/synaptix.js/lib/client";

@withContext
@withRouter
class PhoneEditor extends React.Component {
  state = {
    loading: false,
  };

  render(){
    const {t, actor, relay:{environment}} = this.props;

    const {schema, uiSchema} = generateUIFormPropsForJSONSchemaMutation({
      JSONSchema: this.props.context.getJSONSchema(),
      mutationName: 'updatePhone',
      groups: [
        [
          {name: 'name', width: 4},
          {name: 'number', width: 12}
        ]
      ]
    });

    return (
      <Container>
        <FormConnection listItemOptions={{icon: "phone"}}
                        nodeSchema={schema}
                        nodeUiSchema={uiSchema}
                        addButtonLabel={t("Create phone")}
                        formData={actor.phones}
                        submitButtonLabel={t("Update a phone")}
                        relayUpdateNodeMutation={this.getUpdateMutation()}
                        relayAddNodeMutation={this.getCreateMutation(actor)}
                        nodeExtraActions={(phone) => (
                          <Modules.RemoveObjectButton buttonProps={{negative: true}}
                                                      objectRecord={phone}
                                                      parentRecord={actor}
                                                      connectionKey={'PhoneEditor_phones'}
                                                      confirmMessage={t("Do you really want to remove this phone ?")}
                                                      successMessage={t("The phone has been removed")}
                          />
                        )}
        />
      </Container>
    );
  }

  getUpdateMutation(){
    const {t, relay: {environment}} = this.props;

    if (!this.updateMutation){
      this.updateMutation = new FormMutationDefinition({
        mutation: new Mutations.foaf.UpdatePhoneMutation({
          environment
        }),
        successMessage: t("Phone has been updated.")
      });
    }

    return this.updateMutation;
  }

  getCreateMutation(actor){
    const {t, relay: {environment}} = this.props;


    if (!this.createMutation){
      this.createMutation = new FormMutationDefinition({
        mutation: new Mutations.foaf.CreatePhoneMutation({
          environment,
          parentId: actor.id,
          connectionKey: "PhoneEditor_phones"
        }),
        successMessage: t("Phone has been created."),
        extraInputs: {
          actorId: actor.id
        }
      });
    }

    return this.createMutation;
  }
}

export default withRelay(
  graphql`
    query PhoneEditor_Query($id: ID! $type: ActorEnum) {
      actor(id: $id type: $type){
        ...PhoneEditor_actor
      }
    }
  `,
  {},
  createFragmentContainer(PhoneEditor, graphql`
    fragment PhoneEditor_actor on ActorInterface {
      id
      phones(first: 50) @connection(key: "PhoneEditor_phones", filters:[]){
        edges{
          node{
            ...PhoneBasicFragment @relay(mask: false)
          }
        }
      }
    }
  `)
);