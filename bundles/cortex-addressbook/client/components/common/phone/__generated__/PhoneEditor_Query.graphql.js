/**
 * @flow
 * @relayHash 57d176610781a750dc87a634329fa600
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
type PhoneEditor_actor$ref = any;
export type ActorEnum = "Organisation" | "Person" | "%future added value";
export type PhoneEditor_QueryVariables = {|
  id: string,
  type?: ?ActorEnum,
|};
export type PhoneEditor_QueryResponse = {|
  +actor: ?{|
    +$fragmentRefs: PhoneEditor_actor$ref
  |}
|};
export type PhoneEditor_Query = {|
  variables: PhoneEditor_QueryVariables,
  response: PhoneEditor_QueryResponse,
|};
*/


/*
query PhoneEditor_Query(
  $id: ID!
  $type: ActorEnum
) {
  actor(id: $id, type: $type) {
    __typename
    ...PhoneEditor_actor
    id
  }
}

fragment PhoneEditor_actor on ActorInterface {
  id
  phones(first: 50) {
    edges {
      node {
        id
        uri
        name
        number
        __typename
      }
      cursor
    }
    pageInfo {
      endCursor
      hasNextPage
    }
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "id",
    "type": "ID!",
    "defaultValue": null
  },
  {
    "kind": "LocalArgument",
    "name": "type",
    "type": "ActorEnum",
    "defaultValue": null
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "id",
    "variableName": "id",
    "type": "ID!"
  },
  {
    "kind": "Variable",
    "name": "type",
    "variableName": "type",
    "type": "ActorEnum"
  }
],
v2 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "__typename",
  "args": null,
  "storageKey": null
},
v3 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "id",
  "args": null,
  "storageKey": null
},
v4 = [
  {
    "kind": "Literal",
    "name": "first",
    "value": 50,
    "type": "Int"
  }
];
return {
  "kind": "Request",
  "operationKind": "query",
  "name": "PhoneEditor_Query",
  "id": null,
  "text": "query PhoneEditor_Query(\n  $id: ID!\n  $type: ActorEnum\n) {\n  actor(id: $id, type: $type) {\n    __typename\n    ...PhoneEditor_actor\n    id\n  }\n}\n\nfragment PhoneEditor_actor on ActorInterface {\n  id\n  phones(first: 50) {\n    edges {\n      node {\n        id\n        uri\n        name\n        number\n        __typename\n      }\n      cursor\n    }\n    pageInfo {\n      endCursor\n      hasNextPage\n    }\n  }\n}\n",
  "metadata": {},
  "fragment": {
    "kind": "Fragment",
    "name": "PhoneEditor_Query",
    "type": "Query",
    "metadata": null,
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "actor",
        "storageKey": null,
        "args": v1,
        "concreteType": null,
        "plural": false,
        "selections": [
          {
            "kind": "FragmentSpread",
            "name": "PhoneEditor_actor",
            "args": null
          }
        ]
      }
    ]
  },
  "operation": {
    "kind": "Operation",
    "name": "PhoneEditor_Query",
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "actor",
        "storageKey": null,
        "args": v1,
        "concreteType": null,
        "plural": false,
        "selections": [
          v2,
          v3,
          {
            "kind": "LinkedField",
            "alias": null,
            "name": "phones",
            "storageKey": "phones(first:50)",
            "args": v4,
            "concreteType": "PhoneConnection",
            "plural": false,
            "selections": [
              {
                "kind": "LinkedField",
                "alias": null,
                "name": "edges",
                "storageKey": null,
                "args": null,
                "concreteType": "PhoneEdge",
                "plural": true,
                "selections": [
                  {
                    "kind": "LinkedField",
                    "alias": null,
                    "name": "node",
                    "storageKey": null,
                    "args": null,
                    "concreteType": "Phone",
                    "plural": false,
                    "selections": [
                      v3,
                      {
                        "kind": "ScalarField",
                        "alias": null,
                        "name": "uri",
                        "args": null,
                        "storageKey": null
                      },
                      {
                        "kind": "ScalarField",
                        "alias": null,
                        "name": "name",
                        "args": null,
                        "storageKey": null
                      },
                      {
                        "kind": "ScalarField",
                        "alias": null,
                        "name": "number",
                        "args": null,
                        "storageKey": null
                      },
                      v2
                    ]
                  },
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "cursor",
                    "args": null,
                    "storageKey": null
                  }
                ]
              },
              {
                "kind": "LinkedField",
                "alias": null,
                "name": "pageInfo",
                "storageKey": null,
                "args": null,
                "concreteType": "PageInfo",
                "plural": false,
                "selections": [
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "endCursor",
                    "args": null,
                    "storageKey": null
                  },
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "hasNextPage",
                    "args": null,
                    "storageKey": null
                  }
                ]
              }
            ]
          },
          {
            "kind": "LinkedHandle",
            "alias": null,
            "name": "phones",
            "args": v4,
            "handle": "connection",
            "key": "PhoneEditor_phones",
            "filters": []
          }
        ]
      }
    ]
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = '88f77c7b1a967b81f6f6ec4155532cd2';
module.exports = node;
