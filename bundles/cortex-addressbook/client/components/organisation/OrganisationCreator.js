/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *  
 */
import React from 'react';
import PropTypes from 'prop-types';
import {graphql, createFragmentContainer} from 'react-relay';
import {withRouter} from 'react-router';
import {withContext, withRelay} from '@mnemotix/cortex-core/client';
import {Modal, Header} from 'semantic-ui-react';
import {generateUIFormPropsForJSONSchemaMutation, Form, FormMutationDefinition} from "@mnemotix/cortex-form/client";
import {DefaultMutations as Mutations} from "@mnemotix/synaptix.js/lib/client";

@withContext
@withRouter
class OrganisationCreator extends React.Component {
  static propTypes = {
    onCompleted: PropTypes.func
  };

  state = {
    loading: false,
  };

  render(){
    const {t, organisation, actorsBook, relay:{environment}, history, onCompleted} = this.props;

    const {schema, uiSchema} = generateUIFormPropsForJSONSchemaMutation({
      JSONSchema: this.props.context.getJSONSchema(),
      mutationName: 'updateOrganisation'
    });

    return (
      <Modal open={true} onClose={() => {
        if(!onCompleted){
          history.goBack()
        }
      }} closeOnDimmerClick={true}>
        <Header>
          {t("Create a new organisation")}
        </Header>
        <Modal.Content>
          <Form schema={schema}
                uiSchema={uiSchema}
                formData={organisation}
                relayMutation={new FormMutationDefinition({
                  mutation: new Mutations.foaf.CreateOrganisationMutation({
                    environment,
                    connectionKey: "AddressBook_actors",
                    parentId: actorsBook.id,
                    onCompleted: (actor) => {
                      if (onCompleted){
                        onCompleted(actor);
                      } else {
                        history.push(this.props.context.formatRoute('AddressBook_organisation', {id: actor.id}))
                      }
                    }
                  }),
                  successMessage: t("Organisation has been created.")
                })}
          />
        </Modal.Content>
      </Modal>
    );
  }
}

export default withRelay(
  graphql`
    query OrganisationCreator_Query{
      actorsBook{
        ...OrganisationCreator_actorsBook
      }
    }
  `,
  {},
  createFragmentContainer(OrganisationCreator, graphql`
    fragment OrganisationCreator_actorsBook on ActorsBook {
      id
    }
  `)
);