/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
import React from 'react';
import {graphql, createFragmentContainer} from 'react-relay';
import {withRouter} from 'react-router';
import {Modules, withContext, withRelay} from '@mnemotix/cortex-core/client';
import {Container, Header, Message, Segment} from 'semantic-ui-react';
import {generateUIFormPropsForJSONSchemaMutation, Form, FormMutationDefinition} from "@mnemotix/cortex-form/client";
import {DefaultMutations as Mutations} from "@mnemotix/synaptix.js/lib/client";
import EmailAccountEditor from "../common/emailAccount/EmailAccountEditor";
import PhoneEditor from "../common/phone/PhoneEditor";
import AddressEditor from "../common/address/AddressEditor";
import AffiliationEditor from "../common/affiliation/AffiliationEditor";
import ExternalLinkEditor from "../common/externalLink/ActorExternalLinkEditor";

@withContext
@withRouter
class OrganisationEditor extends React.Component {
  state = {
    loading: false,
  };

  getUpdateMutation(){
    const {t, relay: {environment}} = this.props;

    if (!this.updateMutation){
      this.updateMutation = new FormMutationDefinition({
        mutation: new Mutations.foaf.UpdateOrganisationMutation({
          environment
        }),
        successMessage: t("Organisation has been updated.")
      });
    }

    return this.updateMutation;
  }

  render(){
    const {t, organisation, history, actorsBook} = this.props;

    if (!organisation){
      return (
        <Message negative>
          <Message.Header>{t("Sorry...")}</Message.Header>
          <p>{t("This organisation does not exists...")}</p>
        </Message>
      );
    }

    const {schema, uiSchema} = generateUIFormPropsForJSONSchemaMutation({
      JSONSchema: this.props.context.getJSONSchema(),
      mutationName: 'updateOrganisation'
    });

    return (
      <Container>
        <Segment>
          <Header>
            {t("General information for {{name}}", {name: organisation.displayName})}
          </Header>
        <Form schema={schema}
                uiSchema={uiSchema}
                formData={organisation}
                relayMutation={this.getUpdateMutation()}
          />
        </Segment>

        <Segment>
          <Header>
            {t("Email accounts")}
          </Header>

          <EmailAccountEditor variables={{id: organisation.id, type: "Organisation"}}/>
        </Segment>

        <Segment>
          <Header>
            {t("Phones")}
          </Header>

          <PhoneEditor variables={{id: organisation.id, type: "Organisation"}}/>
        </Segment>

        <Segment>
          <Header>
            {t("Addresses")}
          </Header>

          <AddressEditor variables={{id: organisation.id, type: "Organisation"}}/>
        </Segment>

        <Segment>
          <Header>
            {t("External links")}
          </Header>

          <ExternalLinkEditor variables={{id: organisation.id, type: "Organisation"}}/>
        </Segment>

        <Segment>
          <Header>
            {t("Affiliated persons")}
          </Header>

          <AffiliationEditor variables={{id: organisation.id, type: "Organisation"}}/>
        </Segment>

        <Segment>
          <Header>
            {t("Actions")}
          </Header>

          <Modules.RemoveObjectButton buttonProps={{negative: true}}
                                      objectRecord={organisation}
                                      parentRecord={actorsBook}
                                      connectionKey={'AddressBook_actors'}
                                      confirmMessage={t("Do you really want to remove the organisation {{name}} ?", {name: organisation.displayName})}
                                      onCompleted={() => history.push(this.props.context.formatRoute('AddressBook_organisations'))}
                                      successMessage={t("The organisation {{name}} has been removed", {name: organisation.displayName})}
                                      removeLabel={t("Remove {{name}}", {name: organisation.displayName})}
          />
        </Segment>
      </Container>
    );
  }
}

export default withRelay(
  graphql`
    query OrganisationEditor_Query($id: ID) {
      organisation(id: $id){
        ...OrganisationEditor_organisation
      }
      
      actorsBook{
        ...OrganisationEditor_actorsBook
      }
    }
  `,
  {},
  createFragmentContainer(OrganisationEditor, graphql`
    fragment OrganisationEditor_organisation on Organisation {
      ...OrganisationBasicFragment @relay(mask: false)
    }

    fragment OrganisationEditor_actorsBook on ActorsBook {
      id
    }
  `)
);