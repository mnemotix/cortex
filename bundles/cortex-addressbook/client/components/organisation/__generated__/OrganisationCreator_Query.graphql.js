/**
 * @flow
 * @relayHash 146822f994d8a7d49f3de0b437f47e01
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
type OrganisationCreator_actorsBook$ref = any;
export type OrganisationCreator_QueryVariables = {||};
export type OrganisationCreator_QueryResponse = {|
  +actorsBook: ?{|
    +$fragmentRefs: OrganisationCreator_actorsBook$ref
  |}
|};
export type OrganisationCreator_Query = {|
  variables: OrganisationCreator_QueryVariables,
  response: OrganisationCreator_QueryResponse,
|};
*/


/*
query OrganisationCreator_Query {
  actorsBook {
    ...OrganisationCreator_actorsBook
    id
  }
}

fragment OrganisationCreator_actorsBook on ActorsBook {
  id
}
*/

const node/*: ConcreteRequest*/ = {
  "kind": "Request",
  "operationKind": "query",
  "name": "OrganisationCreator_Query",
  "id": null,
  "text": "query OrganisationCreator_Query {\n  actorsBook {\n    ...OrganisationCreator_actorsBook\n    id\n  }\n}\n\nfragment OrganisationCreator_actorsBook on ActorsBook {\n  id\n}\n",
  "metadata": {},
  "fragment": {
    "kind": "Fragment",
    "name": "OrganisationCreator_Query",
    "type": "Query",
    "metadata": null,
    "argumentDefinitions": [],
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "actorsBook",
        "storageKey": null,
        "args": null,
        "concreteType": "ActorsBook",
        "plural": false,
        "selections": [
          {
            "kind": "FragmentSpread",
            "name": "OrganisationCreator_actorsBook",
            "args": null
          }
        ]
      }
    ]
  },
  "operation": {
    "kind": "Operation",
    "name": "OrganisationCreator_Query",
    "argumentDefinitions": [],
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "actorsBook",
        "storageKey": null,
        "args": null,
        "concreteType": "ActorsBook",
        "plural": false,
        "selections": [
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "id",
            "args": null,
            "storageKey": null
          }
        ]
      }
    ]
  }
};
// prettier-ignore
(node/*: any*/).hash = 'ffac9c6b8a6c27225bb328f1533ee504';
module.exports = node;
