/**
 * @flow
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteFragment } from 'relay-runtime';
import type { FragmentReference } from "relay-runtime";
declare export opaque type OrganisationCreator_actorsBook$ref: FragmentReference;
export type OrganisationCreator_actorsBook = {|
  +id: string,
  +$refType: OrganisationCreator_actorsBook$ref,
|};
*/


const node/*: ConcreteFragment*/ = {
  "kind": "Fragment",
  "name": "OrganisationCreator_actorsBook",
  "type": "ActorsBook",
  "metadata": null,
  "argumentDefinitions": [],
  "selections": [
    {
      "kind": "ScalarField",
      "alias": null,
      "name": "id",
      "args": null,
      "storageKey": null
    }
  ]
};
// prettier-ignore
(node/*: any*/).hash = '4982af73ac15a932fb342f59cd013dfb';
module.exports = node;
