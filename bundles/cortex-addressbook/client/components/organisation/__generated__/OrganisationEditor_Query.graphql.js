/**
 * @flow
 * @relayHash d4e3cb989315c3c8c0617531004395dc
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
type OrganisationEditor_actorsBook$ref = any;
type OrganisationEditor_organisation$ref = any;
export type OrganisationEditor_QueryVariables = {|
  id?: ?string
|};
export type OrganisationEditor_QueryResponse = {|
  +organisation: ?{|
    +$fragmentRefs: OrganisationEditor_organisation$ref
  |},
  +actorsBook: ?{|
    +$fragmentRefs: OrganisationEditor_actorsBook$ref
  |},
|};
export type OrganisationEditor_Query = {|
  variables: OrganisationEditor_QueryVariables,
  response: OrganisationEditor_QueryResponse,
|};
*/


/*
query OrganisationEditor_Query(
  $id: ID
) {
  organisation(id: $id) {
    ...OrganisationEditor_organisation
    id
  }
  actorsBook {
    ...OrganisationEditor_actorsBook
    id
  }
}

fragment OrganisationEditor_organisation on Organisation {
  id
  uri
  displayName
  avatar
  name
  shortName
  description
  shortDescription
}

fragment OrganisationEditor_actorsBook on ActorsBook {
  id
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "id",
    "type": "ID",
    "defaultValue": null
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "id",
    "variableName": "id",
    "type": "ID"
  }
],
v2 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "id",
  "args": null,
  "storageKey": null
};
return {
  "kind": "Request",
  "operationKind": "query",
  "name": "OrganisationEditor_Query",
  "id": null,
  "text": "query OrganisationEditor_Query(\n  $id: ID\n) {\n  organisation(id: $id) {\n    ...OrganisationEditor_organisation\n    id\n  }\n  actorsBook {\n    ...OrganisationEditor_actorsBook\n    id\n  }\n}\n\nfragment OrganisationEditor_organisation on Organisation {\n  id\n  uri\n  displayName\n  avatar\n  name\n  shortName\n  description\n  shortDescription\n}\n\nfragment OrganisationEditor_actorsBook on ActorsBook {\n  id\n}\n",
  "metadata": {},
  "fragment": {
    "kind": "Fragment",
    "name": "OrganisationEditor_Query",
    "type": "Query",
    "metadata": null,
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "organisation",
        "storageKey": null,
        "args": v1,
        "concreteType": "Organisation",
        "plural": false,
        "selections": [
          {
            "kind": "FragmentSpread",
            "name": "OrganisationEditor_organisation",
            "args": null
          }
        ]
      },
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "actorsBook",
        "storageKey": null,
        "args": null,
        "concreteType": "ActorsBook",
        "plural": false,
        "selections": [
          {
            "kind": "FragmentSpread",
            "name": "OrganisationEditor_actorsBook",
            "args": null
          }
        ]
      }
    ]
  },
  "operation": {
    "kind": "Operation",
    "name": "OrganisationEditor_Query",
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "organisation",
        "storageKey": null,
        "args": v1,
        "concreteType": "Organisation",
        "plural": false,
        "selections": [
          v2,
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "uri",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "displayName",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "avatar",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "name",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "shortName",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "description",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "shortDescription",
            "args": null,
            "storageKey": null
          }
        ]
      },
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "actorsBook",
        "storageKey": null,
        "args": null,
        "concreteType": "ActorsBook",
        "plural": false,
        "selections": [
          v2
        ]
      }
    ]
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = '4eaeb3b273187a39fcd0ffc620493d10';
module.exports = node;
