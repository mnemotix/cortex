/**
 * @flow
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteFragment } from 'relay-runtime';
import type { FragmentReference } from "relay-runtime";
declare export opaque type OrganisationEditor_actorsBook$ref: FragmentReference;
export type OrganisationEditor_actorsBook = {|
  +id: string,
  +$refType: OrganisationEditor_actorsBook$ref,
|};
*/


const node/*: ConcreteFragment*/ = {
  "kind": "Fragment",
  "name": "OrganisationEditor_actorsBook",
  "type": "ActorsBook",
  "metadata": null,
  "argumentDefinitions": [],
  "selections": [
    {
      "kind": "ScalarField",
      "alias": null,
      "name": "id",
      "args": null,
      "storageKey": null
    }
  ]
};
// prettier-ignore
(node/*: any*/).hash = 'f400656fcd9d3226aaac1e8b13bc2d6e';
module.exports = node;
