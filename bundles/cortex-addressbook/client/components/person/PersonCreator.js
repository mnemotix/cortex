/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
import React from 'react';
import PropTypes from 'prop-types';
import {graphql, createFragmentContainer} from 'react-relay';
import {withContext, withRelay} from '@mnemotix/cortex-core/client';
import {withRouter} from 'react-router';
import {Header, Modal} from 'semantic-ui-react';
import {generateUIFormPropsForJSONSchemaMutation, Form, FormMutationDefinition} from "@mnemotix/cortex-form/client";
import {DefaultMutations as Mutations} from "@mnemotix/synaptix.js/lib/client";

@withContext
@withRouter
class PersonCreator extends React.Component {
  static propTypes = {
    onCompleted: PropTypes.func
  };

  state = {
    loading: false,
  };

  render(){
    const {t, person, actorsBook, relay: {environment}, history, match, onCompleted} = this.props;

    const {schema, uiSchema} = generateUIFormPropsForJSONSchemaMutation({
      JSONSchema: this.props.context.getJSONSchema(),
      mutationName: 'updatePerson'
    });

    return (
      <Modal open={true} onClose={() => history.goBack()} closeOnDimmerClick={true}>
        <Header>
          {t("Create a new person")}
        </Header>
        <Modal.Content>
          <Form schema={schema}
                uiSchema={uiSchema}
                formData={person}
                relayMutation={new FormMutationDefinition({
                  mutation: new Mutations.foaf.CreatePersonMutation({
                    environment,
                    parentId: actorsBook.id,
                    connectionKey: "AddressBook_actors",
                    onCompleted: (actor) => {
                      if (onCompleted){
                        onCompleted(actor);
                      } else {
                        history.push(this.props.context.formatRoute('AddressBook_person', {id: actor.id}));
                      }
                    }
                  }),
                  successMessage: t("Person has been created.")
                })}
          />
        </Modal.Content>
      </Modal>
    );
  }
}

export default withRelay(
  graphql`
    query PersonCreator_Query{
      actorsBook{
        ...PersonCreator_actorsBook
      }
    }
  `,
  {},
  createFragmentContainer(PersonCreator, graphql`
    fragment PersonCreator_actorsBook on ActorsBook {
      id
    }
  `)
);