/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
import React from 'react';
import {graphql, createFragmentContainer} from 'react-relay';
import {withContext, withRelay, Modules} from '@mnemotix/cortex-core/client';
import {withRouter} from 'react-router';
import {Container, Message, Segment, Header} from 'semantic-ui-react';
import {generateUIFormPropsForJSONSchemaMutation, Form, FormMutationDefinition} from "@mnemotix/cortex-form/client";
import {DefaultMutations as Mutations} from "@mnemotix/synaptix.js/lib/client";
import PhoneEditor from "../common/phone/PhoneEditor";
import AddressEditor from "../common/address/AddressEditor";
import EmailAccountEditor from "../common/emailAccount/EmailAccountEditor";
import AffiliationEditor from "../common/affiliation/AffiliationEditor";
import ExternalLinkEditor from "../common/externalLink/ActorExternalLinkEditor";

@withContext
@withRouter
class PersonEditor extends React.Component {
  state = {
    deleteLoading: false,
  };

  getUpdateMutation(){
    const {t, relay: {environment}} = this.props;

    if (!this.updateMutation){
      this.updateMutation = new FormMutationDefinition({
        mutation: new Mutations.foaf.UpdatePersonMutation({
          environment
        }),
        successMessage: t("Person has been updated.")
      });
    }

    return this.updateMutation;
  }

  render(){
    const {t, person, relay: {environment}, actorsBook, match, history} = this.props;

    if (!person){
      return (
        <Message negative>
          <Message.Header>{t("Sorry...")}</Message.Header>
          <p>{t("This person does not exists...")}</p>
        </Message>
      );
    }

    const {schema, uiSchema} = generateUIFormPropsForJSONSchemaMutation({
      JSONSchema: this.props.context.getJSONSchema(),
      mutationName: 'updatePerson'
    });

    return (
      <Container>
        <Segment>
          <Header>
            {t("General information for {{name}}", {name: person.displayName})}
          </Header>

          <Form schema={schema}
                uiSchema={uiSchema}
                formData={person}
                relayMutation={this.getUpdateMutation()}
          />
        </Segment>

        <Segment>
          <Header>
            {t("Email accounts")}
          </Header>

          <EmailAccountEditor variables={{id: person.id, type: "Person"}}/>
        </Segment>

        <Segment>
          <Header>
            {t("Phones")}
          </Header>

          <PhoneEditor variables={{id: person.id, type: "Person"}}/>
        </Segment>

        <Segment>
          <Header>
            {t("Addresses")}
          </Header>

          <AddressEditor variables={{id: person.id, type: "Person"}}/>
        </Segment>

        <Segment>
          <Header>
            {t("External links")}
          </Header>

          <ExternalLinkEditor variables={{id: person.id, type: "Person"}}/>
        </Segment>

        <Segment>
          <Header>
            {t("Organisation affiliations")}
          </Header>

          <AffiliationEditor variables={{id: person.id, type: "Person"}}/>
        </Segment>

        <Segment>
          <Header>
            {t("Actions")}
          </Header>

          <Modules.RemoveObjectButton buttonProps={{negative: true}}
                                      objectRecord={person}
                                      parentRecord={actorsBook}
                                      connectionKey={'AddressBook_actors'}
                                      confirmMessage={t("Do you really want to remove the person {{name}} ?", {name: person.displayName})}
                                      onCompleted={() => history.push(this.props.context.formatRoute('AddressBook_persons'))}
                                      successMessage={t("The person {{name}} has been removed", {name: person.displayName})}
                                      removeLabel={t("Remove {{name}}", {name: person.displayName})}
          />
        </Segment>
      </Container>
    );
  }
}

export default withRelay(
  graphql`
    query PersonEditor_Query($id: ID) {
      person(id: $id){
        ...PersonEditor_person
      }
      
      actorsBook{
        ...PersonEditor_actorsBook
      }
    }
  `,
  {},
  createFragmentContainer(PersonEditor, graphql`
    fragment PersonEditor_person on Person {
      ...PersonBasicFragment @relay(mask: false)
    }

    fragment PersonEditor_actorsBook on ActorsBook {
      id
    }
  `)
);