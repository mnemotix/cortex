/**
 * @flow
 * @relayHash 6587b0ce98d00f237b9e6640f2f3d0bb
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
type PersonCreator_actorsBook$ref = any;
export type PersonCreator_QueryVariables = {||};
export type PersonCreator_QueryResponse = {|
  +actorsBook: ?{|
    +$fragmentRefs: PersonCreator_actorsBook$ref
  |}
|};
export type PersonCreator_Query = {|
  variables: PersonCreator_QueryVariables,
  response: PersonCreator_QueryResponse,
|};
*/


/*
query PersonCreator_Query {
  actorsBook {
    ...PersonCreator_actorsBook
    id
  }
}

fragment PersonCreator_actorsBook on ActorsBook {
  id
}
*/

const node/*: ConcreteRequest*/ = {
  "kind": "Request",
  "operationKind": "query",
  "name": "PersonCreator_Query",
  "id": null,
  "text": "query PersonCreator_Query {\n  actorsBook {\n    ...PersonCreator_actorsBook\n    id\n  }\n}\n\nfragment PersonCreator_actorsBook on ActorsBook {\n  id\n}\n",
  "metadata": {},
  "fragment": {
    "kind": "Fragment",
    "name": "PersonCreator_Query",
    "type": "Query",
    "metadata": null,
    "argumentDefinitions": [],
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "actorsBook",
        "storageKey": null,
        "args": null,
        "concreteType": "ActorsBook",
        "plural": false,
        "selections": [
          {
            "kind": "FragmentSpread",
            "name": "PersonCreator_actorsBook",
            "args": null
          }
        ]
      }
    ]
  },
  "operation": {
    "kind": "Operation",
    "name": "PersonCreator_Query",
    "argumentDefinitions": [],
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "actorsBook",
        "storageKey": null,
        "args": null,
        "concreteType": "ActorsBook",
        "plural": false,
        "selections": [
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "id",
            "args": null,
            "storageKey": null
          }
        ]
      }
    ]
  }
};
// prettier-ignore
(node/*: any*/).hash = '331447e6366732a224baced37d9cd67e';
module.exports = node;
