/**
 * @flow
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteFragment } from 'relay-runtime';
import type { FragmentReference } from "relay-runtime";
declare export opaque type PersonCreator_actorsBook$ref: FragmentReference;
export type PersonCreator_actorsBook = {|
  +id: string,
  +$refType: PersonCreator_actorsBook$ref,
|};
*/


const node/*: ConcreteFragment*/ = {
  "kind": "Fragment",
  "name": "PersonCreator_actorsBook",
  "type": "ActorsBook",
  "metadata": null,
  "argumentDefinitions": [],
  "selections": [
    {
      "kind": "ScalarField",
      "alias": null,
      "name": "id",
      "args": null,
      "storageKey": null
    }
  ]
};
// prettier-ignore
(node/*: any*/).hash = 'c4c8fecfc2d27e26f7fc31c5bea12dc5';
module.exports = node;
