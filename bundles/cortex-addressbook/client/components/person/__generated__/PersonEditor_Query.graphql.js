/**
 * @flow
 * @relayHash 27c1064e15edb739f899a9cff9b67e70
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
type PersonEditor_actorsBook$ref = any;
type PersonEditor_person$ref = any;
export type PersonEditor_QueryVariables = {|
  id?: ?string
|};
export type PersonEditor_QueryResponse = {|
  +person: ?{|
    +$fragmentRefs: PersonEditor_person$ref
  |},
  +actorsBook: ?{|
    +$fragmentRefs: PersonEditor_actorsBook$ref
  |},
|};
export type PersonEditor_Query = {|
  variables: PersonEditor_QueryVariables,
  response: PersonEditor_QueryResponse,
|};
*/


/*
query PersonEditor_Query(
  $id: ID
) {
  person(id: $id) {
    ...PersonEditor_person
    id
  }
  actorsBook {
    ...PersonEditor_actorsBook
    id
  }
}

fragment PersonEditor_person on Person {
  id
  uri
  displayName
  avatar
  firstName
  lastName
  maidenName
  bio
  shortBio
  bday
  gender
}

fragment PersonEditor_actorsBook on ActorsBook {
  id
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "id",
    "type": "ID",
    "defaultValue": null
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "id",
    "variableName": "id",
    "type": "ID"
  }
],
v2 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "id",
  "args": null,
  "storageKey": null
};
return {
  "kind": "Request",
  "operationKind": "query",
  "name": "PersonEditor_Query",
  "id": null,
  "text": "query PersonEditor_Query(\n  $id: ID\n) {\n  person(id: $id) {\n    ...PersonEditor_person\n    id\n  }\n  actorsBook {\n    ...PersonEditor_actorsBook\n    id\n  }\n}\n\nfragment PersonEditor_person on Person {\n  id\n  uri\n  displayName\n  avatar\n  firstName\n  lastName\n  maidenName\n  bio\n  shortBio\n  bday\n  gender\n}\n\nfragment PersonEditor_actorsBook on ActorsBook {\n  id\n}\n",
  "metadata": {},
  "fragment": {
    "kind": "Fragment",
    "name": "PersonEditor_Query",
    "type": "Query",
    "metadata": null,
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "person",
        "storageKey": null,
        "args": v1,
        "concreteType": "Person",
        "plural": false,
        "selections": [
          {
            "kind": "FragmentSpread",
            "name": "PersonEditor_person",
            "args": null
          }
        ]
      },
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "actorsBook",
        "storageKey": null,
        "args": null,
        "concreteType": "ActorsBook",
        "plural": false,
        "selections": [
          {
            "kind": "FragmentSpread",
            "name": "PersonEditor_actorsBook",
            "args": null
          }
        ]
      }
    ]
  },
  "operation": {
    "kind": "Operation",
    "name": "PersonEditor_Query",
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "person",
        "storageKey": null,
        "args": v1,
        "concreteType": "Person",
        "plural": false,
        "selections": [
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "lastName",
            "args": null,
            "storageKey": null
          },
          v2,
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "displayName",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "avatar",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "firstName",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "uri",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "maidenName",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "bio",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "shortBio",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "bday",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "gender",
            "args": null,
            "storageKey": null
          }
        ]
      },
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "actorsBook",
        "storageKey": null,
        "args": null,
        "concreteType": "ActorsBook",
        "plural": false,
        "selections": [
          v2
        ]
      }
    ]
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = '5bfd1486ceb6b343ec4f469346a39b2e';
module.exports = node;
