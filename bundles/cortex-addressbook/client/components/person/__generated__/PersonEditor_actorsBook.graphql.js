/**
 * @flow
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteFragment } from 'relay-runtime';
import type { FragmentReference } from "relay-runtime";
declare export opaque type PersonEditor_actorsBook$ref: FragmentReference;
export type PersonEditor_actorsBook = {|
  +id: string,
  +$refType: PersonEditor_actorsBook$ref,
|};
*/


const node/*: ConcreteFragment*/ = {
  "kind": "Fragment",
  "name": "PersonEditor_actorsBook",
  "type": "ActorsBook",
  "metadata": null,
  "argumentDefinitions": [],
  "selections": [
    {
      "kind": "ScalarField",
      "alias": null,
      "name": "id",
      "args": null,
      "storageKey": null
    }
  ]
};
// prettier-ignore
(node/*: any*/).hash = '1ab0327d782ddab230ea4e4642e5880e';
module.exports = node;
