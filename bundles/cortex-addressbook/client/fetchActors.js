/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
import React from 'react';
import {graphql} from 'react-relay';
import {fetchQuery, getImageThumbnailURL} from '@mnemotix/cortex-core/client';
import _ from 'lodash';

let query = graphql`
  query fetchActors_Query($first: Int $qs: String $after: String $filterOn: ActorEnum) {
    actorsBook{
      actors(qs: $qs first: $first after: $after filterOn: $filterOn ) {
        edges{
          node{
            __typename
            id
            displayName
            avatar
          }
        }
      }
    }
  }
`;

/**
 * Fetch actors.
 *
 * @param {string} [qs] - Query string
 * @param {int} [first] - Response size
 * @param {string} [after] - Response offset
 * @param {string} [filterOn] - Filter on type (Person|Organisation)
 * @param {boolean} renderAsDropdownOptions - Render as dropdown option.
 */
export let fetchActors = async ({qs, first, after, filterOn, renderAsDropdownOptions}) => {
  if (!first){
    first = 10;
  }

  let rsp = await fetchQuery(query, {qs, first, filterOn});

  if(renderAsDropdownOptions){
    return _.get(rsp, 'actorsBook.actors.edges', []).map(({node: {id, displayName, avatar}}) => ({
      id,
      text: displayName,
      image: getImageThumbnailURL(avatar, 200)
    }));
  } else {
    return rsp;
  }
};