/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
const spawn = require('child_process').spawn;
const path = require('path');


const args = [
  '--extensions',
  'js',
  'jsx',
  '--schema',
  path.resolve(__dirname, '../../../application/server/schema/schema.graphql'), // or wherever you store your schema file
  '--src',
  path.resolve(__dirname, '../../../'), // since we are currently in <rootDir>/scripts
  '--include', // these will be based off of the `--src` flag ☝️
  '.yalc/@mnemotix/synaptix.js/src/client/**',
  'application/client/**',
  'bundles/**/client/**',
];

if (process.argv.includes('--watch')) {
  args.push('--watch');
}

if (process.argv.includes('--no-watchman')) {
  // use this flag if linking node modules
  args.push('--no-watchman');
}

const proc = spawn(
  path.resolve(__dirname, '../node_modules/.bin/relay-compiler'),
  args,
  {
    shell: true
  }
);

proc.stdout.pipe(process.stdout);

proc.on('close', code => {
  process.exit(code);
});