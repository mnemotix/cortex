/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import React from 'react';
import PropTypes from 'prop-types';
import cookie from 'cookie';
import { Progress, Confirm } from 'semantic-ui-react';
import classnames from 'classnames';
import { SemanticToastContainer, toast } from 'react-semantic-toasts';
import i18n from 'i18next';
import localforage from 'localforage';
import _ from 'lodash';

import style from './style.less';

import {ContextProvider} from './utilities/Context';
import RoutesDefinition from "./router/RoutesDefinition";
import getImageThumbnailURL from "./utilities/getImageThumbnailURL";

/**
 * @typedef {boolean} FORCE_LOGGED
 */

export default class Application extends React.Component {
  /** @param {RoutesDefinition} */
  routesDefinition = this.getRoutesDefinition();

  state = {
    context: {
      isLogged: this.isLogged.bind(this),
      getJSONSchema: this.getJSONSchema.bind(this),
      showLoadingBar: this.showLoadingBar.bind(this),
      hideLoadingBar: this.hideLoadingBar.bind(this),
      alert: this.alert.bind(this),
      alertMutationError: this.alertMutationError.bind(this),
      getLocale: this.getLocale.bind(this),
      changeLocale: this.changeLocale.bind(this),
      confirm: this.showConfirm.bind(this),
      getLocalStorage: this.getLocalStorage.bind(this),
      getImageThumbnailURL: this.getImageThumbnailURL.bind(this),
      getProxyURL: this.getProxyURL.bind(this),
      formatRoute: this.formatRoute.bind(this),
      getRoute: this.getRoute.bind(this),
      // showDialog: this.showDialog.bind(this),
      // closeDialog: this.closeDialog.bind(this),
    },
    loadingBarMessage: '',
    loadingBarValue: 0,
    locale: localStorage.getItem('lang') || "fr",
    confirms: []
  };

  render() {
    let {children} = this.props;

    return (
      <ContextProvider value={this.state.context}>
        <Progress active className={classnames(style.loadingBar, {[style.show]: this.state.loadingBarValue > 0})} percent={this.state.loadingBarValue} size='tiny'>
          {this.state.loadingBarMessage}
        </Progress>

        <SemanticToastContainer />
        {this.state.confirms.map(confirm => confirm)}
        {this.renderContent()}
      </ContextProvider>
    )
  }

  /**
   * Renders the application content
   * @return {string|[React.Component]|React.Component}
   */
  renderContent(){
    throw "You must implement this method to use this component.";
  }

  /**
   * Returns the GraphQL Schema (in the JSON format http://json-schema.org)
   * @return {object}
   */
  getJSONSchema(){
    throw "You must implement this method to get json schema.";
  }

  /**
   * @return {RoutesDefinition}
   */
  getRoutesDefinition(){
    throw "You must implement this method to get json schema.";
  }

  /**
   * Returns the pattern of a route.
   * @param {string} name Route name
   * @return {string}
   */
  getRoute(name){
    return this.routesDefinition.getRoute(name)
  }

  /**
   * Returns the URL of a route after processing params
   * @param {string} name Route name
   *  @param {object} params Route params
   * @return {string}
   */
  formatRoute(name, params){
    return this.routesDefinition.formatRoute(name, params)
  }

  isLogged(){
    let cookies = cookie.parse(document.cookie);

    if (typeof FORCE_LOGGED !== "undefined" && JSON.parse(FORCE_LOGGED) === true ) {
      return true;
    } else {
      return _.has(cookies, 'SNXID');
    }
  }

  getUid(){
    let cookies = cookie.parse(document.cookie);
    let jwt = JSON.parse(atob(_.get(cookies, 'SNXID')));
    return _.get(jwt, 'uid');
  }


  showLoadingBar(opts = {}, cb = () => {}){
    let {value, message} = opts;

    this.setState({
      loadingBarMessage: message || '',
      loadingBarValue: value || 0.5
    });

    if (!this.loadingBarTimer){
      this.loadingBarTimer = setTimeout(() => {
        this.showLoadingBar({...opts, value: value + 0.05})
      }, 500);
    }
  }

  hideLoadingBar(){
    if (this.loadingBarTimer){
      clearTimeout(this.loadingBarTimer);
    }

    this.setState({
      loadingBarMessage: '',
      loadingBarValue: 0
    });
  }

  alert(title, params = {}) {
    let {type, icon, description, time} = params;

    if (!type){
      type = "success";
    }

    if(!icon){
      switch (type) {
        case "success":
          icon = "checkmark";
          break;
        case "error":
          icon = "remove";
          break;
        case "warning":
          icon = "warning circle";
          break;
        default:
          icon = "announcement";
          break;
      }
    }

    toast({
      title: description ? title : "",
      description: description || title,
      type,
      icon,
      time
    });
  }

  alertMutationError(error){
    if(error.name === "RRNLRequestError"){
      error = error.message;
    } else {
      error = error.map(({message}) => message).join("\n");
    }

    this.alert(error, {type: "error"});
    console.error(error);
  }

  showConfirm({onConfirm, onCancel, header, content}){
    let {confirms} = this.state;
    let index = confirms.length;

    let handleConfirm = () => {
      this.closeConfirm(index);

      if(onConfirm) {
        onConfirm();
      }
    };

    let handleCancel = () => {
      this.closeConfirm(index);

      if(onCancel){
        onCancel();
      }
    };

    confirms.push(
      <Confirm key={index} open={true} onConfirm={handleConfirm} onCancel={handleCancel} header={header} content={content}/>
    );

    this.setState({confirms});
  }

  closeConfirm(index){
    let {confirms} = this.state;
    confirms.splice(index, 1);
    this.setState({confirms});
  }

  getLocale(){
    return this.state.locale;
  }

  changeLocale(locale){
    i18n.changeLanguage(locale, (err) => {
      if (err){
        console.error(err);
      }
      this.setState({
        locale
      });
      localStorage.setItem('lang', locale);
    });
  }

  getLocalStorage(prefix = "default"){
    return localforage.createInstance({
      name: `${prefix}-${this.getUid()}`
    });
  }

  /**
   * Return a image thumbnail URL
   * @param url
   * @param width
   * @param height
   * @return {string}
   */
  getImageThumbnailURL(url, width = 512, height = 0){
    return getImageThumbnailURL(url, width, height);
  }

  /**
   * Return a proxified URL
   *
   * @param {string} url
   * @param {string} [mime]
   */
  getProxyURL(url, mime){
    return `/proxify?url=${encodeURIComponent(url)}${mime ? `&mime=${mime}`: ''}`;
  }
}