/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom'
import cookie from 'cookie';
import io from 'socket.io-client';

import { I18nextProvider } from 'react-i18next';
import {i18nInitiaze} from "../utilities/i18n/i18n";

/**
 * @param application
 * @param entrypointId
 */
export const generateApplication = ({application, entrypointId}) => {
  ReactDOM.render(
    <I18nextProvider i18n={ i18nInitiaze() }>
      <BrowserRouter>
        {application}
      </BrowserRouter>
    </I18nextProvider>,
    document.getElementById(entrypointId || 'content'),
  );

  // Test connection.
  setInterval(async () => {
    let cookies = cookie.parse(document.cookie);

    if(cookies.SNXID){
      try {
        await fetch('/heartbeat', {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
          },
          credentials: 'include',
        });
      } catch (e){
        location.reload();
      }
    }
  }, 15000);

  let socket = io.connect(`${location.protocol}//${location.host}`);

  socket.on('disconnected',  ({uid}) => {
    let cookies = cookie.parse(document.cookie);

    if(cookies.SNXID === uid){
      location.reload();
    }
  });
};