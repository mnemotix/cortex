/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
import React from 'react';
import {graphql, createFragmentContainer} from 'react-relay';
import PropTypes from 'prop-types';
import { Dropdown, Image, Icon, Item } from 'semantic-ui-react';
import withRelay from "../../../relay/withRelay";
import classnames from 'classnames';

import style from './style.less';

class AvatarDropdown extends React.Component {
  static propTypes = {
    trigger: PropTypes.any,
    avatar: PropTypes.string,
    name: PropTypes.string,
    dropdownItems: PropTypes.any,
    dropdownProps: PropTypes.object,
    className: PropTypes.string
  };

  static defaultProps = {
    hideLoading: true
  };

  render() {
    const {className, dropdownProps, dropdownItems, me} = this.props;
    return (
      <Dropdown className={classnames(className, {placeholder: !me})} trigger={this.getTrigger()} pointing icon={null} {...dropdownProps}>
        <Dropdown.Menu>
          {dropdownItems}
        </Dropdown.Menu>
      </Dropdown>
    )
  }

  getTrigger(){
    const {trigger, me} = this.props;

    return trigger || (
        <Item.Content verticalAlign='middle'>
          <span className={`${style.name}`}>
            {me ? me.firstName : null}
          </span>

          {me && me.avatar ? (
            <Image avatar src={me ? me.avatar : null} />
          ) : (
            <Icon name={'user circle'} size='big' />
          )}

          <Icon name={'caret down'} />
        </Item.Content>
    );
  }
}

export default withRelay(
  graphql`
    query AvatarDropdown_Query{
      me{
        ...AvatarDropdown_me
      }
    }
  `,
  {},
  createFragmentContainer(
    AvatarDropdown,
    graphql`
      fragment AvatarDropdown_me on Person {
        avatar
        firstName
      }
    `
  ),
  true
);