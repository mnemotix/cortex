/**
 * @flow
 * @relayHash 02e1dc4c0db68e8f3e7af2b4dd94b41d
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
type AvatarDropdown_me$ref = any;
export type AvatarDropdown_QueryVariables = {||};
export type AvatarDropdown_QueryResponse = {|
  +me: ?{|
    +$fragmentRefs: AvatarDropdown_me$ref
  |}
|};
export type AvatarDropdown_Query = {|
  variables: AvatarDropdown_QueryVariables,
  response: AvatarDropdown_QueryResponse,
|};
*/


/*
query AvatarDropdown_Query {
  me {
    ...AvatarDropdown_me
    id
  }
}

fragment AvatarDropdown_me on Person {
  avatar
  firstName
}
*/

const node/*: ConcreteRequest*/ = {
  "kind": "Request",
  "operationKind": "query",
  "name": "AvatarDropdown_Query",
  "id": null,
  "text": "query AvatarDropdown_Query {\n  me {\n    ...AvatarDropdown_me\n    id\n  }\n}\n\nfragment AvatarDropdown_me on Person {\n  avatar\n  firstName\n}\n",
  "metadata": {},
  "fragment": {
    "kind": "Fragment",
    "name": "AvatarDropdown_Query",
    "type": "Query",
    "metadata": null,
    "argumentDefinitions": [],
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "me",
        "storageKey": null,
        "args": null,
        "concreteType": "Person",
        "plural": false,
        "selections": [
          {
            "kind": "FragmentSpread",
            "name": "AvatarDropdown_me",
            "args": null
          }
        ]
      }
    ]
  },
  "operation": {
    "kind": "Operation",
    "name": "AvatarDropdown_Query",
    "argumentDefinitions": [],
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "me",
        "storageKey": null,
        "args": null,
        "concreteType": "Person",
        "plural": false,
        "selections": [
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "avatar",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "firstName",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "id",
            "args": null,
            "storageKey": null
          }
        ]
      }
    ]
  }
};
// prettier-ignore
(node/*: any*/).hash = '2b1fee08f09115d70703bd1c09ad5743';
module.exports = node;
