/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
import React from 'react';
import PropTypes from 'prop-types';
import { Dropdown, Icon, Item } from 'semantic-ui-react';

import style from './style.less';
import withContext from "../../../utilities/withContext";

@withContext
export default class LanguageDropdown extends React.Component {
  static propTypes = {
    trigger: PropTypes.any,
    dropdownProps: PropTypes.object,
    className: PropTypes.string,
    langs: PropTypes.array,
    onChange: PropTypes.func
  };

  static defaultProps = {
    langs: [{
      key: "fr",
      text: "Français",
    },{
      key: "en",
      text: "English",
    }]
  };

  render() {
    const {className, dropdownProps, langs} = this.props;
    return (
      <Dropdown inline className={className} trigger={this.getTrigger()} pointing icon={null} {...dropdownProps}>
        <Dropdown.Menu>
          {langs.map(({key, text}) => (
            <Item key={key} onClick={() => this.onChange(key)}>
              {text}
            </Item>
          ))}
        </Dropdown.Menu>
      </Dropdown>
    )
  }

  getTrigger(){
    const {trigger, me} = this.props;

    return trigger || (
      <Item.Content verticalAlign='middle'>
        <span className={`${style.name}`}>
          {this.props.context.getLocale().toUpperCase()}
        </span>

        <Icon name={'caret down'} />
      </Item.Content>
    );
  }

  onChange(locale){
    this.props.context.changeLocale(locale);
  }
}