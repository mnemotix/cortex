/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

/**
 * This file is part of the cortex package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 22/10/2018
 */

import React from 'react';
import PropTypes from 'prop-types';
import {Button} from 'semantic-ui-react';
import {withContext, withEnvironment} from "../../../index";
import {DefaultMutations as Mutations} from "@mnemotix/synaptix.js/lib/client";

@withContext
@withEnvironment
export default class RemoveObjectButton extends React.Component {
  static propTypes = {
    confirmMessage: PropTypes.string.isRequired,
    objectRecord: PropTypes.object.isRequired,
    connectionKeys: PropTypes.array,
    connectionKey: PropTypes.string,
    updateCounters: PropTypes.array,
    parentRecord: PropTypes.object,
    removeButtonLabel: PropTypes.string,
    onCompleted: PropTypes.func,
    onError: PropTypes.func,
    onAfter: PropTypes.func,
    buttonProps: PropTypes.object,
    successMessage: PropTypes.string
  };

  state = {
    loading: false
  };
  
  render() {
    const {t, removeLabel, buttonProps} = this.props;

    return (
      <Button {...buttonProps} loading={this.state.loading} onClick={this._handleRemoveObject}>
        {removeLabel || t("Remove")}
      </Button>
    )
  }

  _handleRemoveObject = (e) => {
    const {t, confirmMessage} = this.props;

    e.preventDefault();

    this.props.context.confirm({
      onConfirm: () => {
        this.removeObject();
      },
      content: confirmMessage
    })
  };

  removeObject(){
    const {environment, objectRecord, connectionKey, connectionKeys, parentRecord,
      onCompleted, onError, onAfter, successMessage, updateCounters} = this.props;

    this.setState({
      loading: true
    });

    (new Mutations.RemoveObjectMutation({
      environment,
      onCompleted: () => {
        if (successMessage) {
          this.props.context.alert(successMessage);
        }

        if (onCompleted){
          onCompleted();
        }
      },
      onError: onError || this.props.context.alertMutationError,
      onAfter: () => {
        this.setState({
          loading: false
        });

        if (onAfter){
          onAfter();
        }
      }
    })).apply({
      objectId: objectRecord.id,
      connectionKey: connectionKey,
      connectionKeys: connectionKeys,
      parentRecord: parentRecord,
      updateCounters
    });
  }
}