/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

export {Link} from "react-router-dom";
export {default as formatRoute} from "./router/formatRoute";
export {default as prepareRoute} from "./router/prepareRoute";
export {default as Application}  from "./Application";
export {default as withContext}  from "./utilities/withContext";
export {default as withRelay}  from "./relay/withRelay";
export {default as withEnvironment}  from "./relay/withEnvironment";
export {default as environment}  from "./relay/environment";
export {default as Route} from "./router/Route";
export {default as RoutesDefinition, generateRoutesDefinition} from "./router/RoutesDefinition";
export {default as getImageThumbnailURL} from "./utilities/getImageThumbnailURL";
export {fetchQuery} from "./relay/fetchQuery";
import AvatarDropdown from "./components/modules/AvatarDropdown/AvatarDropdown";
import LanguageDropdown from "./components/modules/LanguageDropdown/LanguageDropdown";
import RemoveObjectButton from "./components/modules/RemoveObjectButton/RemoveObjectButton";


export const Modules = {
  AvatarDropdown,
  LanguageDropdown,
  RemoveObjectButton
};