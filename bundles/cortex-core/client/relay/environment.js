/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {
  Environment,
  Network,
  RecordSource,
  Store,
} from 'relay-runtime';
import { RelayNetworkLayer, cacheMiddleware } from 'react-relay-network-modern';
import React from "react";

export class DisconnectedException extends Error { }

let environment = new Environment({
  network: new RelayNetworkLayer(
    [
      cacheMiddleware({
        size: 100,   // max 100 requests
        ttl: 120000, // 2 minutes,
        clearOnMutation: true
      }),
      next => async req => {
        req.fetchOpts.credentials = 'include';

        const res = await next(req);

        if (res.status === 401){
          throw new DisconnectedException();
        }

        return res;
      }
    ],
    {
      // subscribeFn : (...args) => subscriber.fetch(...args)
    }
  ),
  store: new Store(new RecordSource())
});

export default environment;