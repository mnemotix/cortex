/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import React from "react";
import environment from "./environment";
import PropTypes from "prop-types";
import {QueryRenderer} from "react-relay";
import {Segment, Dimmer, Loader} from 'semantic-ui-react';
import {translate} from "react-i18next";
import {ErrorBoundary} from "../components/common/ErrorBoundary/ErrorBoundary";

/**
 * High Order Component for embeding a Relay environment in a component
 *
 * @return {WithRelay}
 */
export default function withEnvironment(Component) {
  class WithEnvironment extends React.Component{
    render(){
      return (
        <Component {...this.props} environment={environment} />
      );
    }
  }

  WithEnvironment.displayName = `WithEnvironment(${Component.displayName || Component.name || 'Component'})`;

  return WithEnvironment;
}