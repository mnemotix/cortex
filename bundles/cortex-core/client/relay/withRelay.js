/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import React from "react";
import environment from "./environment";
import PropTypes from "prop-types";
import {QueryRenderer} from "react-relay";
import {Segment, Dimmer, Loader} from 'semantic-ui-react';
import {translate} from "react-i18next";
import {ErrorBoundary} from "../components/common/ErrorBoundary/ErrorBoundary";
import withContext from "../utilities/withContext";
import _ from 'lodash';

/**
 * High Order Component for embeding a Relay container (fragment|refetch|pagination) into a QueryRenderer component
 *
 * @param {graphql} query
 * @param {object|function} variables - Variables to pass to Relay container
 * @param {object} RelayContainer - Relay container definition.
 * @param {boolean} [hideLoading] - Hide loading message
 * @param {React.Component} [loadingElement] - Replace default loading message with a custom element
 * @return {WithRelay}
 */
export default function withRelay(query, variables, RelayContainer, hideLoading, loadingElement) {
  @translate(['labels'])
  @withContext
  class WithRelay extends React.Component{
    static propTypes = {
      hideLoading: PropTypes.bool,
      lazyLoading: PropTypes.bool,
      variables: PropTypes.object,
      renderError: PropTypes.func,
      renderLoading: PropTypes.func,
    };

    render(){
      if (typeof variables === "function") {
        variables = variables(this.props);
      }

      return (
        <QueryRenderer
          environment={environment}
          query={query}
          variables={{
            ...variables,
            ...this.props.variables
          }}
          render={({error, props}) => {
            if (error) {
              return this.props.renderError ? this.props.renderError(error.message) : <div>{error.message}</div>;
            } else if (props || this.props.lazyLoading) {
              return (
                <ErrorBoundary>
                  <RelayContainer {...this.props} {...props} />
                </ErrorBoundary>
              );
            }

            return this.renderLoading();
          }}
        />
      );
    }

    renderLoading(){
      const {t, renderLoading} = this.props;

      if (renderLoading){
        return renderLoading();
      }

      return hideLoading || this.props.hideLoading ? null : loadingElement || (
        <Dimmer.Dimmable as={Segment} placeholder dimmed={true}>
          <Dimmer active inverted>
            <Loader size='medium'>{t("Loading...")}</Loader>
          </Dimmer>
        </Dimmer.Dimmable>
      );
    }
  }

  WithRelay.displayName = `WithRelay(${RelayContainer.displayName || RelayContainer.name || 'Component'})`;

  return WithRelay;
}