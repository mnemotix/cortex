/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import {Route} from "../index";
import {formatRoute} from 'react-router-named-routes';

/**
 * A class to define a bunch of routes
 */
export default class RoutesDefinition {
  _routeDefinitions = {};

  /**
   * @param {Route[]} routes
   * @param {string} [basePath=''] Base path of all routes
   */
  constructor(routes, basePath = ''){
    this.assignRoutes(routes, basePath);
  }

  /**
   * @param {Route[]} routes
   * @param {string} [basePath=''] Base path of all routes
   */
  assignRoutes(routes, basePath = ''){
    routes.map((route) => this.addRouteDefinition(route, basePath));
  }

  /**
   * @param {Route} route
   * @param {string} [basePath=''] Base path of all routes
   */
  addRouteDefinition(route, basePath = ''){
    this._routeDefinitions[route.name] = {
      path: route.path,
      basePath
    };
  }

  /**
   * Returns the pattern of a route.
   * @param {string} name Route name
   * @return {string}
   */
  getRoute(name){
    let routeDefinition = this._routeDefinitions[name];

    if (routeDefinition){
      return routeDefinition.basePath + routeDefinition.path;
    } else {
      throw `Route "${name}" is not defined in the RoutesDefinition`;
    }
  }

  /**
   * Returns the URL of a route after processing params
   * @param {string} name Route name
   * @param {object} params Route params
   * @return {string}
   */
  formatRoute(name, params){
    let routeDefinition = this._routeDefinitions[name];

    if (routeDefinition){
      return formatRoute(routeDefinition.basePath + routeDefinition.path, params);
    } else {
      throw `Route name ${name} is not defined in the RoutesDefinition`;
    }
  }
}

/**
 * @typedef {object} routeDefinitonNotation
 * @property {Route[]} routes List of routes
 * @property {string} basePath Base path of all routes
 */
/**
 * @param {routeDefinitonNotation[]} notations
 */
export let generateRoutesDefinition = (notations) => {
  let routesDefinitions;

  notations.map(notation => {
    if (!routesDefinitions){
      routesDefinitions = new RoutesDefinition(notation.routes, notation.basePath);
    } else {
      routesDefinitions.assignRoutes(notation.routes, notation.basePath);
    }
  });

  return routesDefinitions;
};