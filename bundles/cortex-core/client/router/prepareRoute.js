/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

/**
 * Prepare a route including base pathname (or not)
 *
 * @param match
 * @param routePath
 * @param absolute
 * @return {*}
 */
export default function prepareRoute(match, routePath, absolute){
  if (!match || absolute) {
    return routePath;
  } else {
    let routeFragments = routePath.split('/').filter(routeFragment => routeFragment !== "");
    let matchAt = routeFragments.length > 0 ? match.url.indexOf(routeFragments[0]) : -1;

    if (matchAt > 0){
      return match.url.slice(0, matchAt) + routePath;
    } else {
      return match.url + routePath;
    }
  }
}