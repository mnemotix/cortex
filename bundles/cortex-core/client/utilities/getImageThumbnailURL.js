/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

/**
 * Return a image thumbnail URL
 * @param url
 * @param width
 * @param height
 * @return {string}
 */
export default function getImageThumbnailURL(url, width = 512, height = 0){
  if (window.location.hostname === "localhost"){
    url = (url || '')
      .replace('localhost:8082', 'local-resource-cloud')
      .replace('localhost', 'host.docker.internal')
      .replace(/https?:\/\//, '')
    ;
  }

  return `/thumbnail/${width}x${height}/${url}`;
}