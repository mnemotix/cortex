/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import chalk  from 'chalk';
import figlet from 'figlet';
import express from 'express';
import webpack from 'webpack';
import cookie from 'cookie';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import cors from 'cors';
import http from 'http';
import {logDebug, generateSSOEndpoint, generateGraphQLEndpoint, logInfo, logError, NetworkLayerAMQP, ModelDefinitionsRegister, GraphQLContext} from '@mnemotix/synaptix.js';
import ObjectPath from 'object-path';
import SynaptixAdapter from "./datastores/synaptix/SynaptixAdapter";
import EnvironmentBuilder from "../utilities/config/EnvironmentBuilder";
import {gatherBundlesEnvironmentVariables} from "../utilities/config/helpers";
import {Server} from 'http';
/**
 * @typedef  {object} ExpressApp - The main express application.
 * @property {NetworkLayerAbstract} networkLayer - The network layer used to communicate with Synaptix bus.
 * @property {SynaptixDatastoreAdapter} datastoreAdapter - The datastore adapter
 * @property {Server} server - The HTTP server linked to app
 * @property {function} use - Method to apply middleware
 * @property {function} get - Method to apply a route on GET method
 * @property {function} post -  Method to apply a route on POST method
 * @property {function} delete -  Method to apply a route on DELETE method
 * @property {function} update -  Method to apply a route on UPDATE method
 */

/**
 * @typedef {object} GraphQLEndpointDefinition
 * @property {}
 */

/**
 * Launch an application.
 *
 * @param {object} Package - the npm package.json object
 * @param {object} webpackDevConfig - The webpack config for developpement
 * @param {array} bundles - The list of bundles path (require/import path)
 * @param {NetworkLayerAbstract} networkLayer - The network layer used to comunicate this Synaptix middleware bus
 *
 * @param {GraphQLSchema} [graphQLSchema] - An executable GraphQL Schema (Optional if graphQLEndpoints defined)
 * @param {ModelDefinitionsRegister} [modelDefinitionsRegister] A register containing all model definitions (Optional if graphQLEndpoints defined)
 *
 * @param {[GraphQLEndpointDefinition]} graphQLEndpoints - A list of GraphQL endpoints definitions (Optional if both graphQLSchema and modelDefinitionsRegister defined)
 */
export const launchApplication = async ({Package, webpackDevConfig, graphQLSchema, modelDefinitionsRegister, graphQLEndpoints, bundles, networkLayer}) => {
  if (!graphQLEndpoints && !graphQLSchema && !modelDefinitionsRegister){
    throw "You must provide either a (graphQLSchema, modelDefinitionsRegister) or customized graphQLEndpoints";
  }

  console.info(`${chalk.yellow(figlet.textSync(Package.name, {
    font: 'Ghost',
    horizontalLayout: 'default',
    verticalLayout: 'default'
  }))}

Version ${chalk.yellow(Package.version)}
Coded with ${chalk.red('♥')} by ${chalk.underline.yellow('Mnemotix')}

  `);

  EnvironmentBuilder.initVariables(await gatherBundlesEnvironmentVariables(bundles));

  /** @type {ExpressApp} */
  const app = express();

  /** @type {object} */
  const entrypoint = http.createServer(app);

  app.networkLayer = networkLayer;
  app.server = entrypoint;
  await networkLayer.connect();

  app.use(cookieParser());

  /** @type {object} */
  const io = require('socket.io')(entrypoint);

  io.on('connection', function (socket) {
    let cookies = cookie.parse(ObjectPath.get(socket, 'request.headers.cookie', ''));

    if (cookies && cookies.SNXID) {
      /** @namespace cookies.UID */
      socket.request.user = cookies.SNXID;
    }
  });


  app.use(cors());
  app.use(cookieParser());
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({extended: true}));

  let middlewareDefs = await Promise.all(bundles.map(async (bundle) => {
    try{
      return (await import(`${bundle}/server/middlewares/index.js`));
    } catch (e) {}
  }));

  middlewareDefs.map((middlewareDef) => {
    if (middlewareDef){
      for(let middlewareName in middlewareDef){
        middlewareDef[middlewareName](app);
        logInfo(`${chalk.yellow(middlewareName)} server middleware loaded.`);
      }
    }
  });

  let openIdEndpointURL = `${process.env.OAUTH_REDIRECT_URL}/auth/realms/${process.env.OAUTH_REALM}/protocol/openid-connect`;

  /**
   * @type {Authenticator} passport
   */
  let {authenticationMiddleware} = generateSSOEndpoint(app, {
    authorizationURL: `${openIdEndpointURL}/auth`,
    tokenURL: `${openIdEndpointURL}/token`,
    logoutURL: `${openIdEndpointURL}/logout`,
    clientID: process.env.OAUTH_REALM_CLIENT_ID,
    clientSecret: process.env.OAUTH_REALM_CLIENT_SECRET,
    baseURL: process.env.APP_URL,
    adminUsername: process.env.OAUTH_ADMIN_USERNAME,
    adminPassword:  process.env.OAUTH_ADMIN_PASSWORD,
    registrationEnabled: true,
    registerCallback: async (userAccount, req) => {
      let {username: email, firstName, lastName, password} = req.body;

      userAccount.userId = userAccount.id;
      delete userAccount.id;

      return app.datastoreAdapter
        .getSession(new GraphQLContext({anonymous: true}))
        .createUser({email, firstName, lastName, userAccount});
    }
  });

  if (process.env.NODE_ENV !== 'test') {
    app.use('/graphql', authenticationMiddleware);
    app.use('/heartbeat', authenticationMiddleware);
  }

  /**
   * Setup handler to get heartbeat.
   */
  app.get('/heartbeat', (req, res) => {
    return res.status(200).send("︎💔🎈🏓🎉🕶");
  });

  /**
   * Setup handler to get locales.
   */
  app.get('/locales/:lang', async (req, res) => {
    let lang = req.params.lang;
    let locales = {};

    for(let bundle of bundles){
      try{
        let fragment = (await import(`${bundle}/locales/${lang}/labels.json`)).default;
        locales = Object.assign(locales, fragment)
      } catch (e) {}
    }

    return res.json(locales);
  });

  /**
   * Setup GraphQL endpoints.
   */
  if (!graphQLEndpoints) {
    const amqpURL = `amqp://${process.env.RABBITMQ_LOGIN}:${process.env.RABBITMQ_PASSWORD}@${process.env.RABBITMQ_HOST}:${process.env.RABBITMQ_PORT}`;
    const amqpLayer = new NetworkLayerAMQP(amqpURL, process.env.RABBITMQ_EXCHANGE_NAME);
    await amqpLayer.connect();
    const datastoreAdapter = new SynaptixAdapter(modelDefinitionsRegister, amqpLayer);
    app.datastoreAdapter = datastoreAdapter;

    graphQLEndpoints = [
      {
        endpointURI: '/graphql',
        graphQLSchema,
        datastoreAdapter
      }
    ];
  }

  for (let {endpointURI, graphQLSchema, datastoreAdapter} of graphQLEndpoints ) {
    generateGraphQLEndpoint(app, {
      schema: graphQLSchema,
      endpointURI,
      datastoreAdapter
    });

    if (!app.datastoreAdapter){
      app.datastoreAdapter = datastoreAdapter;
    }
  }

  if (process.env.NODE_ENV !== 'production') {
    const webpackMiddleware = require('webpack-dev-middleware');
    const webpackHotMiddleware = require('webpack-hot-middleware');

    const compiler = webpack(webpackDevConfig);
    const middleware = webpackMiddleware(compiler, {
      publicPath: webpackDevConfig.output.publicPath,
      contentBase: 'src',
      noInfo: true,
      stats: {
        colors: true,
        hash: false,
        timings: true,
        chunks: false,
        chunkModules: false,
        modules: false
      },
      watchOptions: {
        poll: 1000
      }
    });

    app.use(middleware);
    app.use(webpackHotMiddleware(compiler));
    app.get('*', (req, res) =>  {
      res.write(middleware.fileSystem.readFileSync("/dist/index.html"));
      res.end();
    });
  } else {
    app.use(express.static('/dist'));
    app.get('*', (req, res) => {
      res.sendFile("/dist/index.html");
    });
    app.get('*', (req, res) => {
      res.sendFile("/dist/index.html");
    });
  }

  try {
    for (let {datastoreAdapter} of graphQLEndpoints ) {
      await datastoreAdapter.init({});
    }

    entrypoint.listen(process.env.APP_PORT, '0.0.0.0', (err) => {
      if (err) {
        logError(err);
      }

      logInfo(`ExpressJS server started and listening on port ${chalk.red.bold(process.env.APP_PORT)}...`);
    });
  } catch (err) {
    logError(err);
    exit(1);
  }
};

