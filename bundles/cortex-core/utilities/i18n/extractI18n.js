#!/usr/bin/env node
/**
 * This file is part of the weever package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 10/01/2017
 */

let fs = require('fs');
let args = require('minimist')(process.argv.slice(2));
let Parser = require('i18next-scanner').Parser;
let converter = require('i18next-conv');
let glob = require('glob');
let debug = require('debug')('i18next-extract');

let parserOptions = {
  // Include react helpers into parsing
  attr: {
    list: ['data-i18n', 'i18nKey']
  },
  func: {
    list: ['i18next.t', 'i18n.t', 't']
  },
  // Make sure common separators don't break the string
  keySeparator: '>',
  nsSeparator: ':',
  pluralSeparator: '_',
  contextSeparator: '_',
  // Interpolate correctly
  interpolation: {
    prefix: '{{',
    suffix: '}}'
  }
};

let parser = new Parser(parserOptions);

let fileGlob = args.files;
let langs = args.langs.split(',');

if (!fileGlob || !langs) {
  console.log('Missing "files" glob or "langs" settingd');
  process.exit(1)
}

debug('Reading in files for glob: ' + fileGlob);
glob(fileGlob, function (err, files) {
  if (err) {
    console.log(err);
    process.exit(1)
  }

  debug('Loading content of ' + files.length + ' files');
  let content = '';
  files.map(function (file) {
    content += fs.readFileSync(file, 'utf-8')
  });

  debug('Parsing translation keys out of content');
  parser.parseFuncFromString(content, parserOptions);
  parser.parseAttrFromString(content, parserOptions);
  let json = parser.get().en.translation;
  let labels = json;

  langs.forEach(lang => {
    new Promise((done, err) => {
      fs.readFile(`${__dirname}/${lang}/labels.json`, 'utf8', function (err,data) {
        if (err) {
          return console.log(err);
        }

        let locLabels = Object.assign({}, labels, JSON.parse(data));
        let orderedKeys = Object.keys(locLabels).sort(),
          orderedLabels = {};

        orderedKeys.forEach(key => orderedLabels[key] = locLabels[key]);

        fs.writeFile(`${__dirname}/${lang}/labels.json`, JSON.stringify(orderedLabels, null, ' '), done);
      });
    }).then(() => {
      console.log(`OK, ${Object.keys(labels).length} keys written`);
    });
  });
});
