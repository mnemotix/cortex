/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import i18n from 'i18next';
import XHR from 'i18next-xhr-backend';
import 'moment/locale/fr';
import 'moment/locale/en-gb';

import moment from 'moment';

moment.locale('fr');

export const i18nInitiaze = (resources) => {
  i18n
    .use(XHR)
    .init({
      backend: {
        loadPath: '{{lng}}',
        parse: (data) => data,
        ajax: async (lng, options, callback) => {
          try {
            let response = await fetch(`/locales/${lng}`);
            let locale   = await response.json();
            callback(locale, {status: '200'});
          } catch (e) {
            callback(null, {status: '404'});
          }
        }
      },
      fallbackLng: localStorage.getItem('lang') || 'fr',
      // have a common namespace used around the full app
      ns: ['labels'],
      defaultNS: 'labels',
      debug: false,
      interpolation: {
        escapeValue: false // not needed for react!!
      },
      keySeparator: '->',
      resources
    }, (err, t) => {});

  return i18n;
};

export const addI18nResources = (lng, resources) => {
  i18n.addResources(lng, 'labels', resources);
};