/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
import {generateSchema, mergeResolvers, ModelDefinitionsRegister, ModelDefinitionAbstract} from '@mnemotix/synaptix.js';

export default class SchemaBuilder {
  /**
   * @param {array} typeDefs
   * @param {array} resolvers
   * @param {ModelDefinitionAbstract[]} modelDefinitions
   */
  constructor({typeDefs, resolvers, modelDefinitions}){
    this.typeDefs = typeDefs || [];
    this.resolvers = resolvers || [];
    this.modelDefinitions = modelDefinitions || [];
  }

  /**
   * @param {array} typeDefs
   */
  addTypeDefs(typeDefs){
     this.typeDefs = [].concat(this.typeDefs, typeDefs);
  }

  /**
   * @param {array} resolvers
   */
  addResolvers(resolvers){
    this.resolvers = [].concat(this.resolvers, resolvers);
  }

  /**
   * @param {ModelDefinitionAbstract[]} modelDefinitions
   */
  addModelDefinitions(modelDefinitions){
    this.modelDefinitions = [].concat(this.modelDefinitions, modelDefinitions);
  }

  /**
   * @return {array} modelDefinitions
   */
  getTypeDefs() {
    return this.typeDefs;
  }

  /**
   * @return {array} modelDefinitions
   */
  getResolvers() {
    return this.resolvers;
  }

  /**
   * @return {ModelDefinitionAbstract[]} modelDefinitions
   */
  getModelDefinitions() {
    return this.modelDefinitions;
  }

  /**
   * Generate a graphQL executable schema.
   */
  generateExecutableSchema(options){
    return generateSchema({
      typeDefs: this.typeDefs,
      resolvers: mergeResolvers(...this.resolvers),
      ...options
    })
  }

  /**
   * Generate model definitions register
   * @return {ModelDefinitionsRegister}
   */
  generateModelDefinitionsRegister(){
    return new ModelDefinitionsRegister(this.modelDefinitions)
  }

  /**
   * @param {SchemaBuilder[]} schemaBuilders
   */
  mergeWithSchemaBuilders(schemaBuilders){
    for(let schemaBuilder of schemaBuilders) {
      this.addTypeDefs(schemaBuilder.getTypeDefs());
      this.addResolvers(schemaBuilder.getResolvers());
      this.addModelDefinitions(schemaBuilder.getModelDefinitions());
    }
  }
}