# Cortex Finder


This is a widget bundle to manipulate data from the Resource ontology. 

Basically it provides:
 
 - A finder to display a searchable list of resources.
 - UIs to update resources
 
 
## Related bundles

- @mnemotix/cortex-core
- @mnemotix/cortex-form
- @mnemotix/cortex-uploader

