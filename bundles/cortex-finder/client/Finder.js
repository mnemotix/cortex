/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
import React from 'react';
import PropTypes from 'prop-types';
import {Route, matchPath, withRouter, Redirect, Switch} from 'react-router';
import {graphql, createRefetchContainer} from 'react-relay';
import {
  Button,
  List,
  Image,
  Icon,
  Modal,
  Input,
  Segment,
  Grid,
  Table,
  Header,
  Card,
  Visibility,
  Placeholder,
  Checkbox,
  Container
} from 'semantic-ui-react';
import moment from 'moment';
import numberFormat from 'human-format';
import {
  formatMime,
  isImageMime,
} from "./helpers/formats";
import ResourceEditor from "./components/resource/ResourceEditor";
import getMimeIcon from "./helpers/getMimeIcon";
import style from './Finder.less';
import {withRelay, Link, withContext} from '@mnemotix/cortex-core/client';
import {FileUploader} from '@mnemotix/cortex-uploader/client'
import {DefaultMutations} from '@mnemotix/synaptix.js/lib/client'

@withContext
@withRouter
class Finder extends React.Component {
  static propTypes = {
    customRenderTableView: PropTypes.func,
    customRenderListView: PropTypes.func,
    customRenderTileView: PropTypes.func,
    selectable: PropTypes.bool,
    multiple: PropTypes.bool,
    onSelectedResources: PropTypes.func
  };

  static defaultProps = {
    onSelectedResources: () => {}
  };

  state = {
    loading: false,
    loadingMore: false,
    loadingCreation: false,
    qs: '',
    viewType: 'list',
    sortColumn: null,
    sortDirection: null,
    uploadedFiles: [],
    fileUploadModalActive: false,
    selectedResources: [],
    selectAllResources: false
  };

  async componentDidMount() {
    let viewType = await this.props.context.getLocalStorage().getItem("cortex-finder__view-type");

    if (viewType && viewType !== this.state.viewType) {
      this.setState({
        viewType
      });
    }
  }

  render() {
    const {t, match, location, history, person, selectable} = this.props;

    return !selectable && match.isExact ? (
      <Redirect to={this.props.context.formatRoute('Finder_resources')}/>
    ) : (
      <>
        <Grid>
          <Grid.Column width={8}>
            <Input fluid
                   loading={this.state.loading}
                   placeholder={t('Search...')}
                   value={this.state.qs}
                   icon='search'
                   onChange={(e, {value}) => this.search(value)}
            />
          </Grid.Column>
          <Grid.Column width={4} textAlign={'center'}>
            <Button.Group basic>
              {selectable ? null : (
                <Button active={this.state.viewType === "list"} icon={"list"}
                        onClick={this._changeView.bind(this, 'list')}/>
              )}
              <Button active={this.state.viewType === "table"} icon={"table"}
                      onClick={this._changeView.bind(this, 'table')}/>
              <Button active={this.state.viewType === "tile"} icon={"th"}
                      onClick={this._changeView.bind(this, 'tile')}/>
            </Button.Group>
          </Grid.Column>
          <Grid.Column width={4} textAlign={'right'}>
            <Button primary
                    icon={'cloud upload'}
                    content={t("Upload resource")}
                    {...(selectable ? {
                      onClick: () => this.setState({fileUploadModalActive: true})
                    } : {
                      as: Link,
                      to: this.props.context.formatRoute('Finder_resourceUpload')
                    })}
            />
          </Grid.Column>
        </Grid>

        {this.renderView()}

        {this.state.fileUploadModalActive ? this.renderFileUploadModal() : null}

        <Switch>
          <Route path={this.props.context.getRoute('Finder_resourceUpload')} render={() => this.renderFileUploadModal()}/>
        </Switch>
      </>
    );
  }

  renderFileUploadModal(){
    const {history, selectable, match, person, t} = this.props;

    return (
      <Modal closeIcon
             open={true}
             onClose={this._handleCloseFileUploadModalActive}>
        <Modal.Content scrolling>
          <Container fluid textAlign={'center'}>
            <FileUploader autoProceed={true} onFilesUploaded={(uploadedFiles) => {
              this.setState({uploadedFiles});
            }} meta={{
              syncAsResource: true,
              creatorId: person.id
            }}/>
          </Container>
        </Modal.Content>
        <Modal.Actions>
          <Button onClick={this._handleCloseFileUploadModalActive}>
            {t("Cancel")}
          </Button>
          {this.state.uploadedFiles.length > 0 ? (
            <Button loading={this.state.loadingCreation} primary
                    onClick={() => this.createResourcesWithUploadedFiles()}>
              <Icon name='add'/> {t("Create {{count}} resources", {count: this.state.uploadedFiles.length})}
            </Button>
          ) : null}
        </Modal.Actions>
      </Modal>
    )
  }

  _changeView = (viewType) => {
    this.props.context.getLocalStorage().setItem("cortex-finder__view-type", viewType);
    this.setState({
      viewType
    });
  };

  renderView() {
    const {customRenderTableView, customRenderListView, customRenderTileView, selectable} = this.props;

    switch (this.state.viewType) {
      case 'list':
        if (!selectable){
          return customRenderListView ? customRenderListView(this.props) : this.renderListView();
        }
      case 'table':
        return customRenderTableView ? customRenderTableView(this.props) : this.renderTableView();
      case 'tile':
        return customRenderTileView ? customRenderTileView(this.props) : this.renderTileView();
    }
  }

  renderListView() {
    const {finder, location, t} = this.props;

    return (
      <Grid columns='equal'>
        <Grid.Column>
          <List selection divided relaxed verticalAlign='middle'>
            {finder.resources.edges.map(({node: resource}) => {
              let route = this.props.context.formatRoute('Finder_resource', {id: resource.id});

              return (
                <List.Item key={resource.id}
                           as={Link}
                           to={route}
                           active={!!matchPath(location.pathname, route)}
                >
                  <Header as='h5' image>
                    {isImageMime(resource.mime) ? (
                      <Image src={this.props.context.getImageThumbnailURL(resource.publicUrl, 50)} rounded
                             size='mini'/>
                    ) : (
                      <Icon size={"tiny"} color='grey' fitted
                            name={`file outline ${getMimeIcon(resource.mime)}`.trim()}/>
                    )}

                    <Header.Content as={'strong'}>
                      {resource.title || resource.filename}
                      <Header.Subheader>
                        {resource.title ? resource.filename : ""}
                      </Header.Subheader>
                    </Header.Content>
                  </Header>
                </List.Item>
              );
            })}

            {this.state.loadingMore ? Array(8).fill().map((_, index) => (
              <List.Item key={index}>
                <Placeholder>
                  <Placeholder.Header image>
                    <Placeholder.Line/>
                    <Placeholder.Line length='medium'/>
                  </Placeholder.Header>
                </Placeholder>
              </List.Item>
            )) : null}

            <Visibility once onUpdate={(e, {calculations: {topVisible}}) => {
              if (topVisible) {
                this.showMore();
              }
            }}/>

          </List>

          {finder.resources.edges.length === 0 ? (
            <Segment textAlign='center' disabled padded='very'>
              {t("There is no result for this query...")}
            </Segment>
          ) : null}
        </Grid.Column>

        <Route exact path={this.props.context.getRoute('Finder_resource')} render={({match}) => {
          return (
            <Grid.Column width={8}>
              <ResourceEditor singleColumn={true} variables={{id: decodeURIComponent(match.params.id)}}/>
            </Grid.Column>
          );
        }}/>
      </Grid>
    );
  }

  renderTableView() {
    const {finder, location, t, history, selectable} = this.props;
    const {sortColumn, sortDirection} = this.state;

    return (
      <>
        <Table sortable compact selectable basic>
          <Table.Header>
            <Table.Row>
              {selectable ? (
                <Table.HeaderCell>
                  <Checkbox checked={this.state.selectAllResources}
                            indeterminate={!this.state.selectAllResources && this.state.selectedResources.length > 0}
                            onChange={this._handleClearSelectResource}
                  />
                </Table.HeaderCell>
              ) : null}
              <Table.HeaderCell sorted={sortColumn === 'name' ? sortDirection : null}
                                onClick={() => this._handleSort('name')}>
                {t("Name")}
              </Table.HeaderCell>
              <Table.HeaderCell sorted={sortColumn === 'size' ? sortDirection : null}
                                onClick={() => this._handleSort('size')}>
                {t("Size")}
              </Table.HeaderCell>
              <Table.HeaderCell sorted={sortColumn === 'lastUpdate' ? sortDirection : null}
                                onClick={() => this._handleSort('lastUpdate')}>
                {t("Last update")}
              </Table.HeaderCell>
              <Table.HeaderCell sorted={sortColumn === 'mime' ? sortDirection : null}
                                onClick={() => this._handleSort('mime')}>
                {t("Mime")}
              </Table.HeaderCell>
            </Table.Row>
          </Table.Header>
          <Table.Body>
            {finder.resources.edges.map(({node: resource}) => {
              let route = this.props.context.formatRoute('Finder_resource', {id: resource.id});

              return (
                <Table.Row key={resource.id}
                           {...( selectable ? {
                             onClick: this._handleToggleSelectResource.bind(this, resource),
                             active: !!this.state.selectedResources.find(({id}) => id === resource.id)
                           } : {
                             onClick: () => history.push(route),
                             active: !!matchPath(location.pathname, route)
                           })}
                >
                  {selectable ? (
                    <Table.Cell collapsing>
                      <Checkbox checked={!!this.state.selectedResources.find(({id}) => id === resource.id)} onChange={this._handleToggleSelectResource.bind(this, resource)}/>
                    </Table.Cell>
                  ) : null}

                  <Table.Cell>
                    <Header as='h5' image>
                      {isImageMime(resource.mime) ? (
                        <Image src={this.props.context.getImageThumbnailURL(resource.publicUrl, 50)} rounded
                               size='mini'/>
                      ) : (
                        <Icon size={"tiny"} color='grey' fitted
                              name={`file outline ${getMimeIcon(resource.mime)}`.trim()}/>
                      )}

                      <Header.Content as={'strong'}>
                        {resource.title || resource.filename}
                        <Header.Subheader>
                          {resource.title ? resource.filename : ""}
                        </Header.Subheader>
                      </Header.Content>
                    </Header>
                  </Table.Cell>
                  <Table.Cell>{numberFormat(resource.size, {
                    decimals: 1,
                    scale: 'binary',
                    unit: 'B'
                  })}</Table.Cell>
                  <Table.Cell>{moment(resource.lastUpdate).format("LL")}</Table.Cell>
                  <Table.Cell>{formatMime(resource.mime)}</Table.Cell>
                </Table.Row>
              )
            })}

            {this.state.loadingMore ? Array(4).fill().map((_, index) => (
              <Table.Row key={index}>
                {selectable ? (
                  <Table.Cell collapsing>
                    <Checkbox/>
                  </Table.Cell>
                ) : null}

                <Table.Cell>
                  <Placeholder>
                    <Placeholder.Header image>
                      <Placeholder.Line/>
                      <Placeholder.Line length='medium'/>
                    </Placeholder.Header>
                  </Placeholder>
                </Table.Cell>
                <Table.Cell>
                  <Placeholder>
                    <Placeholder.Line length='short'/>
                  </Placeholder>
                </Table.Cell>
                <Table.Cell>
                  <Placeholder>
                    <Placeholder.Line length='short'/>
                  </Placeholder>
                </Table.Cell>
                <Table.Cell>
                  <Placeholder>
                    <Placeholder.Line length='short'/>
                  </Placeholder>
                </Table.Cell>
              </Table.Row>
            )) : null}

            <Visibility as={"tr"} once onUpdate={(e, {calculations: {topVisible}}) => {
              if (topVisible) {
                this.showMore();
              }
            }}/>
          </Table.Body>
        </Table>

        {finder.resources.edges.length === 0 ? (
          <Segment textAlign='center' disabled padded='very'>
            {t("There is no result for this query...")}
          </Segment>
        ) : null}

        <Route exact path={this.props.context.getRoute('Finder_resource')} render={({match}) => {
          return (
            <Modal closeIcon size={"large"} open={true}
                   onClose={() => history.push(this.props.context.formatRoute('Finder_resources'))}>
              <Modal.Content scrolling>
                <ResourceEditor variables={{id: decodeURIComponent(match.params.id)}}/>
              </Modal.Content>
            </Modal>
          );
        }}/>
      </>
    )
  }


  renderTileView() {
    const {finder, selectable, t, history, location} = this.props;

    return (
      <>
        <Card.Group itemsPerRow={selectable ? 4 : 5}>
          {finder.resources.edges.map(({node: resource}) => {
            let route = this.props.context.formatRoute('Finder_resource', {id: resource.id});

            return (
              <Card key={resource.id}
                    {...(selectable ? {
                      onClick: this._handleToggleSelectResource.bind(this, resource),
                      raised: !!this.state.selectedResources.find(({id}) => id === resource.id)
                    } : {
                      onClick: () => history.push(route),
                      raised: !!matchPath(location.pathname, route)
                    })}
              >
                {isImageMime(resource.mime) ? (
                  <Image className={style.tileImage}
                         src={this.props.context.getImageThumbnailURL(resource.publicUrl, 250)} rounded/>
                ) : (
                  <Image className={style.tileImage} centered>
                    <Icon size={"massive"} color='grey' fitted
                          name={`file outline ${getMimeIcon(resource.mime)}`.trim()}/>
                  </Image>
                )}
                <Card.Content>
                  <Card.Description> {resource.title || resource.filename} </Card.Description>
                  <Card.Meta>
                    {numberFormat(resource.size, {
                      decimals: 1,
                      scale: 'binary',
                      unit: 'B'
                    })}
                  </Card.Meta>
                </Card.Content>
                <Card.Content extra>
                  {moment(resource.lastUpdate).format("LL")}
                </Card.Content>
                <Card.Content extra>
                  {formatMime(resource.mime)}
                </Card.Content>
                {selectable ? (
                  <Card.Content extra textAlign={'center'}>
                    <Checkbox checked={!!this.state.selectedResources.find(({id}) => id === resource.id)} onChange={this._handleToggleSelectResource.bind(this, resource)}/>
                  </Card.Content>
                ) : null}
              </Card>
            )
          })}

          {this.state.loadingMore ? Array(8).fill().map((_, index) => (
            <Card key={index}>
              <Image className={style.tileImage}>
                <Placeholder>
                  <Placeholder.Image square/>
                </Placeholder>
              </Image>
              <Card.Content>
                <Placeholder>
                  <Placeholder.Line/>
                  <Placeholder.Line/>
                </Placeholder>
              </Card.Content>
              <Card.Content extra>
                <Placeholder>
                  <Placeholder.Line/>
                </Placeholder>
              </Card.Content>
              <Card.Content extra>
                <Placeholder>
                  <Placeholder.Line/>
                </Placeholder>
              </Card.Content>
            </Card>
          )) : null}

          <Visibility once onUpdate={(e, {calculations: {topVisible}}) => {
            if (topVisible) {
              this.showMore();
            }
          }}/>
        </Card.Group>

        {finder.resources.edges.length === 0 ? (
          <Segment textAlign='center' disabled padded='very'>
            {t("There is no result for this query...")}
          </Segment>
        ) : null}

        <Route exact path={this.props.context.getRoute('Finder_resource')} render={({match}) => {
          return (
            <Modal closeIcon size={"large"} open={true}
                   onClose={() => history.push(this.props.context.formatRoute('Finder_resources'))}>
              <Modal.Content scrolling>
                <ResourceEditor variables={{id: decodeURIComponent(match.params.id)}}/>
              </Modal.Content>
            </Modal>
          );
        }}/>
      </>
    );
  }

  search(qs) {
    this.setState({
      qs,
      loading: true
    });

    if (this.searchTimer) {
      clearTimeout(this.searchTimer);
    }

    this.searchTimer = setTimeout(() => {
      this.props.relay.refetch(() => ({
        qs,
        first: 30
      }), null, () => {
        this.setState({
          loading: false
        });
      });
    }, 250)
  }

  showMore(howMany = 10) {
    const {finder} = this.props;

    if (_.get(finder, 'resources.pageInfo.hasNextPage') && !this.state.loadingMore) {
      const endCursor = _.get(finder, 'resources.pageInfo.endCursor');
      const total = _.get(finder, 'resources.edges.length') + howMany;

      this.setState({
        loadingMore: true
      }, () => {

        this.props.relay.refetch((fragmentVariables) => ({
          ...fragmentVariables,
          qs: this.state.qs,
          first: howMany,
          after: endCursor,
        }), ({
          first: total
        }), () => {
          this.setState({
            loadingMore: false
          });
        });
      });
    }
  }


  _handleSort = (sortColumn) => {
    this.setState({
      sortColumn,
      sortDirection: this.state.sortDirection === 'ascending' ? 'descending' : 'ascending'
    });

    this.props.relay.refetch(() => ({
      qs: this.state.qs,
      first: 30,
      sortings: [
        {
          sortBy: sortColumn,
          sortDirection: this.state.sortDirection === 'ascending' ? 'desc' : 'asc'
        }
      ]
    }), null, () => {
      this.setState({
        loading: false
      });
    });
  };

  _handleCloseFileUploadModalActive = () => {
    const {history, selectable, match} = this.props;

    if(selectable){
      this.setState({fileUploadModalActive: false});
    } else {
      history.push(match.url);
    }
  };

  _handleToggleSelectResource = (resource, e) => {
    if(e){
      e.stopPropagation();
    }

    let {selectedResources} = this.state;
    let indexOf = selectedResources.findIndex(({id}) => id === resource.id);

    if(indexOf === -1){
      selectedResources.push(resource);
    } else {
      selectedResources.splice(indexOf, 1);
    }

    this.setState({selectedResources});
    this.props.onSelectedResources(selectedResources);
  };

  _handleClearSelectResource = (e) => {
    if(e){
      e.stopPropagation();
    }

    let selectedResources = [];

    this.setState({selectedResources});
    this.props.onSelectedResources(selectedResources);
  };

  /**
   * Create resources with uploaded files.
   * @param filePath
   */
  async createResourcesWithUploadedFiles(filePath) {
    let {relay: {environment}, finder, t, person, history, selectable} = this.props;

    if (!this.state.loadingCreation) {
      this.setState({
        loadingCreation: true
      });

      this.state.uploadedFiles.map(async (uploadedFile, index) => {
        let fileInfos = await fetch(uploadedFile.replace(/files\//, "files/infos/"));
        fileInfos = await fileInfos.json();

        // Exemple :
        // {
        //   "internalId": "a31clcjoed70l0.png",
        //   "mime": "image/png",
        //   "size": 214684,
        //   "etag": "549ff7f9a9926dfc1d9c819c64d835b8-1",
        //   "filename": "a34kpjn3j6uhr (1).png"
        // }

        let objectInput = Object.assign(
          _.pick(fileInfos, ["path", "size", "etag", "name", "mime"]),
          {
            publicUrl: uploadedFile
          }
        );

        let mutation = new DefaultMutations.resources.CreateResourceMutation({
          environment,
          parentId: finder.id,
          connectionKey: 'Finder_resources',
          connectionRangeBehavior: 'prepend',
          onCompleted: (resource) => {
            this.props.context.alert(t("Resource {{name}} has been created", {name: resource.filename}));
            if (selectable){
              console.log(resource);
              this._handleToggleSelectResource(resource);
            }
          },
          onError: ({errors}) => {
            this.props.context.alertMutationError(errors);
          },
          onAfter: () => {
            if (index === this.state.uploadedFiles.length - 1) {
              this.setState({
                uploadedFiles: [],
                loadingCreation: false
              }, this._handleCloseFileUploadModalActive)
            }
          }
        });

        mutation.apply({
          objectInput,
          extraInputs: {
            personId: person.id
          }
        });
      });
    }
  }
}

let query = graphql`
  query Finder_Query($me: Boolean $personId: ID $first: Int $after: String $qs: String $sortings: [SortingInput]) {
    person(me: $me id: $personId){
      ...Finder_person
    }

    finder{
      ...Finder_finder
    }
  }
`;

export default withRelay(
  query,
  () => {
    return {
      first: 30,
      sortings: [
        {
          sortBy: "creationDate",
          sortDirection: "desc"
        }
      ],
      me: true
    }
  },
  createRefetchContainer(Finder, graphql`
    fragment Finder_person on Person {
      id
    }

    fragment Finder_finder on Finder {
      id
      resources(qs: $qs first: $first after: $after sortings: $sortings) @connection(key: "Finder_resources", filters: []) {
        edges{
          node{
            id
            title
            description
            lastUpdate
            filename
            mime
            size
            publicUrl
          }
        }
        pageInfo{
          hasNextPage
          endCursor
        }
      }
    }
  `, query)
);