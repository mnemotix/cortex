/**
 * @flow
 * @relayHash 3165c59e0c1308bd0d802cde443c496f
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
type Finder_finder$ref = any;
type Finder_person$ref = any;
export type SortingInput = {
  sortBy?: ?string,
  sortDirection?: ?string,
  sortFilters?: ?$ReadOnlyArray<?string>,
};
export type Finder_QueryVariables = {|
  me?: ?boolean,
  personId?: ?string,
  first?: ?number,
  after?: ?string,
  qs?: ?string,
  sortings?: ?$ReadOnlyArray<?SortingInput>,
|};
export type Finder_QueryResponse = {|
  +person: ?{|
    +$fragmentRefs: Finder_person$ref
  |},
  +finder: ?{|
    +$fragmentRefs: Finder_finder$ref
  |},
|};
export type Finder_Query = {|
  variables: Finder_QueryVariables,
  response: Finder_QueryResponse,
|};
*/


/*
query Finder_Query(
  $me: Boolean
  $personId: ID
  $first: Int
  $after: String
  $qs: String
  $sortings: [SortingInput]
) {
  person(me: $me, id: $personId) {
    ...Finder_person
    id
  }
  finder {
    ...Finder_finder
    id
  }
}

fragment Finder_person on Person {
  id
}

fragment Finder_finder on Finder {
  id
  resources(qs: $qs, first: $first, after: $after, sortings: $sortings) {
    edges {
      node {
        id
        title
        description
        lastUpdate
        filename
        mime
        size
        publicUrl
        __typename
      }
      cursor
    }
    pageInfo {
      hasNextPage
      endCursor
    }
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "me",
    "type": "Boolean",
    "defaultValue": null
  },
  {
    "kind": "LocalArgument",
    "name": "personId",
    "type": "ID",
    "defaultValue": null
  },
  {
    "kind": "LocalArgument",
    "name": "first",
    "type": "Int",
    "defaultValue": null
  },
  {
    "kind": "LocalArgument",
    "name": "after",
    "type": "String",
    "defaultValue": null
  },
  {
    "kind": "LocalArgument",
    "name": "qs",
    "type": "String",
    "defaultValue": null
  },
  {
    "kind": "LocalArgument",
    "name": "sortings",
    "type": "[SortingInput]",
    "defaultValue": null
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "id",
    "variableName": "personId",
    "type": "ID"
  },
  {
    "kind": "Variable",
    "name": "me",
    "variableName": "me",
    "type": "Boolean"
  }
],
v2 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "id",
  "args": null,
  "storageKey": null
},
v3 = [
  {
    "kind": "Variable",
    "name": "after",
    "variableName": "after",
    "type": "String"
  },
  {
    "kind": "Variable",
    "name": "first",
    "variableName": "first",
    "type": "Int"
  },
  {
    "kind": "Variable",
    "name": "qs",
    "variableName": "qs",
    "type": "String"
  },
  {
    "kind": "Variable",
    "name": "sortings",
    "variableName": "sortings",
    "type": "[SortingInput]"
  }
];
return {
  "kind": "Request",
  "operationKind": "query",
  "name": "Finder_Query",
  "id": null,
  "text": "query Finder_Query(\n  $me: Boolean\n  $personId: ID\n  $first: Int\n  $after: String\n  $qs: String\n  $sortings: [SortingInput]\n) {\n  person(me: $me, id: $personId) {\n    ...Finder_person\n    id\n  }\n  finder {\n    ...Finder_finder\n    id\n  }\n}\n\nfragment Finder_person on Person {\n  id\n}\n\nfragment Finder_finder on Finder {\n  id\n  resources(qs: $qs, first: $first, after: $after, sortings: $sortings) {\n    edges {\n      node {\n        id\n        title\n        description\n        lastUpdate\n        filename\n        mime\n        size\n        publicUrl\n        __typename\n      }\n      cursor\n    }\n    pageInfo {\n      hasNextPage\n      endCursor\n    }\n  }\n}\n",
  "metadata": {},
  "fragment": {
    "kind": "Fragment",
    "name": "Finder_Query",
    "type": "Query",
    "metadata": null,
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "person",
        "storageKey": null,
        "args": v1,
        "concreteType": "Person",
        "plural": false,
        "selections": [
          {
            "kind": "FragmentSpread",
            "name": "Finder_person",
            "args": null
          }
        ]
      },
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "finder",
        "storageKey": null,
        "args": null,
        "concreteType": "Finder",
        "plural": false,
        "selections": [
          {
            "kind": "FragmentSpread",
            "name": "Finder_finder",
            "args": null
          }
        ]
      }
    ]
  },
  "operation": {
    "kind": "Operation",
    "name": "Finder_Query",
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "person",
        "storageKey": null,
        "args": v1,
        "concreteType": "Person",
        "plural": false,
        "selections": [
          v2
        ]
      },
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "finder",
        "storageKey": null,
        "args": null,
        "concreteType": "Finder",
        "plural": false,
        "selections": [
          v2,
          {
            "kind": "LinkedField",
            "alias": null,
            "name": "resources",
            "storageKey": null,
            "args": v3,
            "concreteType": "ResourceConnection",
            "plural": false,
            "selections": [
              {
                "kind": "LinkedField",
                "alias": null,
                "name": "edges",
                "storageKey": null,
                "args": null,
                "concreteType": "ResourceEdge",
                "plural": true,
                "selections": [
                  {
                    "kind": "LinkedField",
                    "alias": null,
                    "name": "node",
                    "storageKey": null,
                    "args": null,
                    "concreteType": "Resource",
                    "plural": false,
                    "selections": [
                      v2,
                      {
                        "kind": "ScalarField",
                        "alias": null,
                        "name": "title",
                        "args": null,
                        "storageKey": null
                      },
                      {
                        "kind": "ScalarField",
                        "alias": null,
                        "name": "description",
                        "args": null,
                        "storageKey": null
                      },
                      {
                        "kind": "ScalarField",
                        "alias": null,
                        "name": "lastUpdate",
                        "args": null,
                        "storageKey": null
                      },
                      {
                        "kind": "ScalarField",
                        "alias": null,
                        "name": "filename",
                        "args": null,
                        "storageKey": null
                      },
                      {
                        "kind": "ScalarField",
                        "alias": null,
                        "name": "mime",
                        "args": null,
                        "storageKey": null
                      },
                      {
                        "kind": "ScalarField",
                        "alias": null,
                        "name": "size",
                        "args": null,
                        "storageKey": null
                      },
                      {
                        "kind": "ScalarField",
                        "alias": null,
                        "name": "publicUrl",
                        "args": null,
                        "storageKey": null
                      },
                      {
                        "kind": "ScalarField",
                        "alias": null,
                        "name": "__typename",
                        "args": null,
                        "storageKey": null
                      }
                    ]
                  },
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "cursor",
                    "args": null,
                    "storageKey": null
                  }
                ]
              },
              {
                "kind": "LinkedField",
                "alias": null,
                "name": "pageInfo",
                "storageKey": null,
                "args": null,
                "concreteType": "PageInfo",
                "plural": false,
                "selections": [
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "hasNextPage",
                    "args": null,
                    "storageKey": null
                  },
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "endCursor",
                    "args": null,
                    "storageKey": null
                  }
                ]
              }
            ]
          },
          {
            "kind": "LinkedHandle",
            "alias": null,
            "name": "resources",
            "args": v3,
            "handle": "connection",
            "key": "Finder_resources",
            "filters": []
          }
        ]
      }
    ]
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = '069e4d4b7a3b5292c15ceb2fdafd1494';
module.exports = node;
