/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *  
 */
import React from 'react';
import PropTypes from 'prop-types';
import {graphql, createFragmentContainer} from 'react-relay';
import {withContext, withRelay, Modules} from '@mnemotix/cortex-core/client';
import {withRouter} from 'react-router';
import {Container, Message, Segment, Header, Grid, List, Divider} from 'semantic-ui-react';
import {generateUIFormPropsForJSONSchemaMutation, Form, FormMutationDefinition} from "@mnemotix/cortex-form/client";
import {DefaultMutations as Mutations} from "@mnemotix/synaptix.js/lib/client";
import ResourceViewer from "./ResourceViewer";
import _ from "lodash";
import numberFormat from 'human-format';
import moment from 'moment';
import {ActorAvatar} from '@mnemotix/cortex-addressbook/client'
import ResourceExternalLinkEditor from "./components/externalLink/ResourceExternalLinkEditor";

@withContext
@withRouter
class ResourceEditor extends React.Component {
  static propTypes = {
    singleColumn: PropTypes.bool,
    getLinkOnPerson: PropTypes.func
  };

  state = {
    deleteLoading: false,
  };

  getUpdateMutation() {
    const {t, relay: {environment}, onClickOnPerson} = this.props;

    if (!this.updateMutation) {
      this.updateMutation = new FormMutationDefinition({
        mutation: new Mutations.resources.UpdateResourceMutation({
          environment
        }),
        successMessage: t("Resource has been updated.")
      });
    }

    return this.updateMutation;
  }

  render() {
    const {t, resource, relay: {environment}, finder, match, history, singleColumn} = this.props;

    if (!resource) {
      return (
        <Message negative>
          <Message.Header>{t("Sorry...")}</Message.Header>
          <p>{t("This resource does not exists...")}</p>
        </Message>
      );
    }

    const {schema, uiSchema} = generateUIFormPropsForJSONSchemaMutation({
      JSONSchema: this.props.context.getJSONSchema(),
      mutationName: 'updateResource'
    });

    return (
      <Container>
        <Grid stackable columns={singleColumn ? 1 : 'equal'} stretched>
          <Grid.Column>
            <Segment>
              <Grid columns={1} centered>
                <Grid.Row>
                  <Grid.Column width={singleColumn ? 12: 14}>
                    <ResourceViewer fluid variables={{id: resource.id}}/>
                  </Grid.Column>
                </Grid.Row>
                <Grid.Row>
                  <Grid.Column width={singleColumn ? 12: 14}>
                    <Segment size={'small'}>
                      <List>
                        <List.Item>
                          <List.Header>
                            {t("Creator")}
                          </List.Header>
                          <List.Content>
                            <ActorAvatar variables={{id: _.get(resource, 'creator.id')}}/>
                          </List.Content>
                        </List.Item>

                        <List.Item>
                          <List.Header>
                            {t("Size")}
                          </List.Header>
                          <List.Content>
                            {numberFormat(resource.size, {
                              decimals: 1,
                              scale: 'binary',
                              unit: 'B'
                            })}
                          </List.Content>
                        </List.Item>

                        <List.Item>
                          <List.Header>
                            {t("Creation date")}
                          </List.Header>
                          <List.Content>
                            {moment(resource.creationDate).format("LL")}
                          </List.Content>
                        </List.Item>

                        <List.Item>
                          <List.Header>
                            {t("Last update")}
                          </List.Header>
                          <List.Content>
                            {moment(resource.lastUpdate).format("LL")}
                          </List.Content>
                        </List.Item>
                      </List>
                    </Segment>
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </Segment>
          </Grid.Column>
          <Grid.Column>
            <Segment>
              <Header>
                {t("Properties")}
              </Header>

              <Form schema={schema}
                    uiSchema={uiSchema}
                    formData={resource}
                    relayMutation={this.getUpdateMutation()}
              />
            </Segment>

            <Segment>
              <Header>
                {t("External links")}
              </Header>

              <ResourceExternalLinkEditor variables={{id: resource.id}}/>
            </Segment>

            <Segment>
              <Header>
                {t("Actions")}
              </Header>

              <Modules.RemoveObjectButton buttonProps={{negative: true}}
                                          objectRecord={resource}
                                          parentRecord={finder}
                                          connectionKey={'Finder_resources'}
                                          confirmMessage={t("Do you really want to remove the resource {{name}} ?", {name: resource.displayName})}
                                          onCompleted={() => history.push(this.props.context.formatRoute('Finder_resources'))}
                                          successMessage={t("The resource {{name}} has been removed", {name: resource.displayName})}
                                          removeLabel={t("Remove {{name}}", {name: resource.displayName})}
              />
            </Segment>
          </Grid.Column>
        </Grid>
      </Container>
    );
  }
}

export default withRelay(
  graphql`
    query ResourceEditor_Query($id: ID) {
      resource(id: $id){
        ...ResourceEditor_resource
      }

      finder{
        ...ResourceEditor_finder
      }
    }
  `,
  {},
  createFragmentContainer(ResourceEditor, graphql`
    fragment ResourceEditor_resource on Resource {
      ...ResourceBasicFragment @relay(mask: false)
      creator{
        id
        displayName
      }
    }

    fragment ResourceEditor_finder on Finder {
      id
    }
  `)
);