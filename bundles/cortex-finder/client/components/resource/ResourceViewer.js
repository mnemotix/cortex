/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *  
 */
import React from 'react';
import PropTypes from 'prop-types';
import {graphql, createFragmentContainer} from 'react-relay';
import {withContext, withRelay} from '@mnemotix/cortex-core/client';
import {withRouter} from 'react-router';
import {Container, Image, Icon, Embed, Placeholder} from 'semantic-ui-react';
import classnames from 'classnames';
import ImageLoader from 'react-loading-image';

import style from './ResourceViewer.less';

import {
  isAudioMime,
  isImageMime,
  isPDFMime, isPresentationMime,
  isSpreadSheetMime, isTextMime,
  isVideoMime,
  isWordProcessingMime
} from "../../helpers/formats";
import getMimeIcon from "../../helpers/getMimeIcon";
import getFirstExternalLinksKnownSource from "../../helpers/getFirstExternalLinksKnownSource";

@withContext
@withRouter
class ResourceViewer extends React.Component {
  static propTypes = {
    size: PropTypes.string
  };

  state = {
    deleteLoading: false,
    isFullScreen: false
  };

  viewerRef = React.createRef();

  componentDidMount(){
    document.addEventListener('fullscreenchange', () => {
      this.setState({
        isFullScreen: !this.state.isFullScreen
      });
    })
  }

  render() {
    return (
      <Container textAlign={"center"}>
        {this.renderResource()}
      </Container>
    );
  }

  renderResource() {
    const {t, resource, size} = this.props;

    if (isImageMime(resource.mime)) {
      return (
        <div ref={this.viewerRef}>
          <ImageLoader src={this.props.context.getImageThumbnailURL(resource.publicUrl, 1024)}
                       image={({src}) => (
                           <Icon.Group size='huge'>
                             <Image src={src} className={classnames({[style.isFullScreen]: this.state.isFullScreen})} bordered size={size} centered />
                             <Icon corner name={this.state.isFullScreen ? 'compress' : 'expand'} className={style.expandIcon} onClick={this._toggleFullScreen}/>
                           </Icon.Group>
                         )
                       }
                       loading={() => (
                         <Placeholder>
                           <Placeholder.Image rectangular/>
                         </Placeholder>
                       )}
                       error={() => (
                         <Icon name={'question circle'} bordered disabled size={'massive'}/>
                       )}
          />
        </div>
      );
    }

    if(isPDFMime(resource.mime)) {
     return (
       <div ref={this.viewerRef}>
         <Embed style={{height: "547px"}}
                className={classnames(style.embed, {[style.isFullScreen]: this.state.isFullScreen})}
                defaultActive={true}
                url={this.props.context.getProxyURL(resource.publicUrl, 'application/pdf')}
                icon='file pdf'
                iframe={{
                  allowFullScreen: true,
                  height: 547
                }}
                source={""}
         />
         <Icon corner name='expand' className={style.expandIcon} onClick={this._toggleFullScreen}/>
       </div>

     );
    }

    if(isVideoMime(resource.mime)){
      let knowSource = getFirstExternalLinksKnownSource(resource.externalLinks.edges.map(({node}) => node));

      if (knowSource) {
        const {source, id} = knowSource;

        console.log(id);

        return (
          <Embed className={classnames(style.embed, {[style.isFullScreen]: this.state.isFullScreen})}
                 defaultActive={true}
                 autoplay={false}
                 id={id}
                 iframe={{
                   allowFullScreen: true,
                 }}
                 source={source}
          />
        )
      }
    }

    return (
      <Icon size='massive' name={`file outline ${getMimeIcon(resource.mime)}`.trim()}/>
    );
  }

  _toggleFullScreen = async () => {
    if (this.state.isFullScreen){
      await document.exitFullscreen();
    } else {
      await this.viewerRef.current.requestFullscreen();
    }
  };
}

export default withRelay(
  graphql`
    query ResourceViewer_Query($id: ID) {
      resource(id: $id){
        ...ResourceViewer_resource
      }
    }
  `,
  {},
  createFragmentContainer(ResourceViewer, graphql`
    fragment ResourceViewer_resource on Resource {
      id
      mime
      publicUrl
      externalLinks(first: 10){
        edges{
          node{
            link
          }
        }
      }
    }
  `)
);