/**
 * @flow
 * @relayHash 8803c50f556cbdec2b7e11799d89eeec
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
type ResourceEditor_finder$ref = any;
type ResourceEditor_resource$ref = any;
export type ResourceEditor_QueryVariables = {|
  id?: ?string
|};
export type ResourceEditor_QueryResponse = {|
  +resource: ?{|
    +$fragmentRefs: ResourceEditor_resource$ref
  |},
  +finder: ?{|
    +$fragmentRefs: ResourceEditor_finder$ref
  |},
|};
export type ResourceEditor_Query = {|
  variables: ResourceEditor_QueryVariables,
  response: ResourceEditor_QueryResponse,
|};
*/


/*
query ResourceEditor_Query(
  $id: ID
) {
  resource(id: $id) {
    ...ResourceEditor_resource
    id
  }
  finder {
    ...ResourceEditor_finder
    id
  }
}

fragment ResourceEditor_resource on Resource {
  id
  uri
  title
  description
  filename
  creationDate
  lastUpdate
  size
  publicUrl
  mime
  creator {
    id
    displayName
  }
}

fragment ResourceEditor_finder on Finder {
  id
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "id",
    "type": "ID",
    "defaultValue": null
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "id",
    "variableName": "id",
    "type": "ID"
  }
],
v2 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "id",
  "args": null,
  "storageKey": null
};
return {
  "kind": "Request",
  "operationKind": "query",
  "name": "ResourceEditor_Query",
  "id": null,
  "text": "query ResourceEditor_Query(\n  $id: ID\n) {\n  resource(id: $id) {\n    ...ResourceEditor_resource\n    id\n  }\n  finder {\n    ...ResourceEditor_finder\n    id\n  }\n}\n\nfragment ResourceEditor_resource on Resource {\n  id\n  uri\n  title\n  description\n  filename\n  creationDate\n  lastUpdate\n  size\n  publicUrl\n  mime\n  creator {\n    id\n    displayName\n  }\n}\n\nfragment ResourceEditor_finder on Finder {\n  id\n}\n",
  "metadata": {},
  "fragment": {
    "kind": "Fragment",
    "name": "ResourceEditor_Query",
    "type": "Query",
    "metadata": null,
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "resource",
        "storageKey": null,
        "args": v1,
        "concreteType": "Resource",
        "plural": false,
        "selections": [
          {
            "kind": "FragmentSpread",
            "name": "ResourceEditor_resource",
            "args": null
          }
        ]
      },
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "finder",
        "storageKey": null,
        "args": null,
        "concreteType": "Finder",
        "plural": false,
        "selections": [
          {
            "kind": "FragmentSpread",
            "name": "ResourceEditor_finder",
            "args": null
          }
        ]
      }
    ]
  },
  "operation": {
    "kind": "Operation",
    "name": "ResourceEditor_Query",
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "resource",
        "storageKey": null,
        "args": v1,
        "concreteType": "Resource",
        "plural": false,
        "selections": [
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "creationDate",
            "args": null,
            "storageKey": null
          },
          v2,
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "title",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "description",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "filename",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "uri",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "lastUpdate",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "size",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "publicUrl",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "mime",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "LinkedField",
            "alias": null,
            "name": "creator",
            "storageKey": null,
            "args": null,
            "concreteType": "Person",
            "plural": false,
            "selections": [
              v2,
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "displayName",
                "args": null,
                "storageKey": null
              }
            ]
          }
        ]
      },
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "finder",
        "storageKey": null,
        "args": null,
        "concreteType": "Finder",
        "plural": false,
        "selections": [
          v2
        ]
      }
    ]
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = '884007c28fc09025eda54f7b00b6a3da';
module.exports = node;
