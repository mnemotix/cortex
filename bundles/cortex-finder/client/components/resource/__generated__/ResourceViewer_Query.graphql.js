/**
 * @flow
 * @relayHash 6d6c3a2c547fdcf85fb9f990287b49d1
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
type ResourceViewer_resource$ref = any;
export type ResourceViewer_QueryVariables = {|
  id?: ?string
|};
export type ResourceViewer_QueryResponse = {|
  +resource: ?{|
    +$fragmentRefs: ResourceViewer_resource$ref
  |}
|};
export type ResourceViewer_Query = {|
  variables: ResourceViewer_QueryVariables,
  response: ResourceViewer_QueryResponse,
|};
*/


/*
query ResourceViewer_Query(
  $id: ID
) {
  resource(id: $id) {
    ...ResourceViewer_resource
    id
  }
}

fragment ResourceViewer_resource on Resource {
  id
  mime
  publicUrl
  externalLinks(first: 10) {
    edges {
      node {
        link
        id
      }
    }
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "id",
    "type": "ID",
    "defaultValue": null
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "id",
    "variableName": "id",
    "type": "ID"
  }
],
v2 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "id",
  "args": null,
  "storageKey": null
};
return {
  "kind": "Request",
  "operationKind": "query",
  "name": "ResourceViewer_Query",
  "id": null,
  "text": "query ResourceViewer_Query(\n  $id: ID\n) {\n  resource(id: $id) {\n    ...ResourceViewer_resource\n    id\n  }\n}\n\nfragment ResourceViewer_resource on Resource {\n  id\n  mime\n  publicUrl\n  externalLinks(first: 10) {\n    edges {\n      node {\n        link\n        id\n      }\n    }\n  }\n}\n",
  "metadata": {},
  "fragment": {
    "kind": "Fragment",
    "name": "ResourceViewer_Query",
    "type": "Query",
    "metadata": null,
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "resource",
        "storageKey": null,
        "args": v1,
        "concreteType": "Resource",
        "plural": false,
        "selections": [
          {
            "kind": "FragmentSpread",
            "name": "ResourceViewer_resource",
            "args": null
          }
        ]
      }
    ]
  },
  "operation": {
    "kind": "Operation",
    "name": "ResourceViewer_Query",
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "resource",
        "storageKey": null,
        "args": v1,
        "concreteType": "Resource",
        "plural": false,
        "selections": [
          v2,
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "mime",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "publicUrl",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "LinkedField",
            "alias": null,
            "name": "externalLinks",
            "storageKey": "externalLinks(first:10)",
            "args": [
              {
                "kind": "Literal",
                "name": "first",
                "value": 10,
                "type": "Int"
              }
            ],
            "concreteType": "ExternalLinkConnection",
            "plural": false,
            "selections": [
              {
                "kind": "LinkedField",
                "alias": null,
                "name": "edges",
                "storageKey": null,
                "args": null,
                "concreteType": "ExternalLinkEdge",
                "plural": true,
                "selections": [
                  {
                    "kind": "LinkedField",
                    "alias": null,
                    "name": "node",
                    "storageKey": null,
                    "args": null,
                    "concreteType": "ExternalLink",
                    "plural": false,
                    "selections": [
                      {
                        "kind": "ScalarField",
                        "alias": null,
                        "name": "link",
                        "args": null,
                        "storageKey": null
                      },
                      v2
                    ]
                  }
                ]
              }
            ]
          }
        ]
      }
    ]
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = '9097f28e1999207cb7ba20251fca9797';
module.exports = node;
