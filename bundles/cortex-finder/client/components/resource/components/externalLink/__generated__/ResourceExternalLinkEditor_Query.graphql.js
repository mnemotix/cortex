/**
 * @flow
 * @relayHash b4a0390a13823af231e6189b2acfcae3
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
type ResourceExternalLinkEditor_resource$ref = any;
export type ResourceExternalLinkEditor_QueryVariables = {|
  id: string
|};
export type ResourceExternalLinkEditor_QueryResponse = {|
  +resource: ?{|
    +$fragmentRefs: ResourceExternalLinkEditor_resource$ref
  |}
|};
export type ResourceExternalLinkEditor_Query = {|
  variables: ResourceExternalLinkEditor_QueryVariables,
  response: ResourceExternalLinkEditor_QueryResponse,
|};
*/


/*
query ResourceExternalLinkEditor_Query(
  $id: ID!
) {
  resource(id: $id) {
    ...ResourceExternalLinkEditor_resource
    id
  }
}

fragment ResourceExternalLinkEditor_resource on Resource {
  id
  externalLinks(first: 50) {
    edges {
      node {
        id
        link
        name
        __typename
      }
      cursor
    }
    pageInfo {
      endCursor
      hasNextPage
    }
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "id",
    "type": "ID!",
    "defaultValue": null
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "id",
    "variableName": "id",
    "type": "ID"
  }
],
v2 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "id",
  "args": null,
  "storageKey": null
},
v3 = [
  {
    "kind": "Literal",
    "name": "first",
    "value": 50,
    "type": "Int"
  }
];
return {
  "kind": "Request",
  "operationKind": "query",
  "name": "ResourceExternalLinkEditor_Query",
  "id": null,
  "text": "query ResourceExternalLinkEditor_Query(\n  $id: ID!\n) {\n  resource(id: $id) {\n    ...ResourceExternalLinkEditor_resource\n    id\n  }\n}\n\nfragment ResourceExternalLinkEditor_resource on Resource {\n  id\n  externalLinks(first: 50) {\n    edges {\n      node {\n        id\n        link\n        name\n        __typename\n      }\n      cursor\n    }\n    pageInfo {\n      endCursor\n      hasNextPage\n    }\n  }\n}\n",
  "metadata": {},
  "fragment": {
    "kind": "Fragment",
    "name": "ResourceExternalLinkEditor_Query",
    "type": "Query",
    "metadata": null,
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "resource",
        "storageKey": null,
        "args": v1,
        "concreteType": "Resource",
        "plural": false,
        "selections": [
          {
            "kind": "FragmentSpread",
            "name": "ResourceExternalLinkEditor_resource",
            "args": null
          }
        ]
      }
    ]
  },
  "operation": {
    "kind": "Operation",
    "name": "ResourceExternalLinkEditor_Query",
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "resource",
        "storageKey": null,
        "args": v1,
        "concreteType": "Resource",
        "plural": false,
        "selections": [
          v2,
          {
            "kind": "LinkedField",
            "alias": null,
            "name": "externalLinks",
            "storageKey": "externalLinks(first:50)",
            "args": v3,
            "concreteType": "ExternalLinkConnection",
            "plural": false,
            "selections": [
              {
                "kind": "LinkedField",
                "alias": null,
                "name": "edges",
                "storageKey": null,
                "args": null,
                "concreteType": "ExternalLinkEdge",
                "plural": true,
                "selections": [
                  {
                    "kind": "LinkedField",
                    "alias": null,
                    "name": "node",
                    "storageKey": null,
                    "args": null,
                    "concreteType": "ExternalLink",
                    "plural": false,
                    "selections": [
                      v2,
                      {
                        "kind": "ScalarField",
                        "alias": null,
                        "name": "link",
                        "args": null,
                        "storageKey": null
                      },
                      {
                        "kind": "ScalarField",
                        "alias": null,
                        "name": "name",
                        "args": null,
                        "storageKey": null
                      },
                      {
                        "kind": "ScalarField",
                        "alias": null,
                        "name": "__typename",
                        "args": null,
                        "storageKey": null
                      }
                    ]
                  },
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "cursor",
                    "args": null,
                    "storageKey": null
                  }
                ]
              },
              {
                "kind": "LinkedField",
                "alias": null,
                "name": "pageInfo",
                "storageKey": null,
                "args": null,
                "concreteType": "PageInfo",
                "plural": false,
                "selections": [
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "endCursor",
                    "args": null,
                    "storageKey": null
                  },
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "hasNextPage",
                    "args": null,
                    "storageKey": null
                  }
                ]
              }
            ]
          },
          {
            "kind": "LinkedHandle",
            "alias": null,
            "name": "externalLinks",
            "args": v3,
            "handle": "connection",
            "key": "ResourceExternalLinkEditor_externalLinks",
            "filters": []
          }
        ]
      }
    ]
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = '3044283529251ada01e7c22cf0d41fb7';
module.exports = node;
