/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *  
 */

export let isImageMime = function (mime) {
  return mime.search(/^image\//) !== -1;
};

export let isAudioMime = function (mime) {
  return mime.search(/^audio\//) !== -1;
};

export let isVideoMime = function (mime) {
  return mime.search(/^video\//) !== -1;
};

export let isPDFMime = function (mime) {
  return mime === "application/pdf";
};

export let isMailMime = function (mime) {
  return mime === "message/rfc822";
};

export let isMailName = function (name) {
  return name.search(/\.eml$/) !== -1;
};

export let isWordProcessingMime = function(mime) {
  return mime.search(/opendocument\.text|msword|wordprocessingml/) !== -1;
};

export let isSpreadSheetMime = function(mime) {
  return mime.search(/opendocument\.spreadsheet|excel|spreadsheetml/) !== -1;
};

export let isPresentationMime = function(mime) {
  return mime.search(/opendocument\.presentation|powerpoint|presentationml/) !== -1;
};

export let isTextMime = function(mime) {
  return mime.search(/text\/plain/) !== -1;
};

export let formatMime = function (mime) {
  return (mime || '').replace(/;.*$/, '');
};