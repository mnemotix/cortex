/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

/**
 * @typedef {object} externalLinksKnownSource
 * @param {string} source : link source name
 * @param {string} url : link
 */

const KNOWN_SOURCES = [
  {
    type: "video",
    source: "youtube",
    matches: [
      /www.youtube.com\/watch\?v=([^&]*)/,
      /youtu\.be\/([^?]*)/
    ]
  },
  {
    type: "video",
    source: "vimeo",
    matches: [
      /vimeo.com\/([^?]*)/
    ]
  },
  /** Disabled while https://github.com/Semantic-Org/Semantic-UI-React/issues/3276
  {
    type: "video",
    source: "dailymotion",
    match: /dailymotion.com\/([^?]*)/
  }
  */
];

/**
 * Get an array of externaLinks and returs
 * @param externalLinks
 * @return {externalLinksKnownSource}
 */
export default function getFirstExternalLinksKnownSource(externalLinks){
  let matching;
  let knownSource = KNOWN_SOURCES.find((source) => {
    return externalLinks.find((externalLink) => {
      for(let match of source.matches){
        let matched = externalLink.link.match(match);
        if(!!matched){
          matching = matched;
          return true;
        }
      }
    });
  });

  if(knownSource) {
    return {
      source: knownSource.source,
      id: matching[1]
    }
  }
}