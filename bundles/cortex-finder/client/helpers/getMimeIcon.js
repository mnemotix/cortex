/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import {
  isAudioMime,
  isPDFMime,
  isPresentationMime,
  isSpreadSheetMime, isTextMime,
  isVideoMime,
  isWordProcessingMime
} from "./formats";

export default function getMimeIcon(mime){
  if (isPDFMime(mime)) {
    return 'pdf';
  }

  if (isVideoMime(mime)) {
    return 'video';
  }

  if (isAudioMime(mime)) {
    return 'audio';
  }

  if (isSpreadSheetMime(mime)) {
    return 'excel';
  }

  if (isWordProcessingMime(mime)) {
    return 'word';
  }

  if (isPresentationMime(mime)) {
    return 'powerpoint';
  }

  if (isTextMime(mime)) {
    return 'alternate';
  }

  return '';
}