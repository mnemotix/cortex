/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import React, { Component } from "react";
import PropTypes from "prop-types";
import { Button, Form as FormSematicUI } from "semantic-ui-react";
import _ from 'lodash';

import { default as DefaultErrorList } from "./ErrorList";
import {
  getDefaultFormState,
  shouldRender,
  toIdSchema,
  setState,
  getDefaultRegistry,
} from "../utils";
import validateFormData from "../validate";

import {DefaultMutations} from "@mnemotix/synaptix.js/lib/client";
import {withContext} from "@mnemotix/cortex-core/client";
import {translate} from "react-i18next";
import FormMutationDefinition from "./FormMutationDefinition";

@translate(['labels'])
@withContext
export default class Form extends Component {
  static propTypes = {
    schema: PropTypes.object.isRequired,
    uiSchema: PropTypes.object,
    formData: PropTypes.any,
    widgets: PropTypes.objectOf(
      PropTypes.oneOfType([PropTypes.func, PropTypes.object])
    ),
    fields: PropTypes.objectOf(PropTypes.func),
    ArrayFieldTemplate: PropTypes.func,
    ObjectFieldTemplate: PropTypes.func,
    FieldTemplate: PropTypes.func,
    ErrorList: PropTypes.func,
    onChange: PropTypes.func,
    onError: PropTypes.func,
    showErrorList: PropTypes.bool,
    onSubmit: PropTypes.func,
    id: PropTypes.string,
    className: PropTypes.string,
    name: PropTypes.string,
    method: PropTypes.string,
    target: PropTypes.string,
    action: PropTypes.string,
    autocomplete: PropTypes.string,
    enctype: PropTypes.string,
    acceptcharset: PropTypes.string,
    noValidate: PropTypes.bool,
    noHtml5Validate: PropTypes.bool,
    liveValidate: PropTypes.bool,
    validate: PropTypes.func,
    transformErrors: PropTypes.func,
    safeRenderCompletion: PropTypes.bool,
    formContext: PropTypes.object,
    relayMutation: PropTypes.instanceOf(FormMutationDefinition),
    relayCreationMutation: PropTypes.instanceOf(FormMutationDefinition),
    relayUpdateMutation: PropTypes.instanceOf(FormMutationDefinition),
    submitButtonLabel: PropTypes.string,
    extraActions: PropTypes.element
  };

  static defaultProps = {
    uiSchema: {},
    noValidate: false,
    liveValidate: false,
    safeRenderCompletion: false,
    noHtml5Validate: false,
    ErrorList: DefaultErrorList,
  };

  constructor(props) {
    super(props);
    this.state = this.getStateFromProps(props);
  }

  componentDidMount(){
    let mutationDefinition = this.getRelayMutationDefinition(this.state.formData);

    if (mutationDefinition) {
      let {mutation, successMessage, onCompleted} = mutationDefinition;

      mutation.addCallbacks({
        onCompleted: (rsp) => {
          this.props.context.alert(successMessage || this.props.t("Success !"));

          if(onCompleted){
            onCompleted(rsp)
          }
        },
        onError:  this.props.context.alertMutationError,
        onAfter: () => {
          this.setState({
            loading: false
          });
        }
      });
    }
  }

  componentWillReceiveProps(nextProps) {
    this.setState(this.getStateFromProps(nextProps));
  }

  getStateFromProps(props) {
    const state = this.state || {};
    const schema = "schema" in props ? _.cloneDeep(props.schema) : _.cloneDeep(this.props.schema);
    const uiSchema = "uiSchema" in props ? _.cloneDeep(props.uiSchema) : _.cloneDeep(this.props.uiSchema);
    const edit = typeof props.formData !== "undefined";
    const liveValidate = props.liveValidate || this.props.liveValidate;
    const mustValidate = edit && !props.noValidate && liveValidate;
    const { definitions } = schema;
    const formData = getDefaultFormState(schema, props.formData, definitions);
    const { errors, errorSchema } = mustValidate
      ? this.validate(formData, schema)
      : {
          errors: state.errors || [],
          errorSchema: state.errorSchema || {},
        };
    const idSchema = toIdSchema(
      schema,
      uiSchema["ui:rootFieldId"],
      definitions,
      formData
    );
    return {
      schema,
      uiSchema,
      idSchema,
      formData,
      edit,
      errors,
      errorSchema,
    };
  }

  shouldComponentUpdate(nextProps, nextState) {
    return shouldRender(this, nextProps, nextState);
  }

  validate(formData, schema) {
    const { validate, transformErrors } = this.props;

    formData = Object.entries(formData).reduce((acc, [propName, value]) => {
      if (typeof value !== "undefined" && value !== null) {
        acc[propName] = value;
      }
      return acc;
    }, {});

    schema =  schema || this.props.schema;

    schema.properties = Object.entries(schema.properties).reduce((acc, [propName, property]) => {
      if (property.type !== "custom") {
        acc[propName] = property;
      }
      return acc;
    }, {});

    return validateFormData(
      formData,
      schema,
      validate,
      transformErrors
    );
  }

  renderErrors() {
    const { errors, errorSchema, schema, uiSchema } = this.state;
    const { ErrorList, showErrorList, formContext } = this.props;

    if (errors.length && showErrorList !== false) {
      return (
        <ErrorList
          errors={errors}
          errorSchema={errorSchema}
          schema={schema}
          uiSchema={uiSchema}
          formContext={formContext}
        />
      );
    }
    return null;
  }

  onChange = (formData, options = { validate: false }) => {
    const mustValidate =
      !this.props.noValidate && (this.props.liveValidate || options.validate);
    let state = { formData };
    if (mustValidate) {
      const { errors, errorSchema } = this.validate(formData);
      state = { ...state, errors, errorSchema };
    }
    setState(this, state, () => {
      if (this.props.onChange) {
        this.props.onChange(this.state);
      }
    });
  };

  onBlur = (...args) => {
    if (this.props.onBlur) {
      this.props.onBlur(...args);
    }
  };

  onFocus = (...args) => {
    if (this.props.onFocus) {
      this.props.onFocus(...args);
    }
  };

  onSubmit = event => {
    event.preventDefault();

    if (!this.props.noValidate) {
      const { errors, errorSchema } = this.validate(this.state.formData);
      if (Object.keys(errors).length > 0) {

        setState(this, { errors, errorSchema }, () => {
          if (this.props.onError) {
            this.props.onError(errors);
          } else {
            console.error("Form validation failed", errors);
          }
        });
        return;
      }
    }

    if (this.props.onSubmit) {
      this.props.onSubmit({ ...this.state, status: "submitted" });
    }

    let mutationDefinition = this.getRelayMutationDefinition(this.state.formData);

    if (mutationDefinition) {
      let {mutation, extraInputs} = mutationDefinition;

      let objectInput = _.pick(this.state.formData, Object.keys(this.props.schema.properties));

      console.log(mutation, mutation instanceof DefaultMutations.CreateObjectMutation);

      if (mutation instanceof DefaultMutations.UpdateObjectMutation){
        this.setState({
          loading: true
        });

        mutation.apply(objectInput);
      }

      if (mutation instanceof DefaultMutations.CreateObjectMutation){
        this.setState({
          loading: true
        });

        mutation.apply({objectInput, extraInputs});
      }
    }

    this.setState({ errors: [], errorSchema: {} });
  };

  /**
   * @param formData
   * @return {FormMutationDefinition}
   */
  getRelayMutationDefinition(formData){
    const {relayMutation, relayCreationMutation, relayUpdateMutation} = this.props;

    if (formData.id) {
      return relayUpdateMutation || relayMutation;
    } else {
      return relayCreationMutation || relayMutation;
    }
  }

  getRegistry() {
    // For BC, accept passed SchemaField and TitleField props and pass them to
    // the "fields" registry one.
    const { fields, widgets } = getDefaultRegistry();
    return {
      fields: { ...fields, ...this.props.fields },
      widgets: { ...widgets, ...this.props.widgets },
      ArrayFieldTemplate: this.props.ArrayFieldTemplate,
      ObjectFieldTemplate: this.props.ObjectFieldTemplate,
      FieldTemplate: this.props.FieldTemplate,
      definitions: this.props.schema.definitions || {},
      formContext: this.props.formContext || {},
    };
  }

  render() {
    const {
      children,
      safeRenderCompletion,
      id,
      name,
      method,
      target,
      action,
      autocomplete,
      enctype,
      acceptcharset,
      noHtml5Validate,
      submitButtonLabel,
      t
    } = this.props;

    const { schema, uiSchema, formData, errorSchema, idSchema } = this.state;
    const registry = this.getRegistry();
    const SchemaField = registry.fields.SchemaField;

    return (
      <FormSematicUI
        id={id}
        name={name}
        method={method}
        target={target}
        action={action}
        autoComplete={autocomplete}
        encType={enctype}
        acceptCharset={acceptcharset}
        noValidate={noHtml5Validate}
        onSubmit={this.onSubmit}
      >
        {this.renderErrors()}
        <SchemaField
          schema={schema}
          uiSchema={uiSchema}
          errorSchema={errorSchema}
          idSchema={idSchema}
          formData={formData}
          onChange={this.onChange}
          onBlur={this.onBlur}
          onFocus={this.onFocus}
          registry={registry}
          safeRenderCompletion={safeRenderCompletion}
        />

        {children ? (
          children
        ) : (
          <div>
            <Button primary type="submit" loading={this.state.loading}>
              {submitButtonLabel || (formData.id ? t("Update") : t("Create"))}
            </Button>

            {this.props.extraActions}
          </div>
        )}
      </FormSematicUI>
    );
  }
}
