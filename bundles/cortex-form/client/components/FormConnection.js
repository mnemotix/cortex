/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import React, {Component} from "react";
import PropTypes from "prop-types";
import {Button, List, Container} from "semantic-ui-react";
import Form from './Form';
import _ from 'lodash';

import {withContext} from "@mnemotix/cortex-core/client";
import {translate} from "react-i18next";
import FormMutationDefinition from "./FormMutationDefinition";

@translate(['labels'])
@withContext
export default class FormConnection extends Component {
  static propTypes = {
    nodeSchema: PropTypes.object.isRequired,
    nodeUiSchema: PropTypes.object.isRequired,
    formData: PropTypes.any,
    relayAddNodeMutation: PropTypes.instanceOf(FormMutationDefinition),
    relayUpdateNodeMutation: PropTypes.instanceOf(FormMutationDefinition).isRequired,
    relayRemoveNodeMutation: PropTypes.instanceOf(FormMutationDefinition),
    submitButtonLabel: PropTypes.string,
    extraActions: PropTypes.element,
    addButtonLabel: PropTypes.string,
    nodeExtraActions: PropTypes.func,
    listItemOptions: PropTypes.object,
    listOptions: PropTypes.object,
  };

  static defaultProps = {
    listItemBeforeContent: () => {},
    listItemCreationBeforeContent: () => {}
  };

  state = {
    addActive: false
  };

  componentWillMount(){
    if (this.props.relayAddNodeMutation) {
      this.props.relayAddNodeMutation.mutation.addCallbacks({
        onCompleted: () => {
          this.setState({
            addActive: false
          });
        }
      });
    }
  }

  componentWillReceiveProps(nextProps){
    if(!this.props.relayAddNodeMutation && nextProps.relayAddNodeMutation !== this.props.relayAddNodeMutation){
      nextProps.relayAddNodeMutation.mutation.addCallbacks({
        onCompleted: () => {
          this.setState({
            addActive: false
          });
        }
      });

      this.setState({
        addActive: true
      });
    }
  }

  render() {
    const {
      t,
      addButtonLabel,
      formData,
      nodeSchema,
      nodeUiSchema,
      relayUpdateNodeMutation,
      relayAddNodeMutation,
      nodeExtraActions,
      listItemOptions,
      listOptions
    } = this.props;


    return (
      <List relaxed={'very'} size='large' {...listOptions}>
        {formData.edges.map(({node}, index) => (
          <List.Item key={index} {...listItemOptions} content={(
            <Form schema={nodeSchema}
                  uiSchema={nodeUiSchema}
                  formData={node}
                  relayMutation={relayUpdateNodeMutation}
                  extraActions={nodeExtraActions(node)}
            />
          )}/>
        ))}

        {this.state.addActive ? (
          <List.Item {...listItemOptions} content={this.renderAddForm()}/>
        ) : relayAddNodeMutation ? (
          <Container textAlign={"center"}>
            <Button onClick={() => this.setState({addActive: true})}>
              {addButtonLabel || t("Add")}
            </Button>
          </Container>
        ) : null}
      </List>
    );
  }

  renderAddForm(){
    const {t, addButtonLabel, nodeSchema, nodeUiSchema, relayAddNodeMutation,} = this.props;

    return (
      <Form schema={nodeSchema}
            uiSchema={nodeUiSchema}
            formData={{}}
            relayMutation={relayAddNodeMutation}
            submitButtonLabel={addButtonLabel}
            extraActions={
              <Button onClick={(e) => {
                e.preventDefault();
                this.setState({addActive: false})
              }}>{t("Cancel")}</Button>
            }
      />
    );
  }
}
