import React from "react";
import PropTypes from "prop-types";

export default class CustomField extends React.Component{
  static propTypes = {
    schema: PropTypes.object.isRequired,
    uiSchema: PropTypes.object,
    idSchema: PropTypes.object,
    onChange: PropTypes.func.isRequired,
    formData: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    required: PropTypes.bool,
    formContext: PropTypes.object.isRequired,
  };

  static defaultProps ={
    uiSchema: {}
  };

  render(){
    const {schema, globalFormData} = this.props;

    return schema.render ? schema.render(globalFormData) : null;
  }
};
