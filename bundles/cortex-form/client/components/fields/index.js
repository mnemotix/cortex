import ArrayField from "./ArrayField";
import BooleanField from "./BooleanField";
import DescriptionField from "./DescriptionField";
import NumberField from "./NumberField";
import ObjectField from "./ObjectField";
import SchemaField from "./SchemaField";
import StringField from "./StringField";
import TitleField from "./TitleField";
import CustomField from './CustomField';
import UnsupportedField from "./UnsupportedField";

export default {
  ArrayField,
  BooleanField,
  DescriptionField,
  NumberField,
  ObjectField,
  SchemaField,
  StringField,
  TitleField,
  CustomField,
  UnsupportedField,
};
