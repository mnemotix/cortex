import React from "react";
import PropTypes from "prop-types";
import reactCSS from "reactcss";
import { TwitterPicker } from "react-color";
import style from './ColorWidget.less';

function ColorWidget(props) {
  const {
    value,
    options,
    schema,
    formContext,
    registry,
    ...inputProps
  } = props;

  inputProps.type = "color";
  inputProps.label = null;

  const onChangeHandler = value => {
    const color = value.hex;
    return props.onChange(color === "" ? options.emptyValue : color);
  };

  return <SketchExample value={props.value} changeHandler={onChangeHandler} />;
}

class SketchExample extends React.Component {
  state = {
    displayColorPicker: false,
    color: {
      r: 0,
      g: 0,
      b: 0,
      a: 1,
    },
  };

  constructor(props) {
    super(props);
    this.state.color = this.hexToRgbA(this.props.value);
  }

  handleClick = () => {
    this.setState({ displayColorPicker: !this.state.displayColorPicker });
  };

  handleClose = () => {
    this.setState({ displayColorPicker: false });
  };

  handleChange = color => {
    this.setState({ color: color.rgb });
    this.props.changeHandler(color);
  };

  hexToRgbA = hex => {
    let c;

    if (/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)) {
      c = hex.substring(1).split("");

      if (c.length === 3) {
        c = [c[0], c[0], c[1], c[1], c[2], c[2]];
      }
      c = "0x" + c.join("");

      return {
        r: (c >> 16) & 255,
        g: (c >> 8) & 255,
        b: c & 255,
        a: 1,
      };
    }

    return {
      r: 0,
      g: 0,
      b: 0,
      a: 1,
    };
  };

  render() {
    return (
      <div className={style.widget}>
        <div className={style.swatch} onClick={this.handleClick}>
          <div className={style.color} style={{background : `rgba(
            ${this.state.color.r},
            ${this.state.color.g},
            ${this.state.color.b},
            ${this.state.color.a})`}}/>
        </div>

        {this.state.displayColorPicker ? (
          <div className={style.popover}>
            <div className={style.cover} onClick={this.handleClose} />
            <TwitterPicker
              color={this.state.color}
              onChange={this.handleChange}
            />
          </div>
        ) : null}
      </div>
    );
  }
}

ColorWidget.defaultProps = {
  type: "text",
  required: false,
  disabled: false,
  readOnly: false,
  autoFocus: false,
};

if (process.env.NODE_ENV !== "production") {
  ColorWidget.propTypes = {
    id: PropTypes.string.isRequired,
    placeholder: PropTypes.string,
    value: PropTypes.any,
    required: PropTypes.bool,
    disabled: PropTypes.bool,
    readOnly: PropTypes.bool,
    autoFocus: PropTypes.bool,
    onChange: PropTypes.func,
    onBlur: PropTypes.func,
    onFocus: PropTypes.func,
  };
}

export default ColorWidget;
