/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import React from "react";
import 'react-dates/initialize';
import 'react-dates/lib/css/_datepicker.css';
import {DayPickerSingleDateController} from 'react-dates';
import { Input, Modal, Button } from "semantic-ui-react";
import style from './DateWidget.less';
import classnames from 'classnames';
import moment from 'moment';
import {translate} from "react-i18next";

@translate(['labels'])
export default class DateWidget extends React.Component{
  state = {
    focused: false,
    date: this.props.value ? moment(this.props.value) : null,
    modalOpen: false,
  };

  render() {
    const { onChange, required, value, t } = this.props;
    const { focused, date } = this.state;

    const dateString = date && moment(date).format('L');

    return (
      <div className={style.datePickerInput}>
        <Input value={dateString || value || ''} required={required} placeholder={'JJ/MM/AAAA'}
               action={{
                 icon: 'calendar alternate outline',
                 onClick: (e) =>{
                   e.preventDefault();
                   this.setState({modalOpen: true})
                 }
               }}
        />

        <Modal open={this.state.modalOpen}
               basic
        >
          <Modal.Content className={style.datePickerModalContent}>
            <div className={style.datePickerController}>
              <DayPickerSingleDateController
                onDateChange={this._onDateChange}
                onFocusChange={this._onFocusChange}
                focused={true}
                date={date}
                numberOfMonths={3}
              />
            </div>
          </Modal.Content>
          <Modal.Actions>
            <Button disabled={!this.state.date} color='green' onClick={(e) =>  {
              e.preventDefault();
              this.props.onChange(date.valueOf());
              this._onClose();
            }}>
              {t("Select")}
            </Button>
            <Button color='red' onClick={(e) =>  {
              e.preventDefault();
              this.setState({
                date: this.props.value ? moment(this.props.value) : null
              }, this._onClose);
            }}>
              {t("Cancel")}
            </Button>
          </Modal.Actions>
        </Modal>
      </div>
    );
  }

  _onDateChange = (date) => {
    this.setState({
      date
    });
  };

  _onFocusChange = () => {
    this.setState({ focused: true });
  };

  _onClose = () => {
    this.setState({ modalOpen: false, focused: false });
  };
}