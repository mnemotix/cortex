/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import React from "react";
import {Button, Input, Modal, Image, Icon, Container} from "semantic-ui-react";
import style from './ImageWidget.less';
import classnames from 'classnames';
import {translate} from "react-i18next";
import FileUploader from '@mnemotix/cortex-uploader/client/FileUploader';

@translate(['labels'])
export default class ImageWidget extends React.Component {
  state = {
    modalActive: false,
    value: this.props.value
  };

  render() {
    const {id, t, required} = this.props;
    const {value, modalActive} = this.state;

    return (
      <div>
        <Input type="hidden" id={id} value={value || ""}/>

        <Container>
          <Image className={style.image} src={value} size='small' rounded bordered verticalAlign='middle'>
            {!value || value === "" ? (
              <Icon name={'image'} size={"huge"} disabled circular bordered/>
            ) : null}
          </Image>

          <Modal required={required}
                 closeIcon
                 open={modalActive}
                 onClose={() => this.setState({modalActive: false})}
                 trigger={
                   <Button.Group>
                     <Button content={t("Change")} onClick={(e) => {
                       e.preventDefault();
                       this.setState({modalActive: true})
                     }}/>

                     {value && value !== "" ? (
                       <Button icon='trash' content={t("Remove")} color='red' onClick={() => this._onChange("")}/>
                     ) : null}
                   </Button.Group>
                 }>
            <Modal.Content>
              <Container textAlign={"center"}>
                <FileUploader maxNumberOfFiles={1} allowedFileTypes={['image/*']} onFilesUploaded={this._onChange}/>
              </Container>
            </Modal.Content>
          </Modal>

        </Container>
      </div>
    );
  }

  _onChange = (value) => {
    this.setState({
      modalActive: false,
      value
    });

    this.props.onChange(value);
  };
}