/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import _ from 'lodash';

/**
 * @param {object} JSONSchema
 * @param {string} mutationName
 * @param {array} groups
 * @param {object} extraProperties
 */
export function generateUIFormPropsForJSONSchemaMutation({JSONSchema, mutationName, groups, extraProperties}){
  let mutationSchema = _.get(JSONSchema, `properties.Mutation.properties.${mutationName}`);

  if (!mutationSchema){
    throw `No mutation found at ${`properties.Mutation.properties.${mutationName}`}`;
  }

  let mutationInputRef = _.get(mutationSchema, 'properties.arguments.properties.input.$ref');

  if (!mutationInputRef){
    throw `No mutation input ref found at ${'properties.arguments.properties.input.$ref'} for mutation schema ${mutationName}`;
  }

  let muationInputDefinitionName = mutationInputRef.replace('#/definitions/', '');
  let mutationInputSchema = _.get(JSONSchema, `definitions.${muationInputDefinitionName}`);

  if (!mutationInputRef){
    throw `No mutation input definition ref found at ${'properties.arguments.properties.input.$ref'} for mutation schema ${mutationName}`;
  }

  let inputRef = _.get(mutationInputSchema, 'properties.objectInput.$ref');

  if (!mutationInputRef){
    throw `No input ref found at ${'properties.objectInput.$ref'} for definition ${inputDefinitionName}`;
  }

  let inputDefinitionName = inputRef.replace('#/definitions/', '');

  let schema = _.cloneDeep(_.get(JSONSchema, `definitions.${inputDefinitionName}`));
  let uiSchema = {
    "ui:order": Object.keys(schema.properties)
  };


  schema.properties = Object.entries(schema.properties).reduce((acc, [propName, prop]) => {
    let formInputDef = _.get(prop, 'formInput');

    if (formInputDef) {
      let {type, placeholder, label, enumValues, showIfPropName, showIfPropEnumValue, disabled} = formInputDef;

      if (disabled === true){
        uiSchema["ui:order"].splice(uiSchema["ui:order"].indexOf(propName), 1);
        return acc;
      }

      uiSchema[propName] = {
        "ui:emptyValue": ""
      };

      if (type){
        if(type === "date"){
          uiSchema[propName]['ui:widget'] = 'date';
        } else if (['email', 'uri', 'data-url'].includes(type)) {
          prop.format = type;
        } else {
          uiSchema[propName]['ui:widget'] = type;
        }
      }

      if (label){
        prop.title = label;
      }

      if (placeholder) {
        uiSchema[propName]['ui:placeholder'] = placeholder;
      }

      if (enumValues){
        prop.enum = enumValues;
      }

      if(showIfPropName) {
        let index = uiSchema["ui:order"].indexOf(showIfPropName);

        if (index >= 0) {
          uiSchema["ui:order"][index + 1] = "*";
        }

        if (showIfPropEnumValue){
          schema.dependencies = {
            ...schema.dependencies,
            [showIfPropName]: {
              oneOf: [
                {
                  properties: {
                    [showIfPropName]: {
                      "enum": [showIfPropEnumValue]
                    },
                    [propName]: prop
                  }
                },
                {
                  properties: {
                    [showIfPropName]: {
                      "enum": _.without(acc[showIfPropName].enum, showIfPropEnumValue)
                    }
                  }
                }
              ]
            }
          }
        } else {
          schema.dependencies = {
            ...schema.dependencies,
            [showIfPropName]: {
              properties : {
                [propName]: prop
              }
            }
          }
        }

      } else {
        acc[propName] = prop;
      }
    } else {
      acc[propName] = prop;
    }

    if (!prop.title && prop.$comment){
      prop.title = prop.$comment;
    }

    return acc;
  }, {});

  if (extraProperties){
    for(let extraProperty in extraProperties){
      schema.properties[extraProperty] = extraProperties[extraProperty];
    }
  }


  if (groups) {
    uiSchema['ui:groups'] = [];
    groups.map(group => {
      uiSchema['ui:groups'].push(group.map(({name, content}) => name || content));
      group.map(({name, width}) => {
        uiSchema[name] = {
          ...uiSchema[name],
          'ui:options': {
            width
          }
        };
      });
    });
    uiSchema["ui:order"] = ["*"];
  }


  return {
    schema,
    uiSchema
  }
}