/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
import React from 'react';
import PropTypes from 'prop-types';
import {Button, Checkbox, Form, Segment, Grid, Message} from 'semantic-ui-react'
import classnames from 'classnames';

import style from './style.less';

import {withContext} from "@mnemotix/cortex-core/client";

@withContext
export default class Login extends React.Component {
  static propTypes = {
    splashScreen: PropTypes.oneOfType([PropTypes.element, PropTypes.string]),
    lostPasswordMessage: PropTypes.oneOfType([PropTypes.element, PropTypes.string]),
    registrationEnabled: PropTypes.bool
  };

  initialState = {
    username: "",
    password: "",
    firstName: "",
    lastName: "",
    passwordValidate: "",
    firstNameError: false,
    lastNameError: false,
    usernameError: false,
    passwordError: false,
    passwordValidateError: false,
    error: null
  };


  state = {
    ...this.initialState,
    username: localStorage.getItem("username") || "",
    password: localStorage.getItem("password") || "",
    rememberMe: JSON.parse(localStorage.getItem("rememberMe")) || true,
    lostPasswordActive: false,
    registrationActive: false,
    loading: false
  };

  componentDidMount() {
    if (this.usernameInput) {
      this.usernameInput.focus();
    }
  }

  render() {
    const {t, splashScreen, lostPasswordMessage} = this.props;

    return (
      <Grid centered verticalAlign={"middle"} className={style.login}>
        <Grid.Column width={6}>
          <Segment raised attached className={style.loginPanel}>
            {splashScreen}

            {this.state.lostPasswordActive ? lostPasswordMessage ? lostPasswordMessage : (
              <Message className={style.lostPasswordMessage}>
                {t("Renseignez votre adresse electronique sur laquelle vous voulez recevoir le mail de réinitialisation de mot de passe.")}
              </Message>
            ) : null}


            <div className={style.body}>
              <Form>
                <Form.Field error={this.state.usernameError}>
                  <label>{t("Email")}</label>
                  <input ref={(input) => this.usernameInput = input}
                         value={this.state.username}
                         onChange={(e) => this.setState({username: e.target.value})}
                         name="username"
                         onKeyPress={this._handleKeyPressed}
                  />
                </Form.Field>

                {this.state.registrationActive ? (
                  <Form.Group  widths='equal'>
                    <Form.Input fluid
                                label={t('First name')}
                                value={this.state.firstName}
                                onChange={(e) => this.setState({firstName: e.target.value})}
                                error={this.state.firstNameError}
                    />
                    <Form.Input fluid
                                label={t('Last name')}
                                value={this.state.lastName}
                                onChange={(e) => this.setState({lastName: e.target.value})}
                                error={this.state.lastNameError}
                    />
                  </Form.Group>
                ) : null}

                {this.state.lostPasswordActive ? null : (
                  <Form.Input fluid
                              label={t('Password')}
                              type={"password"}
                              value={this.state.password}
                              onChange={(e) => this.setState({password: e.target.value})}
                              error={this.state.passwordError}
                              onKeyPress={this._handleKeyPressed}
                  />
                )}

                {this.state.lostPasswordActive || this.state.registrationActive ? null : (
                  <Grid className={style.options} columns={2}>
                    <Grid.Column>
                      <Checkbox className={style.rememberMe}
                                checked={this.state.rememberMe}
                                label={t("Remember me.")}
                                onChange={() => this.setState({rememberMe: !this.state.rememberMe})}
                      />
                    </Grid.Column>

                    <Grid.Column textAlign={"right"}>
                      <a className={classnames(style.lostPassword)}
                         onClick={() => this.setState({lostPasswordActive: true})}>
                        {t("Forgot password")}
                      </a>
                    </Grid.Column>
                  </Grid>
                )}

                {this.state.registrationActive ? (
                  <Form.Input fluid
                              label={t('Validate password')}
                              type={"password"}
                              value={this.state.passwordValidate}
                              onChange={(e) => this.setState({passwordValidate: e.target.value})}
                              error={this.state.passwordValidateError}
                  />
                ) : null}
              </Form>

              {this.state.error ? (
                <Message error>
                  {this.state.error}
                </Message>
              ) : null}
            </div>
          </Segment>

          {this.state.lostPasswordActive ? (
            <Button.Group attached={"bottom"}>
              <Button primary onClick={() => this.reinitPassword()} loading={this.state.loading}>
                {t("Reset password")}
              </Button>
              <Button onClick={() => this.setState({lostPasswordActive: false})}>
                {t("Cancel")}
              </Button>
            </Button.Group>
          ) : this.state.registrationActive ? (
            <Button.Group attached={"bottom"}>
              <Button primary onClick={() => this.register()} loading={this.state.loading}>
                {t("Create account")}
              </Button>
              <Button onClick={() => this.setState({registrationActive: false})}>
                {t("Cancel")}
              </Button>
            </Button.Group>
          ) : (
            <Button.Group attached={"bottom"}>
              <Button primary onClick={() => this.login()} loading={this.state.loading}>
                {t("Connect")}
              </Button>
              {this.props.registrationEnabled ? (
                <Button onClick={() => this.setState({registrationActive: true, ...this.initialState})}>
                  {t("Create account")}
                </Button>
              ) : null}
            </Button.Group>
          )}
        </Grid.Column>
      </Grid>
    )
  }

  _handleKeyPressed = (e) => {
    let {username, password} = this.state;
    let code = e.which || e.keyCode;

    if (code === 13 && username !== "" && password !== "") {
      this.login();
    }
  };

  async login() {
    let {t, router} = this.props;
    let {username, password, rememberMe} = this.state;

    if (rememberMe) {
      localStorage.setItem("rememberMe", JSON.stringify(true));
      localStorage.setItem("username", username);
      localStorage.setItem("password", password);
    } else {
      localStorage.removeItem("rememberMe");
      localStorage.removeItem("username");
      localStorage.removeItem("password");
    }

    this.setState({loading: true});
    this.props.context.showLoadingBar();

    const response = await fetch(`/auth/login`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
      credentials: 'include',
      body: JSON.stringify({username, password}),
    });

    let data = await response.json();

    this.props.context.hideLoadingBar();
    this.setState({loading: false});

    if (data.error) {
      this.setState({
        error: t(data.error)
      });
    }
  }

  async reinitPassword() {
    const {t} = this.props;
    let {username} = this.state;

    this.props.context.showLoadingBar();

    try {
      const response = await fetch(`/auth/reset-password`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        },
        credentials: 'include',
        body: JSON.stringify({username}),
      });
      this.props.context.alert(t("Vous allez recevoir un email pour réinitialiser votre mot de passe"));
    } catch (e) {
      this.props.context.alert(t("Un problème technique empêche la réinitialisation du mot passe. Veuillez réessayer ultérieurement."), {type: "error"});
    }

    this.props.context.hideLoadingBar();
    this.setState({lostPasswordActive: false});
  }

  async register(){
    const {t} = this.props;
    let {username, firstName, lastName, password, passwordValidate} = this.state;
    let errorState = {};

    errorState.usernameError = username === "";
    errorState.firstNameError =  firstName === "";
    errorState.lastNameError =  lastName === "";
    errorState.passwordError = password === "";
    errorState.passwordValidateError =  passwordValidate === "" || passwordValidate !== password;

    this.setState(errorState);

    if(!Object.values(errorState).find(value => value === true)){
      this.setState({loading: true});
      this.props.context.showLoadingBar();
      let data;

      try {
        const response = await fetch(`/auth/register`, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
          },
          credentials: 'include',
          body: JSON.stringify({username, firstName, lastName, password}),
        });

        data = await response.json();
      } catch(e){
        data = {error: e.message};
      }

      if (data.error) {
        this.setState({
          error: t(data.error)
        });
      } else {
        await this.login();
      }

      this.props.context.hideLoadingBar();
      this.setState({loading: false});
    }
  }
}