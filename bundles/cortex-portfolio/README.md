# Cortex Portfolio


This is a widget bundle to manipulate data from the Project ontology. 

Basically it provides:
 
 - Component to display/manipulate a searchable list of projects.
 - Component to display/manipulate a timeline view of a project.
 - Component 
 
 
 
## Related bundles

- @mnemotix/cortex-core
- @mnemotix/cortex-form
- @mnemotix/cortex-finder
- @mnemotix/cortex-addressbook
- @mnemotix/cortex-rich-editor


