/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *  
 */
import React from 'react';
import {withContext} from "@mnemotix/cortex-core/client";
import {withRouter, Redirect, Route, Switch} from "react-router";
import ProjectsList from "./components/project/ProjectsList/ProjectsList";
import ProjectViewer from "./components/project/ProjectViewer/ProjectViewer";

@withContext
@withRouter
class Portfolio extends React.Component {
  render() {
    const {t} = this.props;

    const {match} = this.props;

    return match.isExact ? (
      <Redirect to={this.props.context.formatRoute('Portfolio_projects')}/>
    ) : (
      <Switch>
        <Route path={this.props.context.getRoute('Portfolio_projects')} component={ProjectsList}/>
      </Switch>
    )
  }
}

export default Portfolio;