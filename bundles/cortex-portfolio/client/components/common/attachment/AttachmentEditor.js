/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *  
 */
import React from 'react';
import PropTypes from 'prop-types';
import {graphql, createRefetchContainer} from 'react-relay';
import {Route, withRouter} from 'react-router';
import {withContext, withRelay} from '@mnemotix/cortex-core/client';
import {
  Button, Modal, Image, Icon, Visibility, Segment, Table, Header, Placeholder, Grid, Input
} from 'semantic-ui-react';
import {DefaultMutations as Mutations} from "@mnemotix/synaptix.js/lib/client";
import {formatMime, isImageMime} from "@mnemotix/cortex-finder/client/helpers/formats";
import getMimeIcon from "@mnemotix/cortex-finder/client/helpers/getMimeIcon";
import numberFormat from "human-format";
import moment from "moment";
import {Link, Switch} from "react-router-dom";
import Finder from "@mnemotix/cortex-finder/client/Finder";

@withContext
@withRouter
class AttachmentEditor extends React.Component {
  static propTypes = {
    extraRelayConnectionDefinitions: PropTypes.array,
    extraRelayCountersDefinitions: PropTypes.array
  };

  static defaultProps = {
    extraRelayConnectionDefinitions: [],
    extraRelayCountersDefinitions: []
  };

  state = {
    loading: false,
    pickActorModalOpen: false,
    sortColumn: null,
    sortDirection: null,
    loadingMore: false,
    viewType: "table",
    selectedResources: []
  };

  async componentDidMount() {
    const {event} = this.props;

    let viewType = await this.props.context.getLocalStorage().getItem(`cortex-portfolio__event-${event.id}__view-type`);

    if (viewType !== this.state.viewType) {
      this.setState({
        viewType
      });
    }
  }

  render(){
    const {t, history, match} = this.props;

    return (
      <>
        {this.renderTable()}
        <Switch>
          <Route path={this.props.context.getRoute('Portfolio_project_event_attachments_add')} render={() => (
            <Modal closeIcon
                   open={true}
                   onClose={() => {
                     history.push(match.url);
                     this.setState({selectedResources: []});
                   }}>
              <Modal.Content scrolling>
                <Finder selectable={true} onSelectedResources={(selectedResources) => {
                  this.setState({selectedResources});
                }}/>
              </Modal.Content>
              <Modal.Actions>
                <Button onClick={() => {
                  history.push(match.url);
                  this.setState({selectedResources: []});
                }}>
                  {t("Cancel")}
                </Button>
                {this.state.selectedResources.length > 0 ? (
                  <Button loading={this.state.loadingCreation}  primary onClick={() => this.createAttachments()}>
                    <Icon name='add'/> {t("Create {{count}} attachments", {count: this.state.selectedResources.length})}
                  </Button>
                ) : null}
              </Modal.Actions>
            </Modal>
          )}/>
        </Switch>
      </>
    )
  }

  renderTable(){
    const {t, event, project} = this.props;

    const {sortColumn, sortDirection} = this.state;

    return (
      <>
        <Grid>
          <Grid.Column width={8}>
            <Input fluid
                   loading={this.state.loading}
                   placeholder={t('Search...')}
                   value={this.state.qs}
                   icon='search'
                   onChange={(e, {value}) => this.search(value)}
            />
          </Grid.Column>
          <Grid.Column width={4} textAlign={'center'}>
            <Button.Group basic floated={"right"}>
              <Button active={this.state.viewType === "table"} icon={"table"}
                      onClick={this._changeView.bind(this, 'table')}/>
              <Button active={this.state.viewType === "tile"} icon={"th"}
                      onClick={this._changeView.bind(this, 'tile')}/>
            </Button.Group>
          </Grid.Column>
          <Grid.Column width={4} textAlign={'right'}>
            <Button primary
                    icon={'archive'}
                    content={t("Add an attachment")}
                    as={Link}
                    to={this.props.context.formatRoute('Portfolio_project_event_attachments_add',{
                      id: project.id,
                      eventId: event.id
                    })}
            />
          </Grid.Column>
        </Grid>

        <Table sortable compact selectable basic>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell sorted={sortColumn === 'name' ? sortDirection : null}
                                onClick={() => this._handleSort('name')}>
                {t("Name")}
              </Table.HeaderCell>
              <Table.HeaderCell sorted={sortColumn === 'size' ? sortDirection : null}
                                onClick={() => this._handleSort('size')}>
                {t("Size")}
              </Table.HeaderCell>
              <Table.HeaderCell sorted={sortColumn === 'lastUpdate' ? sortDirection : null}
                                onClick={() => this._handleSort('lastUpdate')}>
                {t("Last update")}
              </Table.HeaderCell>
              <Table.HeaderCell sorted={sortColumn === 'mime' ? sortDirection : null}
                                onClick={() => this._handleSort('mime')}>
                {t("Mime")}
              </Table.HeaderCell>
            </Table.Row>
          </Table.Header>
          <Table.Body>
            {event.attachments.edges.map(({node: {resource}}, index) => {
              return (
                <Table.Row key={index}
                           onClick={() => {/*history.push(route)*/}}
                           active={false/*!!matchPath(location.pathname, route)*/}>
                  <Table.Cell>
                    <Header as='h5' image>
                      {isImageMime(resource.mime) ? (
                        <Image src={this.props.context.getImageThumbnailURL(resource.publicUrl, 50)} rounded
                               size='mini'/>
                      ) : (
                        <Icon size={"tiny"} color='grey' fitted
                              name={`file outline ${getMimeIcon(resource.mime)}`.trim()}/>
                      )}

                      <Header.Content as={'strong'}>
                        {resource.title || resource.filename}
                        <Header.Subheader>
                          {resource.title ? resource.filename : ""}
                        </Header.Subheader>
                      </Header.Content>
                    </Header>
                  </Table.Cell>
                  <Table.Cell>{numberFormat(resource.size, {
                    decimals: 1,
                    scale: 'binary',
                    unit: 'B'
                  })}</Table.Cell>
                  <Table.Cell>{moment(resource.lastUpdate).format("LL")}</Table.Cell>
                  <Table.Cell>{formatMime(resource.mime)}</Table.Cell>
                </Table.Row>
              )
            })}

            {this.state.loadingMore ? Array(4).fill().map((_, index) => (
              <Table.Row key={index}>
                <Table.Cell>
                  <Placeholder>
                    <Placeholder.Header image>
                      <Placeholder.Line/>
                      <Placeholder.Line length='medium'/>
                    </Placeholder.Header>
                  </Placeholder>
                </Table.Cell>
                <Table.Cell>
                  <Placeholder>
                    <Placeholder.Line length='small'/>
                  </Placeholder>
                </Table.Cell>
                <Table.Cell>
                  <Placeholder>
                    <Placeholder.Line length='small'/>
                  </Placeholder>
                </Table.Cell>
                <Table.Cell>
                  <Placeholder>
                    <Placeholder.Line length='small'/>
                  </Placeholder>
                </Table.Cell>
              </Table.Row>
            )) : null}

            <Visibility as={'tr'} once onUpdate={(e, {calculations: {topVisible}}) => {
              if (topVisible) {
                this.showMore();
              }
            }}/>
          </Table.Body>
        </Table>

        {event.attachments.edges.length === 0 ? (
          <Segment textAlign='center' disabled padded='very'>
            {t("This event has no attachment...")}
          </Segment>
        ) : null}
      </>
    );
  }

  _changeView = (viewType) => {
    const {event} = this.props;

    this.props.context.getLocalStorage().setItem(`cortex-portfolio__event-${event.id}__view-type`, viewType);
    this.setState({
      viewType
    });
  };

  createAttachments(){
    const {selectedResources} = this.state;
    const {extraRelayConnectionDefinitions, extraRelayCountersDefinitions} = this.props;
    const {t, relay: {environment}, event, history, match} = this.props;

    if ( selectedResources.length > 0) {
      this.setState({
        loadingCreation: true
      });
    }

    for(let [index, resource] of selectedResources.entries()){
      (new Mutations.projects.CreateAttachmentMutation({
        environment,
        parentId: event.id,
        updateCounters: extraRelayCountersDefinitions,
        connectionDefinitions: (extraRelayConnectionDefinitions || []).concat([
          {
            connectionKey: "AttachmentEditor_attachments",
          }
        ]),
        onCompleted: () => {
          if (index === selectedResources.length - 1){
            this.props.context.alert(t("{{count}} attachments has been created", {count: selectedResources.length}));
          }
        },
        onError: (errors) => {
          this.props.context.alertMutationError(errors);
        },
        onAfter: () => {
          if (index === selectedResources.length - 1) {
            this.setState({
              selectedResources: [],
              loadingCreation: false
            }, () => {
              history.push(match.url)
            })
          }
        }
      })).apply({
        extraInputs: {
          eventId: event.id,
          resourceId: resource.id
        }
      });
    }
  }

  showMore(howMany = 10) {
    const {event} = this.props;

    if (_.get(event, 'attachments.pageInfo.hasNextPage') && !this.state.loadingMore) {
      const endCursor = _.get(event, 'attachments.pageInfo.endCursor');
      const total = _.get(event, 'attachments.edges.length') + howMany;

      this.setState({
        loadingMore: true
      }, () => {

        this.props.relay.refetch((fragmentVariables) => ({
          ...fragmentVariables,
          qs: this.state.qs,
          first: howMany,
          after: endCursor,
        }), ({
          first: total
        }), () => {
          this.setState({
            loadingMore: false
          });
        });
      });
    }
  }


  _handleSort = (sortColumn) => {
    this.setState({
      sortColumn,
      sortDirection: this.state.sortDirection === 'ascending' ? 'descending' : 'ascending'
    });

    this.props.relay.refetch(() => ({
      qs: this.state.qs,
      first: 20,
      sortings: [
        {
          sortBy: sortColumn,
          sortDirection: this.state.sortDirection === 'ascending' ? 'desc' : 'asc'
        }
      ]
    }), null, () => {
      this.setState({
        loading: false
      });
    });
  };
}

let query = graphql`
  query AttachmentEditor_Query($id: ID! $qs: String $first: Int $after: String $sortings: [SortingInput]) {
    event(id: $id){
      ...AttachmentEditor_event
    }
  }
`;

export default withRelay(query,
  {
    first: 20
  },
  createRefetchContainer(AttachmentEditor, graphql`
    fragment AttachmentEditor_event on Event {
      __typename
      id
      attachments(first: $first qs: $qs after: $after sortings: $sortings) @connection(key: "AttachmentEditor_attachments", filters:[]){
        edges{
          node{
            resource{
              ...ResourceBasicFragment @relay(mask: false)
            }
          }
        }
      }
    }
  `),
  query
);