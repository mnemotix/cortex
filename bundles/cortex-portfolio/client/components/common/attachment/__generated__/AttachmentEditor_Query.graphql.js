/**
 * @flow
 * @relayHash ee589dbce7fa0a90fe5f4332569934df
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
type AttachmentEditor_event$ref = any;
export type SortingInput = {
  sortBy?: ?string,
  sortDirection?: ?string,
  sortFilters?: ?$ReadOnlyArray<?string>,
};
export type AttachmentEditor_QueryVariables = {|
  id: string,
  qs?: ?string,
  first?: ?number,
  after?: ?string,
  sortings?: ?$ReadOnlyArray<?SortingInput>,
|};
export type AttachmentEditor_QueryResponse = {|
  +event: ?{|
    +$fragmentRefs: AttachmentEditor_event$ref
  |}
|};
export type AttachmentEditor_Query = {|
  variables: AttachmentEditor_QueryVariables,
  response: AttachmentEditor_QueryResponse,
|};
*/


/*
query AttachmentEditor_Query(
  $id: ID!
  $qs: String
  $first: Int
  $after: String
  $sortings: [SortingInput]
) {
  event(id: $id) {
    ...AttachmentEditor_event
    id
  }
}

fragment AttachmentEditor_event on Event {
  __typename
  id
  attachments(first: $first, qs: $qs, after: $after, sortings: $sortings) {
    edges {
      node {
        resource {
          id
          uri
          title
          description
          filename
          creationDate
          lastUpdate
          size
          publicUrl
          mime
        }
        id
        __typename
      }
      cursor
    }
    pageInfo {
      endCursor
      hasNextPage
    }
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "id",
    "type": "ID!",
    "defaultValue": null
  },
  {
    "kind": "LocalArgument",
    "name": "qs",
    "type": "String",
    "defaultValue": null
  },
  {
    "kind": "LocalArgument",
    "name": "first",
    "type": "Int",
    "defaultValue": null
  },
  {
    "kind": "LocalArgument",
    "name": "after",
    "type": "String",
    "defaultValue": null
  },
  {
    "kind": "LocalArgument",
    "name": "sortings",
    "type": "[SortingInput]",
    "defaultValue": null
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "id",
    "variableName": "id",
    "type": "ID!"
  }
],
v2 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "__typename",
  "args": null,
  "storageKey": null
},
v3 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "id",
  "args": null,
  "storageKey": null
},
v4 = [
  {
    "kind": "Variable",
    "name": "after",
    "variableName": "after",
    "type": "String"
  },
  {
    "kind": "Variable",
    "name": "first",
    "variableName": "first",
    "type": "Int"
  },
  {
    "kind": "Variable",
    "name": "qs",
    "variableName": "qs",
    "type": "String"
  },
  {
    "kind": "Variable",
    "name": "sortings",
    "variableName": "sortings",
    "type": "[SortingInput]"
  }
];
return {
  "kind": "Request",
  "operationKind": "query",
  "name": "AttachmentEditor_Query",
  "id": null,
  "text": "query AttachmentEditor_Query(\n  $id: ID!\n  $qs: String\n  $first: Int\n  $after: String\n  $sortings: [SortingInput]\n) {\n  event(id: $id) {\n    ...AttachmentEditor_event\n    id\n  }\n}\n\nfragment AttachmentEditor_event on Event {\n  __typename\n  id\n  attachments(first: $first, qs: $qs, after: $after, sortings: $sortings) {\n    edges {\n      node {\n        resource {\n          id\n          uri\n          title\n          description\n          filename\n          creationDate\n          lastUpdate\n          size\n          publicUrl\n          mime\n        }\n        id\n        __typename\n      }\n      cursor\n    }\n    pageInfo {\n      endCursor\n      hasNextPage\n    }\n  }\n}\n",
  "metadata": {},
  "fragment": {
    "kind": "Fragment",
    "name": "AttachmentEditor_Query",
    "type": "Query",
    "metadata": null,
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "event",
        "storageKey": null,
        "args": v1,
        "concreteType": "Event",
        "plural": false,
        "selections": [
          {
            "kind": "FragmentSpread",
            "name": "AttachmentEditor_event",
            "args": null
          }
        ]
      }
    ]
  },
  "operation": {
    "kind": "Operation",
    "name": "AttachmentEditor_Query",
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "event",
        "storageKey": null,
        "args": v1,
        "concreteType": "Event",
        "plural": false,
        "selections": [
          v2,
          v3,
          {
            "kind": "LinkedField",
            "alias": null,
            "name": "attachments",
            "storageKey": null,
            "args": v4,
            "concreteType": "AttachmentConnection",
            "plural": false,
            "selections": [
              {
                "kind": "LinkedField",
                "alias": null,
                "name": "edges",
                "storageKey": null,
                "args": null,
                "concreteType": "AttachmentEdge",
                "plural": true,
                "selections": [
                  {
                    "kind": "LinkedField",
                    "alias": null,
                    "name": "node",
                    "storageKey": null,
                    "args": null,
                    "concreteType": "Attachment",
                    "plural": false,
                    "selections": [
                      {
                        "kind": "LinkedField",
                        "alias": null,
                        "name": "resource",
                        "storageKey": null,
                        "args": null,
                        "concreteType": "Resource",
                        "plural": false,
                        "selections": [
                          v3,
                          {
                            "kind": "ScalarField",
                            "alias": null,
                            "name": "uri",
                            "args": null,
                            "storageKey": null
                          },
                          {
                            "kind": "ScalarField",
                            "alias": null,
                            "name": "title",
                            "args": null,
                            "storageKey": null
                          },
                          {
                            "kind": "ScalarField",
                            "alias": null,
                            "name": "description",
                            "args": null,
                            "storageKey": null
                          },
                          {
                            "kind": "ScalarField",
                            "alias": null,
                            "name": "filename",
                            "args": null,
                            "storageKey": null
                          },
                          {
                            "kind": "ScalarField",
                            "alias": null,
                            "name": "creationDate",
                            "args": null,
                            "storageKey": null
                          },
                          {
                            "kind": "ScalarField",
                            "alias": null,
                            "name": "lastUpdate",
                            "args": null,
                            "storageKey": null
                          },
                          {
                            "kind": "ScalarField",
                            "alias": null,
                            "name": "size",
                            "args": null,
                            "storageKey": null
                          },
                          {
                            "kind": "ScalarField",
                            "alias": null,
                            "name": "publicUrl",
                            "args": null,
                            "storageKey": null
                          },
                          {
                            "kind": "ScalarField",
                            "alias": null,
                            "name": "mime",
                            "args": null,
                            "storageKey": null
                          }
                        ]
                      },
                      v3,
                      v2
                    ]
                  },
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "cursor",
                    "args": null,
                    "storageKey": null
                  }
                ]
              },
              {
                "kind": "LinkedField",
                "alias": null,
                "name": "pageInfo",
                "storageKey": null,
                "args": null,
                "concreteType": "PageInfo",
                "plural": false,
                "selections": [
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "endCursor",
                    "args": null,
                    "storageKey": null
                  },
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "hasNextPage",
                    "args": null,
                    "storageKey": null
                  }
                ]
              }
            ]
          },
          {
            "kind": "LinkedHandle",
            "alias": null,
            "name": "attachments",
            "args": v4,
            "handle": "connection",
            "key": "AttachmentEditor_attachments",
            "filters": []
          }
        ]
      }
    ]
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = 'c7a9a93b52ab306e9bc4cb1ddee884ae';
module.exports = node;
