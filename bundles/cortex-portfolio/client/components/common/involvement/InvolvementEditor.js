/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *  
 */
import React from 'react';
import PropTypes from 'prop-types';
import {graphql, createFragmentContainer} from 'react-relay';
import {withRouter} from 'react-router';
import {Modules, withContext, withRelay} from '@mnemotix/cortex-core/client';
import {Button, Container, Modal, Label, Input} from 'semantic-ui-react';
import {generateUIFormPropsForJSONSchemaMutation, FormConnection, FormMutationDefinition} from "@mnemotix/cortex-form/client";
import {DefaultMutations as Mutations} from "@mnemotix/synaptix.js/lib/client";
import {AddressBookSelector} from "@mnemotix/cortex-addressbook/client";
import style from './InvolvementEditor.less';

@withContext
@withRouter
class InvolvementEditor extends React.Component {
  static propTypes = {
    extraRelayConnectionDefinitions: PropTypes.array,
    extraRelayCountersDefinitions: PropTypes.array
  };

  static defaultProps = {
    extraRelayConnectionDefinitions: [],
    extraRelayCountersDefinitions: []
  };

  state = {
    loading: false,
    pickActorModalOpen: false,
    selectedActor: null,
  };

  render(){
    const {t, event, relay:{environment}, extraRelayConnectionDefinitions, extraRelayCountersDefinitions} = this.props;

    const {schema, uiSchema} = generateUIFormPropsForJSONSchemaMutation({
      JSONSchema: this.props.context.getJSONSchema(),
      mutationName: 'updateInvolvement',
      extraProperties: {
        involvedActor : {
          title: t("Actor"),
          type: 'custom',
          render: this.renderTargetAvatar.bind(this)
        }
      },
      groups: [
        [
          {name: 'involvedActor', width: 4},
          {name: 'startDate', width: 4},
          {name: 'endDate', width: 4},
          {name: 'role', width: 4}
        ]
      ]
    });

    let connectionKeys = extraRelayConnectionDefinitions.map(({connectionKey}) => connectionKey).concat(['InvolvementEditor_involvements']);
    return (
      <Container>
        <FormConnection listItemOptions={{icon: "id badge"}}
                        nodeSchema={schema}
                        nodeUiSchema={uiSchema}
                        formData={event.involvements}
                        submitButtonLabel={t("Update an involvement")}
                        relayUpdateNodeMutation={this.getUpdateMutation()}
                        relayAddNodeMutation={this.getCreateMutation()}
                        nodeExtraActions={(involvement) => (
                          <Modules.RemoveObjectButton buttonProps={{negative: true}}
                                                      objectRecord={involvement}
                                                      parentRecord={event}
                                                      connectionKeys={connectionKeys}
                                                      confirmMessage={t("Do you really want to remove this involvement ?")}
                                                      successMessage={t("The involvement has been removed")}
                                                      updateCounters={extraRelayCountersDefinitions}
                          />
                        )}
        />
        {this.state.selectedActor ? (
          <div>

          </div>
        ) : (
          <Modal trigger={(
                  <Container textAlign={"center"}>
                    <Button onClick={() => this.setState({pickActorModalOpen: true})}>
                      {("Create involvement")}
                    </Button>
                  </Container>
                )}
                 open={this.state.pickActorModalOpen}
                 onClose={() => this.setState({pickActorModalOpen: false})}
          >
            <Modal.Header>
              {t("Select an actor to involve in event {{title}}", {title: event.title})}
            </Modal.Header>
            <Modal.Content>
              <AddressBookSelector variables={{filterOn: "Person"}}
                                   onSelectActor={(selectedActor) => {
                                     this.setState({
                                       pickActorModalOpen: false,
                                       selectedActor
                                     })
                                   }}
              />
            </Modal.Content>
          </Modal>
        )}
      </Container>
    );
  }

  renderTargetAvatar(involvement){
    const {t} = this.props;
    let actor;

    if (involvement.id) {
      actor = involvement.actor;
    } else {
      actor = this.state.selectedActor;
    }

    return (
      <Label className={style.actor} content={actor.displayName} size='small' image={{
        avatar: true,
        spaced: 'right',
        src: actor.avatar
      }}/>
    )
  }

  getUpdateMutation(){
    const {t, relay: {environment}} = this.props;

    if (!this.updateMutation){
      this.updateMutation = new FormMutationDefinition({
        mutation: new Mutations.projects.UpdateInvolvementMutation({
          environment
        }),
        successMessage: t("Involvement has been updated.")
      });
    }

    return this.updateMutation;
  }

  getCreateMutation(){
    const {extraRelayConnectionDefinitions, extraRelayCountersDefinitions} = this.props;

    if (this.state.selectedActor){
      const {t, relay: {environment}, event} = this.props;

      return new FormMutationDefinition({
        mutation: new Mutations.projects.CreateInvolvementMutation({
          environment,
          parentId: event.id,
          connectionDefinitions: (extraRelayConnectionDefinitions || []).concat([
            {
              connectionKey: "InvolvementEditor_involvements"
            }
          ]),
          updateCounters: extraRelayCountersDefinitions
        }),
        successMessage: t("Involvement has been created."),
        extraInputs: {
          actorId: this.state.selectedActor.id,
          eventId: event.id
        }
      });
    }
  }
}

export default withRelay(
  graphql`
    query InvolvementEditor_Query($id: ID!) {
      event(id: $id){
        ...InvolvementEditor_event
      }
    }
  `,
  {},
  createFragmentContainer(InvolvementEditor, graphql`
    fragment InvolvementEditor_event on Event {
      __typename
      id
      involvements(first: 50) @connection(key: "InvolvementEditor_involvements", filters:[]){
        edges{
          node{
            ...InvolvementBasicFragment @relay(mask: false)
            actor{
              id
              displayName
              avatar
            }
          }
        }
      }
    }
  `)
);