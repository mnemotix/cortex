/**
 * @flow
 * @relayHash 86df720259b47f9f211dd7e198ed5ec0
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
type InvolvementEditor_event$ref = any;
export type InvolvementEditor_QueryVariables = {|
  id: string
|};
export type InvolvementEditor_QueryResponse = {|
  +event: ?{|
    +$fragmentRefs: InvolvementEditor_event$ref
  |}
|};
export type InvolvementEditor_Query = {|
  variables: InvolvementEditor_QueryVariables,
  response: InvolvementEditor_QueryResponse,
|};
*/


/*
query InvolvementEditor_Query(
  $id: ID!
) {
  event(id: $id) {
    ...InvolvementEditor_event
    id
  }
}

fragment InvolvementEditor_event on Event {
  __typename
  id
  involvements(first: 50) {
    edges {
      node {
        id
        uri
        role
        startDate
        endDate
        actor {
          __typename
          id
          uri
          displayName
          avatar
        }
        event {
          id
          uri
          title
          description
          shortDescription
          endDate
          startDate
          creationDate
          lastUpdate
          creator {
            ... on ActorInterface {
              id
              uri
              displayName
              avatar
            }
            id
          }
        }
        project {
          id
          uri
          color
          image
          title
          description
          shortDescription
          creationDate
          lastUpdate
          creator {
            ... on ActorInterface {
              id
              uri
              displayName
              avatar
            }
            id
          }
        }
        __typename
      }
      cursor
    }
    pageInfo {
      endCursor
      hasNextPage
    }
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "id",
    "type": "ID!",
    "defaultValue": null
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "id",
    "variableName": "id",
    "type": "ID!"
  }
],
v2 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "__typename",
  "args": null,
  "storageKey": null
},
v3 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "id",
  "args": null,
  "storageKey": null
},
v4 = [
  {
    "kind": "Literal",
    "name": "first",
    "value": 50,
    "type": "Int"
  }
],
v5 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "uri",
  "args": null,
  "storageKey": null
},
v6 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "startDate",
  "args": null,
  "storageKey": null
},
v7 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "endDate",
  "args": null,
  "storageKey": null
},
v8 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "displayName",
  "args": null,
  "storageKey": null
},
v9 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "avatar",
  "args": null,
  "storageKey": null
},
v10 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "title",
  "args": null,
  "storageKey": null
},
v11 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "description",
  "args": null,
  "storageKey": null
},
v12 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "shortDescription",
  "args": null,
  "storageKey": null
},
v13 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "creationDate",
  "args": null,
  "storageKey": null
},
v14 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "lastUpdate",
  "args": null,
  "storageKey": null
},
v15 = {
  "kind": "LinkedField",
  "alias": null,
  "name": "creator",
  "storageKey": null,
  "args": null,
  "concreteType": "Person",
  "plural": false,
  "selections": [
    v3,
    v5,
    v8,
    v9
  ]
};
return {
  "kind": "Request",
  "operationKind": "query",
  "name": "InvolvementEditor_Query",
  "id": null,
  "text": "query InvolvementEditor_Query(\n  $id: ID!\n) {\n  event(id: $id) {\n    ...InvolvementEditor_event\n    id\n  }\n}\n\nfragment InvolvementEditor_event on Event {\n  __typename\n  id\n  involvements(first: 50) {\n    edges {\n      node {\n        id\n        uri\n        role\n        startDate\n        endDate\n        actor {\n          __typename\n          id\n          uri\n          displayName\n          avatar\n        }\n        event {\n          id\n          uri\n          title\n          description\n          shortDescription\n          endDate\n          startDate\n          creationDate\n          lastUpdate\n          creator {\n            ... on ActorInterface {\n              id\n              uri\n              displayName\n              avatar\n            }\n            id\n          }\n        }\n        project {\n          id\n          uri\n          color\n          image\n          title\n          description\n          shortDescription\n          creationDate\n          lastUpdate\n          creator {\n            ... on ActorInterface {\n              id\n              uri\n              displayName\n              avatar\n            }\n            id\n          }\n        }\n        __typename\n      }\n      cursor\n    }\n    pageInfo {\n      endCursor\n      hasNextPage\n    }\n  }\n}\n",
  "metadata": {},
  "fragment": {
    "kind": "Fragment",
    "name": "InvolvementEditor_Query",
    "type": "Query",
    "metadata": null,
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "event",
        "storageKey": null,
        "args": v1,
        "concreteType": "Event",
        "plural": false,
        "selections": [
          {
            "kind": "FragmentSpread",
            "name": "InvolvementEditor_event",
            "args": null
          }
        ]
      }
    ]
  },
  "operation": {
    "kind": "Operation",
    "name": "InvolvementEditor_Query",
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "event",
        "storageKey": null,
        "args": v1,
        "concreteType": "Event",
        "plural": false,
        "selections": [
          v2,
          v3,
          {
            "kind": "LinkedField",
            "alias": null,
            "name": "involvements",
            "storageKey": "involvements(first:50)",
            "args": v4,
            "concreteType": "InvolvementConnection",
            "plural": false,
            "selections": [
              {
                "kind": "LinkedField",
                "alias": null,
                "name": "edges",
                "storageKey": null,
                "args": null,
                "concreteType": "InvolvementEdge",
                "plural": true,
                "selections": [
                  {
                    "kind": "LinkedField",
                    "alias": null,
                    "name": "node",
                    "storageKey": null,
                    "args": null,
                    "concreteType": "Involvement",
                    "plural": false,
                    "selections": [
                      v3,
                      v5,
                      {
                        "kind": "ScalarField",
                        "alias": null,
                        "name": "role",
                        "args": null,
                        "storageKey": null
                      },
                      v6,
                      v7,
                      {
                        "kind": "LinkedField",
                        "alias": null,
                        "name": "actor",
                        "storageKey": null,
                        "args": null,
                        "concreteType": null,
                        "plural": false,
                        "selections": [
                          v2,
                          v3,
                          v5,
                          v8,
                          v9
                        ]
                      },
                      {
                        "kind": "LinkedField",
                        "alias": null,
                        "name": "event",
                        "storageKey": null,
                        "args": null,
                        "concreteType": "Event",
                        "plural": false,
                        "selections": [
                          v3,
                          v5,
                          v10,
                          v11,
                          v12,
                          v7,
                          v6,
                          v13,
                          v14,
                          v15
                        ]
                      },
                      {
                        "kind": "LinkedField",
                        "alias": null,
                        "name": "project",
                        "storageKey": null,
                        "args": null,
                        "concreteType": "Project",
                        "plural": false,
                        "selections": [
                          v3,
                          v5,
                          {
                            "kind": "ScalarField",
                            "alias": null,
                            "name": "color",
                            "args": null,
                            "storageKey": null
                          },
                          {
                            "kind": "ScalarField",
                            "alias": null,
                            "name": "image",
                            "args": null,
                            "storageKey": null
                          },
                          v10,
                          v11,
                          v12,
                          v13,
                          v14,
                          v15
                        ]
                      },
                      v2
                    ]
                  },
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "cursor",
                    "args": null,
                    "storageKey": null
                  }
                ]
              },
              {
                "kind": "LinkedField",
                "alias": null,
                "name": "pageInfo",
                "storageKey": null,
                "args": null,
                "concreteType": "PageInfo",
                "plural": false,
                "selections": [
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "endCursor",
                    "args": null,
                    "storageKey": null
                  },
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "hasNextPage",
                    "args": null,
                    "storageKey": null
                  }
                ]
              }
            ]
          },
          {
            "kind": "LinkedHandle",
            "alias": null,
            "name": "involvements",
            "args": v4,
            "handle": "connection",
            "key": "InvolvementEditor_involvements",
            "filters": []
          }
        ]
      }
    ]
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = 'd9e4df3dbd88b9747e7f23886b03e1e5';
module.exports = node;
