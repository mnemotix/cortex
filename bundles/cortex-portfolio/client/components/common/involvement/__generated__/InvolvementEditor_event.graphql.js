/**
 * @flow
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteFragment } from 'relay-runtime';
import type { FragmentReference } from "relay-runtime";
declare export opaque type InvolvementEditor_event$ref: FragmentReference;
export type InvolvementEditor_event = {|
  +id: string,
  +involvements: ?{|
    +edges: ?$ReadOnlyArray<?{|
      +node: ?{|
        +id: string,
        +uri: ?string,
        +role: ?string,
        +startDate: ?number,
        +endDate: ?number,
        +actor: ?{|
          +id: string,
          +uri: ?string,
          +displayName: ?string,
          +avatar: ?string,
        |},
        +event: ?{|
          +id: string,
          +uri: ?string,
          +title: ?string,
          +description: ?string,
          +shortDescription: ?string,
          +endDate: ?number,
          +startDate: ?number,
          +creationDate: ?number,
          +lastUpdate: ?number,
          +creator: ?{|
            +id?: string,
            +uri?: ?string,
            +displayName?: ?string,
            +avatar?: ?string,
          |},
        |},
        +project: ?{|
          +id: string,
          +uri: ?string,
          +color: ?string,
          +image: ?string,
          +title: ?string,
          +description: ?string,
          +shortDescription: ?string,
          +creationDate: ?number,
          +lastUpdate: ?number,
          +creator: ?{|
            +id?: string,
            +uri?: ?string,
            +displayName?: ?string,
            +avatar?: ?string,
          |},
        |},
      |}
    |}>
  |},
  +__typename: "Event",
  +$refType: InvolvementEditor_event$ref,
|};
*/


const node/*: ConcreteFragment*/ = (function(){
var v0 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "__typename",
  "args": null,
  "storageKey": null
},
v1 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "id",
  "args": null,
  "storageKey": null
},
v2 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "uri",
  "args": null,
  "storageKey": null
},
v3 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "startDate",
  "args": null,
  "storageKey": null
},
v4 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "endDate",
  "args": null,
  "storageKey": null
},
v5 = [
  v1,
  v2,
  {
    "kind": "ScalarField",
    "alias": null,
    "name": "displayName",
    "args": null,
    "storageKey": null
  },
  {
    "kind": "ScalarField",
    "alias": null,
    "name": "avatar",
    "args": null,
    "storageKey": null
  }
],
v6 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "title",
  "args": null,
  "storageKey": null
},
v7 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "description",
  "args": null,
  "storageKey": null
},
v8 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "shortDescription",
  "args": null,
  "storageKey": null
},
v9 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "creationDate",
  "args": null,
  "storageKey": null
},
v10 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "lastUpdate",
  "args": null,
  "storageKey": null
},
v11 = {
  "kind": "LinkedField",
  "alias": null,
  "name": "creator",
  "storageKey": null,
  "args": null,
  "concreteType": "Person",
  "plural": false,
  "selections": v5
};
return {
  "kind": "Fragment",
  "name": "InvolvementEditor_event",
  "type": "Event",
  "metadata": {
    "connection": [
      {
        "count": null,
        "cursor": null,
        "direction": "forward",
        "path": [
          "involvements"
        ]
      }
    ]
  },
  "argumentDefinitions": [],
  "selections": [
    v0,
    v1,
    {
      "kind": "LinkedField",
      "alias": "involvements",
      "name": "__InvolvementEditor_involvements_connection",
      "storageKey": null,
      "args": null,
      "concreteType": "InvolvementConnection",
      "plural": false,
      "selections": [
        {
          "kind": "LinkedField",
          "alias": null,
          "name": "edges",
          "storageKey": null,
          "args": null,
          "concreteType": "InvolvementEdge",
          "plural": true,
          "selections": [
            {
              "kind": "LinkedField",
              "alias": null,
              "name": "node",
              "storageKey": null,
              "args": null,
              "concreteType": "Involvement",
              "plural": false,
              "selections": [
                v1,
                v2,
                {
                  "kind": "ScalarField",
                  "alias": null,
                  "name": "role",
                  "args": null,
                  "storageKey": null
                },
                v3,
                v4,
                {
                  "kind": "LinkedField",
                  "alias": null,
                  "name": "actor",
                  "storageKey": null,
                  "args": null,
                  "concreteType": null,
                  "plural": false,
                  "selections": v5
                },
                {
                  "kind": "LinkedField",
                  "alias": null,
                  "name": "event",
                  "storageKey": null,
                  "args": null,
                  "concreteType": "Event",
                  "plural": false,
                  "selections": [
                    v1,
                    v2,
                    v6,
                    v7,
                    v8,
                    v4,
                    v3,
                    v9,
                    v10,
                    v11
                  ]
                },
                {
                  "kind": "LinkedField",
                  "alias": null,
                  "name": "project",
                  "storageKey": null,
                  "args": null,
                  "concreteType": "Project",
                  "plural": false,
                  "selections": [
                    v1,
                    v2,
                    {
                      "kind": "ScalarField",
                      "alias": null,
                      "name": "color",
                      "args": null,
                      "storageKey": null
                    },
                    {
                      "kind": "ScalarField",
                      "alias": null,
                      "name": "image",
                      "args": null,
                      "storageKey": null
                    },
                    v6,
                    v7,
                    v8,
                    v9,
                    v10,
                    v11
                  ]
                },
                v0
              ]
            },
            {
              "kind": "ScalarField",
              "alias": null,
              "name": "cursor",
              "args": null,
              "storageKey": null
            }
          ]
        },
        {
          "kind": "LinkedField",
          "alias": null,
          "name": "pageInfo",
          "storageKey": null,
          "args": null,
          "concreteType": "PageInfo",
          "plural": false,
          "selections": [
            {
              "kind": "ScalarField",
              "alias": null,
              "name": "endCursor",
              "args": null,
              "storageKey": null
            },
            {
              "kind": "ScalarField",
              "alias": null,
              "name": "hasNextPage",
              "args": null,
              "storageKey": null
            }
          ]
        }
      ]
    }
  ]
};
})();
// prettier-ignore
(node/*: any*/).hash = '27cf833160158d921787ddb5e1408aa2';
module.exports = node;
