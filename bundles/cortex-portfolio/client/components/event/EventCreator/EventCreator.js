/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *  
 */
import React from 'react';
import PropTypes from 'prop-types';
import {graphql, createFragmentContainer} from 'react-relay';
import {withRouter} from 'react-router';
import {withContext, withRelay} from '@mnemotix/cortex-core/client';
import {generateUIFormPropsForJSONSchemaMutation, Form, FormMutationDefinition} from "@mnemotix/cortex-form/client";
import {DefaultMutations as Mutations} from "@mnemotix/synaptix.js/lib/client";
import {Grid, Modal} from 'semantic-ui-react';

@withContext
@withRouter
class EventCreator extends React.Component {
  static propTypes = {
    extraRelayConnectionDefinitions: PropTypes.array,
    extraRelayCountersDefinitions: PropTypes.array,
    onCompleted: PropTypes.func
  };

  static defaultProps = {
    extraRelayConnectionDefinitions: [],
    extraRelayCountersDefinitions: []
  };

  state = {
    loading: false,
  };

  render() {
    const {t, project, relay: {environment}, onCompleted, extraRelayConnectionDefinitions, extraRelayCountersDefinitions} = this.props;

    const {schema, uiSchema} = generateUIFormPropsForJSONSchemaMutation({
      JSONSchema: this.props.context.getJSONSchema(),
      mutationName: 'updateEvent',
      groups: [
        [
          {name: 'startDate', width: 8},
          {name: 'endDate', width: 8}
        ]
      ]
    });

    return (
      <Form schema={schema}
            uiSchema={uiSchema}
            formData={{}}
            relayMutation={new FormMutationDefinition({
              mutation: new Mutations.projects.CreateEventMutation({
                environment,
                parentId: project.id,
                updateCounters: extraRelayCountersDefinitions || [],
                connectionDefinitions: extraRelayConnectionDefinitions || [],
                onCompleted: (event) => {
                  if (onCompleted) {
                    onCompleted(event);
                  }
                }
              }),
              successMessage: t("Event has been created."),
              extraInputs: {
                projectId: project.id
              }
            })}
      />
    );
  }
}

export default withRelay(
  graphql`
    query EventCreator_Query($projectId: ID!){
      project(id: $projectId) {
        ...EventCreator_project
      }
    }
  `,
  {},
  createFragmentContainer(EventCreator, graphql`
    fragment EventCreator_project on Project {
      id
    }
  `)
);