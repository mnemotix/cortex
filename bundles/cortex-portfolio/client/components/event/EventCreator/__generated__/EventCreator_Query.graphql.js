/**
 * @flow
 * @relayHash 034e68112bb836b2689ec0e6301f1338
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
type EventCreator_project$ref = any;
export type EventCreator_QueryVariables = {|
  projectId: string
|};
export type EventCreator_QueryResponse = {|
  +project: ?{|
    +$fragmentRefs: EventCreator_project$ref
  |}
|};
export type EventCreator_Query = {|
  variables: EventCreator_QueryVariables,
  response: EventCreator_QueryResponse,
|};
*/


/*
query EventCreator_Query(
  $projectId: ID!
) {
  project(id: $projectId) {
    ...EventCreator_project
    id
  }
}

fragment EventCreator_project on Project {
  id
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "projectId",
    "type": "ID!",
    "defaultValue": null
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "id",
    "variableName": "projectId",
    "type": "ID!"
  }
];
return {
  "kind": "Request",
  "operationKind": "query",
  "name": "EventCreator_Query",
  "id": null,
  "text": "query EventCreator_Query(\n  $projectId: ID!\n) {\n  project(id: $projectId) {\n    ...EventCreator_project\n    id\n  }\n}\n\nfragment EventCreator_project on Project {\n  id\n}\n",
  "metadata": {},
  "fragment": {
    "kind": "Fragment",
    "name": "EventCreator_Query",
    "type": "Query",
    "metadata": null,
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "project",
        "storageKey": null,
        "args": v1,
        "concreteType": "Project",
        "plural": false,
        "selections": [
          {
            "kind": "FragmentSpread",
            "name": "EventCreator_project",
            "args": null
          }
        ]
      }
    ]
  },
  "operation": {
    "kind": "Operation",
    "name": "EventCreator_Query",
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "project",
        "storageKey": null,
        "args": v1,
        "concreteType": "Project",
        "plural": false,
        "selections": [
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "id",
            "args": null,
            "storageKey": null
          }
        ]
      }
    ]
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = 'f3752aea345b151dcbc11e2827780b5e';
module.exports = node;
