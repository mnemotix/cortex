/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *  
 */
import React from 'react';
import PropTypes from 'prop-types';
import {graphql, createFragmentContainer} from 'react-relay';
import {withRouter} from 'react-router';
import {withContext, withRelay} from '@mnemotix/cortex-core/client';
import {generateUIFormPropsForJSONSchemaMutation, Form, FormMutationDefinition} from "@mnemotix/cortex-form/client";
import {DefaultMutations as Mutations} from "@mnemotix/synaptix.js/lib/client";
import {Grid} from 'semantic-ui-react';
import InvolvementEditor from "../../common/involvement/InvolvementEditor";

@withContext
@withRouter
class EventEditor extends React.Component {
  static propTypes = {
    onCompleted: PropTypes.func
  };

  state = {
    loading: false,
  };

  render() {
    const {t, event, relay: {environment}, onCompleted} = this.props;

    const {schema, uiSchema} = generateUIFormPropsForJSONSchemaMutation({
      JSONSchema: this.props.context.getJSONSchema(),
      mutationName: 'updateEvent'
    });

    return (
      <Form schema={schema}
            uiSchema={uiSchema}
            formData={event}
            relayMutation={new FormMutationDefinition({
              mutation: new Mutations.projects.UpdateEventMutation({
                environment,
                onCompleted: (actor) => {
                  if (onCompleted) {
                    onCompleted(actor);
                  }
                }
              }),
              successMessage: t("Event has been updated."),
            })}
      />
    );
  }
}

export default withRelay(
  graphql`
    query EventEditor_Query($id: ID!){
      event(id: $id) {
        ...EventEditor_event
      }
    }
  `,
  {},
  createFragmentContainer(EventEditor, graphql`
    fragment EventEditor_event on Event {
      ...EventBasicFragment @relay(mask: false)
    }
  `)
);