/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *  
 */
import React from 'react';
import PropTypes from 'prop-types';
import {graphql, createFragmentContainer} from 'react-relay';
import {withRouter} from 'react-router';
import {withContext, withRelay} from '@mnemotix/cortex-core/client';
import {generateUIFormPropsForJSONSchemaMutation, Form, FormMutationDefinition} from "@mnemotix/cortex-form/client";
import {DefaultMutations as Mutations} from "@mnemotix/synaptix.js/lib/client";

@withContext
@withRouter
class ProjectCreator extends React.Component {
  static propTypes = {
    onCompleted: PropTypes.func
  };

  state = {
    loading: false,
  };

  render(){
    const {t, project, portfolio, relay: {environment}, history, me, onCompleted} = this.props;

    const {schema, uiSchema} = generateUIFormPropsForJSONSchemaMutation({
      JSONSchema: this.props.context.getJSONSchema(),
      mutationName: 'updateProject'
    });

    return (
      <Form schema={schema}
            uiSchema={uiSchema}
            formData={project}
            relayMutation={new FormMutationDefinition({
              mutation: new Mutations.projects.CreateProjectMutation({
                environment,
                parentId: portfolio.id,
                connectionKey: "Portfolio_projects",
                onCompleted: (actor) => {
                  if (onCompleted){
                    onCompleted(actor);
                  } else {
                    history.push(this.props.context.formatRoute('Portfolio_project', {id: actor.id}));
                  }
                }
              }),
              successMessage: t("Project has been created."),
              extraInputs:{
                creatorId: me.id
              }
            })}
      />
    );
  }
}

export default withRelay(
  graphql`
    query ProjectCreator_Query{
      me {
        ...ProjectCreator_me
      }
      
      portfolio{
        ...ProjectCreator_portfolio
      }
    }
  `,
  {},
  createFragmentContainer(ProjectCreator, graphql`
    fragment ProjectCreator_me on Person {
      id
    }
    
    fragment ProjectCreator_portfolio on Portfolio {
      id
    }
  `)
);