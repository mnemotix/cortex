/**
 * @flow
 * @relayHash ef9f4a42cc1a95508a79b4f104abd148
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
type ProjectCreator_me$ref = any;
type ProjectCreator_portfolio$ref = any;
export type ProjectCreator_QueryVariables = {||};
export type ProjectCreator_QueryResponse = {|
  +me: ?{|
    +$fragmentRefs: ProjectCreator_me$ref
  |},
  +portfolio: ?{|
    +$fragmentRefs: ProjectCreator_portfolio$ref
  |},
|};
export type ProjectCreator_Query = {|
  variables: ProjectCreator_QueryVariables,
  response: ProjectCreator_QueryResponse,
|};
*/


/*
query ProjectCreator_Query {
  me {
    ...ProjectCreator_me
    id
  }
  portfolio {
    ...ProjectCreator_portfolio
    id
  }
}

fragment ProjectCreator_me on Person {
  id
}

fragment ProjectCreator_portfolio on Portfolio {
  id
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "ScalarField",
    "alias": null,
    "name": "id",
    "args": null,
    "storageKey": null
  }
];
return {
  "kind": "Request",
  "operationKind": "query",
  "name": "ProjectCreator_Query",
  "id": null,
  "text": "query ProjectCreator_Query {\n  me {\n    ...ProjectCreator_me\n    id\n  }\n  portfolio {\n    ...ProjectCreator_portfolio\n    id\n  }\n}\n\nfragment ProjectCreator_me on Person {\n  id\n}\n\nfragment ProjectCreator_portfolio on Portfolio {\n  id\n}\n",
  "metadata": {},
  "fragment": {
    "kind": "Fragment",
    "name": "ProjectCreator_Query",
    "type": "Query",
    "metadata": null,
    "argumentDefinitions": [],
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "me",
        "storageKey": null,
        "args": null,
        "concreteType": "Person",
        "plural": false,
        "selections": [
          {
            "kind": "FragmentSpread",
            "name": "ProjectCreator_me",
            "args": null
          }
        ]
      },
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "portfolio",
        "storageKey": null,
        "args": null,
        "concreteType": "Portfolio",
        "plural": false,
        "selections": [
          {
            "kind": "FragmentSpread",
            "name": "ProjectCreator_portfolio",
            "args": null
          }
        ]
      }
    ]
  },
  "operation": {
    "kind": "Operation",
    "name": "ProjectCreator_Query",
    "argumentDefinitions": [],
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "me",
        "storageKey": null,
        "args": null,
        "concreteType": "Person",
        "plural": false,
        "selections": v0
      },
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "portfolio",
        "storageKey": null,
        "args": null,
        "concreteType": "Portfolio",
        "plural": false,
        "selections": v0
      }
    ]
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = '61bd968388848ed4c51343340535f82e';
module.exports = node;
