/**
 * @flow
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteFragment } from 'relay-runtime';
import type { FragmentReference } from "relay-runtime";
declare export opaque type ProjectCreator_portfolio$ref: FragmentReference;
export type ProjectCreator_portfolio = {|
  +id: string,
  +$refType: ProjectCreator_portfolio$ref,
|};
*/


const node/*: ConcreteFragment*/ = {
  "kind": "Fragment",
  "name": "ProjectCreator_portfolio",
  "type": "Portfolio",
  "metadata": null,
  "argumentDefinitions": [],
  "selections": [
    {
      "kind": "ScalarField",
      "alias": null,
      "name": "id",
      "args": null,
      "storageKey": null
    }
  ]
};
// prettier-ignore
(node/*: any*/).hash = 'f88b53e11e381cda1771faa19a36553d';
module.exports = node;
