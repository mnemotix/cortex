/**
 * @flow
 * @relayHash 9226e4fc7fd2b30a1cdefafeaabbc164
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
type ProjectEditor_project$ref = any;
export type ProjectEditor_QueryVariables = {|
  id: string
|};
export type ProjectEditor_QueryResponse = {|
  +project: ?{|
    +$fragmentRefs: ProjectEditor_project$ref
  |}
|};
export type ProjectEditor_Query = {|
  variables: ProjectEditor_QueryVariables,
  response: ProjectEditor_QueryResponse,
|};
*/


/*
query ProjectEditor_Query(
  $id: ID!
) {
  project(id: $id) {
    ...ProjectEditor_project
    id
  }
}

fragment ProjectEditor_project on Project {
  id
  uri
  color
  image
  title
  description
  shortDescription
  creationDate
  lastUpdate
  creator {
    ... on ActorInterface {
      id
      uri
      displayName
      avatar
    }
    id
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "id",
    "type": "ID!",
    "defaultValue": null
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "id",
    "variableName": "id",
    "type": "ID!"
  }
],
v2 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "id",
  "args": null,
  "storageKey": null
},
v3 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "uri",
  "args": null,
  "storageKey": null
};
return {
  "kind": "Request",
  "operationKind": "query",
  "name": "ProjectEditor_Query",
  "id": null,
  "text": "query ProjectEditor_Query(\n  $id: ID!\n) {\n  project(id: $id) {\n    ...ProjectEditor_project\n    id\n  }\n}\n\nfragment ProjectEditor_project on Project {\n  id\n  uri\n  color\n  image\n  title\n  description\n  shortDescription\n  creationDate\n  lastUpdate\n  creator {\n    ... on ActorInterface {\n      id\n      uri\n      displayName\n      avatar\n    }\n    id\n  }\n}\n",
  "metadata": {},
  "fragment": {
    "kind": "Fragment",
    "name": "ProjectEditor_Query",
    "type": "Query",
    "metadata": null,
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "project",
        "storageKey": null,
        "args": v1,
        "concreteType": "Project",
        "plural": false,
        "selections": [
          {
            "kind": "FragmentSpread",
            "name": "ProjectEditor_project",
            "args": null
          }
        ]
      }
    ]
  },
  "operation": {
    "kind": "Operation",
    "name": "ProjectEditor_Query",
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "project",
        "storageKey": null,
        "args": v1,
        "concreteType": "Project",
        "plural": false,
        "selections": [
          v2,
          v3,
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "color",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "image",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "title",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "description",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "shortDescription",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "creationDate",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "lastUpdate",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "LinkedField",
            "alias": null,
            "name": "creator",
            "storageKey": null,
            "args": null,
            "concreteType": "Person",
            "plural": false,
            "selections": [
              v2,
              v3,
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "displayName",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "avatar",
                "args": null,
                "storageKey": null
              }
            ]
          }
        ]
      }
    ]
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = '79321e5024477d6e9ec864acc5c032e8';
module.exports = node;
