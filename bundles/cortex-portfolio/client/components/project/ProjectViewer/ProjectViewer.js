/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *  
 */
import React from 'react';
import PropTypes from 'prop-types';
import {graphql, createFragmentContainer} from 'react-relay';
import {withContext, withRelay} from '@mnemotix/cortex-core/client';
import {withRouter, matchPath, Switch, Route, Redirect} from 'react-router';
import {Link} from 'react-router-dom';
import {Button, Container, Segment, Grid, Header, Sticky, Image, Placeholder, Modal, Menu} from 'semantic-ui-react';
import _ from "lodash";
import moment from "moment";
import style from './ProjectViewer.less';
import classnames from "classnames";
import ImageLoader from "react-loading-image";
import ProjectEditor from "../ProjectEditor/ProjectEditor";
import ProjectCreator from "../ProjectCreator/ProjectCreator";
import ActorAvatar from "@mnemotix/cortex-addressbook/client/components/common/avatar/ActorAvatar";
import ProjectStats from "./components/stats/ProjectStats";
import ProjectTimeline from "./components/timeline/ProjectTimeline";
import EventCreator from "../../event/EventCreator/EventCreator";

@withContext
@withRouter
class ProjectViewer extends React.Component {
  static propTypes = {
    onCompleted: PropTypes.func
  };

  headingSegmentRef = React.createRef();

  state = {
    loading: false,
    headingSegmentRef: null
  };

  render() {
    const {t, project, match, history, location} = this.props;

    return (
      <>
        <Grid stretched columns={2} relaxed stackable>
          <Grid.Column width={6}>
            <Sticky context={this.headingSegmentRef.current} offset={20}>
              <Segment className={style.header}>
                <div className={style.projectImage}>
                  <ImageLoader src={this.props.context.getImageThumbnailURL(project.image, 1024)}
                               image={({src}) => (
                                 <Image src={src} centered/>
                               )}
                               loading={() => (
                                 <Placeholder>
                                   <Placeholder.Image rectangular/>
                                 </Placeholder>
                               )}
                  />
                </div>

                <div className={style.coloredSpace} style={{backgroundColor: project.color}}/>

                <Container fluid className={style.headerSegment}>
                  <Button className={style.projectEditSettingButton}
                          icon={'cog'}
                          as={Link}
                          to={this.props.context.formatRoute('Portfolio_project_edit', {id: project.id})}/>


                  <ActorAvatar className={style.creatorAvatar} variables={{id: _.get(project, 'creator.id')}}
                               hideName={true} imageProps={{size: 'tiny'}}/>

                  <Header className={style.projectTitle}>
                    {project.title}
                    <Header.Subheader>
                      {t("by {{name}}", {name: _.get(project, 'creator.displayName')})}
                    </Header.Subheader>
                  </Header>
                </Container>
              </Segment>

              <Segment className={style.stats}>
                <ProjectStats variables={{id: project.id}} size={'small'}/>
              </Segment>
            </Sticky>
          </Grid.Column>


          <Grid.Column width={10}>
            <div ref={this.headingSegmentRef}>
              <Menu secondary>
                <Menu.Item name={t("Timeline")}
                           active={!!matchPath(location.pathname, this.props.context.getRoute('Portfolio_project'))}
                           as={Link}
                           to={this.props.context.formatRoute('Portfolio_project', {id: project.id})}
                />
                <Menu.Item name={t("Notebook")}/>

                <Menu.Menu position='right'>
                  <Menu.Item>
                    <Button primary
                            icon={"add"}
                            content={t("Add an event")}
                            as={Link}
                            to={this.props.context.formatRoute('Portfolio_project_event_create', {id: project.id})}
                    />
                  </Menu.Item>
                </Menu.Menu>
              </Menu>

              <Route path={this.props.context.getRoute('Portfolio_project')} render={({match}) => {
                return (
                  <ProjectTimeline variables={{id: decodeURIComponent(match.params.id)}} pureComponentOnVariables/>
                )
              }}/>
            </div>
          </Grid.Column>

        </Grid>

        <Switch>
          <Route path={this.props.context.getRoute('Portfolio_project_edit')} render={({match}) => (
            <Modal open={true} onClose={() => history.goBack()} closeOnDimmerClick={true}>
              <Modal.Content>
                <ProjectEditor variables={{id: decodeURIComponent(match.params.id)}}
                               onCompleted={() => history.goBack()}/>
              </Modal.Content>
            </Modal>
          )}/>

          <Route path={this.props.context.getRoute('Portfolio_project_event_create')} render={({match}) => {
            let projectId = decodeURIComponent(match.params.id);

            return (
              <Modal open={true} onClose={() => history.goBack()} closeOnDimmerClick={true} closeIcon={true}>
                <Modal.Header>
                  {t("Create an event")}
                </Modal.Header>
                <Modal.Content>
                  <EventCreator variables={{projectId}}
                                onCompleted={() => history.goBack()}
                                extraRelayConnectionDefinitions={[
                                  {
                                    connectionKey: "ProjectTimeline_events"
                                  }
                                ]}
                                extraRelayCountersDefinitions={[
                                  {
                                    id: projectId,
                                    propName: 'eventsCount'
                                  }
                                ]}
                  />
                </Modal.Content>
              </Modal>
            )
          }}/>
        </Switch>
      </>
    );
  }
}

export default withRelay(
  graphql`
    query ProjectViewer_Query($id: ID!){
      project(id: $id){
        ...ProjectViewer_project
      }
    }
  `,
  {},
  createFragmentContainer(ProjectViewer, graphql`
    fragment ProjectViewer_project on Project {
      ...ProjectBasicFragment @relay(mask: false)
      creator{
        id
        displayName
      }
    }
  `)
);