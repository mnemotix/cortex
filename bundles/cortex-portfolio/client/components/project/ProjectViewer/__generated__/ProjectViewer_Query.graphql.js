/**
 * @flow
 * @relayHash 4c4085f4a27dd801d5b786ee8154219f
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
type ProjectViewer_project$ref = any;
export type ProjectViewer_QueryVariables = {|
  id: string
|};
export type ProjectViewer_QueryResponse = {|
  +project: ?{|
    +$fragmentRefs: ProjectViewer_project$ref
  |}
|};
export type ProjectViewer_Query = {|
  variables: ProjectViewer_QueryVariables,
  response: ProjectViewer_QueryResponse,
|};
*/


/*
query ProjectViewer_Query(
  $id: ID!
) {
  project(id: $id) {
    ...ProjectViewer_project
    id
  }
}

fragment ProjectViewer_project on Project {
  id
  uri
  color
  image
  title
  description
  shortDescription
  creationDate
  lastUpdate
  creator {
    ... on ActorInterface {
      id
      uri
      displayName
      avatar
    }
    id
    displayName
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "id",
    "type": "ID!",
    "defaultValue": null
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "id",
    "variableName": "id",
    "type": "ID!"
  }
],
v2 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "id",
  "args": null,
  "storageKey": null
},
v3 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "uri",
  "args": null,
  "storageKey": null
};
return {
  "kind": "Request",
  "operationKind": "query",
  "name": "ProjectViewer_Query",
  "id": null,
  "text": "query ProjectViewer_Query(\n  $id: ID!\n) {\n  project(id: $id) {\n    ...ProjectViewer_project\n    id\n  }\n}\n\nfragment ProjectViewer_project on Project {\n  id\n  uri\n  color\n  image\n  title\n  description\n  shortDescription\n  creationDate\n  lastUpdate\n  creator {\n    ... on ActorInterface {\n      id\n      uri\n      displayName\n      avatar\n    }\n    id\n    displayName\n  }\n}\n",
  "metadata": {},
  "fragment": {
    "kind": "Fragment",
    "name": "ProjectViewer_Query",
    "type": "Query",
    "metadata": null,
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "project",
        "storageKey": null,
        "args": v1,
        "concreteType": "Project",
        "plural": false,
        "selections": [
          {
            "kind": "FragmentSpread",
            "name": "ProjectViewer_project",
            "args": null
          }
        ]
      }
    ]
  },
  "operation": {
    "kind": "Operation",
    "name": "ProjectViewer_Query",
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "project",
        "storageKey": null,
        "args": v1,
        "concreteType": "Project",
        "plural": false,
        "selections": [
          v2,
          v3,
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "color",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "image",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "title",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "description",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "shortDescription",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "creationDate",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "lastUpdate",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "LinkedField",
            "alias": null,
            "name": "creator",
            "storageKey": null,
            "args": null,
            "concreteType": "Person",
            "plural": false,
            "selections": [
              v2,
              v3,
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "displayName",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "avatar",
                "args": null,
                "storageKey": null
              }
            ]
          }
        ]
      }
    ]
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = '35cc8a322c2f82505003f2becf8aa205';
module.exports = node;
