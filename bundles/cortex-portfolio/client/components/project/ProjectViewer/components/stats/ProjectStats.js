/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *  
 */
import React from 'react';
import PropTypes from 'prop-types';
import {graphql, createFragmentContainer} from 'react-relay';
import {withContext, withRelay} from '../../../../../../../cortex-core/client/index';
import {withRouter} from 'react-router';
import {Statistic} from 'semantic-ui-react';
import style from './ProjectStats.less';

@withContext
@withRouter
class ProjectStats extends React.Component {
  static propTypes = {
    size: PropTypes.oneOf(["mini","tiny", "small", "large", "huge"])
  };

  render() {
    const {t, project, size} = this.props;

    return (
      <Statistic.Group size={size}>
        <Statistic>
          <Statistic.Value>{project.eventsCount}</Statistic.Value>
          <Statistic.Label>{project.eventsCount <= 1 ? t("Event") : t("Events")}</Statistic.Label>
        </Statistic>
      </Statistic.Group>
    );
  }
}

export default withRelay(
  graphql`
    query ProjectStats_Query($id: ID!){
      project(id: $id){
        ...ProjectStats_project
      }
    }
  `,
  {},
  createFragmentContainer(ProjectStats, graphql`
    fragment ProjectStats_project on Project {
      eventsCount
    }
  `)
);