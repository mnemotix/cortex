/**
 * @flow
 * @relayHash cc1f3f778195cf2409a2a423901b3dfb
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
type ProjectStats_project$ref = any;
export type ProjectStats_QueryVariables = {|
  id: string
|};
export type ProjectStats_QueryResponse = {|
  +project: ?{|
    +$fragmentRefs: ProjectStats_project$ref
  |}
|};
export type ProjectStats_Query = {|
  variables: ProjectStats_QueryVariables,
  response: ProjectStats_QueryResponse,
|};
*/


/*
query ProjectStats_Query(
  $id: ID!
) {
  project(id: $id) {
    ...ProjectStats_project
    id
  }
}

fragment ProjectStats_project on Project {
  eventsCount
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "id",
    "type": "ID!",
    "defaultValue": null
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "id",
    "variableName": "id",
    "type": "ID!"
  }
];
return {
  "kind": "Request",
  "operationKind": "query",
  "name": "ProjectStats_Query",
  "id": null,
  "text": "query ProjectStats_Query(\n  $id: ID!\n) {\n  project(id: $id) {\n    ...ProjectStats_project\n    id\n  }\n}\n\nfragment ProjectStats_project on Project {\n  eventsCount\n}\n",
  "metadata": {},
  "fragment": {
    "kind": "Fragment",
    "name": "ProjectStats_Query",
    "type": "Query",
    "metadata": null,
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "project",
        "storageKey": null,
        "args": v1,
        "concreteType": "Project",
        "plural": false,
        "selections": [
          {
            "kind": "FragmentSpread",
            "name": "ProjectStats_project",
            "args": null
          }
        ]
      }
    ]
  },
  "operation": {
    "kind": "Operation",
    "name": "ProjectStats_Query",
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "project",
        "storageKey": null,
        "args": v1,
        "concreteType": "Project",
        "plural": false,
        "selections": [
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "eventsCount",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "id",
            "args": null,
            "storageKey": null
          }
        ]
      }
    ]
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = 'c8a0840ffe32ff5b6be8de622034e09e';
module.exports = node;
