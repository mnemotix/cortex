/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *  
 */
import React from 'react';
import PropTypes from 'prop-types';
import {graphql, createRefetchContainer} from 'react-relay';
import {withContext, withRelay} from '@mnemotix/cortex-core/client';
import {withRouter} from 'react-router';
import {Link, Switch, Route} from 'react-router-dom';
import {
  Icon,
  Segment,
  Grid,
  Placeholder,
  Card,
  Visibility,
  Input,
  Sticky,
  Modal,
  Popup,
  Menu
} from 'semantic-ui-react';
import ActorFeed from "@mnemotix/cortex-addressbook/client/components/common/avatar/ActorFeed";
import _ from "lodash";
import moment from "moment";
import style from './ProjectTimeline.less';
import EventResourcesPreview from "./components/event/components/attachments/EventAttachmentsPreview";
import EventInvolvementsPreview from "./components/event/components/involvements/EventInvolvementsPreview";
import EventEditor from "../../../../event/EventEditor/EventEditor";
import InvolvementEditor from "../../../../common/involvement/InvolvementEditor";
import AttachmentEditor from "../../../../common/attachment/AttachmentEditor";
import EventMemos from "./components/event/components/memos/EventMemos";
import ActorAvatar from "@mnemotix/cortex-addressbook/client/components/common/avatar/ActorAvatar";
import ProjectTimelineEvent from "./components/event/ProjectTimelineEvent";

@withContext
@withRouter
class ProjectTimeline extends React.PureComponent {
  static propTypes = {
    onCompleted: PropTypes.func
  };

  state = {
    loading: false,
    loadingMore: false,
    loadingCreation: false,
    qs: '',
    viewType: 'list',
    sortColumn: null,
    sortDirection: null,
    currentVisibleIndex: 0,
    currentVisibleOffset: 0
  };

  timelineRef = React.createRef();

  render() {
    const {t, project, history, match: timelineMatch} = this.props;

    return (
      <>
        <div className={style.searchBar}>
          <Input fluid
                 loading={this.state.loading}
                 placeholder={t('Search...')}
                 value={this.state.qs}
                 icon='search'
                 onChange={(e, {value}) => this.search(value)}
          />
        </div>
        <Grid columns={2} stretched>
          {project.events.edges.length === 0 ? (
            <Grid.Column width={16}>
              <Segment textAlign={"center"} disabled padded='very'>
                {t("There is no event yet...")}
              </Segment>
            </Grid.Column>
          ) : (
            <>
              <Grid.Column width={12}>
                <div className={style.timeline} ref={this.timelineRef}>
                  {project.events.edges.map(({node: event}, index) => this.renderEvent(event, index))}

                  {this.state.loadingMore ? _.range(0, 2).map((index) => (
                    <Segment key={index}>
                      <Placeholder fluid>
                        <Placeholder.Header image>
                          <Placeholder.Line/>
                          <Placeholder.Line/>
                        </Placeholder.Header>
                        <Placeholder.Paragraph>
                          <Placeholder.Line/>
                          <Placeholder.Line/>
                          <Placeholder.Line/>
                        </Placeholder.Paragraph>
                      </Placeholder>
                    </Segment>
                  )) : null}

                  <Visibility once onUpdate={(e, {calculations: {topVisible}}) => {
                    if (topVisible) {
                      this.showMore();
                    }
                  }}/>
                </div>
              </Grid.Column>
              <Grid.Column width={4}>
                <Sticky context={this.timelineRef.current} offset={20}>
                  {this.renderTimelinePreview()}
                </Sticky>
              </Grid.Column>
            </>
          )}
        </Grid>

        <Switch>
          <Route path={this.props.context.getRoute('Portfolio_project_event_edit')} render={({match}) => (
            <Modal closeIcon open={true} onClose={() => history.push(timelineMatch.url) } closeOnDimmerClick={true}>
              <Modal.Header>
                {t("Event settings")}
              </Modal.Header>
              <Modal.Content>
                <EventEditor variables={{id: decodeURIComponent(match.params.eventId)}}
                             onCompleted={() => history.push(timelineMatch.url)}/>
              </Modal.Content>
            </Modal>
          )}/>

          <Route path={this.props.context.getRoute('Portfolio_project_event_involvements')}
                 render={({match}) => (
                   <Modal closeIcon open={true} onClose={() =>  history.push(timelineMatch.url)} closeOnDimmerClick={true}>
                     <Modal.Header>
                       {t("Event involvements")}
                     </Modal.Header>
                     <Modal.Content>
                       <InvolvementEditor variables={{id: decodeURIComponent(match.params.eventId)}}
                                          onCompleted={() => history.push(timelineMatch.url)}
                                          extraRelayConnectionDefinitions={[
                                            {
                                              connectionKey: "EventInvolvementsPreview_involvements"
                                            }
                                          ]}
                                          extraRelayCountersDefinitions={[
                                            {
                                              id: decodeURIComponent(match.params.eventId),
                                              propName: 'involvementsCount'
                                            }
                                          ]}/>
                     </Modal.Content>
                   </Modal>
                 )}
          />

          <Route path={this.props.context.getRoute('Portfolio_project_event_attachments')}
                 render={({match}) => (
                   <Modal closeIcon open={true} onClose={() => history.push(timelineMatch.url)} closeOnDimmerClick={true}>
                     <Modal.Header>
                       {t("Event attachments")}
                     </Modal.Header>
                     <Modal.Content>
                       <AttachmentEditor variables={{id: decodeURIComponent(match.params.eventId)}}
                                         project={project}
                                         onCompleted={() => history.goBack()}
                                         extraRelayConnectionDefinitions={[
                                            {
                                              connectionKey: "EventAttachmentsPreview_attachments"
                                            }
                                          ]}
                                         extraRelayCountersDefinitions={[
                                           {
                                             id: decodeURIComponent(match.params.eventId),
                                             propName: 'attachmentsCount'
                                           }
                                         ]}
                       />
                     </Modal.Content>
                   </Modal>
                 )}
          />
        </Switch>
      </>
    );
  }

  renderEvent(event, index) {
    const {t, project} = this.props;

    return (
      <Visibility fireOnMount className={style.event} key={event.id}
                  onUpdate={(e, {calculations: {bottomVisible, percentagePassed, onScreen, direction}}) => {
                    if (onScreen && bottomVisible) {
                      if (direction === "down" && index >= this.state.currentVisibleIndex ||
                        direction === "up" && index <= this.state.currentVisibleIndex
                      ) {
                        this.setState({
                          currentVisibleIndex: index,
                          currentVisibleOffset: percentagePassed
                        });
                      }
                    }
                  }}>
        <ProjectTimelineEvent variables={{id: event.id}} project={project}/>
      </Visibility>
    )
  }

  renderTimelinePreview() {
    const {startDate, endDate, events, eventsCount} = this.props.project;
    let event = _.get(events, `edges.${this.state.currentVisibleIndex}.node`);
    let lineHeight = 200;

    if (startDate && endDate && startDate !== endDate && event) {
      let eventHeight = lineHeight / eventsCount;

      return (
        <div className={style.timelinePreview}>
          <div className={style.startDate}>
            {moment(endDate).format('ll')}
          </div>

          <div className={style.line} style={{height: lineHeight, borderColor: this.props.project.color}}>
            <div className={style.cursor} style={{
              height: eventHeight,
              top: eventHeight * this.state.currentVisibleIndex + (this.state.currentVisibleOffset * eventHeight),
              backgroundColor: this.props.project.color
            }}>
              <div className={style.label}>
                {moment(event.startDate).format('MMM YYYY')}
              </div>
            </div>
          </div>

          <div className={style.startDate}>
            {moment(startDate).format('ll')}
          </div>
        </div>
      )
    }
  }

  search(qs) {
    this.setState({
      qs,
      loading: true
    });

    if (this.searchTimer) {
      clearTimeout(this.searchTimer);
    }

    this.searchTimer = setTimeout(() => {
      this.props.relay.refetch(() => ({
        qs,
        first: 5
      }), null, () => {
        this.setState({
          loading: false
        });
      });
    }, 250)
  }

  showMore(howMany = 5) {
    const {project, relay} = this.props;

    if (_.get(project, 'events.pageInfo.hasNextPage') && !this.state.loadingMore) {
      const endCursor = _.get(project, 'events.pageInfo.endCursor');
      const total = _.get(project, 'events.edges.length') + howMany;

      this.setState({
        loadingMore: true
      }, () => {

        relay.refetch((fragmentVariables) => ({
          ...fragmentVariables,
          qs: this.state.qs,
          first: howMany,
          after: endCursor,
        }), ({
          first: total
        }), () => {
          this.setState({
            loadingMore: false
          });
        });
      });
    }
  }

  _handleSort = (sortColumn) => {
    this.setState({
      sortColumn,
      sortDirection: this.state.sortDirection === 'ascending' ? 'descending' : 'ascending'
    });

    this.props.relay.refetch(() => ({
      qs: this.state.qs,
      first: 5,
      sortings: [
        {
          sortBy: sortColumn,
          sortDirection: this.state.sortDirection === 'ascending' ? 'desc' : 'asc'
        }
      ]
    }), null, () => {
      this.setState({
        loading: false
      });
    });
  };
}

let query = graphql`
  query ProjectTimeline_Query($id: ID! $qs: String $first: Int $after: String $sortings: [SortingInput]){
    project(id: $id){
      ...ProjectTimeline_project
    }
    
  }
`;

export default withRelay(query,
  {
    first: 5,
    sortings: [
      {
        sortBy: 'startDate',
        sortDirection: 'desc'
      }
    ]
  },
  createRefetchContainer(ProjectTimeline, graphql`
    fragment ProjectTimeline_project on Project {
      id
      color
      startDate
      endDate
      eventsCount
      events(qs: $qs first: $first after: $after sortings: $sortings) @connection(key: "ProjectTimeline_events", filters:[]){
        edges{
          node{
            id
            startDate
          }
        }
        pageInfo{
          hasNextPage
          endCursor
        }
      }
    }
  `, query),
  false,
  (
    <Grid columns={2}>
      <Grid.Column width={12}>
        {_.range(0, 10).map((index) => (
          <Segment key={index}>
            <Placeholder fluid>
              <Placeholder.Header image>
                <Placeholder.Line/>
                <Placeholder.Line/>
              </Placeholder.Header>
              <Placeholder.Paragraph>
                <Placeholder.Line/>
                <Placeholder.Line/>
                <Placeholder.Line/>
              </Placeholder.Paragraph>
            </Placeholder>
          </Segment>
        ))}
      </Grid.Column>
      <Grid.Column width={4}>
      </Grid.Column>
    </Grid>
  )
);