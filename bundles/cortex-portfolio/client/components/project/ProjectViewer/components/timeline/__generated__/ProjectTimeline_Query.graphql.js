/**
 * @flow
 * @relayHash 3b58204fcdcd30c2d1af03d9a29569df
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
type ProjectTimeline_project$ref = any;
export type SortingInput = {
  sortBy?: ?string,
  sortDirection?: ?string,
  sortFilters?: ?$ReadOnlyArray<?string>,
};
export type ProjectTimeline_QueryVariables = {|
  id: string,
  qs?: ?string,
  first?: ?number,
  after?: ?string,
  sortings?: ?$ReadOnlyArray<?SortingInput>,
|};
export type ProjectTimeline_QueryResponse = {|
  +project: ?{|
    +$fragmentRefs: ProjectTimeline_project$ref
  |}
|};
export type ProjectTimeline_Query = {|
  variables: ProjectTimeline_QueryVariables,
  response: ProjectTimeline_QueryResponse,
|};
*/


/*
query ProjectTimeline_Query(
  $id: ID!
  $qs: String
  $first: Int
  $after: String
  $sortings: [SortingInput]
) {
  project(id: $id) {
    ...ProjectTimeline_project
    id
  }
}

fragment ProjectTimeline_project on Project {
  id
  color
  startDate
  endDate
  eventsCount
  events(qs: $qs, first: $first, after: $after, sortings: $sortings) {
    edges {
      node {
        id
        startDate
        __typename
      }
      cursor
    }
    pageInfo {
      hasNextPage
      endCursor
    }
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "id",
    "type": "ID!",
    "defaultValue": null
  },
  {
    "kind": "LocalArgument",
    "name": "qs",
    "type": "String",
    "defaultValue": null
  },
  {
    "kind": "LocalArgument",
    "name": "first",
    "type": "Int",
    "defaultValue": null
  },
  {
    "kind": "LocalArgument",
    "name": "after",
    "type": "String",
    "defaultValue": null
  },
  {
    "kind": "LocalArgument",
    "name": "sortings",
    "type": "[SortingInput]",
    "defaultValue": null
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "id",
    "variableName": "id",
    "type": "ID!"
  }
],
v2 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "id",
  "args": null,
  "storageKey": null
},
v3 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "startDate",
  "args": null,
  "storageKey": null
},
v4 = [
  {
    "kind": "Variable",
    "name": "after",
    "variableName": "after",
    "type": "String"
  },
  {
    "kind": "Variable",
    "name": "first",
    "variableName": "first",
    "type": "Int"
  },
  {
    "kind": "Variable",
    "name": "qs",
    "variableName": "qs",
    "type": "String"
  },
  {
    "kind": "Variable",
    "name": "sortings",
    "variableName": "sortings",
    "type": "[SortingInput]"
  }
];
return {
  "kind": "Request",
  "operationKind": "query",
  "name": "ProjectTimeline_Query",
  "id": null,
  "text": "query ProjectTimeline_Query(\n  $id: ID!\n  $qs: String\n  $first: Int\n  $after: String\n  $sortings: [SortingInput]\n) {\n  project(id: $id) {\n    ...ProjectTimeline_project\n    id\n  }\n}\n\nfragment ProjectTimeline_project on Project {\n  id\n  color\n  startDate\n  endDate\n  eventsCount\n  events(qs: $qs, first: $first, after: $after, sortings: $sortings) {\n    edges {\n      node {\n        id\n        startDate\n        __typename\n      }\n      cursor\n    }\n    pageInfo {\n      hasNextPage\n      endCursor\n    }\n  }\n}\n",
  "metadata": {},
  "fragment": {
    "kind": "Fragment",
    "name": "ProjectTimeline_Query",
    "type": "Query",
    "metadata": null,
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "project",
        "storageKey": null,
        "args": v1,
        "concreteType": "Project",
        "plural": false,
        "selections": [
          {
            "kind": "FragmentSpread",
            "name": "ProjectTimeline_project",
            "args": null
          }
        ]
      }
    ]
  },
  "operation": {
    "kind": "Operation",
    "name": "ProjectTimeline_Query",
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "project",
        "storageKey": null,
        "args": v1,
        "concreteType": "Project",
        "plural": false,
        "selections": [
          v2,
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "color",
            "args": null,
            "storageKey": null
          },
          v3,
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "endDate",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "eventsCount",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "LinkedField",
            "alias": null,
            "name": "events",
            "storageKey": null,
            "args": v4,
            "concreteType": "EventConnection",
            "plural": false,
            "selections": [
              {
                "kind": "LinkedField",
                "alias": null,
                "name": "edges",
                "storageKey": null,
                "args": null,
                "concreteType": "EventEdge",
                "plural": true,
                "selections": [
                  {
                    "kind": "LinkedField",
                    "alias": null,
                    "name": "node",
                    "storageKey": null,
                    "args": null,
                    "concreteType": "Event",
                    "plural": false,
                    "selections": [
                      v2,
                      v3,
                      {
                        "kind": "ScalarField",
                        "alias": null,
                        "name": "__typename",
                        "args": null,
                        "storageKey": null
                      }
                    ]
                  },
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "cursor",
                    "args": null,
                    "storageKey": null
                  }
                ]
              },
              {
                "kind": "LinkedField",
                "alias": null,
                "name": "pageInfo",
                "storageKey": null,
                "args": null,
                "concreteType": "PageInfo",
                "plural": false,
                "selections": [
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "hasNextPage",
                    "args": null,
                    "storageKey": null
                  },
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "endCursor",
                    "args": null,
                    "storageKey": null
                  }
                ]
              }
            ]
          },
          {
            "kind": "LinkedHandle",
            "alias": null,
            "name": "events",
            "args": v4,
            "handle": "connection",
            "key": "ProjectTimeline_events",
            "filters": []
          }
        ]
      }
    ]
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = '59bc451de3a9356d63459656f2ee7ebc';
module.exports = node;
