/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *  
 */
import React from 'react';
import PropTypes from 'prop-types';
import {graphql, createRefetchContainer} from 'react-relay';
import {withContext, withRelay} from '@mnemotix/cortex-core/client';
import {withRouter} from 'react-router';
import {Link, Switch, Route} from 'react-router-dom';
import {
  Icon,
  Segment,
  Grid,
  Placeholder,
  Card,
  Visibility,
  Input,
  Sticky,
  Modal,
  Popup,
  Menu
} from 'semantic-ui-react';
import ActorFeed from "@mnemotix/cortex-addressbook/client/components/common/avatar/ActorFeed";
import _ from "lodash";
import moment from "moment";
import style from './ProjectTimelineEvent.less';
import EventResourcesPreview from "./components/attachments/EventAttachmentsPreview";
import EventInvolvementsPreview from "./components/involvements/EventInvolvementsPreview";
import EventMemos from "./components/memos/EventMemos";
import ActorAvatar from "@mnemotix/cortex-addressbook/client/components/common/avatar/ActorAvatar";
import {DefaultMutations as Mutations} from "@mnemotix/synaptix.js/lib/client";

@withContext
@withRouter
class ProjectTimelineEvent extends React.PureComponent {
  static propTypes = {
    onCompleted: PropTypes.func
  };

  state = {
    loading: false,
    loadingMore: false,
    loadingCreation: false,
    currentVisibleIndex: 0,
    currentVisibleOffset: 0,
    editingMemo: null
  };

  render() {
    const {t, project, me, event} = this.props;

    return (
      <Card fluid className={style.event}>
        <Card.Content className={style.eventContent}>
          <ActorFeed className={style.creatorAvatar}
                     variables={{id: _.get(event, 'creator.id')}}
                     imageProps={{size: 'big', className: style.image}}
                     headerMessage={moment(event.startDate).format('LL')}
          />

          <Menu icon size={"mini"} className={style.eventEditSettingButton}>
            <Popup trigger={(
              <Menu.Item as={Link}
                         to={this.props.context.formatRoute('Portfolio_project_event_edit', {
                           id: project.id,
                           eventId: event.id
                         })}>
                <Icon name='cog'/>
              </Menu.Item>
            )} content={t("Edit event settings")} size={'small'}/>

            <Popup trigger={(
              <Menu.Item as={Link}
                         to={this.props.context.formatRoute('Portfolio_project_event_involvements', {
                           id: project.id,
                           eventId: event.id
                         })}>
                <Icon name='group'/>
              </Menu.Item>
            )} content={t("Edit event involvements")} size={'small'}/>

            <Popup trigger={(
              <Menu.Item as={Link}
                         to={this.props.context.formatRoute('Portfolio_project_event_attachments', {
                           id: project.id,
                           eventId: event.id
                         })}>
                <Icon name='attach'/>
              </Menu.Item>
            )} content={t("Edit event resources")} size={'small'}/>
          </Menu>

          <Card.Header>
            {event.title}
          </Card.Header>

          <Card.Description>
            {event.description}
          </Card.Description>
        </Card.Content>

        {event.attachmentsCount > 0 ? (
          <Card.Content extra>
            <EventResourcesPreview variables={{id: event.id}}/>
          </Card.Content>
        ) : null}

        {event.involvementsCount > 0 ? (
          <Card.Content extra>
            <EventInvolvementsPreview variables={{id: event.id}}/>
          </Card.Content>
        ) : null}

        <Card.Content extra>
          <EventMemos variables={{id: event.id}} editingMemo={this.state.editingMemo}/>

          {event.memosCount > 0 ? null : (
            <Input fluid
                   iconPosition='left'
                   icon={<ActorAvatar variables={{id: me.id, type: 'Person'}} asIcon/>}
                   placeholder={t("Commentez cet événement...")}
                   onFocus={() => this.createMemo()}
            />
          )}
        </Card.Content>
      </Card>
    )
  }

  createMemo() {
    const {t, event, relay: {environment}} = this.props;

    (new Mutations.projects.CreateMemoMutation({
      environment,
      parentId: event.id,
      updateCounters: [
        {
          id: event.id,
          propName: 'memosCount'
        }
      ],
      connectionDefinitions: [{
        connectionKey: "EventMemos_memos"
      }],
      onCompleted: (memo) => {
        this.setState({
          editingMemo: memo
        })
      }
    })).apply({
      extraInputs: {
        eventId: event.id
      }
    })
  }
}

let query = graphql`
  query ProjectTimelineEvent_Query($id: ID!){
    event(id: $id){
      ...ProjectTimelineEvent_event
    }

    me{
      ...ProjectTimelineEvent_me
    }
  }
`;

export default withRelay(query,
  {
    first: 5,
    sortings: [
      {
        sortBy: 'startDate',
        sortDirection: 'desc'
      }
    ]
  },
  createRefetchContainer(ProjectTimelineEvent, graphql`
    fragment ProjectTimelineEvent_me on Person {
      id
    }

    fragment ProjectTimelineEvent_event on Event {
      id
      ...EventBasicFragment @relay(mask: false)
      attachmentsCount
      involvementsCount
      memosCount
    }
  `, query),
  false,
  (
    <Grid columns={2}>
      <Grid.Column width={12}>
        {_.range(0, 10).map((index) => (
          <Segment key={index}>
            <Placeholder fluid>
              <Placeholder.Header image>
                <Placeholder.Line/>
                <Placeholder.Line/>
              </Placeholder.Header>
              <Placeholder.Paragraph>
                <Placeholder.Line/>
                <Placeholder.Line/>
                <Placeholder.Line/>
              </Placeholder.Paragraph>
            </Placeholder>
          </Segment>
        ))}
      </Grid.Column>
      <Grid.Column width={4}>
      </Grid.Column>
    </Grid>
  )
);