/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *  
 */
import React from 'react';
import {graphql, createRefetchContainer} from 'react-relay';
import {withContext, withRelay} from '../../../../../../../../../../../cortex-core/client';
import {withRouter, matchPath, Switch, Route, Redirect} from 'react-router';
import PhotoGrid from './PhotoGrid';
import {Placeholder, Modal, Icon, Image, Grid} from 'semantic-ui-react';
import keyboardjs from "keyboardjs";
import ImageLoader from "react-loading-image";
import style from './EventAttachmentsPreview.less';

@withContext
@withRouter
class EventAttachmentsPreview extends React.Component {
  images = [];

  state = {
    images: [],
    imageViewingIndex: -1,
  };

  componentDidMount(){
    keyboardjs.bind('left', this._handlePreviousImage);
    keyboardjs.bind('right', this._handleNextImage);
  }

  componentWillUnmount(){
    keyboardjs.unbind('left', this._handlePreviousImage);
    keyboardjs.unbind('right', this._handleNextImage);
  }

  render() {
    const {t, event} = this.props;

    this.images = event.attachments.edges
      .filter(({node: {resource}}) => !!resource && resource.publicUrl && resource.mime.match(/image\/.*/))
      .map(({node: {resource}}) => (
        {
          src: location.origin + this.props.context.getImageThumbnailURL(resource.publicUrl, 600),
          ...resource
        }
      ));

    return this.images.length > 0 ? (
      <>
        <PhotoGrid images={this.images || []} onImageClick={(image) => {
          this.setState({
            imageViewingIndex: this.images.findIndex(({id}) => id === image.id)
          })
        }}/>

        <Modal closeIcon basic open={this.state.imageViewingIndex >= 0}
               onClose={() => this.setState({imageViewingIndex: -1})}
               closeOnDimmerClick={true}>
          <Modal.Content>
            <Grid columns={'equal'} verticalAlign={"middle"} stretched>
              <Grid.Column width={1} textAlign={'center'}>
                <Icon link name={'chevron left'} size={'huge'} fitted onClick={this._handlePreviousImage}/>
              </Grid.Column>
              <Grid.Column>
                {this.state.imageViewingIndex >= 0 ? (
                  <ImageLoader
                    src={this.props.context.getImageThumbnailURL(this.images[this.state.imageViewingIndex].publicUrl, 1024)}
                    image={({src}) => (
                      <Image className={style.previewedImage} src={src} centered/>
                    )
                    }
                    loading={() => (
                      <Placeholder fluid>
                        <Placeholder.Image rectangular/>
                      </Placeholder>
                    )}
                    error={() => (
                      <Icon name={'question circle'} bordered disabled size={'massive'}/>
                    )}
                  />
                ) : null}
              </Grid.Column>
              <Grid.Column width={1} textAlign={'center'}>
                <Icon link name={'chevron right'} size={'huge'} fitted
                      onClick={this._handleNextImage}/>
              </Grid.Column>
            </Grid>
          </Modal.Content>
        </Modal>
      </>
    ) : null;
  }

  _handleNextImage = () => {
    if (this.state.imageViewingIndex !== -1){
      let imageViewingIndex = this.state.imageViewingIndex - 1;
      if (imageViewingIndex < 0){
        imageViewingIndex = this.images.length - 1;
      }
      this.setState({imageViewingIndex});
    }
  };

  _handlePreviousImage = () => {
    if (this.state.imageViewingIndex !== -1) {
      let imageViewingIndex = this.state.imageViewingIndex + 1;
      if (imageViewingIndex >= this.images.length) {
        imageViewingIndex = 0;
      }
      this.setState({imageViewingIndex});
    }
  };
}

export default withRelay(
  graphql`
    query EventAttachmentsPreview_Query($id: ID! $first: Int $filters: [String]){
      event(id: $id){
        ...EventAttachmentsPreview_event
      }
    }
  `,
  {
    first: 50,
    filters: ["mime:image/.*"] // Regexp filtering
  },
  createRefetchContainer(EventAttachmentsPreview, graphql`
    fragment EventAttachmentsPreview_event on Event {
      attachmentsCount
      attachments(first: $first filters: $filters sortBy: "creationDate" sortDirection: "asc") @connection(key: "EventAttachmentsPreview_attachments", filters:[]){
        edges{
          node{
            resource{
              id
              mime
              publicUrl
            }
          }
        }
      }
    }
  `),
  false,
  (
    <Placeholder fluid>
      <Placeholder.Image/>
    </Placeholder>
  )
);