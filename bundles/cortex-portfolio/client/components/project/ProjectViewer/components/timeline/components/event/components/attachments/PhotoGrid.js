/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */


/**
 * Inspired from @url https://github.com/mukeshsoni/react-photo-grid
 */

import React from "react";
import ReactDOM from "react-dom";
import PropTypes from "prop-types";
import ImageLoader from "react-loading-image";
import _ from "lodash";
import {Placeholder, Dimmer, Icon, Container, Grid} from "semantic-ui-react";
import style from './PhotoGrid.less';

let imageElements = [];

export default class ReactPhotoGrid extends React.Component {
  static propTypes = {
    images: PropTypes.array.isRequired,
    gridSize: PropTypes.string,
    onImageClick: PropTypes.func
  };

  static defaultProps = {
    images: []
  };

  constructor(props) {
    super(props);

    const {gridSize, images} = props;

    let containerWidth = 500,
      containerHeight = 500;

    if (gridSize) {
      let container = gridSize.split("x");
      containerWidth = container[0] || 500;
      containerHeight = container[1] || 500;
    }

    let imageData = images.length <= 4 ? images : images.slice(0, 4);

    // take care of letiations in property data
    // if someone just passes an array of path strings
    if (imageData[0] && _.isString(imageData[0])) {
      imageData = imageData.map(function (imagePath) {
        return {
          id: Math.random() * 1000,
          path: imagePath
        }
      })
    } else if (imageData[0] && _.isObject(imageData[0])) {
      imageData = imageData.map(function (image) {
        return {
          id: Math.random() * 1000,
          path: image.src, // in case someone supplies a src property instead of path
          ...image
        }
      })
    }

    let state = {
      ladyLuck: Math.floor(Math.random() * 2),
      containerWidth: containerWidth,
      containerHeight: containerHeight,
      imagesToShow: imageData
    };

    if (this.props.containerWidth) {
      state.containerWidth = this.props.containerWidth
    }

    this.state = state;
  }

  componentWillUnmount() {
    for(let imageElement of imageElements){
      imageElement.removeEventListener(
        "load",
        imageLoadCallback(imageElement.id, this._handleRecalculateGrid)
      )
    }
  }

  componentDidMount() {
    for(let image of this.state.imagesToShow || []){
      getImageDimensions(image.path, image.id, this._handleRecalculateGrid);
    }

    // only set it to parents width/height if no gridsize is provided
    if (!this.props.gridSize) {
      this.setState({
        containerWidth: ReactDOM.findDOMNode(this).offsetWidth,
        containerHeight: ReactDOM.findDOMNode(this).offsetHeight
      })
    }

    // $(ReactDOM.findDOMNode(this)..resize(this.onResize);
    // elementResizeEvent(ReactDOM.findDOMNode(this). this.onResize);
  }

  getComponentStyles(images) {
    let numberOfImages = images.length;

    let marginSetters = ["Bottom", "Right"];
    let contenders = ["Width", "Height"];
    let winner = contenders[this.state.ladyLuck];
    let loser = first(without(contenders, winner));
    let marginWinner = marginSetters[this.state.ladyLuck];
    let marginLoser = first(without(marginSetters, marginWinner));

    let smallestDimensionRaw = Math.floor(
      this.state["container" + winner] / (numberOfImages - 1)
    );

    let margin = 2;
    let smallImageDimension = smallestDimensionRaw - margin;
    let styles = [];
    let commonStyle = {
      display: "inline-block",
      position: "relative",
      overflow: "hidden",
      float: "left",
      verticalAlign: "top",
      cursor: "pointer"
    };

    switch (numberOfImages) {
      case 0:
        break;
      case 1:
        // set some big numbers in case width and height not provided
        if (!images[0].width) images[0].width = 1000000;
        if (!images[0].height) images[0].height = 1000000;

        if (images[0].width > images[0].height) {
          styles = [
            {
              width:
                Math.min(
                  this.state.containerWidth,
                  images[0].width
                ) - margin,
              height:
                Math.min(
                  this.state.containerWidth,
                  images[0].width
                ) *
                images[0].height /
                images[0].width -
                margin,
              margin: margin
            }
          ]
        } else {
          styles = [
            {
              width: Math.min(this.state.containerHeight, images[0].height) * images[0].width / images[0].height - margin,
              height: Math.min(this.state.containerHeight, images[0].height) - margin,
              margin: margin
            }
          ]
        }
        break;
      case 2:
        styles[0] = styles[1] = {};

        styles[0][winner.toLowerCase()] = styles[1][
          winner.toLowerCase()
          ] =
          this.state["container" + winner] - margin;
        styles[0][loser.toLowerCase()] = styles[1][
          loser.toLowerCase()
          ] =
          Math.min(smallImageDimension / 2) - margin;
        styles[0]["margin" + marginWinner] = margin;
        break;
      default:
        styles[0] = {};
        styles[0][winner.toLowerCase()] = this.state["container" + winner];
        styles[0][loser.toLowerCase()] = this.state["container" + loser] - smallImageDimension - margin;
        styles[0]["margin" + marginWinner] = margin;

        let styleForSmallerImages = {
          width: smallImageDimension,
          height: smallImageDimension
        };
        styleForSmallerImages["margin" + marginLoser] = margin;

        for (let i = 1; i < numberOfImages && i < 4; i++) {
          // cloning is important here because otherwise changing the dimension of last image changes it for everyone
          styles.push({...styleForSmallerImages})
        }

        // adjust the width/height of the last image in case of round off errors in division
        styles[numberOfImages - 1][winner.toLowerCase()] +=
          styles[0][winner.toLowerCase()] -
          smallImageDimension * (numberOfImages - 1) -
          margin * (numberOfImages - 2);
        styles[numberOfImages - 1]["margin" + marginLoser] = 0
    }

    return styles.map(function (style) {
      return {
        ...commonStyle,
        ...style
      }
    })
  }

  render() {
    let componentStyles = this.getComponentStyles(this.state.imagesToShow);

    let images = this.state.imagesToShow.map(function (image, index) {
      let componentStyle = componentStyles[index];

      // max width and height has to be dynamic depending on this image's dimensions
      let imageStyle;

      if (image.width && image.height) {
        if (
          image.width <= componentStyle.width ||
          image.height <= componentStyle.height
        ) {
          // do nothing
        } else if (
          image.width / componentStyle.width <
          image.height / componentStyle.height
        ) {
          imageStyle = {
            maxWidth: componentStyle.width
          }
        } else {
          imageStyle = {
            maxHeight: componentStyle.height
          }
        }
      }

      let renderImg = (src) => (
        <img style={imageStyle} src={src} onClick={this._handleImageClick.bind(this, image)}/>
      );

      let imagesNotShownCount = 0;

      if (index === this.state.imagesToShow.length - 1 && this.props.images.length > 4){
        imagesNotShownCount = this.props.images.length - 4;
      }

      return (
        <div key={"image_" + index} style={componentStyle}>
          <ImageLoader src={image.path}
                       image={({src}) => imagesNotShownCount === 0 ? renderImg(src) : (
                         <Dimmer.Dimmable as={Container} fluid dimmed={true}>
                           {renderImg(src)}
                           <Dimmer active={true} className={style.dimmedMessage} style={{height: componentStyle.height}} onClick={this._handleImageClick.bind(this, image)}>
                             +{imagesNotShownCount}
                           </Dimmer>
                         </Dimmer.Dimmable>
                       )}
                       loading={() => (
                         <Placeholder fluid className={style.placeholder}>
                           <Placeholder.Image className={style.image}/>
                         </Placeholder>
                       )}
                       error={() => (
                         <Grid stretched verticalAlign={'middle'} style={{height: "100%"}}>
                           <Grid.Column textAlign={"center"}>
                             <Icon name={'file image'} size={"massive"} disabled fitted/>
                           </Grid.Column>
                         </Grid>
                       )}
          />
        </div>
      )
    }, this);

    let containerStyle = {
      width: this.state.containerWidth,
      height: this.state.containerHeight,
      backgroundColor: "white"
    };
    return (
      <div>
        <div style={containerStyle}>
          {images}
          <div style={{clear: "both"}}/>
        </div>
      </div>
    )
  }

  _handleResize = () => {
    this.setState({
      containerWidth: ReactDOM.findDOMNode(this).offsetWidth,
      containerHeight: ReactDOM.findDOMNode(this).offsetHeight
    })
  };

  _handleImageClick = (image) => {
    this.props.onImageClick && this.props.onImageClick(image)
  };

  _handleRecalculateGrid = (id, width, height) => {
    let _imagesToShow = [...this.state.imagesToShow];

    let imageIndex = findIndex(_imagesToShow, image => image.id === id);
    _imagesToShow[imageIndex].width = width;
    _imagesToShow[imageIndex].height = height;
    let indexForMaxDimensionImage = 0;
    let container = {
      width: this.state.containerWidth,
      height: this.state.containerHeight
    };

    let contenders = ["Width", "Height"];
    let winner = contenders[this.state.ladyLuck].toLowerCase();
    let loser = first(
      without(contenders, contenders[this.state.ladyLuck])
    ).toLowerCase();

    // if all the images have width and height, we can rotate the array around the image with max height,
    // so that the first image has the max height and can be displayed properly on the left side
    if (
      all(_imagesToShow, function (image) {
        return image.width && image.height
      })
    ) {
      // TODO - the logic should not only look the the image with max height but with height >= containerHeight and max(height/width ratio)

      let maxDimensionImage = max(_imagesToShow, function (image) {
        return image[winner]
      });

      indexForMaxDimensionImage = findIndex(
        _imagesToShow,
        image => image.id === maxDimensionImage.id
      );

      if (_imagesToShow[indexForMaxDimensionImage][winner] < container[winner]) {
        container[winner] = _imagesToShow[indexForMaxDimensionImage][winner];
        container[loser] = container[winner];
      }

      let indexForBestMaxImage = _imagesToShow.reduce((result, image, index) => {
        if (
          image[winner] >= container[winner] &&
          image[winner] / image[loser] >
          _imagesToShow[result][winner] /
          _imagesToShow[result][loser]
        ) {
          return index
        }
        return result
      }, 0);

      _imagesToShow.push.apply(
        _imagesToShow,
        _imagesToShow.splice(0, indexForBestMaxImage)
      );

      this.setState({
        imagesToShow: _imagesToShow,
        containerHeight: container.height,
        containerWidth: container.width
      })
    }
  };
}

function imageLoadCallback(id, callback) {
  return function (e) {
    callback(id, this.naturalWidth, this.naturalHeight)
  }
}

function getImageDimensions(src, id, cb) {
  let img = new Image();
  img.id = id;
  imageElements.push(img);
  img.addEventListener("load", imageLoadCallback(id, cb));

  img.src = src
}

function first(arr) {
  return arr[0]
}

function without(arr, exclude) {
  return arr.filter(item => item !== exclude)
}

function findIndex(arr, pred) {
  return arr.reduce((acc, val, index) => {
    if (acc >= 0) {
      return acc
    }

    return pred(val) ? index : acc
  }, -1)
}

function all(arr, pred) {
  return arr.reduce((acc, item) => {
    return pred(item) && acc
  }, true)
}

function max(arr, iteratee) {
  return arr.reduce((acc, item) => {
    if (iteratee(item) > iteratee(acc)) {
      return item
    } else {
      return acc
    }
  }, arr[0])
}