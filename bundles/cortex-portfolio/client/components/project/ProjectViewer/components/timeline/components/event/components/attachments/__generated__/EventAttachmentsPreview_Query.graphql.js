/**
 * @flow
 * @relayHash fcb84fe6af651117fa53e92c04c0bb9a
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
type EventAttachmentsPreview_event$ref = any;
export type EventAttachmentsPreview_QueryVariables = {|
  id: string,
  first?: ?number,
  filters?: ?$ReadOnlyArray<?string>,
|};
export type EventAttachmentsPreview_QueryResponse = {|
  +event: ?{|
    +$fragmentRefs: EventAttachmentsPreview_event$ref
  |}
|};
export type EventAttachmentsPreview_Query = {|
  variables: EventAttachmentsPreview_QueryVariables,
  response: EventAttachmentsPreview_QueryResponse,
|};
*/


/*
query EventAttachmentsPreview_Query(
  $id: ID!
  $first: Int
  $filters: [String]
) {
  event(id: $id) {
    ...EventAttachmentsPreview_event
    id
  }
}

fragment EventAttachmentsPreview_event on Event {
  attachmentsCount
  attachments(first: $first, filters: $filters, sortBy: "creationDate", sortDirection: "asc") {
    edges {
      node {
        resource {
          id
          mime
          publicUrl
        }
        id
        __typename
      }
      cursor
    }
    pageInfo {
      endCursor
      hasNextPage
    }
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "id",
    "type": "ID!",
    "defaultValue": null
  },
  {
    "kind": "LocalArgument",
    "name": "first",
    "type": "Int",
    "defaultValue": null
  },
  {
    "kind": "LocalArgument",
    "name": "filters",
    "type": "[String]",
    "defaultValue": null
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "id",
    "variableName": "id",
    "type": "ID!"
  }
],
v2 = [
  {
    "kind": "Variable",
    "name": "filters",
    "variableName": "filters",
    "type": "[String]"
  },
  {
    "kind": "Variable",
    "name": "first",
    "variableName": "first",
    "type": "Int"
  },
  {
    "kind": "Literal",
    "name": "sortBy",
    "value": "creationDate",
    "type": "String"
  },
  {
    "kind": "Literal",
    "name": "sortDirection",
    "value": "asc",
    "type": "String"
  }
],
v3 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "id",
  "args": null,
  "storageKey": null
};
return {
  "kind": "Request",
  "operationKind": "query",
  "name": "EventAttachmentsPreview_Query",
  "id": null,
  "text": "query EventAttachmentsPreview_Query(\n  $id: ID!\n  $first: Int\n  $filters: [String]\n) {\n  event(id: $id) {\n    ...EventAttachmentsPreview_event\n    id\n  }\n}\n\nfragment EventAttachmentsPreview_event on Event {\n  attachmentsCount\n  attachments(first: $first, filters: $filters, sortBy: \"creationDate\", sortDirection: \"asc\") {\n    edges {\n      node {\n        resource {\n          id\n          mime\n          publicUrl\n        }\n        id\n        __typename\n      }\n      cursor\n    }\n    pageInfo {\n      endCursor\n      hasNextPage\n    }\n  }\n}\n",
  "metadata": {},
  "fragment": {
    "kind": "Fragment",
    "name": "EventAttachmentsPreview_Query",
    "type": "Query",
    "metadata": null,
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "event",
        "storageKey": null,
        "args": v1,
        "concreteType": "Event",
        "plural": false,
        "selections": [
          {
            "kind": "FragmentSpread",
            "name": "EventAttachmentsPreview_event",
            "args": null
          }
        ]
      }
    ]
  },
  "operation": {
    "kind": "Operation",
    "name": "EventAttachmentsPreview_Query",
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "event",
        "storageKey": null,
        "args": v1,
        "concreteType": "Event",
        "plural": false,
        "selections": [
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "attachmentsCount",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "LinkedField",
            "alias": null,
            "name": "attachments",
            "storageKey": null,
            "args": v2,
            "concreteType": "AttachmentConnection",
            "plural": false,
            "selections": [
              {
                "kind": "LinkedField",
                "alias": null,
                "name": "edges",
                "storageKey": null,
                "args": null,
                "concreteType": "AttachmentEdge",
                "plural": true,
                "selections": [
                  {
                    "kind": "LinkedField",
                    "alias": null,
                    "name": "node",
                    "storageKey": null,
                    "args": null,
                    "concreteType": "Attachment",
                    "plural": false,
                    "selections": [
                      {
                        "kind": "LinkedField",
                        "alias": null,
                        "name": "resource",
                        "storageKey": null,
                        "args": null,
                        "concreteType": "Resource",
                        "plural": false,
                        "selections": [
                          v3,
                          {
                            "kind": "ScalarField",
                            "alias": null,
                            "name": "mime",
                            "args": null,
                            "storageKey": null
                          },
                          {
                            "kind": "ScalarField",
                            "alias": null,
                            "name": "publicUrl",
                            "args": null,
                            "storageKey": null
                          }
                        ]
                      },
                      v3,
                      {
                        "kind": "ScalarField",
                        "alias": null,
                        "name": "__typename",
                        "args": null,
                        "storageKey": null
                      }
                    ]
                  },
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "cursor",
                    "args": null,
                    "storageKey": null
                  }
                ]
              },
              {
                "kind": "LinkedField",
                "alias": null,
                "name": "pageInfo",
                "storageKey": null,
                "args": null,
                "concreteType": "PageInfo",
                "plural": false,
                "selections": [
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "endCursor",
                    "args": null,
                    "storageKey": null
                  },
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "hasNextPage",
                    "args": null,
                    "storageKey": null
                  }
                ]
              }
            ]
          },
          {
            "kind": "LinkedHandle",
            "alias": null,
            "name": "attachments",
            "args": v2,
            "handle": "connection",
            "key": "EventAttachmentsPreview_attachments",
            "filters": []
          },
          v3
        ]
      }
    ]
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = 'ccd070fd795b9dae433dd7764822aa58';
module.exports = node;
