/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *  
 */
import React from 'react';
import {graphql, createRefetchContainer} from 'react-relay';
import {withContext, withRelay} from '../../../../../../../../../../../cortex-core/client';
import {withRouter} from 'react-router';
import {Placeholder, Modal, Icon, Image, Grid} from 'semantic-ui-react';
import style from './EventInvolvementsPreview.less';
import {ActorAvatar} from "../../../../../../../../../../../cortex-addressbook/client";
import _ from 'lodash';
import classnames from 'classnames';

@withContext
@withRouter
class EventInvolvementsPreview extends React.Component {
  state = {
    maxCount: 6
  };

  render() {
    const {t, event} = this.props;

    return (
      <>
        {event.involvements.edges.slice(0, this.state.maxCount).map(({node: involvement}) => {
          let role = involvement.role;

          return (
            <ActorAvatar key={involvement.id}
                         className={classnames(style.avatar, {[style.single]: event.involvements.edges.length === 1})}
                         popupName
                         popupProps={{ size:'small', wide:'very'}}
                         extraTextAfterName={role ? <span>: {' '} <strong>{role}</strong></span> : ''}
                         variables={{id: _.get(involvement, 'actor.id'), type: _.get(involvement, 'actor.__typename')}}/>
          )
        })}

        <span className={style.message}>
          {this.renderMessage()}
        </span>
      </>
    )
  }

  renderMessage(){
    const {t, event} = this.props;

    if (event.involvementsCount === 1){
      return t("is involved in this event.");
    }

    if (event.involvementsCount <= this.state.maxCount){
      return t("are involved in this event.");
    }

    return t("and {{count}} others are involved in this event.", {count: event.involvementsCount - this.state.maxCount});
  }
}

export default withRelay(
  graphql`
    query EventInvolvementsPreview_Query($id: ID! $first: Int $filters: [String]){
      event(id: $id){
        ...EventInvolvementsPreview_event
      }
    }
  `,
  {
    first: 50
  },
  createRefetchContainer(EventInvolvementsPreview, graphql`
    fragment EventInvolvementsPreview_event on Event {
      involvementsCount
      involvements(first: $first filters: $filters sortBy: "creationDate" sortDirection: "asc") @connection(key: "EventInvolvementsPreview_involvements", filters:[]){
        edges{
          node{
            id
            actor{
              __typename
              id
            }
            role
          }
        }
      }
    }
  `),
  false,
  (
    <Placeholder fluid>
      <Placeholder.Header image/>
      <Placeholder.Header image/>
      <Placeholder.Header image/>
    </Placeholder>
  )
);