/**
 * @flow
 * @relayHash 8dc4530ccb6e00434116f1f5a9ae60d4
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
type EventInvolvementsPreview_event$ref = any;
export type EventInvolvementsPreview_QueryVariables = {|
  id: string,
  first?: ?number,
  filters?: ?$ReadOnlyArray<?string>,
|};
export type EventInvolvementsPreview_QueryResponse = {|
  +event: ?{|
    +$fragmentRefs: EventInvolvementsPreview_event$ref
  |}
|};
export type EventInvolvementsPreview_Query = {|
  variables: EventInvolvementsPreview_QueryVariables,
  response: EventInvolvementsPreview_QueryResponse,
|};
*/


/*
query EventInvolvementsPreview_Query(
  $id: ID!
  $first: Int
  $filters: [String]
) {
  event(id: $id) {
    ...EventInvolvementsPreview_event
    id
  }
}

fragment EventInvolvementsPreview_event on Event {
  involvementsCount
  involvements(first: $first, filters: $filters, sortBy: "creationDate", sortDirection: "asc") {
    edges {
      node {
        id
        actor {
          __typename
          id
        }
        role
        __typename
      }
      cursor
    }
    pageInfo {
      endCursor
      hasNextPage
    }
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "id",
    "type": "ID!",
    "defaultValue": null
  },
  {
    "kind": "LocalArgument",
    "name": "first",
    "type": "Int",
    "defaultValue": null
  },
  {
    "kind": "LocalArgument",
    "name": "filters",
    "type": "[String]",
    "defaultValue": null
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "id",
    "variableName": "id",
    "type": "ID!"
  }
],
v2 = [
  {
    "kind": "Variable",
    "name": "filters",
    "variableName": "filters",
    "type": "[String]"
  },
  {
    "kind": "Variable",
    "name": "first",
    "variableName": "first",
    "type": "Int"
  },
  {
    "kind": "Literal",
    "name": "sortBy",
    "value": "creationDate",
    "type": "String"
  },
  {
    "kind": "Literal",
    "name": "sortDirection",
    "value": "asc",
    "type": "String"
  }
],
v3 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "id",
  "args": null,
  "storageKey": null
},
v4 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "__typename",
  "args": null,
  "storageKey": null
};
return {
  "kind": "Request",
  "operationKind": "query",
  "name": "EventInvolvementsPreview_Query",
  "id": null,
  "text": "query EventInvolvementsPreview_Query(\n  $id: ID!\n  $first: Int\n  $filters: [String]\n) {\n  event(id: $id) {\n    ...EventInvolvementsPreview_event\n    id\n  }\n}\n\nfragment EventInvolvementsPreview_event on Event {\n  involvementsCount\n  involvements(first: $first, filters: $filters, sortBy: \"creationDate\", sortDirection: \"asc\") {\n    edges {\n      node {\n        id\n        actor {\n          __typename\n          id\n        }\n        role\n        __typename\n      }\n      cursor\n    }\n    pageInfo {\n      endCursor\n      hasNextPage\n    }\n  }\n}\n",
  "metadata": {},
  "fragment": {
    "kind": "Fragment",
    "name": "EventInvolvementsPreview_Query",
    "type": "Query",
    "metadata": null,
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "event",
        "storageKey": null,
        "args": v1,
        "concreteType": "Event",
        "plural": false,
        "selections": [
          {
            "kind": "FragmentSpread",
            "name": "EventInvolvementsPreview_event",
            "args": null
          }
        ]
      }
    ]
  },
  "operation": {
    "kind": "Operation",
    "name": "EventInvolvementsPreview_Query",
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "event",
        "storageKey": null,
        "args": v1,
        "concreteType": "Event",
        "plural": false,
        "selections": [
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "involvementsCount",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "LinkedField",
            "alias": null,
            "name": "involvements",
            "storageKey": null,
            "args": v2,
            "concreteType": "InvolvementConnection",
            "plural": false,
            "selections": [
              {
                "kind": "LinkedField",
                "alias": null,
                "name": "edges",
                "storageKey": null,
                "args": null,
                "concreteType": "InvolvementEdge",
                "plural": true,
                "selections": [
                  {
                    "kind": "LinkedField",
                    "alias": null,
                    "name": "node",
                    "storageKey": null,
                    "args": null,
                    "concreteType": "Involvement",
                    "plural": false,
                    "selections": [
                      v3,
                      {
                        "kind": "LinkedField",
                        "alias": null,
                        "name": "actor",
                        "storageKey": null,
                        "args": null,
                        "concreteType": null,
                        "plural": false,
                        "selections": [
                          v4,
                          v3
                        ]
                      },
                      {
                        "kind": "ScalarField",
                        "alias": null,
                        "name": "role",
                        "args": null,
                        "storageKey": null
                      },
                      v4
                    ]
                  },
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "cursor",
                    "args": null,
                    "storageKey": null
                  }
                ]
              },
              {
                "kind": "LinkedField",
                "alias": null,
                "name": "pageInfo",
                "storageKey": null,
                "args": null,
                "concreteType": "PageInfo",
                "plural": false,
                "selections": [
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "endCursor",
                    "args": null,
                    "storageKey": null
                  },
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "hasNextPage",
                    "args": null,
                    "storageKey": null
                  }
                ]
              }
            ]
          },
          {
            "kind": "LinkedHandle",
            "alias": null,
            "name": "involvements",
            "args": v2,
            "handle": "connection",
            "key": "EventInvolvementsPreview_involvements",
            "filters": []
          },
          v3
        ]
      }
    ]
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = '41f9e261d493bf38a085299c72364b62';
module.exports = node;
