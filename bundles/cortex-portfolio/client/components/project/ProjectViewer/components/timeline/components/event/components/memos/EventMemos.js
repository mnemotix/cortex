/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *  
 */
import React from 'react';
import PropTypes from 'prop-types';
import {graphql, createRefetchContainer} from 'react-relay';
import {withRouter} from 'react-router';
import {Button, Placeholder, Popup, Segment} from 'semantic-ui-react';
import style from './EventMemos.less';
import _ from 'lodash';
import moment from 'moment';
import {withContext, withRelay, fetchQuery} from '../../../../../../../../../../../cortex-core/client';
import {ActorFeedRaw as ActorFeed, fetchActors} from "../../../../../../../../../../../cortex-addressbook/client";
import {DefaultMutations} from "../../../../../../../../../../../../.yalc/@mnemotix/synaptix.js/lib/client";
import {RichEditor} from "../../../../../../../../../../../cortex-rich-editor/client";
import InvolvementNode from "./components/InvolvementNode";
import TaggingNode from "./components/TaggingNode";
import {fetchEventInvolvements} from "../../../../../../../../../fetchs/fetchEventInvolvements";
import {fromGlobalId} from "graphql-relay/lib/node/node";

@withContext
@withRouter
class EventMemos extends React.Component {
  static propTypes = {
    editingMemo: PropTypes.object
  };

  state = {
    maxCount: 1,
    editingMemo: this.props.editingMemo,
    savingMemo: false
  };

  componentWillReceiveProps(nextProps){
    if (nextProps.editingMemo !== this.props.editingMemo){
      this.setState({
        editingMemo: nextProps.editingMemo
      });
    }
  }

  render() {
    const {t, event, me} = this.props;

    return (
      <>
        {event.memos.edges.slice(0, this.state.maxCount).map(({node: memo}) => {
          let editable = me.id === _.get(memo, 'creator.id');
          let editing = this.state.editingMemo && this.state.editingMemo.id === memo.id;

          return (
            <div key={memo.id} className={style.memo}>
              <ActorFeed key={memo.id}
                         className={style.creator}
                         size={'small'}
                         headerMessage={ moment(memo.creationDate).format('LL')}
                         actor={memo.creator}
              />

              {editable ? (
                <Popup content={editing ? t('Save changes') : t('Edit memo')}
                       size={'mini'}
                       position={'top center'}
                       trigger={(
                         <Button basic size={"mini"}
                                 className={style.editButton}
                                 icon={editing ? 'save' : 'edit'}
                                 loading={this.state.savingMemo}
                                 onClick={() => {
                                   if (editing){
                                     this.setState({editingMemo: null});
                                   } else {
                                     this.setState({editingMemo: memo});
                                   }
                                 }}/>
                       )}
                />
              ) : null}

              <RichEditor className={style.editor}
                          editing={editing}
                          content={memo.content || ''}
                          renderTypedTag={this._handleRenderTypedTag}
                          onChange={this._handleSaveMemoContent.bind(this, memo)}
                          richTextDropdownDefinitions={[
                            {
                              triggerChar: '@',
                              type: 'Involvement',
                              renderDropdownOptions: (qs) => fetchActors({qs, first: 10, renderAsDropdownOptions: true}),
                              createMentionFromClickedOption: async (type, option) => {
                                return await this.createMentionFromClickedOption(type, option);
                              }
                            }
                          ]}
              />
            </div>
          )
        })}
      </>
    )
  }

  _handleRenderTypedTag = (type, id, prefetchedData, defaultRendered) => {
    switch (type) {
      case 'Involvement':
        return (
          <InvolvementNode variables={{id}}
                           renderLoading={() => defaultRendered}
                           renderError={() => defaultRendered}
          />
        );
      case 'Tagging':
        return (
          <TaggingNode variables={{id}}
                       renderLoading={() => defaultRendered}
                       renderError={() => defaultRendered}
          />
        )
    }
    return defaultRendered;
  };

  _handleSaveMemoContent = (memo, content, alertSuccess = false) => {
    const {relay: {environment}} = this.props;

    if(memo.content !== content){
      this.setState({
        savingMemo: true
      });
      (new DefaultMutations.projects.UpdateMemoMutation({
        environment,
        onCompleted: () => {
        },
        onError: (errors) => {
          this.props.context.alertMutationError(errors);
        },
        onAfter: () => {
          this.setState({
            savingMemo: false
          });
        }
      }))
        .apply({
          id: memo.id,
          content
        });
    }
  };

  async createMentionFromClickedOption(type, option){
    const {relay: {environment}, event} = this.props;

    switch (type) {
      case 'Involvement':
        let involvements = await fetchEventInvolvements({
          eventId: event.id,
          filters: `actor:${fromGlobalId(option.id).id}`
        });

        if (involvements.length > 0){
          return involvements[0];
        } else {
          return new Promise((done) => {
             (new DefaultMutations.projects.CreateInvolvementMutation({
              environment,
              parentId: event.id,
              connectionDefinitions: [{
                connectionKey: "EventInvolvementsPreview_involvements"
              }],
              onCompleted: (involvement) => {
                done(involvement);
              },
              onError: (errors) => {
                this.props.context.alertMutationError(errors);
              }
            }))
              .apply({
                extraInputs: {
                  eventId: event.id,
                  actorId: option.id
                },
              });
          });
        }
      case 'Tagging':
      // return (new DefaultMutations.projects.CreateInvolvementMutation({
      //   environment,
      //   onCompleted: (involvement) => {
      //     done(involvement);
      //   },
      //   onError: (errors) => {
      //     this.props.context.alertMutationError(errors);
      //   }
      // }))
      //   .apply({
      //     eventId: event.id,
      //     actorId: option.id
      //   });
    }
  }
}

export default withRelay(
  graphql`
    query EventMemos_Query($id: ID! $first: Int $filters: [String]){
      me{
        ...EventMemos_me 
      }
      
      event(id: $id){
        ...EventMemos_event
      }
    }
  `,
  {
    first: 1,
  },
  createRefetchContainer(EventMemos, graphql`
    fragment EventMemos_me on Person{
      id  
    }
    
    fragment EventMemos_event on Event {
      id
      memos(first: $first filters: $filters sortBy: "creationDate" sortDirection: "asc") @connection(key: "EventMemos_memos", filters:[]){
        edges{
          node{
            id
            creationDate
            content
            creator{
              id
              ...ActorFeed_actor @relay(mask: false)
            }
          }
        }
      }
    }
  `),
  false,
  (
    <Placeholder fluid>
      <Placeholder.Header image/>
      <Placeholder.Image />
    </Placeholder>
  )
);