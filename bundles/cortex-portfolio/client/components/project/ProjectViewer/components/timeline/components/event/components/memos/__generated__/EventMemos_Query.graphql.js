/**
 * @flow
 * @relayHash 42cf309005f7d02f269a8c8193a8c7d1
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
type EventMemos_event$ref = any;
type EventMemos_me$ref = any;
export type EventMemos_QueryVariables = {|
  id: string,
  first?: ?number,
  filters?: ?$ReadOnlyArray<?string>,
|};
export type EventMemos_QueryResponse = {|
  +me: ?{|
    +$fragmentRefs: EventMemos_me$ref
  |},
  +event: ?{|
    +$fragmentRefs: EventMemos_event$ref
  |},
|};
export type EventMemos_Query = {|
  variables: EventMemos_QueryVariables,
  response: EventMemos_QueryResponse,
|};
*/


/*
query EventMemos_Query(
  $id: ID!
  $first: Int
  $filters: [String]
) {
  me {
    ...EventMemos_me
    id
  }
  event(id: $id) {
    ...EventMemos_event
    id
  }
}

fragment EventMemos_me on Person {
  id
}

fragment EventMemos_event on Event {
  id
  memos(first: $first, filters: $filters, sortBy: "creationDate", sortDirection: "asc") {
    edges {
      node {
        id
        creationDate
        content
        creator {
          id
          ... on ActorInterface {
            __typename
            id
            avatar
            displayName
          }
        }
        __typename
      }
      cursor
    }
    pageInfo {
      endCursor
      hasNextPage
    }
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "id",
    "type": "ID!",
    "defaultValue": null
  },
  {
    "kind": "LocalArgument",
    "name": "first",
    "type": "Int",
    "defaultValue": null
  },
  {
    "kind": "LocalArgument",
    "name": "filters",
    "type": "[String]",
    "defaultValue": null
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "id",
    "variableName": "id",
    "type": "ID!"
  }
],
v2 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "id",
  "args": null,
  "storageKey": null
},
v3 = [
  {
    "kind": "Variable",
    "name": "filters",
    "variableName": "filters",
    "type": "[String]"
  },
  {
    "kind": "Variable",
    "name": "first",
    "variableName": "first",
    "type": "Int"
  },
  {
    "kind": "Literal",
    "name": "sortBy",
    "value": "creationDate",
    "type": "String"
  },
  {
    "kind": "Literal",
    "name": "sortDirection",
    "value": "asc",
    "type": "String"
  }
],
v4 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "__typename",
  "args": null,
  "storageKey": null
};
return {
  "kind": "Request",
  "operationKind": "query",
  "name": "EventMemos_Query",
  "id": null,
  "text": "query EventMemos_Query(\n  $id: ID!\n  $first: Int\n  $filters: [String]\n) {\n  me {\n    ...EventMemos_me\n    id\n  }\n  event(id: $id) {\n    ...EventMemos_event\n    id\n  }\n}\n\nfragment EventMemos_me on Person {\n  id\n}\n\nfragment EventMemos_event on Event {\n  id\n  memos(first: $first, filters: $filters, sortBy: \"creationDate\", sortDirection: \"asc\") {\n    edges {\n      node {\n        id\n        creationDate\n        content\n        creator {\n          id\n          ... on ActorInterface {\n            __typename\n            id\n            avatar\n            displayName\n          }\n        }\n        __typename\n      }\n      cursor\n    }\n    pageInfo {\n      endCursor\n      hasNextPage\n    }\n  }\n}\n",
  "metadata": {},
  "fragment": {
    "kind": "Fragment",
    "name": "EventMemos_Query",
    "type": "Query",
    "metadata": null,
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "me",
        "storageKey": null,
        "args": null,
        "concreteType": "Person",
        "plural": false,
        "selections": [
          {
            "kind": "FragmentSpread",
            "name": "EventMemos_me",
            "args": null
          }
        ]
      },
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "event",
        "storageKey": null,
        "args": v1,
        "concreteType": "Event",
        "plural": false,
        "selections": [
          {
            "kind": "FragmentSpread",
            "name": "EventMemos_event",
            "args": null
          }
        ]
      }
    ]
  },
  "operation": {
    "kind": "Operation",
    "name": "EventMemos_Query",
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "me",
        "storageKey": null,
        "args": null,
        "concreteType": "Person",
        "plural": false,
        "selections": [
          v2
        ]
      },
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "event",
        "storageKey": null,
        "args": v1,
        "concreteType": "Event",
        "plural": false,
        "selections": [
          v2,
          {
            "kind": "LinkedField",
            "alias": null,
            "name": "memos",
            "storageKey": null,
            "args": v3,
            "concreteType": "MemoConnection",
            "plural": false,
            "selections": [
              {
                "kind": "LinkedField",
                "alias": null,
                "name": "edges",
                "storageKey": null,
                "args": null,
                "concreteType": "MemoEdge",
                "plural": true,
                "selections": [
                  {
                    "kind": "LinkedField",
                    "alias": null,
                    "name": "node",
                    "storageKey": null,
                    "args": null,
                    "concreteType": "Memo",
                    "plural": false,
                    "selections": [
                      v2,
                      {
                        "kind": "ScalarField",
                        "alias": null,
                        "name": "creationDate",
                        "args": null,
                        "storageKey": null
                      },
                      {
                        "kind": "ScalarField",
                        "alias": null,
                        "name": "content",
                        "args": null,
                        "storageKey": null
                      },
                      {
                        "kind": "LinkedField",
                        "alias": null,
                        "name": "creator",
                        "storageKey": null,
                        "args": null,
                        "concreteType": "Person",
                        "plural": false,
                        "selections": [
                          v2,
                          v4,
                          {
                            "kind": "ScalarField",
                            "alias": null,
                            "name": "avatar",
                            "args": null,
                            "storageKey": null
                          },
                          {
                            "kind": "ScalarField",
                            "alias": null,
                            "name": "displayName",
                            "args": null,
                            "storageKey": null
                          }
                        ]
                      },
                      v4
                    ]
                  },
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "cursor",
                    "args": null,
                    "storageKey": null
                  }
                ]
              },
              {
                "kind": "LinkedField",
                "alias": null,
                "name": "pageInfo",
                "storageKey": null,
                "args": null,
                "concreteType": "PageInfo",
                "plural": false,
                "selections": [
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "endCursor",
                    "args": null,
                    "storageKey": null
                  },
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "hasNextPage",
                    "args": null,
                    "storageKey": null
                  }
                ]
              }
            ]
          },
          {
            "kind": "LinkedHandle",
            "alias": null,
            "name": "memos",
            "args": v3,
            "handle": "connection",
            "key": "EventMemos_memos",
            "filters": []
          }
        ]
      }
    ]
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = '1f82ff45ecdab372c84e48430e5dfe24';
module.exports = node;
