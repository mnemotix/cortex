/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *  
 */
import React from 'react';
import PropTypes from 'prop-types';
import {graphql, createFragmentContainer} from 'react-relay';
import {withContext, withRelay, Modules} from '../../../../../../../../../../../../cortex-core/client';
import {withRouter} from 'react-router';
import {ActorAvatarRaw} from "../../../../../../../../../../../../cortex-addressbook/client";
import style from './InvolvementNode.less';

@withContext
@withRouter
class InvolvementNode extends React.Component {
  static propTypes = {
    popupName: PropTypes.bool
  };

  static defaultProps = {
    extraTextAfterName:  '',
    extraTextBeforeName: ''
  };

  render() {
    const {t, involvement, className} = this.props;

    return (
      <ActorAvatarRaw className={style.involvement} actor={involvement.actor} popupAvatar />
    )
  }
}

export default withRelay(
  graphql`
    query InvolvementNode_Query($id: ID!) {
      involvement(id: $id){
        ...InvolvementNode_involvement
      }
    }
  `,
  {},
  createFragmentContainer(InvolvementNode, graphql`
    fragment InvolvementNode_involvement on Involvement {
      __typename
      id
      actor{
        ...ActorAvatar_actor @relay(mask: false)
      }
    }
  `),
  true
);