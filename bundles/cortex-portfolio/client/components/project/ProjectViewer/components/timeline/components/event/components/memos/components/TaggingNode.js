/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *  
 */
import React from 'react';
import PropTypes from 'prop-types';
import {graphql, createFragmentContainer} from 'react-relay';
import {withContext, withRelay, Modules} from '../../../../../../../../../../../../cortex-core/client';
import {withRouter} from 'react-router';
import {Label} from 'semantic-ui-react';

@withContext
@withRouter
class TaggingNode extends React.Component {
  static propTypes = {
  };

  render() {
    const {tagging} = this.props;

    return (
      <Label style={{backgroundColor: _.get(tagging, 'concept.schemes.edges.0.node.color')}} horizontal>
        {_.get(tagging, 'concept.prefLabel')}
      </Label>
    )
  }
}

export default withRelay(
  graphql`
    query TaggingNode_Query($id: ID!) {
      tagging(id: $id){
        ...TaggingNode_tagging
      }
    }
  `,
  {},
  createFragmentContainer(TaggingNode, graphql`
    fragment TaggingNode_tagging on Tagging {
      __typename
      id
      concept{
        prefLabel
        schemes(first: 1){
          edges{
            node{
              color
            }
          }
        }
      }
    }
  `),
  true
);