/**
 * @flow
 * @relayHash 21f1a6baaf8251e153a77d88ff825e79
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
type InvolvementNode_involvement$ref = any;
export type InvolvementNode_QueryVariables = {|
  id: string
|};
export type InvolvementNode_QueryResponse = {|
  +involvement: ?{|
    +$fragmentRefs: InvolvementNode_involvement$ref
  |}
|};
export type InvolvementNode_Query = {|
  variables: InvolvementNode_QueryVariables,
  response: InvolvementNode_QueryResponse,
|};
*/


/*
query InvolvementNode_Query(
  $id: ID!
) {
  involvement(id: $id) {
    ...InvolvementNode_involvement
    id
  }
}

fragment InvolvementNode_involvement on Involvement {
  __typename
  id
  actor {
    __typename
    id
    avatar
    displayName
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "id",
    "type": "ID!",
    "defaultValue": null
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "id",
    "variableName": "id",
    "type": "ID"
  }
],
v2 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "__typename",
  "args": null,
  "storageKey": null
},
v3 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "id",
  "args": null,
  "storageKey": null
};
return {
  "kind": "Request",
  "operationKind": "query",
  "name": "InvolvementNode_Query",
  "id": null,
  "text": "query InvolvementNode_Query(\n  $id: ID!\n) {\n  involvement(id: $id) {\n    ...InvolvementNode_involvement\n    id\n  }\n}\n\nfragment InvolvementNode_involvement on Involvement {\n  __typename\n  id\n  actor {\n    __typename\n    id\n    avatar\n    displayName\n  }\n}\n",
  "metadata": {},
  "fragment": {
    "kind": "Fragment",
    "name": "InvolvementNode_Query",
    "type": "Query",
    "metadata": null,
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "involvement",
        "storageKey": null,
        "args": v1,
        "concreteType": "Involvement",
        "plural": false,
        "selections": [
          {
            "kind": "FragmentSpread",
            "name": "InvolvementNode_involvement",
            "args": null
          }
        ]
      }
    ]
  },
  "operation": {
    "kind": "Operation",
    "name": "InvolvementNode_Query",
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "involvement",
        "storageKey": null,
        "args": v1,
        "concreteType": "Involvement",
        "plural": false,
        "selections": [
          v2,
          v3,
          {
            "kind": "LinkedField",
            "alias": null,
            "name": "actor",
            "storageKey": null,
            "args": null,
            "concreteType": null,
            "plural": false,
            "selections": [
              v2,
              v3,
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "avatar",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "displayName",
                "args": null,
                "storageKey": null
              }
            ]
          }
        ]
      }
    ]
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = 'fdf38a29bfcce16462b1859800c457f2';
module.exports = node;
