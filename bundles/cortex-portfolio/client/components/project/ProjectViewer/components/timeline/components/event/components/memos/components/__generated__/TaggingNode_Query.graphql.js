/**
 * @flow
 * @relayHash 442859110766b699c861ece1b1d7d656
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
type TaggingNode_tagging$ref = any;
export type TaggingNode_QueryVariables = {|
  id: string
|};
export type TaggingNode_QueryResponse = {|
  +tagging: ?{|
    +$fragmentRefs: TaggingNode_tagging$ref
  |}
|};
export type TaggingNode_Query = {|
  variables: TaggingNode_QueryVariables,
  response: TaggingNode_QueryResponse,
|};
*/


/*
query TaggingNode_Query(
  $id: ID!
) {
  tagging(id: $id) {
    ...TaggingNode_tagging
    id
  }
}

fragment TaggingNode_tagging on Tagging {
  __typename
  id
  concept {
    prefLabel
    schemes(first: 1) {
      edges {
        node {
          color
          id
        }
      }
    }
    id
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "id",
    "type": "ID!",
    "defaultValue": null
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "id",
    "variableName": "id",
    "type": "ID"
  }
],
v2 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "id",
  "args": null,
  "storageKey": null
};
return {
  "kind": "Request",
  "operationKind": "query",
  "name": "TaggingNode_Query",
  "id": null,
  "text": "query TaggingNode_Query(\n  $id: ID!\n) {\n  tagging(id: $id) {\n    ...TaggingNode_tagging\n    id\n  }\n}\n\nfragment TaggingNode_tagging on Tagging {\n  __typename\n  id\n  concept {\n    prefLabel\n    schemes(first: 1) {\n      edges {\n        node {\n          color\n          id\n        }\n      }\n    }\n    id\n  }\n}\n",
  "metadata": {},
  "fragment": {
    "kind": "Fragment",
    "name": "TaggingNode_Query",
    "type": "Query",
    "metadata": null,
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "tagging",
        "storageKey": null,
        "args": v1,
        "concreteType": "Tagging",
        "plural": false,
        "selections": [
          {
            "kind": "FragmentSpread",
            "name": "TaggingNode_tagging",
            "args": null
          }
        ]
      }
    ]
  },
  "operation": {
    "kind": "Operation",
    "name": "TaggingNode_Query",
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "tagging",
        "storageKey": null,
        "args": v1,
        "concreteType": "Tagging",
        "plural": false,
        "selections": [
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "__typename",
            "args": null,
            "storageKey": null
          },
          v2,
          {
            "kind": "LinkedField",
            "alias": null,
            "name": "concept",
            "storageKey": null,
            "args": null,
            "concreteType": "Concept",
            "plural": false,
            "selections": [
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "prefLabel",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "LinkedField",
                "alias": null,
                "name": "schemes",
                "storageKey": "schemes(first:1)",
                "args": [
                  {
                    "kind": "Literal",
                    "name": "first",
                    "value": 1,
                    "type": "Int"
                  }
                ],
                "concreteType": "SchemeConnection",
                "plural": false,
                "selections": [
                  {
                    "kind": "LinkedField",
                    "alias": null,
                    "name": "edges",
                    "storageKey": null,
                    "args": null,
                    "concreteType": "SchemeEdge",
                    "plural": true,
                    "selections": [
                      {
                        "kind": "LinkedField",
                        "alias": null,
                        "name": "node",
                        "storageKey": null,
                        "args": null,
                        "concreteType": "Scheme",
                        "plural": false,
                        "selections": [
                          {
                            "kind": "ScalarField",
                            "alias": null,
                            "name": "color",
                            "args": null,
                            "storageKey": null
                          },
                          v2
                        ]
                      }
                    ]
                  }
                ]
              },
              v2
            ]
          }
        ]
      }
    ]
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = '7d022ce4dac2c65b587fbb51c9924b8d';
module.exports = node;
