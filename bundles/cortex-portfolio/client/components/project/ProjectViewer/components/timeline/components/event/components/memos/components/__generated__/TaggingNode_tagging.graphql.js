/**
 * @flow
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteFragment } from 'relay-runtime';
import type { FragmentReference } from "relay-runtime";
declare export opaque type TaggingNode_tagging$ref: FragmentReference;
export type TaggingNode_tagging = {|
  +id: string,
  +concept: ?{|
    +prefLabel: ?string,
    +schemes: ?{|
      +edges: ?$ReadOnlyArray<?{|
        +node: ?{|
          +color: ?string
        |}
      |}>
    |},
  |},
  +__typename: "Tagging",
  +$refType: TaggingNode_tagging$ref,
|};
*/


const node/*: ConcreteFragment*/ = {
  "kind": "Fragment",
  "name": "TaggingNode_tagging",
  "type": "Tagging",
  "metadata": null,
  "argumentDefinitions": [],
  "selections": [
    {
      "kind": "ScalarField",
      "alias": null,
      "name": "__typename",
      "args": null,
      "storageKey": null
    },
    {
      "kind": "ScalarField",
      "alias": null,
      "name": "id",
      "args": null,
      "storageKey": null
    },
    {
      "kind": "LinkedField",
      "alias": null,
      "name": "concept",
      "storageKey": null,
      "args": null,
      "concreteType": "Concept",
      "plural": false,
      "selections": [
        {
          "kind": "ScalarField",
          "alias": null,
          "name": "prefLabel",
          "args": null,
          "storageKey": null
        },
        {
          "kind": "LinkedField",
          "alias": null,
          "name": "schemes",
          "storageKey": "schemes(first:1)",
          "args": [
            {
              "kind": "Literal",
              "name": "first",
              "value": 1,
              "type": "Int"
            }
          ],
          "concreteType": "SchemeConnection",
          "plural": false,
          "selections": [
            {
              "kind": "LinkedField",
              "alias": null,
              "name": "edges",
              "storageKey": null,
              "args": null,
              "concreteType": "SchemeEdge",
              "plural": true,
              "selections": [
                {
                  "kind": "LinkedField",
                  "alias": null,
                  "name": "node",
                  "storageKey": null,
                  "args": null,
                  "concreteType": "Scheme",
                  "plural": false,
                  "selections": [
                    {
                      "kind": "ScalarField",
                      "alias": null,
                      "name": "color",
                      "args": null,
                      "storageKey": null
                    }
                  ]
                }
              ]
            }
          ]
        }
      ]
    }
  ]
};
// prettier-ignore
(node/*: any*/).hash = '59f82a2c2cdf85432459efc629c0355e';
module.exports = node;
