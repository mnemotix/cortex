/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *  
 */
import React from 'react';
import PropTypes from 'prop-types';
import {
  Button, Container, Input, Segment, Grid, Image, Icon,
  Visibility, Header, List, Placeholder, Card, Transition, Modal
} from 'semantic-ui-react'
import classnames from 'classnames';

import style from './ProjectsList.less';

import withContext from "@mnemotix/cortex-core/client/utilities/withContext";
import {createRefetchContainer, graphql} from "react-relay";
import {withRelay} from "@mnemotix/cortex-core/client";
import {Link} from "react-router-dom";
import {matchPath, Redirect, Route, Switch} from "react-router";
import moment from "moment";
import {ActorAvatar} from '@mnemotix/cortex-addressbook/client';
import _ from 'lodash';
import ActorFeed from "@mnemotix/cortex-addressbook/client/components/common/avatar/ActorFeed";
import ProjectCreator from "../ProjectCreator/ProjectCreator";
import ProjectViewer from "../ProjectViewer/ProjectViewer";

@withContext
class ProjectsList extends React.Component {
  static propTypes = {
    basic: PropTypes.bool
  };

  state = {
    loading: false,
    loadingMore: false,
    loadingCreation: false,
    qs: '',
    viewType: 'list',
    sortColumn: null,
    sortDirection: null,
  };

  async componentDidMount() {
    let viewType = await this.props.context.getLocalStorage().getItem("cortex-portfolio__view-type");

    if (viewType && viewType !== this.state.viewType) {
      this.setState({
        viewType
      });
    }
  }

  render() {
    const {t, history, basic, location} = this.props;

    let isProjectOpen = !!matchPath(location.pathname, this.props.context.getRoute('Portfolio_project'));

    return (
      <>
        {isProjectOpen ? null : (
          <Segment basic={basic}>
            <Grid>
              <Grid.Column width={8}>
                <Input fluid
                       loading={this.state.loading}
                       placeholder={t('Search...')}
                       value={this.state.qs}
                       icon='search'
                       onChange={(e, {value}) => this.search(value)}
                />
              </Grid.Column>
              <Grid.Column width={4} textAlign={'center'}>
                <Button.Group basic>
                  <Button active={this.state.viewType === "list"} icon={"list"}
                          onClick={this._changeView.bind(this, 'list')}/>
                  <Button active={this.state.viewType === "tile"} icon={"th"}
                          onClick={this._changeView.bind(this, 'tile')}/>
                </Button.Group>
              </Grid.Column>
              <Grid.Column width={4} textAlign={'right'}>
                <Button primary
                        icon={'archive'}
                        content={t("Create a project")}
                        as={Link}
                        to={this.props.context.formatRoute('Portfolio_project_create')}
                />
              </Grid.Column>
            </Grid>

            {this.renderView()}
          </Segment>
        )}

        <Switch>
          <Route path={this.props.context.getRoute('Portfolio_project_create')} render={() => (
            <Modal open={true} onClose={() => history.goBack()} closeOnDimmerClick={true}>
              <Header>
                {t("Create a new project")}
              </Header>
              <Modal.Content>
                <ProjectCreator/>
              </Modal.Content>
            </Modal>
          )}/>

          <Route path={this.props.context.getRoute('Portfolio_project')} render={({match}) => {
            return (
              <ProjectViewer variables={{id: decodeURIComponent(match.params.id)}}/>
            )
          }}/>
        </Switch>
      </>
    )
  }

  _changeView = (viewType) => {
    this.props.context.getLocalStorage().setItem("cortex-portfolio__view-type", viewType);
    this.setState({
      viewType
    });
  };


  renderView() {
    const {customRenderTableView, customRenderListView, customRenderTileView} = this.props;

    switch (this.state.viewType) {
      case 'list':
        return customRenderListView ? customRenderListView(this.props) : this.renderListView();
      case 'tile':
        return customRenderTileView ? customRenderTileView(this.props) : this.renderTileView();
    }
  }

  renderListView() {
    const {portfolio, location, t} = this.props;

    return (
      <Grid columns='equal'>
        <Grid.Column>
          <Transition.Group as={List} duration={200} selection relaxed divided verticalAlign='middle'>
            {portfolio.projects.edges.map(({node: project}) => {
              let route = this.props.context.formatRoute('Portfolio_project', {id: project.id});

              return (
                <List.Item key={project.id}
                           active={!!matchPath(location.pathname, route)}
                >
                  <Grid columns={3} stretched verticalAlign={'middle'}>
                    <Grid.Column width={1}
                                 as={Link}
                                 to={route}>
                      {project.image ? (
                        <Image src={this.props.context.getImageThumbnailURL(project.image, 50)} rounded
                               size='big'/>
                      ) : (
                        <Icon size={"big"} circular fitted name={"archive"}
                              style={{color: project.color || 'grey', borderColor: project.color}}/>
                      )}
                    </Grid.Column>
                    <Grid.Column width={10}
                                 as={Link}
                                 to={route}>
                      <List.Header>
                        {project.title}
                      </List.Header>
                      {project.description}
                    </Grid.Column>
                    <Grid.Column width={5}>
                      <ActorFeed variables={{id: _.get(project, 'creator.id')}}
                                 headerMessage={t("Created by")}
                                 inlineMessage={t("on {{date}}", {date: moment(project.creationDate).format('LL')})}
                                 footerMessage={t("Last modified on {{date}}", {date: moment(project.lastUpdate).format('LL')})}
                                 size={"small"}
                      />
                    </Grid.Column>
                  </Grid>
                </List.Item>
              );
            })}

            {this.state.loadingMore ? Array(8).fill().map((_, index) => (
              <List.Item key={index}>
                <Placeholder>
                  <Placeholder.Header image>
                    <Placeholder.Line/>
                    <Placeholder.Line length='medium'/>
                  </Placeholder.Header>
                </Placeholder>
              </List.Item>
            )) : null}

            <Visibility once onUpdate={(e, {calculations: {topVisible}}) => {
              if (topVisible) {
                this.showMore();
              }
            }}/>
          </Transition.Group>

          {portfolio.projects.edges.length === 0 ? (
            <Segment textAlign='center' disabled padded='very'>
              {t("There is no result for this query...")}
            </Segment>
          ) : null}
        </Grid.Column>
      </Grid>
    );
  }

  renderTileView() {
    const {portfolio, location, t, history} = this.props;

    return (
      <>
        <Card.Group itemsPerRow={4}>
          {portfolio.projects.edges.map(({node: project}) => {
            let route = this.props.context.formatRoute('Portfolio_project', {id: project.id});

            return (
              <Card key={project.id} as={Link} to={route}>
                {project.image ? (
                  <Image className={style.tileImage}
                         src={this.props.context.getImageThumbnailURL(project.image, 250)} rounded/>
                ) : (
                  <Image className={style.tileImage} centered>
                    <Icon size={"massive"} style={{color: project.color || 'grey'}} fitted name={"archive"}/>
                  </Image>
                )}
                <Card.Content>
                  <Card.Header>
                    {project.title}
                  </Card.Header>

                  <Card.Description>
                    {project.description}
                  </Card.Description>

                  <Card.Meta>

                  </Card.Meta>
                </Card.Content>
                <Card.Content extra>
                  {moment(project.lastUpdate).format("LL")}
                </Card.Content>
                <Card.Content extra>
                  <ActorAvatar variables={{id: _.get(project, 'creator.id')}}/>
                </Card.Content>
              </Card>
            )
          })}

          {this.state.loadingMore ? Array(8).fill().map((_, index) => (
            <Card key={index}>
              <Image className={style.tileImage}>
                <Placeholder>
                  <Placeholder.Image square/>
                </Placeholder>
              </Image>
              <Card.Content>
                <Placeholder>
                  <Placeholder.Line/>
                  <Placeholder.Line/>
                </Placeholder>
              </Card.Content>
              <Card.Content extra>
                <Placeholder>
                  <Placeholder.Line/>
                </Placeholder>
              </Card.Content>
              <Card.Content extra>
                <Placeholder>
                  <Placeholder.Line/>
                </Placeholder>
              </Card.Content>
            </Card>
          )) : null}

          <Visibility once onUpdate={(e, {calculations: {topVisible}}) => {
            if (topVisible) {
              this.showMore();
            }
          }}/>
        </Card.Group>

        {portfolio.projects.edges.length === 0 ? (
          <Segment textAlign='center' disabled padded='very'>
            {t("There is no result for this query...")}
          </Segment>
        ) : null}
      </>
    );
  }

  search(qs) {
    this.setState({
      qs,
      loading: true
    });

    if (this.searchTimer) {
      clearTimeout(this.searchTimer);
    }

    this.searchTimer = setTimeout(() => {
      this.props.relay.refetch(() => ({
        qs,
        first: 30
      }), null, () => {
        this.setState({
          loading: false
        });
      });
    }, 250)
  }

  showMore(howMany = 10) {
    const {portfolio} = this.props;

    if (_.get(portfolio, 'projects.pageInfo.hasNextPage') && !this.state.loadingMore) {
      const endCursor = _.get(portfolio, 'projects.pageInfo.endCursor');
      const total = _.get(portfolio, 'projects.edges.length') + howMany;

      this.setState({
        loadingMore: true
      }, () => {

        this.props.relay.refetch((fragmentVariables) => ({
          ...fragmentVariables,
          qs: this.state.qs,
          first: howMany,
          after: endCursor,
        }), ({
          first: total
        }), () => {
          this.setState({
            loadingMore: false
          });
        });
      });
    }
  }

  _handleSort = (sortColumn) => {
    this.setState({
      sortColumn,
      sortDirection: this.state.sortDirection === 'ascending' ? 'descending' : 'ascending'
    });

    this.props.relay.refetch(() => ({
      qs: this.state.qs,
      first: 30,
      sortings: [
        {
          sortBy: sortColumn,
          sortDirection: this.state.sortDirection === 'ascending' ? 'desc' : 'asc'
        }
      ]
    }), null, () => {
      this.setState({
        loading: false
      });
    });
  };
}

let query = graphql`
  query ProjectsList_Query($first: Int $after: String $qs: String $sortings: [SortingInput]) {
    person(me: true){
      ...ProjectsList_person
    }

    portfolio{
      ...ProjectsList_portfolio
    }
  }
`;

export default withRelay(
  query,
  () => {
    return {
      first: 30,
      sortings: [
        {
          sortBy: "lastUpdate",
          sortDirection: "desc"
        }
      ]
    }
  },
  createRefetchContainer(ProjectsList, graphql`
    fragment ProjectsList_person on Person {
      id
    }

    fragment ProjectsList_portfolio on Portfolio {
      id
      projects(qs: $qs first: $first after: $after sortings: $sortings) @connection(key: "ProjectsList_projects", filters: []) {
        edges{
          node{
            ...ProjectBasicFragment @relay(mask: false)
            subProjectsCount
            subProjects(first: 20){
              edges{
                node{
                  title
                  color
                  image
                }
              }
            }
          }
        }
        pageInfo{
          hasNextPage
          endCursor
        }
      }
    }
  `, query)
);