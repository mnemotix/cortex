/**
 * @flow
 * @relayHash 7e6657c9cf1563a6654ff653a639a358
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
type ProjectsList_person$ref = any;
type ProjectsList_portfolio$ref = any;
export type SortingInput = {
  sortBy?: ?string,
  sortDirection?: ?string,
  sortFilters?: ?$ReadOnlyArray<?string>,
};
export type ProjectsList_QueryVariables = {|
  first?: ?number,
  after?: ?string,
  qs?: ?string,
  sortings?: ?$ReadOnlyArray<?SortingInput>,
|};
export type ProjectsList_QueryResponse = {|
  +person: ?{|
    +$fragmentRefs: ProjectsList_person$ref
  |},
  +portfolio: ?{|
    +$fragmentRefs: ProjectsList_portfolio$ref
  |},
|};
export type ProjectsList_Query = {|
  variables: ProjectsList_QueryVariables,
  response: ProjectsList_QueryResponse,
|};
*/


/*
query ProjectsList_Query(
  $first: Int
  $after: String
  $qs: String
  $sortings: [SortingInput]
) {
  person(me: true) {
    ...ProjectsList_person
    id
  }
  portfolio {
    ...ProjectsList_portfolio
    id
  }
}

fragment ProjectsList_person on Person {
  id
}

fragment ProjectsList_portfolio on Portfolio {
  id
  projects(qs: $qs, first: $first, after: $after, sortings: $sortings) {
    edges {
      node {
        id
        uri
        color
        image
        title
        description
        shortDescription
        creationDate
        lastUpdate
        creator {
          ... on ActorInterface {
            id
            uri
            displayName
            avatar
          }
          id
        }
        subProjectsCount
        subProjects(first: 20) {
          edges {
            node {
              title
              color
              image
              id
            }
          }
        }
        __typename
      }
      cursor
    }
    pageInfo {
      hasNextPage
      endCursor
    }
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "first",
    "type": "Int",
    "defaultValue": null
  },
  {
    "kind": "LocalArgument",
    "name": "after",
    "type": "String",
    "defaultValue": null
  },
  {
    "kind": "LocalArgument",
    "name": "qs",
    "type": "String",
    "defaultValue": null
  },
  {
    "kind": "LocalArgument",
    "name": "sortings",
    "type": "[SortingInput]",
    "defaultValue": null
  }
],
v1 = [
  {
    "kind": "Literal",
    "name": "me",
    "value": true,
    "type": "Boolean"
  }
],
v2 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "id",
  "args": null,
  "storageKey": null
},
v3 = [
  {
    "kind": "Variable",
    "name": "after",
    "variableName": "after",
    "type": "String"
  },
  {
    "kind": "Variable",
    "name": "first",
    "variableName": "first",
    "type": "Int"
  },
  {
    "kind": "Variable",
    "name": "qs",
    "variableName": "qs",
    "type": "String"
  },
  {
    "kind": "Variable",
    "name": "sortings",
    "variableName": "sortings",
    "type": "[SortingInput]"
  }
],
v4 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "color",
  "args": null,
  "storageKey": null
},
v5 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "image",
  "args": null,
  "storageKey": null
},
v6 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "title",
  "args": null,
  "storageKey": null
},
v7 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "uri",
  "args": null,
  "storageKey": null
};
return {
  "kind": "Request",
  "operationKind": "query",
  "name": "ProjectsList_Query",
  "id": null,
  "text": "query ProjectsList_Query(\n  $first: Int\n  $after: String\n  $qs: String\n  $sortings: [SortingInput]\n) {\n  person(me: true) {\n    ...ProjectsList_person\n    id\n  }\n  portfolio {\n    ...ProjectsList_portfolio\n    id\n  }\n}\n\nfragment ProjectsList_person on Person {\n  id\n}\n\nfragment ProjectsList_portfolio on Portfolio {\n  id\n  projects(qs: $qs, first: $first, after: $after, sortings: $sortings) {\n    edges {\n      node {\n        id\n        uri\n        color\n        image\n        title\n        description\n        shortDescription\n        creationDate\n        lastUpdate\n        creator {\n          ... on ActorInterface {\n            id\n            uri\n            displayName\n            avatar\n          }\n          id\n        }\n        subProjectsCount\n        subProjects(first: 20) {\n          edges {\n            node {\n              title\n              color\n              image\n              id\n            }\n          }\n        }\n        __typename\n      }\n      cursor\n    }\n    pageInfo {\n      hasNextPage\n      endCursor\n    }\n  }\n}\n",
  "metadata": {},
  "fragment": {
    "kind": "Fragment",
    "name": "ProjectsList_Query",
    "type": "Query",
    "metadata": null,
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "person",
        "storageKey": "person(me:true)",
        "args": v1,
        "concreteType": "Person",
        "plural": false,
        "selections": [
          {
            "kind": "FragmentSpread",
            "name": "ProjectsList_person",
            "args": null
          }
        ]
      },
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "portfolio",
        "storageKey": null,
        "args": null,
        "concreteType": "Portfolio",
        "plural": false,
        "selections": [
          {
            "kind": "FragmentSpread",
            "name": "ProjectsList_portfolio",
            "args": null
          }
        ]
      }
    ]
  },
  "operation": {
    "kind": "Operation",
    "name": "ProjectsList_Query",
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "person",
        "storageKey": "person(me:true)",
        "args": v1,
        "concreteType": "Person",
        "plural": false,
        "selections": [
          v2
        ]
      },
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "portfolio",
        "storageKey": null,
        "args": null,
        "concreteType": "Portfolio",
        "plural": false,
        "selections": [
          v2,
          {
            "kind": "LinkedField",
            "alias": null,
            "name": "projects",
            "storageKey": null,
            "args": v3,
            "concreteType": "ProjectConnection",
            "plural": false,
            "selections": [
              {
                "kind": "LinkedField",
                "alias": null,
                "name": "edges",
                "storageKey": null,
                "args": null,
                "concreteType": "ProjectEdge",
                "plural": true,
                "selections": [
                  {
                    "kind": "LinkedField",
                    "alias": null,
                    "name": "node",
                    "storageKey": null,
                    "args": null,
                    "concreteType": "Project",
                    "plural": false,
                    "selections": [
                      {
                        "kind": "ScalarField",
                        "alias": null,
                        "name": "shortDescription",
                        "args": null,
                        "storageKey": null
                      },
                      v2,
                      v4,
                      v5,
                      v6,
                      {
                        "kind": "ScalarField",
                        "alias": null,
                        "name": "description",
                        "args": null,
                        "storageKey": null
                      },
                      v7,
                      {
                        "kind": "ScalarField",
                        "alias": null,
                        "name": "creationDate",
                        "args": null,
                        "storageKey": null
                      },
                      {
                        "kind": "ScalarField",
                        "alias": null,
                        "name": "lastUpdate",
                        "args": null,
                        "storageKey": null
                      },
                      {
                        "kind": "LinkedField",
                        "alias": null,
                        "name": "creator",
                        "storageKey": null,
                        "args": null,
                        "concreteType": "Person",
                        "plural": false,
                        "selections": [
                          v2,
                          v7,
                          {
                            "kind": "ScalarField",
                            "alias": null,
                            "name": "displayName",
                            "args": null,
                            "storageKey": null
                          },
                          {
                            "kind": "ScalarField",
                            "alias": null,
                            "name": "avatar",
                            "args": null,
                            "storageKey": null
                          }
                        ]
                      },
                      {
                        "kind": "ScalarField",
                        "alias": null,
                        "name": "subProjectsCount",
                        "args": null,
                        "storageKey": null
                      },
                      {
                        "kind": "LinkedField",
                        "alias": null,
                        "name": "subProjects",
                        "storageKey": "subProjects(first:20)",
                        "args": [
                          {
                            "kind": "Literal",
                            "name": "first",
                            "value": 20,
                            "type": "Int"
                          }
                        ],
                        "concreteType": "ProjectConnection",
                        "plural": false,
                        "selections": [
                          {
                            "kind": "LinkedField",
                            "alias": null,
                            "name": "edges",
                            "storageKey": null,
                            "args": null,
                            "concreteType": "ProjectEdge",
                            "plural": true,
                            "selections": [
                              {
                                "kind": "LinkedField",
                                "alias": null,
                                "name": "node",
                                "storageKey": null,
                                "args": null,
                                "concreteType": "Project",
                                "plural": false,
                                "selections": [
                                  v6,
                                  v4,
                                  v5,
                                  v2
                                ]
                              }
                            ]
                          }
                        ]
                      },
                      {
                        "kind": "ScalarField",
                        "alias": null,
                        "name": "__typename",
                        "args": null,
                        "storageKey": null
                      }
                    ]
                  },
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "cursor",
                    "args": null,
                    "storageKey": null
                  }
                ]
              },
              {
                "kind": "LinkedField",
                "alias": null,
                "name": "pageInfo",
                "storageKey": null,
                "args": null,
                "concreteType": "PageInfo",
                "plural": false,
                "selections": [
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "hasNextPage",
                    "args": null,
                    "storageKey": null
                  },
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "endCursor",
                    "args": null,
                    "storageKey": null
                  }
                ]
              }
            ]
          },
          {
            "kind": "LinkedHandle",
            "alias": null,
            "name": "projects",
            "args": v3,
            "handle": "connection",
            "key": "ProjectsList_projects",
            "filters": []
          }
        ]
      }
    ]
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = '17ef5587ef166f274a3510e48a3b374b';
module.exports = node;
