/**
 * @flow
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteFragment } from 'relay-runtime';
import type { FragmentReference } from "relay-runtime";
declare export opaque type ProjectsList_portfolio$ref: FragmentReference;
export type ProjectsList_portfolio = {|
  +id: string,
  +projects: ?{|
    +edges: ?$ReadOnlyArray<?{|
      +node: ?{|
        +id: string,
        +uri: ?string,
        +color: ?string,
        +image: ?string,
        +title: ?string,
        +description: ?string,
        +shortDescription: ?string,
        +creationDate: ?number,
        +lastUpdate: ?number,
        +creator: ?{|
          +id?: string,
          +uri?: ?string,
          +displayName?: ?string,
          +avatar?: ?string,
        |},
        +subProjectsCount: ?number,
        +subProjects: ?{|
          +edges: ?$ReadOnlyArray<?{|
            +node: ?{|
              +title: ?string,
              +color: ?string,
              +image: ?string,
            |}
          |}>
        |},
      |}
    |}>,
    +pageInfo: {|
      +hasNextPage: boolean,
      +endCursor: ?string,
    |},
  |},
  +$refType: ProjectsList_portfolio$ref,
|};
*/


const node/*: ConcreteFragment*/ = (function(){
var v0 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "id",
  "args": null,
  "storageKey": null
},
v1 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "color",
  "args": null,
  "storageKey": null
},
v2 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "image",
  "args": null,
  "storageKey": null
},
v3 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "title",
  "args": null,
  "storageKey": null
},
v4 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "uri",
  "args": null,
  "storageKey": null
};
return {
  "kind": "Fragment",
  "name": "ProjectsList_portfolio",
  "type": "Portfolio",
  "metadata": {
    "connection": [
      {
        "count": "first",
        "cursor": "after",
        "direction": "forward",
        "path": [
          "projects"
        ]
      }
    ]
  },
  "argumentDefinitions": [
    {
      "kind": "RootArgument",
      "name": "qs",
      "type": "String"
    },
    {
      "kind": "RootArgument",
      "name": "first",
      "type": "Int"
    },
    {
      "kind": "RootArgument",
      "name": "after",
      "type": "String"
    },
    {
      "kind": "RootArgument",
      "name": "sortings",
      "type": "[SortingInput]"
    }
  ],
  "selections": [
    v0,
    {
      "kind": "LinkedField",
      "alias": "projects",
      "name": "__ProjectsList_projects_connection",
      "storageKey": null,
      "args": null,
      "concreteType": "ProjectConnection",
      "plural": false,
      "selections": [
        {
          "kind": "LinkedField",
          "alias": null,
          "name": "edges",
          "storageKey": null,
          "args": null,
          "concreteType": "ProjectEdge",
          "plural": true,
          "selections": [
            {
              "kind": "LinkedField",
              "alias": null,
              "name": "node",
              "storageKey": null,
              "args": null,
              "concreteType": "Project",
              "plural": false,
              "selections": [
                {
                  "kind": "ScalarField",
                  "alias": null,
                  "name": "shortDescription",
                  "args": null,
                  "storageKey": null
                },
                v0,
                v1,
                v2,
                v3,
                {
                  "kind": "ScalarField",
                  "alias": null,
                  "name": "description",
                  "args": null,
                  "storageKey": null
                },
                v4,
                {
                  "kind": "ScalarField",
                  "alias": null,
                  "name": "creationDate",
                  "args": null,
                  "storageKey": null
                },
                {
                  "kind": "ScalarField",
                  "alias": null,
                  "name": "lastUpdate",
                  "args": null,
                  "storageKey": null
                },
                {
                  "kind": "LinkedField",
                  "alias": null,
                  "name": "creator",
                  "storageKey": null,
                  "args": null,
                  "concreteType": "Person",
                  "plural": false,
                  "selections": [
                    v0,
                    v4,
                    {
                      "kind": "ScalarField",
                      "alias": null,
                      "name": "displayName",
                      "args": null,
                      "storageKey": null
                    },
                    {
                      "kind": "ScalarField",
                      "alias": null,
                      "name": "avatar",
                      "args": null,
                      "storageKey": null
                    }
                  ]
                },
                {
                  "kind": "ScalarField",
                  "alias": null,
                  "name": "subProjectsCount",
                  "args": null,
                  "storageKey": null
                },
                {
                  "kind": "LinkedField",
                  "alias": null,
                  "name": "subProjects",
                  "storageKey": "subProjects(first:20)",
                  "args": [
                    {
                      "kind": "Literal",
                      "name": "first",
                      "value": 20,
                      "type": "Int"
                    }
                  ],
                  "concreteType": "ProjectConnection",
                  "plural": false,
                  "selections": [
                    {
                      "kind": "LinkedField",
                      "alias": null,
                      "name": "edges",
                      "storageKey": null,
                      "args": null,
                      "concreteType": "ProjectEdge",
                      "plural": true,
                      "selections": [
                        {
                          "kind": "LinkedField",
                          "alias": null,
                          "name": "node",
                          "storageKey": null,
                          "args": null,
                          "concreteType": "Project",
                          "plural": false,
                          "selections": [
                            v3,
                            v1,
                            v2
                          ]
                        }
                      ]
                    }
                  ]
                },
                {
                  "kind": "ScalarField",
                  "alias": null,
                  "name": "__typename",
                  "args": null,
                  "storageKey": null
                }
              ]
            },
            {
              "kind": "ScalarField",
              "alias": null,
              "name": "cursor",
              "args": null,
              "storageKey": null
            }
          ]
        },
        {
          "kind": "LinkedField",
          "alias": null,
          "name": "pageInfo",
          "storageKey": null,
          "args": null,
          "concreteType": "PageInfo",
          "plural": false,
          "selections": [
            {
              "kind": "ScalarField",
              "alias": null,
              "name": "hasNextPage",
              "args": null,
              "storageKey": null
            },
            {
              "kind": "ScalarField",
              "alias": null,
              "name": "endCursor",
              "args": null,
              "storageKey": null
            }
          ]
        }
      ]
    }
  ]
};
})();
// prettier-ignore
(node/*: any*/).hash = 'e9257aa577afba270944452573f82a86';
module.exports = node;
