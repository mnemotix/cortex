/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *  
 */

import {Route} from "@mnemotix/cortex-core/client";

export default [
  new Route('Portfolio_projects', "/projects"),
  new Route('Portfolio_project_create', '/projects/create/new'),
  new Route('Portfolio_project', '/projects/:id'),
  new Route('Portfolio_project_edit', '/projects/:id/edit'),
  new Route('Portfolio_project_event_create', '/projects/:id/events/create/new'),
  new Route('Portfolio_project_event_edit', '/projects/:id/events/:eventId/edit'),
  new Route('Portfolio_project_event_involvements', '/projects/:id/events/:eventId/involvements'),
  new Route('Portfolio_project_event_attachments', '/projects/:id/events/:eventId/attachments'),
  new Route('Portfolio_project_event_attachments_add', '/projects/:id/events/:eventId/attachments/create'),
  new Route('Portfolio_project_event_attachment', '/projects/:id/events/:eventId/attachments/:attachmentId'),
  new Route('Portfolio_project_events', '/projects/:id/events'),
  new Route('Portfolio_project_event', '/projects/:id/events/:eventId'),
];