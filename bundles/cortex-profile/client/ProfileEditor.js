/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
import React from 'react';
import PropTypes from 'prop-types';
import {Container} from 'semantic-ui-react'
import classnames from 'classnames';

import style from './style.less';

import {withContext, withRelay} from "@mnemotix/cortex-core/client";
import {createFragmentContainer, graphql} from "react-relay";
import {Form, generateUIFormPropsForJSONSchemaMutation, FormMutationDefinition} from "@mnemotix/cortex-form/client";
import {DefaultMutations as Mutations} from "@mnemotix/synaptix.js/lib/client";

@withContext
class ProfileEditor extends React.Component {
  static propTypes = {
  };

  state = {
  };

  render() {
    const {t, person} = this.props;

    const {schema, uiSchema} = generateUIFormPropsForJSONSchemaMutation({
      JSONSchema: this.props.context.getJSONSchema(),
      mutationName: 'updatePerson'
    });


    return (
      <Container>
        <Form schema={schema}
              uiSchema={uiSchema}
              formData={person}
              relayMutation={new FormMutationDefinition({
                mutation: new Mutations.foaf.UpdatePersonMutation({
                  environment: this.props.relay.environment
                }),
                successMessage: t("Profile has been saved.")
              })}
        />
      </Container>
    )
  }
}

export {
  ProfileEditor
};

export default withRelay(
  graphql`
    query ProfileEditor_Query{
      person(me: true){
        ...ProfileEditor_person
      }
    }
  `,
  {
  },
  createFragmentContainer(
    ProfileEditor,
    graphql`
      fragment ProfileEditor_person on Person {
        ...PersonBasicFragment @relay(mask: false)
      }
    `
  )
);