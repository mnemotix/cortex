/**
 * @flow
 * @relayHash 65e28129c10db254b1d96e60e2352e29
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
type ProfileEditor_person$ref = any;
export type ProfileEditor_QueryVariables = {||};
export type ProfileEditor_QueryResponse = {|
  +person: ?{|
    +$fragmentRefs: ProfileEditor_person$ref
  |}
|};
export type ProfileEditor_Query = {|
  variables: ProfileEditor_QueryVariables,
  response: ProfileEditor_QueryResponse,
|};
*/


/*
query ProfileEditor_Query {
  person(me: true) {
    ...ProfileEditor_person
    id
  }
}

fragment ProfileEditor_person on Person {
  id
  uri
  displayName
  avatar
  firstName
  lastName
  maidenName
  bio
  shortBio
  bday
  gender
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "Literal",
    "name": "me",
    "value": true,
    "type": "Boolean"
  }
];
return {
  "kind": "Request",
  "operationKind": "query",
  "name": "ProfileEditor_Query",
  "id": null,
  "text": "query ProfileEditor_Query {\n  person(me: true) {\n    ...ProfileEditor_person\n    id\n  }\n}\n\nfragment ProfileEditor_person on Person {\n  id\n  uri\n  displayName\n  avatar\n  firstName\n  lastName\n  maidenName\n  bio\n  shortBio\n  bday\n  gender\n}\n",
  "metadata": {},
  "fragment": {
    "kind": "Fragment",
    "name": "ProfileEditor_Query",
    "type": "Query",
    "metadata": null,
    "argumentDefinitions": [],
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "person",
        "storageKey": "person(me:true)",
        "args": v0,
        "concreteType": "Person",
        "plural": false,
        "selections": [
          {
            "kind": "FragmentSpread",
            "name": "ProfileEditor_person",
            "args": null
          }
        ]
      }
    ]
  },
  "operation": {
    "kind": "Operation",
    "name": "ProfileEditor_Query",
    "argumentDefinitions": [],
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "person",
        "storageKey": "person(me:true)",
        "args": v0,
        "concreteType": "Person",
        "plural": false,
        "selections": [
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "lastName",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "id",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "displayName",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "avatar",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "firstName",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "uri",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "maidenName",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "bio",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "shortBio",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "bday",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "gender",
            "args": null,
            "storageKey": null
          }
        ]
      }
    ]
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = '36cbb71309dd7f14c7ab01b27588356d';
module.exports = node;
