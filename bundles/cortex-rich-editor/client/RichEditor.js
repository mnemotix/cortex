/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import {Editor, getEventRange} from 'slate-react';
import {Data} from 'slate';
import isHotkey from 'is-hotkey';
import {Button, Segment, Dropdown} from 'semantic-ui-react';
import style from './RichEditor.less';
import {withContext} from "@mnemotix/cortex-core/client";
import htmlSerializer from './helpers/htmlSerializer';
import {hasValidAncestors} from "./helpers/hasValidAncestors";
import Suggestions from "./Suggestions";

const CONTEXT_MARK_TYPE = 'mention';
const DEFAULT_NODE = 'paragraph';

const isBoldHotkey = isHotkey('mod+b');
const isItalicHotkey = isHotkey('mod+i');
const isUnderlinedHotkey = isHotkey('mod+u');
const isCodeHotkey = isHotkey('mod+`');
const isEscapeHotkey = isHotkey('esc');
const isEnterHotkey = isHotkey('enter');

const TYPED_TYPE = "typed";

const schema = {
  inlines: {
    [TYPED_TYPE]: {
      isVoid: true,
    },
  },
};

/**
 * @typedef {object} RichTextDropdownDefinition
 * @property {string} triggerChar - On which character the dropdown is triggered
 * @property {string} type        - Type of block
 * @property {function} renderDropdownOptions - Render options
 * @property {function} createMentionFromClickedOption - Process option clicked
 */


@withContext
export default class RichEditor extends React.Component {
  saveTimeout;
  editorRef = React.createRef();

  static propTypes = {
    editing: PropTypes.bool,
    content: PropTypes.string,
    renderTypedTag: PropTypes.func,
    onChange: PropTypes.func,
    className: PropTypes.string,
    placeholder: PropTypes.string,
    richTextDropdownDefinitions: PropTypes.array
  };

  static defaultProps = {
    content: '',
    renderTypedTag: (type, value, domNodes) => {
      return domNodes;
    },
    richTextDropdownDefinitions: []
  };

  state = {
    value: htmlSerializer.deserialize(this.props.content),
    richTextDropdownOptions: [],
    mentionLoading: false
  };

  componentWillReceiveProps(nextProps) {
    if (!this.props.editing && nextProps.editing) {
      // See https://github.com/ianstormtaylor/slate/issues/1776#issuecomment-386209349
      setTimeout(() => {
        this.editorRef.current.focus();
      }, 0);
    }
  }

  /**
   * Render.
   *
   * @return {Element}
   */

  render() {
    const {editing, t, className, placeholder} = this.props;

    return (
      <div className={style.editor}>
        {editing ? (
          <Button.Group basic size={"tiny"} attached='top'>
            {this.renderMarkButton('bold', 'bold')}
            {this.renderMarkButton('italic', 'italic')}
            {this.renderMarkButton('underlined', 'underline')}
            {this.renderMarkButton('code', 'code')}
            {this.renderBlockButton('heading-one', 'header')}
            {this.renderBlockButton('block-quote', 'quote left')}
            {this.renderBlockButton('numbered-list', 'list ol')}
            {this.renderBlockButton('bulleted-list', 'list ul')}
          </Button.Group>
        ) : null}

        <Segment {...(editing ? {attached: 'bottom'} : {basic: true})}>
          <Editor
            className={className}
            readOnly={!editing}
            spellCheck
            placeholder={placeholder}
            ref={this.editorRef}
            value={this.state.value}
            onChange={this._handleChange.bind(this)}
            onKeyDown={this._handleKeyDown}
            renderNode={this.renderNode}
            renderMark={this.renderMark}
            schema={schema}
          />
        </Segment>
      </div>
    )
  }

  renderBlockButton = (type, icon) => {
    let isActive = this.hasBlock(type);

    if (['numbered-list', 'bulleted-list'].includes(type)) {
      const {value: {document, blocks}} = this.state;

      if (blocks.size > 0) {
        const parent = document.getParent(blocks.first().key);
        isActive = this.hasBlock('list-item') && parent && parent.type === type;
      }
    }

    return (
      <Button icon={icon} active={isActive} onMouseDown={event => this._handleClickBlock(event, type)}/>
    );
  };

  hasBlock = type => {
    return this.state.value.blocks.some(node => node.type === type)
  };

  renderNode = (props, editor, next) => {
    const {attributes, children, node} = props;

    switch (node.type) {
      case 'block-quote':
        return <blockquote {...attributes}>{children}</blockquote>;
      case 'bulleted-list':
        return <ul {...attributes}>{children}</ul>;
      case 'heading-one':
        return <h1 {...attributes}>{children}</h1>;
      case 'heading-two':
        return <h2 {...attributes}>{children}</h2>;
      case 'list-item':
        return <li {...attributes}>{children}</li>;
      case 'numbered-list':
        return <ol {...attributes}>{children}</ol>;
      case TYPED_TYPE:
        let rendered = this.props.renderTypedTag(node.data.get('type'), node.data.get('id'), node.data.get('prefetchedData'), children);

        if(rendered){
          return <span {...attributes}>{rendered}</span>;
        }
      default:
        return next()
    }
  };

  renderMarkButton = (type, icon) => {
    const isActive = this.hasMark(type);

    return (
      <Button icon={icon} active={isActive} onClick={e => this._handleClickMark(e, type)}/>
    )
  };

  hasMark = (type) => {
    return this.state.value.activeMarks.some(mark => mark.type === type);
  };

  renderMark = (props, editor, next) => {
    const {children, mark, attributes, text} = props;

    switch (mark.type) {
      case 'bold':
        return <strong {...attributes}>{children}</strong>;
      case 'code':
        return <code {...attributes}>{children}</code>;
      case 'italic':
        return <em {...attributes}>{children}</em>;
      case 'underlined':
        return <u {...attributes}>{children}</u>;
      case CONTEXT_MARK_TYPE:
        return (
          <span {...props.attributes} className="mentionContext">
            {!this.state.mentionLoading ? (
              <Suggestions options={this.state.richTextDropdownOptions.map((option, index) => ({
                key: index,
                ...option
              }))}
                           anchor={".mentionContext"}
                           onSelectOption={this.insertMention.bind(this)}
              />
            ) : (
              <span>
                Loading...
              </span>
            )}

            {props.children}
          </span>
        );
      default:
        return next()
    }
  };

  /**
   * @typedef {Object} MentionDefinition
   * @property {RichTextDropdownDefinition} richTextDropdownDefinition
   * @property {string} mentionText
   */

  /**
   * Check if mention occured
   * @param value
   * @return {MentionDefinition}
   */
  checkMention(value) {
    // In some cases, like if the node that was selected gets deleted,
    // `startText` can be null.
    if (!value.startText) {
      return null
    }

    let startOffset = value.selection.start.offset;
    let textBefore = value.startText.text.slice(0, startOffset);
    let mentionText = null;

    let richTextDropdownDefinition = this.props.richTextDropdownDefinitions.find(({triggerChar}) => {
      let result = new RegExp(`${triggerChar}(\\S*)$`).exec(textBefore);

      if (result) {
        mentionText = result[1];
        return true;
      }

      return false;
    });

    if (richTextDropdownDefinition) {
      return {
        richTextDropdownDefinition,
        mentionText
      }
    }
  }

  cancelMention() {
    this.setState({
      richTextDropdownOptions: []
    });
  }

  async insertMention(option){
    const editor = this.editorRef.current;

    const {value} = editor;

    const {mentionText, richTextDropdownDefinition} = this.checkMention(value) || {};
    const optionText = option.text;

    editor.deleteBackward(mentionText.length + 1);

    let selection = editor.value.selection;

    editor.insertText(optionText);

    let mention = await richTextDropdownDefinition.createMentionFromClickedOption(richTextDropdownDefinition.type, option);

    editor
      .deleteForwardAtRange(selection, optionText.length)
      .insertInlineAtRange(selection, {
        data: {
          type: richTextDropdownDefinition.type,
          id: mention.id,
          prefetchedData: mention
        },
        type: TYPED_TYPE,
        nodes: [
          {
            object: 'text',
            leaves: [
              {
                text: optionText,
              },
            ],
          },
        ]
      })
      .insertText(' ')
      .focus()
  };

  _handleKeyDown = (event, editor, next) => {
    let mark;

    event.persist();

    if (isBoldHotkey(event)) {
      mark = 'bold';
    } else if (isItalicHotkey(event)) {
      mark = 'italic';
    } else if (isUnderlinedHotkey(event)) {
      mark = 'underlined';
    } else if (isCodeHotkey(event)) {
      mark = 'code';
    } else if (isEscapeHotkey(event)) {
      setTimeout(() => {
        this.cancelMention();
      }, 0);
      return next();
    } else if (isEnterHotkey(event)) {
      if (this.state.richTextDropdownOptions.length === 0) {
        return next();
      }
    } else {
      return next();
    }

    event.preventDefault();

    if(mark){
      editor.toggleMark(mark);
    }
  };

  _handleClickMark = (event, mark) => {
    event.preventDefault();
    this.editorRef.current.toggleMark(mark);
  };

  _handleClickBlock = (event, type) => {
    event.preventDefault();

    const {editor} = this;
    const {value} = editor;
    const {document} = value;

    // Handle everything but list buttons.
    if (type !== 'bulleted-list' && type !== 'numbered-list') {
      const isActive = this.hasBlock(type);
      const isList = this.hasBlock('list-item');

      if (isList) {
        editor
          .setBlocks(isActive ? DEFAULT_NODE : type)
          .unwrapBlock('bulleted-list')
          .unwrapBlock('numbered-list');
      } else {
        editor.setBlocks(isActive ? DEFAULT_NODE : type);
      }
    } else {
      // Handle the extra wrapping required for list buttons.
      const isList = this.hasBlock('list-item');
      const isType = value.blocks.some(block => {
        return !!document.getClosest(block.key, parent => parent.type === type);
      });

      if (isList && isType) {
        editor
          .setBlocks(DEFAULT_NODE)
          .unwrapBlock('bulleted-list')
          .unwrapBlock('numbered-list');
      } else if (isList) {
        editor
          .unwrapBlock(
            type === 'bulleted-list' ? 'numbered-list' : 'bulleted-list'
          )
          .wrapBlock(type)
      } else {
        editor.setBlocks('list-item').wrapBlock(type)
      }
    }
  };

  async _handleChange({value}) {
    const {
      richTextDropdownDefinition,
      mentionText
    } = this.checkMention(value) || {};

    if (richTextDropdownDefinition && mentionText !== this.lastMentionText) {
      this.lastMentionText = mentionText;

      const {selection} = value;

      let decorations = value.decorations.filter(
        value => value.mark.type !== CONTEXT_MARK_TYPE
      );

      if (mentionText && hasValidAncestors(value)) {
        decorations = decorations.push({
          anchor: {
            key: selection.start.key,
            offset: selection.start.offset - mentionText.length,
          },
          focus: {
            key: selection.start.key,
            offset: selection.start.offset,
          },
          mark: {
            type: CONTEXT_MARK_TYPE,
          },
        });
      }

      let richTextDropdownOptions = await richTextDropdownDefinition.renderDropdownOptions(mentionText);

      this.setState({
        value,
        richTextDropdownOptions
      }, () => {
        // We need to set decorations after the value flushes into the editor.
        this.editorRef.current.setDecorations(decorations);
        this._handleSave();
      });
    } else {
      this.setState({value}, this._handleSave);
    }
  }

  _handleSave = () => {
    const {value} = this.state;

    if (this.props.onChange && value && value.document) {
      if (this.saveTimeout) {
        clearTimeout(this.saveTimeout);
      }

      this.saveTimeout = setTimeout(() => {
        this.props.onChange(htmlSerializer.serialize(value));
      }, 1000);
    }
  };
}