/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import React from "react";
import PropTypes from "prop-types";
import ReactDOM from "react-dom";
import {Segment, List, Image} from "semantic-ui-react";
import style from "./Suggestions.less";
import isHotkey from 'is-hotkey';

const isUpHotkey = isHotkey('up');
const isDownHotkey = isHotkey('down');
const isEnterHotkey = isHotkey('return');


export default class Suggestions extends React.Component {
  static propTypes = {
    onSelectOption: PropTypes.func.isRequired
  };

  state = {
    top: 0,
    left: 0,
    selectedIndex: 0
  };

  componentWillMount(){
    window.addEventListener('keydown', this._handleKeyDown);
  }

  componentWillUnmount(){
    window.removeEventListener('keydown', this._handleKeyDown);
  }

  _handleKeyDown = (e) => {
    const {options, onSelectOption} = this.props;

    if(isUpHotkey(e) || isDownHotkey(e) || isEnterHotkey(e)){
      e.preventDefault();
      e.stopPropagation();

      if(isDownHotkey(e) && this.state.selectedIndex < options.length - 1){
        this.setState({
          selectedIndex: this.state.selectedIndex + 1
        });
      }

      if(isUpHotkey(e) && this.state.selectedIndex > 0){
        this.setState({
          selectedIndex: this.state.selectedIndex - 1
        });
      }

      if (isEnterHotkey(e)){
        onSelectOption(options[this.state.selectedIndex]);
      }
    }
  };

  componentDidMount() {
    const anchor = window.document.querySelector(this.props.anchor);

    if (!anchor) {
      return;
    }

    const anchorRect = anchor.getBoundingClientRect();

    this.setState({
      top: anchorRect.top + window.scrollY + 5,
      left: anchorRect.left + window.scrollX,
    });
  }

  render() {
    const {options, onSelectOption} = this.props;
    const body = window.document.body;

    return ReactDOM.createPortal(
      <Segment
        compact
        className={style.suggestionDropdown}
        style={{
          top: this.state.top,
          left: this.state.left,
        }}
        onChange={this.props.onSelectOption}
      >
        <List selection verticalAlign='middle'>
          {options.map((option, index) => (
            <List.Item key={option.id} active={this.state.selectedIndex === index} onClick={() => onSelectOption(option)}>
              <Image avatar src={option.image} />
              <List.Content>
                {option.text}
              </List.Content>
            </List.Item>
          ))}
        </List>
      </Segment>,
      body
    )
  }
}