/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import Html from 'slate-html-serializer';
import {Data} from "slate";
import React from "react";

export default new Html({
  rules: [
    {
      deserialize: (el, next) => {
        const BLOCK_TAGS = {
          blockquote: 'quote',
          p: 'paragraph',
          pre: 'code',
        };

        const type = BLOCK_TAGS[el.tagName.toLowerCase()];

        if (type) {
          return {
            object: 'block',
            type: type,
            data: {
              className: el.getAttribute('class'),
            },
            nodes: next(el.childNodes),
          }
        }
      },
      serialize: (obj, children) => {
        if (obj.object === 'block') {
          switch (obj.type) {
            case 'code':
              return (
                <pre>
                <code>{children}</code>
              </pre>
              );
            case 'paragraph':
              return <p className={obj.data.get('className')}>{children}</p>;
            case 'quote':
              return <blockquote>{children}</blockquote>
          }
        }
      },
    },
    {
      deserialize: (el, next) => {
        const MARK_TAGS = {
          em: 'italic',
          strong: 'bold',
          u: 'underline'
        };

        const type = MARK_TAGS[el.tagName.toLowerCase()];
        if (type) {
          return {
            object: 'mark',
            type: type,
            nodes: next(el.childNodes),
          }
        }
      },
      serialize: (obj, children) => {
        if (obj.object === 'mark') {
          switch (obj.type) {
            case 'bold':
              return <strong>{children}</strong>;
            case 'italic':
              return <em>{children}</em>;
            case 'underline':
              return <u>{children}</u>
          }
        }
      },
    },
    {
      deserialize: (el, next) => {
        if(el.attributes){
          let dataType = el.attributes.getNamedItem('data-type');
          let dataId = el.attributes.getNamedItem('data-id');
          if (dataType && dataId){
            return {
              object: 'inline',
              type: 'typed',
              data: Data.create({type: dataType.value, id: dataId.value}),
              nodes: next(el.childNodes),
            }
          }
        }
      },
      serialize: (obj, children) => {
        if (obj.type === 'typed'){
          return (
            <span data-type={obj.data.get('type')} data-id={obj.data.get('id')}>
              {children}
            </span>
          );
        }
      }
    }
  ]
})