/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
import React from 'react';
import PropTypes from 'prop-types';
import '@uppy/core/dist/style.min.css';
import '@uppy/dashboard/dist/style.min.css';
import '@uppy/webcam/dist/style.css';
import {Container} from 'semantic-ui-react';

import classnames from 'classnames';

import style from './style.less';

import withContext from "@mnemotix/cortex-core/client/utilities/withContext";
import Uppy from '@uppy/core';
import Tus from '@uppy/tus';
import Url from '@uppy/url';
import Webcam from '@uppy/webcam';
import {Dashboard} from '@uppy/react';
import {translate} from "react-i18next";

@withContext
@translate(['labels'])
export default class FileUploader extends React.Component {
  static propTypes = {
    onFilesUploaded: PropTypes.func.isRequired,
    maxNumberOfFiles: PropTypes.number,
    allowedFileTypes: PropTypes.array,
    autoProceed: PropTypes.bool,
    debug: PropTypes.bool,
    meta: PropTypes.object
  };

  static defaultProps = {
    autoProceed: true,
    debug: false
  };

  state = {
  };

  componentWillMount() {
    const {t, autoProceed, maxNumberOfFiles, allowedFileTypes, debug, meta} = this.props;

    this.uppy = new Uppy({
      id: 'uppy',
      autoProceed,
      debug,
      restrictions: {
        maxNumberOfFiles,
        allowedFileTypes
      },
      meta,
      locale: {
        strings: {
          selectToUpload: t('Select files to upload'),
          closeModal: t('Close Modal'),
          upload: t('Upload'),
          importFrom: t('Import from %{name}'),
          addingMoreFiles: t('Adding more files'),
          addMoreFiles: t('Add more files'),
          dashboardWindowTitle: t('Uppy Dashboard Window (Press escape to close)'),
          dashboardTitle: t('Uppy Dashboard'),
          copyLinkToClipboardSuccess: t('Link copied to clipboard'),
          copyLinkToClipboardFallback: t('Copy the URL below'),
          copyLink: t('Copy link'),
          fileSource: t('File source: %{name}'),
          done: t('Done'),
          back: t('Back'),
          name: t('Name'),
          removeFile: t('Remove file'),
          editFile: t('Edit file'),
          editing: t('Editing %{file}'),
          edit: t('Edit'),
          finishEditingFile: t('Finish editing file'),
          saveChanges: t('Save changes'),
          cancel: t('Cancel'),
          localDisk: t('Local Disk'),
          myDevice: t('My Device'),
          dropPasteImport: t('Drop files here, paste, %{browse} or import from'),
          dropPaste: t('Dropppppppy files here, paste or %{browse}'),
          browse: t('browse'),
          fileProgress: t('File progress: upload speed and ETA'),
          numberOfSelectedFiles: t('Number of selected files'),
          uploadAllNewFiles: t('Upload all new files'),
          emptyFolderAdded: t('No files were added from empty folder'),
          uploadComplete: t('Upload complete'),
          resumeUpload: t('Resume upload'),
          pauseUpload: t('Pause upload'),
          retryUpload: t('Retry upload'),
          cancelUpload: t('Cancel upload'),
          xFilesSelected: {
            0: t('%{smart_count} file selected'),
            1: t('%{smart_count} files selected')
          },
          uploadXFiles: {
            0: t('Upload %{smart_count} file'),
            1: t('Upload %{smart_count} files')
          },
          uploadXNewFiles: {
            0: t('Upload +%{smart_count} file'),
            1: t('Upload +%{smart_count} files')
          },
          folderAdded: {
            0: t('Added %{smart_count} file from %{folder}'),
            1: t('Added %{smart_count} files from %{folder}')
          }
        }
      }
    });

    console.log(window.location);

    this.uppy
      .use(Tus, {
        endpoint: `${window.location.origin}/uploads`,
        autoRetry: false,
        chunkSize: 5 * 1024 * 1024,
        resume: true
      })
      .use(Webcam, {
        locale: {
          strings: {
            smile: t('Smile!'),
            takePicture: t('Take a picture'),
            startRecording: t('Begin video recording'),
            stopRecording: t('Stop video recording'),
            allowAccessTitle: t('Please allow access to your camera'),
            allowAccessDescription: t('In order to take pictures or record video with your camera, please allow camera access for this site.')
          }
        }
      })
    /*.use(Url, {
      serverUrl: `${window.location.origin}/remote-uploads`,
      locale: {
        import: t('Import'),
        enterUrlToImport: t('Enter URL to import a file'),
        failedToFetch: t('Companion failed to fetch this URL, please make sure it’s correct'),
        enterCorrectUrl: t('Incorrect URL: Please make sure you are entering a direct link to a file')
      }
    })*/;

    this.uppy.on('complete', (result) => {
      if (this.props.maxNumberOfFiles === 1) {
        this.props.onFilesUploaded(result.successful[0].uploadURL);
      } else {
        this.props.onFilesUploaded(result.successful.map(({uploadURL}) => uploadURL));
      }
    })
  }

  componentWillUnmount() {
    this.uppy.close();
  }

  render() {
    const {t} = this.props;

    return (
      <Container fluid className={style.uploader}>
        <Dashboard uppy={this.uppy} plugins={['Webcam'/*, 'Url'*/]}/>
      </Container>
    )
  }
}