/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

export default {
  MINIO_HOST : {
    description: 'This is Minio server host',
    defaultValue: '127.0.0.1'
  },
  MINIO_PORT : {
    description: 'This is Minio server port',
    defaultValue: 9099,
    defaultValueInProduction: true
  },
  MINIO_BUCKET : {
    description: 'This is Minio server bucket',
    defaultValue: 'local-cortex'
  },
  MINIO_ACCESS_KEY : {
    description: 'This is Minio access key',
    defaultValue: 'DXR57286P0K4KFR4HANE'
  },
  MINIO_SECRET_KEY: {
    description: 'This is Minio secret key',
    defaultValue: '3dpnVgIsHsWNe8izhNb0DwFtF7VoNI7lmcDpWZhf'
  },
  MINIO_REGION: {
    description: 'This is Minio region',
    defaultValue: 'eu-west-1',
    defaultValueInProduction: true
  },
};
