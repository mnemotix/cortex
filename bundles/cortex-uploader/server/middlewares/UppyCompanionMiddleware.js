/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import os from 'os';
import companion from '@uppy/companion';

/**
 * This a Uppy companion middleware to import files available on the web.
 *
 * @param {ExpressApp} app
 * @param {object} args
 * @constructor
 */
export default function UppyCompanionMiddleware(app, args = {}){
  const basePath = args.basePath || `/remote-uploads`;
  const filesPath = `${basePath}/files`;

  const options = {
    filePath: `${os.tmpdir()}`,
    secret: "ImAS3Cr4t",
    providerOptions: {
    },
    debug: true
  };

  app.use(basePath, companion.app(options));
  companion.socket(app.server, options)
}