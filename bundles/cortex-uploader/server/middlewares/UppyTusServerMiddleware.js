/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import path from 'path';
import mime from 'mime-types';
import {FileStore, Server} from 'tus-node-server';
import uniqid from 'uniqid';
import bodyParser from 'body-parser';
import MinioStore from "./store/MinioStore";
import {Client as MinioClient} from 'minio';
import {logError, logInfo} from "@mnemotix/synaptix.js";
import _ from 'lodash';

let parseMetadata = (metadata) => {
  const keyValuePairList = metadata.split(',');

  return keyValuePairList.reduce((metadata, keyValuePair) => {
    let [key, base64Value] = keyValuePair.split(' ');
    metadata[key] = new Buffer(base64Value, "base64").toString("ascii");

    return metadata
  }, {});
};


let namingFunction = (req) => {
  const metadata = parseMetadata(req.get('Upload-Metadata'));
  let extension = mime.extension(mime.lookup(metadata.name));
  return `${uniqid()}.${extension}`;
};

let initMinioStore = ({filesPath}) => {
  try{
    let minioStore = new MinioStore({
      path: filesPath,
      namingFunction,
      bucket: process.env.MINIO_BUCKET,
      accessKeyId: process.env.MINIO_ACCESS_KEY,
      secretAccessKey: process.env.MINIO_SECRET_KEY,
      endpoint: `http://${process.env.MINIO_HOST}:${process.env.MINIO_PORT}`,
      s3ForcePathStyle: true,
      signatureVersion: 'v4',
      partSize: 5 * 1024 * 1024, // each uploaded part will have ~5MB,
      region: process.env.MINIO_REGION
    });

    return minioStore;
  }catch (e) {
    logError("Something gone wrong with Minio Store", e);
  }
};

/**
 * Setup a Tus middleware to handle resumable file uploads using tus.io protocol
 *
 * @param {ExpressApp} app
 * @param args
 * @param {string} args.basePath base path used for uploading/getting files (default: /uploads)
 * @constructor
 */
export default function UppyTusServerMiddleware(app, args = {}){
  try {
    const server = new Server();

    const basePath = args.basePath || `/uploads`;
    const filesPath = `${basePath}/files`;
    const filePath = `${filesPath}/:filename`;
    const fileInfosPath = `${filesPath}/infos/:filename`;

    server.datastore = initMinioStore({filesPath});

    let minioClient;

    try {
      minioClient = new MinioClient({
        endPoint: process.env.MINIO_HOST,
        accessKey: process.env.MINIO_ACCESS_KEY,
        secretKey: process.env.MINIO_SECRET_KEY,
        port: parseInt(process.env.MINIO_PORT),
        useSSL: false,
        region: process.env.MINIO_REGION
      });
    } catch (e) {
      logError(e);
    }

    minioClient.bucketExists(process.env.MINIO_BUCKET, (err, exists) => {
      if (err){
        return logError(err);
      }

      if(!exists){
        logInfo(`Minio bucket "${process.env.MINIO_BUCKET}" doesn't exist, creating it...`);

        minioClient.makeBucket(process.env.MINIO_BUCKET, process.env.MINIO_REGION, (err) => {
          if (err){
            return logError(err);
          }
        })
      }
    });

    /** POST */
    app.post(basePath, server.handle.bind(server));
    /** HEAD */
    app.head(filePath, server.handle.bind(server));
    /** PATCH */
    app.patch(filePath, bodyParser.raw({
      type: "application/offset+octet-stream",
      limit: "10mb"
    }), server.handle.bind(server));
    /** GET */
    app.get(filePath, async (req, res) => {
      try {
        let stream = await minioClient.getObject(process.env.MINIO_BUCKET, req.params.filename);
        res.setHeader('Content-Type', mime.contentType(path.extname(req.params.filename)));
        stream.pipe(res);
      } catch (err) {
        return res.status(500).send(err);
      }
    });
    /** GET */
    app.get(fileInfosPath, async (req, res) => {
      try {
        let stats = await minioClient.statObject(process.env.MINIO_BUCKET, req.params.filename);
        let metaData = parseMetadata(_.get(stats, 'metaData.upload_metadata', ''));
        let infos = Object.assign({
            path: req.params.filename,
            mime: _.get(metaData, 'filetype'),
            name: _.get(metaData, 'filename')
          },
          _.pick(stats, ["size", "etag"]),
        );

        res.json(infos);
      } catch (err) {
        return res.status(500).send(err);
      }
    });
  } catch (e) {
    logError(e);
  }
}