/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import {S3Store, ERRORS} from 'tus-node-server';
import debug from 'debug';
const log = debug('tus-node-server:stores:s3store');

export default class MinioStore extends S3Store {
  /**
   * @inheritDoc
   */
  _uploadPart(metadata, read_stream, current_part_number) {
    return this.client
      .uploadPart({
        Bucket: this.bucket_name,
        Key: metadata.file.id,
        UploadId: metadata.upload_id,
        PartNumber: current_part_number,
        Body: read_stream.body,
      })
      .promise()
      .then((data) => {
        return data.ETag;
      })
      .catch((err) => {
        throw err;
      });
  }

  /**
   * @inheritDoc
   */
  async _finishMultipartUpload(metadata, parts) {
    const {Location} = await super._finishMultipartUpload(metadata, parts);

    await this.client.deleteObject({
      Bucket: this.bucket_name,
      Key: `${metadata.file.id}.info`
    }).promise();

    log(`[${metadata.file.id}] removed .info metadata on s3`);

    return Location;
  }

  /**
   * @inheritDoc
   */
  getOffset(file_id, with_parts = false) {
    return new Promise((resolve, reject) => {
      this._getMetadata(file_id)
        .then((metadata) => {
          return this._retrieveParts(file_id)
            .then((parts) => {
              return {
                parts,
                metadata,
              };
            })
            .catch((err) => {
              throw err;
            });
        })
        .then((data) => {
          // if no parts are found, offset is 0
          if (data.parts.length === 0) {
            return resolve({
              size: 0,
              upload_length: data.metadata.file.upload_length,
            });
          }

          const offset = data.parts.reduce((a, b) => {
            a += parseInt(b.Size, 10);

            return a;
          }, 0);

          const output = Object.assign({}, this.cache[file_id].file, {
            size: offset,
          });

          const result = !with_parts
            ? output
            : Object.assign({}, output, { parts: data.parts });

          return resolve(result);
        })
        .catch((err) => {
          if (['NotFound', 'NoSuchUpload'].includes(err.code)) {
            return reject(ERRORS.FILE_NOT_FOUND);
          }

          throw err;
        });
    });
  }
}